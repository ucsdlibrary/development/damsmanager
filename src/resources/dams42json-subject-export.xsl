<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
      xmlns:dams="http://library.ucsd.edu/ontology/dams#"
      xmlns:mads="http://www.loc.gov/mads/rdf/v1#"
      xmlns:owl="http://www.w3.org/2002/07/owl#"
      xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
      xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
      xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
      exclude-result-prefixes="dams mads owl rdf rdfs">
  <xsl:output method="text" encoding="utf-8" version="4.0"/>

    <xsl:variable name="lower">abcdefgijklmnopqrstuvwxyz</xsl:variable>
    <xsl:variable name="upper">ABCDEFGIJKLMNOPQRSTUVWXYZ</xsl:variable>

    <xsl:template match="rdf:RDF">
        <xsl:call-template name="startJsonObject"/>

        <xsl:for-each select="//*[@rdf:about]">
            <xsl:variable name="id"><xsl:value-of select="@rdf:about"/></xsl:variable>
            <xsl:variable name="count"><xsl:number level="any" count="@rdf:about"/></xsl:variable>
            <xsl:variable name="objectId">
                <xsl:choose>
                  <xsl:when test="@rdf:about = 'ARK'"><xsl:value-of select="concat($id, '#', $count)"/></xsl:when>
                  <xsl:otherwise><xsl:value-of select="substring-after(@rdf:about, '/20775/')" /></xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:call-template name="toQuotedValue">
                <xsl:with-param name="val"><xsl:value-of select="concat($id, '#', $count)"/></xsl:with-param>
            </xsl:call-template>
            <xsl:text>:</xsl:text>
            <xsl:call-template name="startJsonArray"/>
            <xsl:call-template name="appendJsonObject">
                <xsl:with-param name="key">ARK</xsl:with-param>
                <xsl:with-param name="val"><xsl:value-of select="$objectId"/></xsl:with-param>
            </xsl:call-template>

            <xsl:call-template name="appendJsonObject">
                <xsl:with-param name="key">subject type</xsl:with-param>
                <xsl:with-param name="val"><xsl:call-template name="subjectType"/></xsl:with-param>
            </xsl:call-template>

            <xsl:for-each select="mads:authoritativeLabel">
	            <xsl:call-template name="appendJsonObject">
	                <xsl:with-param name="key">subject term</xsl:with-param>
	                <xsl:with-param name="val"><xsl:call-template name="encodeXmlLang"/></xsl:with-param>
	            </xsl:call-template>
            </xsl:for-each>

            <xsl:for-each select="mads:hasExactExternalAuthority">
                <xsl:sort select="dams:order" data-type="number" order="ascending" />
                <xsl:call-template name="appendJsonObject">
                    <xsl:with-param name="key">exactMatch</xsl:with-param>
                    <xsl:with-param name="val"><xsl:call-template name="encodeXmlLang"/></xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>

            <xsl:for-each select="mads:hasCloseExternalAuthority">
                <xsl:sort select="dams:order" data-type="number" order="ascending" />
                <xsl:call-template name="appendJsonObject">
                    <xsl:with-param name="key">closeMatch</xsl:with-param>
                    <xsl:with-param name="val"><xsl:call-template name="encodeXmlLang"/></xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>

            <xsl:for-each select="mads:hasVariant/mads:Variant/mads:variantLabel">
                <xsl:sort select="dams:order" data-type="number" order="ascending" />
                <xsl:call-template name="appendJsonObject">
                    <xsl:with-param name="key">variant</xsl:with-param>
                    <xsl:with-param name="val"><xsl:call-template name="encodeXmlLang"/></xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>

            <xsl:for-each select="mads:hasHiddenVariant">
                <xsl:sort select="dams:order" data-type="number" order="ascending" />
                <xsl:call-template name="appendJsonObject">
                    <xsl:with-param name="key">hiddenVariant</xsl:with-param>
                    <xsl:with-param name="val"><xsl:call-template name="encodeXmlLang"/></xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>

            <xsl:call-template name="endJsonArray"/>
            <xsl:text>,</xsl:text>
        </xsl:for-each>

        <xsl:call-template name="endJsonObject"/>
    </xsl:template>

    <xsl:template name="subjectType">
        <xsl:variable name="name" select="local-name()"/>
        <xsl:choose>
            <xsl:when test="$name = 'CommonName'">Subject:common name</xsl:when>
            <xsl:when test="$name = 'ConferenceName'">Subject:conference name</xsl:when>
            <xsl:when test="$name = 'CorporateName'">Subject:corporate name</xsl:when>
            <xsl:when test="$name = 'CulturalContext'">Subject:culturalContext</xsl:when>
            <xsl:when test="$name = 'FamilyName'">Subject:family name</xsl:when>
            <xsl:when test="$name = 'PersonalName'">Subject:personal name</xsl:when>
            <xsl:when test="$name = 'ScientificName'">Subject:scientific name</xsl:when>
            <xsl:when test="$name = 'GenreForm'">Subject:genre</xsl:when>
            <xsl:when test="$name = 'ComplexSubject'">Subject:Complex</xsl:when>
            <xsl:when test="$name = 'Name'">Subject:Name</xsl:when>
            <xsl:when test="$name = 'Authority' or $name = 'Language'">mads:<xsl:value-of select="$name"/></xsl:when>
            <xsl:otherwise>Subject:<xsl:value-of select="translate($name,'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')"/></xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="encodeXmlLang">
        <xsl:choose>
            <xsl:when test="@xml:lang">"<xsl:value-of select="."/>"@<xsl:value-of select="@xml:lang"/></xsl:when>
            <xsl:when test="@rdf:resource"><xsl:value-of select="@rdf:resource"/></xsl:when>
            <xsl:otherwise><xsl:value-of select="."/></xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="capitalized">
        <xsl:param name="text"/>
        <xsl:value-of select="translate(substring($text,1,1), $lower, $upper)" /><xsl:value-of select="substring($text, 2, string-length($text)-1)" />
    </xsl:template>

    <xsl:template name="string-replace">
        <xsl:param name="text" />
        <xsl:param name="replace" />
        <xsl:param name="by" />
        <xsl:choose>
            <xsl:when test="$text = '' or $replace = '' or not($replace)" >
                <xsl:value-of select="$text" />
            </xsl:when>
            <xsl:when test="contains($text, $replace)">
                <xsl:value-of select="substring-before($text,$replace)" />
                <xsl:value-of select="$by" />
                <xsl:call-template name="string-replace">
                    <xsl:with-param name="text" select="substring-after($text,$replace)" />
                    <xsl:with-param name="replace" select="$replace" />
                    <xsl:with-param name="by" select="$by" />
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$text" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="appendJsonObject">
        <xsl:param name="key"/>
        <xsl:param name="val"/>

        <xsl:call-template name="startJsonObject"/>
            <xsl:call-template name="toQuotedValue">
                <xsl:with-param name="val"><xsl:value-of select="$key"/></xsl:with-param>
            </xsl:call-template>
            <xsl:text>:</xsl:text>
            <xsl:call-template name="toQuotedValue">
                 <xsl:with-param name="val"><xsl:value-of select="$val"/></xsl:with-param>
            </xsl:call-template>
        <xsl:call-template name="endJsonObject"/>
        <xsl:text>,</xsl:text>
    </xsl:template>

    <xsl:template name="toQuotedValue">
        <xsl:param name="val"/>
        <xsl:variable name="escapedVal">
            <xsl:call-template name="string-replace">
                <xsl:with-param name="text" select="$val" />
                <xsl:with-param name="replace" select="'&quot;'" />
                <xsl:with-param name="by" select="'\&quot;'" />
            </xsl:call-template>
        </xsl:variable>
        <xsl:text>"</xsl:text>
          <xsl:value-of select="$escapedVal" />
        <xsl:text>"</xsl:text>
    </xsl:template>

    <xsl:template name="startJsonObject">
        <xsl:text>{</xsl:text>
    </xsl:template>

    <xsl:template name="endJsonObject">
        <xsl:text>}</xsl:text>
    </xsl:template>

    <xsl:template name="startJsonArray">
        <xsl:text>[</xsl:text>
    </xsl:template>

    <xsl:template name="endJsonArray">
        <xsl:text>]</xsl:text>
    </xsl:template>

</xsl:stylesheet>
