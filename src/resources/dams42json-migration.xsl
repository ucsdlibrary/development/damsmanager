<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
      xmlns:dams="http://library.ucsd.edu/ontology/dams#"
      xmlns:mads="http://www.loc.gov/mads/rdf/v1#"
      xmlns:owl="http://www.w3.org/2002/07/owl#"
      xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
      xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
      xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
      exclude-result-prefixes="dams mads owl rdf rdfs">
    <xsl:include href="dams42json-migration-utils.xsl"/>
    <xsl:include href="dams42json-migration-metadata.xsl"/>

    <xsl:output method="text" encoding="utf-8" version="4.0"/>
    <xsl:param name="ark"/>
    <xsl:param name="model" />
    <xsl:param name="path" />
    <xsl:param name="visibility" />

    <xsl:template match="rdf:RDF">
        <xsl:call-template name="startJsonObject"/>

        <xsl:for-each select="//*[@rdf:about=$ark] | *[contains(local-name(), 'Object') or contains(local-name(), 'Collection')]">
          <xsl:variable name="id"><xsl:value-of select="@rdf:about"/></xsl:variable>
          <xsl:if test="$ark = '' or $ark = $id">
            <xsl:variable name="count"><xsl:number level="any" count="dams:Object"/></xsl:variable>
            <xsl:variable name="unitCode"><xsl:value-of select="//dams:Unit/dams:code"/></xsl:variable>
            <xsl:variable name="objectId">
                <xsl:choose>
                  <xsl:when test="@rdf:about = 'ARK'"><xsl:value-of select="concat($id, '#', $count)"/></xsl:when>
                  <xsl:otherwise><xsl:value-of select="substring-after(@rdf:about, '/20775/')" /></xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:variable name="modelName">
                <xsl:choose>
                    <xsl:when test="contains(local-name(), 'Collection')">Collection</xsl:when>
                    <xsl:when test="$model = '' and $unitCode = 'rdcp'">RdcpObject</xsl:when>
                    <xsl:when test="$model = ''">GenericObject</xsl:when>
                    <xsl:otherwise><xsl:value-of select="$model" /></xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
 
            <xsl:call-template name="toQuotedValue">
                <xsl:with-param name="val"><xsl:value-of select="concat($modelName, '|', $id, '#', $count)"/></xsl:with-param>
            </xsl:call-template>
            <xsl:text>:</xsl:text>
            <xsl:call-template name="startJsonArray"/>

            <xsl:call-template name="appendJsonObject">
                <xsl:with-param name="key">model</xsl:with-param>
                <xsl:with-param name="val"><xsl:value-of select="$modelName"/></xsl:with-param>
            </xsl:call-template>

            <xsl:call-template name="appendJsonObject">
                <xsl:with-param name="key">source_identifier</xsl:with-param>
                <xsl:with-param name="val"><xsl:value-of select="$objectId"/></xsl:with-param>
            </xsl:call-template>

            <xsl:call-template name="appendJsonObject">
                <xsl:with-param name="key">visibility</xsl:with-param>
                <xsl:with-param name="val"><xsl:value-of select="$visibility"/></xsl:with-param>
            </xsl:call-template>

            <xsl:call-template name="damsElements"/>

            <!-- dams:File -->
            <xsl:for-each select="dams:hasFile/dams:File">
                <xsl:sort select="dams:order" data-type="number" order="ascending" />
                <xsl:if test="contains(dams:use, '-source') or contains(@rdf:about, '/1.')">
                    <xsl:call-template name="damsFile">
                       <xsl:with-param name="fileId"><xsl:value-of select="translate(substring-after(@rdf:about, '/20775/'), '/', '-')" /></xsl:with-param>
                       <xsl:with-param name="parentId"><xsl:value-of select="$objectId" /></xsl:with-param>
                   </xsl:call-template>
               </xsl:if>
            </xsl:for-each>

            <!-- dams:Component -->
            <xsl:for-each select="dams:hasComponent/dams:Component">
                <xsl:sort select="dams:order" data-type="number" order="ascending" />
                <xsl:call-template name="damsComponent">
                    <xsl:with-param name="objectId"><xsl:value-of select="translate(substring-after(@rdf:about, '/20775/'), '/', '-')" /></xsl:with-param>
                   <xsl:with-param name="parentId"><xsl:value-of select="$objectId" /></xsl:with-param>
                   <xsl:with-param name="modelName"><xsl:value-of select="$modelName" /></xsl:with-param>
                   <xsl:with-param name="visibility"><xsl:value-of select="$visibility" /></xsl:with-param>
               </xsl:call-template>
            </xsl:for-each>

            <xsl:call-template name="endJsonArray"/>
            <xsl:text>,</xsl:text>
          </xsl:if>
        </xsl:for-each>

        <xsl:call-template name="endJsonObject"/>
    </xsl:template>

    <xsl:template name="damsComponent">
        <xsl:param name="objectId"/>
        <xsl:param name="parentId"/>
        <xsl:param name="modelName"/>
        <xsl:param name="visibility" />

        <xsl:call-template name="startJsonObject"/>
        <xsl:call-template name="toQuotedValue">
            <xsl:with-param name="val">Component|<xsl:value-of select="@rdf:about"/></xsl:with-param>
        </xsl:call-template>
        <xsl:text>:</xsl:text>
        <xsl:call-template name="startJsonArray"/>

        <xsl:call-template name="appendJsonObject">
            <xsl:with-param name="key">model</xsl:with-param>
            <xsl:with-param name="val"><xsl:value-of select="$modelName" /></xsl:with-param>
        </xsl:call-template>

        <xsl:call-template name="appendJsonObject">
            <xsl:with-param name="key">source_identifier</xsl:with-param>
            <xsl:with-param name="val"><xsl:value-of select="$objectId"/></xsl:with-param>
        </xsl:call-template>

        <xsl:call-template name="appendJsonObject">
            <xsl:with-param name="key">visibility</xsl:with-param>
            <xsl:with-param name="val"><xsl:value-of select="$visibility"/></xsl:with-param>
        </xsl:call-template>

        <xsl:call-template name="appendJsonObject">
            <xsl:with-param name="key">parents</xsl:with-param>
            <xsl:with-param name="val"><xsl:value-of select="$parentId" /></xsl:with-param>
        </xsl:call-template>

        <xsl:call-template name="damsElements"/>

        <!-- dams:File -->
        <xsl:for-each select="dams:hasFile/dams:File">
            <xsl:sort select="dams:order" data-type="number" order="ascending" />
            <xsl:if test="contains(dams:use, '-source') or contains(@rdf:about, '/1.')">
                <xsl:call-template name="damsFile">
                   <xsl:with-param name="fileId"><xsl:value-of select="translate(substring-after(@rdf:about, '/20775/'), '/', '-')" /></xsl:with-param>
                   <xsl:with-param name="parentId"><xsl:value-of select="$objectId" /></xsl:with-param>
               </xsl:call-template>
           </xsl:if>
        </xsl:for-each>

        <!-- dams:Component -->
        <xsl:for-each select="dams:hasComponent/dams:Component">
            <xsl:sort select="dams:order" data-type="number" order="ascending" />
            <xsl:call-template name="damsComponent">
               <xsl:with-param name="objectId"><xsl:value-of select="translate(substring-after(@rdf:about, '/20775/'), '/', '-')" /></xsl:with-param>
               <xsl:with-param name="parentId"><xsl:value-of select="$objectId"/></xsl:with-param>
               <xsl:with-param name="modelName"><xsl:value-of select="$modelName" /></xsl:with-param>
               <xsl:with-param name="visibility"><xsl:value-of select="$visibility" /></xsl:with-param>
           </xsl:call-template>
        </xsl:for-each>

        <xsl:call-template name="endJsonArray"/>
        <xsl:call-template name="endJsonObject"/>
        <xsl:text>,</xsl:text>
    </xsl:template>

    <xsl:template name="damsFile" match="dams:File">
        <xsl:param name="fileId"/>
        <xsl:param name="parentId"/>

        <xsl:variable name="use"><xsl:value-of select="dams:use" /></xsl:variable>
        <xsl:variable name="arkFileUri"><xsl:value-of select="translate(substring-after(@rdf:about, '/20775/'), '/', '-')" /></xsl:variable>
        <xsl:variable name="arkFileName">
            <xsl:choose>
                <xsl:when test="contains($parentId, '-')">
                    <xsl:value-of select="concat('20775-', $arkFileUri)" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="concat('20775-', substring-before($arkFileUri, '-'), '-0-', substring-after($arkFileUri, '-'))" />
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:call-template name="startJsonObject"/>
        <xsl:call-template name="toQuotedValue">
            <xsl:with-param name="val">File|<xsl:value-of select="@rdf:about"/></xsl:with-param>
        </xsl:call-template>
        <xsl:text>:</xsl:text>
        <xsl:call-template name="startJsonArray"/>

        <xsl:call-template name="appendJsonObject">
            <xsl:with-param name="key">model</xsl:with-param>
            <xsl:with-param name="val">FileSet</xsl:with-param>
        </xsl:call-template>

        <xsl:call-template name="appendJsonObject">
            <xsl:with-param name="key">source_identifier</xsl:with-param>
            <xsl:with-param name="val"><xsl:value-of select="$fileId"/></xsl:with-param>
        </xsl:call-template>

        <xsl:call-template name="appendJsonObject">
            <xsl:with-param name="key">parents</xsl:with-param>
            <xsl:with-param name="val"><xsl:value-of select="$parentId" /></xsl:with-param>
        </xsl:call-template>

        <xsl:call-template name="appendJsonObject">
            <xsl:with-param name="key">title</xsl:with-param>
            <xsl:with-param name="val"><xsl:value-of select="dams:sourceFileName" /></xsl:with-param>
        </xsl:call-template>

        <xsl:call-template name="appendJsonObject">
            <xsl:with-param name="key">
                <xsl:choose>
                   <xsl:when test="contains($use, '-source')">use:OriginalFile</xsl:when>
                   <xsl:when test="contains($use, '-service')">use:Service</xsl:when>
                   <xsl:when test="contains($use, '-alternate')">use:PreservationFile</xsl:when>
                   <xsl:otherwise>use:Unknown</xsl:otherwise>
               </xsl:choose>
            </xsl:with-param>
            <xsl:with-param name="val">
                <xsl:choose>
                   <xsl:when test="$path = ''"><xsl:value-of select="$arkFileName" /></xsl:when>
                   <xsl:otherwise><xsl:value-of select="concat($path, '/', $arkFileName)" /></xsl:otherwise>
               </xsl:choose>
            </xsl:with-param>
        </xsl:call-template>

        <xsl:call-template name="endJsonArray"/>
        <xsl:call-template name="endJsonObject"/>
        <xsl:text>,</xsl:text>
    </xsl:template>
</xsl:stylesheet>
