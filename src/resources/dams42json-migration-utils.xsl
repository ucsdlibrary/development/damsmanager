<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
      xmlns:dams="http://library.ucsd.edu/ontology/dams#"
      xmlns:mads="http://www.loc.gov/mads/rdf/v1#"
      xmlns:owl="http://www.w3.org/2002/07/owl#"
      xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
      xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
      xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
      exclude-result-prefixes="dams mads owl rdf rdfs">

    <xsl:variable name="lower">abcdefgijklmnopqrstuvwxyz</xsl:variable>
    <xsl:variable name="upper">ABCDEFGIJKLMNOPQRSTUVWXYZ</xsl:variable>

    <xsl:template name="capitalized">
        <xsl:param name="text"/>
        <xsl:value-of select="translate(substring($text,1,1), $lower, $upper)" /><xsl:value-of select="substring($text, 2, string-length($text)-1)" />
    </xsl:template>

    <xsl:template name="string-replace">
        <xsl:param name="text" />
        <xsl:param name="replace" />
        <xsl:param name="by" />
        <xsl:choose>
            <xsl:when test="$text = '' or $replace = '' or not($replace)" >
                <xsl:value-of select="$text" />
            </xsl:when>
            <xsl:when test="contains($text, $replace)">
                <xsl:value-of select="substring-before($text,$replace)" />
                <xsl:value-of select="$by" />
                <xsl:call-template name="string-replace">
                    <xsl:with-param name="text" select="substring-after($text,$replace)" />
                    <xsl:with-param name="replace" select="$replace" />
                    <xsl:with-param name="by" select="$by" />
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$text" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="appendJsonObject">
        <xsl:param name="key"/>
        <xsl:param name="val"/>

        <xsl:call-template name="startJsonObject"/>
            <xsl:call-template name="toQuotedValue">
                <xsl:with-param name="val"><xsl:value-of select="$key"/></xsl:with-param>
            </xsl:call-template>
            <xsl:text>:</xsl:text>
            <xsl:call-template name="toQuotedValue">
                 <xsl:with-param name="val"><xsl:value-of select="$val"/></xsl:with-param>
            </xsl:call-template>
        <xsl:call-template name="endJsonObject"/>
        <xsl:text>,</xsl:text>
    </xsl:template>

    <xsl:template name="toQuotedValue">
        <xsl:param name="val"/>
        <xsl:variable name="escapedVal">
            <xsl:call-template name="string-replace">
                <xsl:with-param name="text" select="$val" />
                <xsl:with-param name="replace" select="'&quot;'" />
                <xsl:with-param name="by" select="'\&quot;'" />
            </xsl:call-template>
        </xsl:variable>
        <xsl:text>"</xsl:text>
          <xsl:value-of select="$escapedVal" />
        <xsl:text>"</xsl:text>
    </xsl:template>

    <xsl:template name="escapeRelatedResource">
        <xsl:param name="val" />
        <xsl:variable name="lt">&lt;</xsl:variable>
        <xsl:variable name="gt">&gt;</xsl:variable>
        <xsl:choose>
            <xsl:when test="contains($val, $lt) and contains($val, $gt)">
                <xsl:variable name="escapedVal">
                   <xsl:call-template name="string-replace">
                       <xsl:with-param name="text" select="$val" />
                       <xsl:with-param name="replace" select="$lt" />
                       <xsl:with-param name="by" select="'&amp;lt;'" />
                   </xsl:call-template>
                </xsl:variable>
                <xsl:call-template name="string-replace">
                   <xsl:with-param name="text" select="$escapedVal" />
                   <xsl:with-param name="replace" select="$gt" />
                   <xsl:with-param name="by" select="'&amp;gt;'" />
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="contains($val, $lt)">
                <xsl:call-template name="string-replace">
                    <xsl:with-param name="text" select="$val" />
                    <xsl:with-param name="replace" select="$lt" />
                    <xsl:with-param name="by" select="'&amp;lt;'" />
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="contains($val, $gt)">
                <xsl:call-template name="string-replace">
                    <xsl:with-param name="text" select="$val" />
                    <xsl:with-param name="replace" select="$gt" />
                    <xsl:with-param name="by" select="'&amp;gt;'" />
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$val" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="startJsonObject">
        <xsl:text>{</xsl:text>
    </xsl:template>

    <xsl:template name="endJsonObject">
        <xsl:text>}</xsl:text>
    </xsl:template>

    <xsl:template name="startJsonArray">
        <xsl:text>[</xsl:text>
    </xsl:template>

    <xsl:template name="endJsonArray">
        <xsl:text>]</xsl:text>
    </xsl:template>

    <xsl:template name="damsResource">
        <xsl:value-of select="local-name()"/>
    </xsl:template>

    <xsl:template name="rdfDescription" match="rdf:Description">
        <xsl:variable name="ark"><xsl:value-of select="substring-after(@rdf:about, '/20775/')" /></xsl:variable>
        <xsl:variable name="elemmentName">
            <xsl:choose>
                <xsl:when test="..">
                    <xsl:call-template name="capitalized">
                        <xsl:with-param name="text"><xsl:value-of select="concat(local-name(..), 'Corrupted')" /></xsl:with-param>
                    </xsl:call-template>
                </xsl:when>
                <xsl:otherwise>Corrupted</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:call-template name="appendJsonObject">
           <xsl:with-param name="key"><xsl:value-of select="$elemmentName"/></xsl:with-param>
           <xsl:with-param name="val"><xsl:value-of select="concat(mads:authoritativeLabel, ' @ ', $ark)"/></xsl:with-param>
        </xsl:call-template>
    </xsl:template>
</xsl:stylesheet>
