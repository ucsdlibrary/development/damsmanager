<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
      xmlns:dams="http://library.ucsd.edu/ontology/dams#"
      xmlns:mads="http://www.loc.gov/mads/rdf/v1#"
      xmlns:owl="http://www.w3.org/2002/07/owl#"
      xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
      xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
      xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
      exclude-result-prefixes="dams mads owl rdf rdfs">

    <xsl:template name="damsElements">
        <!-- section title: mads:Title -->
        <xsl:for-each select="*[local-name()='title']">
            <xsl:apply-templates />
        </xsl:for-each>

        <!-- section contributions: dams:Relationship -->
        <xsl:for-each select="*[local-name()='relationship']">
            <xsl:apply-templates />
        </xsl:for-each>

        <!-- Collection(s) -->
        <xsl:if test="*[contains(local-name(), 'Collection')]">
            <xsl:variable name="collections">
                <xsl:for-each select="*[contains(local-name(), 'Collection') and not(contains(local-name(), 'has'))]">
                    <xsl:sort select="*/@rdf:about | @rdf:resource"/>
                    <xsl:variable name='resArk'><xsl:value-of select="*/@rdf:about | @rdf:resource"/></xsl:variable>
                    <xsl:variable name="ark"><xsl:value-of select="substring-after($resArk, '/20775/')" /></xsl:variable>
                    <xsl:value-of select="$ark"/>
                </xsl:for-each>
            </xsl:variable>
            <xsl:call-template name="appendJsonObject">
               <xsl:with-param name="key">parents</xsl:with-param>
               <xsl:with-param name="val"><xsl:value-of select="$collections"/></xsl:with-param>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>

    <!-- section: title -->
    <xsl:template name="madsTitle" match="mads:Title">
        <xsl:for-each select="mads:elementList">
            <xsl:for-each select="*[local-name() != 'NonSortElement']">
                <xsl:variable name="titleName">
                    <xsl:choose>
                        <xsl:when test="local-name()='MainTitleElement'">title</xsl:when>
                        <xsl:when test="local-name()='SubTitleElement'">title_subtitle</xsl:when>
                        <xsl:otherwise><xsl:value-of select="concat('title_unknown:', local-name())"/></xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>

                <xsl:if test="$titleName != ''">
                    <xsl:call-template name="appendJsonObject">
                        <xsl:with-param name="key"><xsl:value-of select="$titleName"/></xsl:with-param>
                        <xsl:with-param name="val">
                            <xsl:choose>
                                <xsl:when test="../mads:NonSortElement"><xsl:value-of select="concat(../mads:NonSortElement,' ',mads:elementValue)"/></xsl:when>
                                <xsl:otherwise><xsl:value-of select="mads:elementValue"/></xsl:otherwise>
                            </xsl:choose>
                        </xsl:with-param>
                    </xsl:call-template>
                </xsl:if>
            </xsl:for-each>
        </xsl:for-each>
        <xsl:for-each select="*[contains(local-name(), 'Variant')]">
            <xsl:call-template name="appendJsonObject">
                <xsl:with-param name="key">
                    <xsl:choose>
                        <xsl:when test="$model = 'GeospatialObject'">title_alternative_geospatial</xsl:when>
                        <xsl:otherwise>title_alternative</xsl:otherwise>
                    </xsl:choose>
                </xsl:with-param>
                <xsl:with-param name="val"><xsl:value-of select="mads:Variant/mads:variantLabel"/></xsl:with-param>
            </xsl:call-template>
        </xsl:for-each>
    </xsl:template>

    <!-- section: contributors -->
    <xsl:template name="damsRelationship" match="dams:Relationship">
        <xsl:variable name="roleArk" select="dams:role//@rdf:about | dams:role/@rdf:resource" />
        <xsl:variable name="nameArk" select="*[contains(local-name(), 'Name') or contains(local-name(), 'name')]//@rdf:about | *[contains(local-name(), 'Name') or contains(local-name(), 'name')]/@rdf:resource" />
        <xsl:variable name="ark"><xsl:value-of select="substring-after($nameArk, '/20775/')" /></xsl:variable>
        <xsl:variable name="role">
          <xsl:choose>
              <xsl:when test="//mads:Authority[@rdf:about=$roleArk]/mads:authoritativeLabel | //mads:Authority[@rdf:about=$roleArk]/rdf:value">
                  <xsl:value-of select="//mads:Authority[@rdf:about=$roleArk]/mads:authoritativeLabel | //mads:Authority[@rdf:about=$roleArk]/rdf:value"/>
              </xsl:when>
              <xsl:when test="//*[@rdf:about=$roleArk]/mads:authoritativeLabel | //*[@rdf:about=$roleArk]/rdf:value">
                  <xsl:value-of select="concat(//*[@rdf:about=$roleArk]/mads:authoritativeLabel | //*[@rdf:about=$roleArk]/rdf:value, 'Corrupted')"/>
              </xsl:when>
              <xsl:otherwise>
                  <xsl:value-of select="substring-after($roleArk, '/20775/')"/>
              </xsl:otherwise>
          </xsl:choose>
        </xsl:variable>

        <xsl:variable name="name">
            <xsl:choose>
                <xsl:when test="*[contains(local-name(), 'personal')] and //mads:PersonalName[@rdf:about=$nameArk]">Person</xsl:when>
                <xsl:when test="*[contains(local-name(), 'corporate')] and //mads:CorporateName[@rdf:about=$nameArk]">Corporate</xsl:when>
                <xsl:when test="*[local-name()='name'] and //mads:Name[@rdf:about=$nameArk]">Name</xsl:when>
                <xsl:when test="*[contains(local-name(), 'personal')]">PersonCorrupted</xsl:when>
                <xsl:when test="*[contains(local-name(), 'corporate')]">CorporateCorrupted</xsl:when>
                <xsl:when test="*[local-name()='name']">NameCorrupted</xsl:when>
                <xsl:otherwise>NameUnknown</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="agent_key">
            <xsl:choose>
                <xsl:when test="$model = 'GeospatialObject' and $role = 'Creator' and $name = 'Person'">creator_geospatial</xsl:when>
                <xsl:when test="$model = 'GeospatialObject' and $role = 'Creator' and $name = 'Corporate'">creator_geospatial</xsl:when>
                <xsl:when test="$role = 'Creator' and $name = 'Person'">agent_creator</xsl:when>
                <xsl:when test="$role = 'Creator' and $name = 'Corporate'">agent_creator</xsl:when>
                <xsl:when test="$role = 'Principal Investigator' and $name = 'Person'">agent_pi</xsl:when>
                <xsl:when test="$role = 'Author' and $name = 'Person'">agent_author</xsl:when>
                <xsl:when test="$role = 'Photographer' and $name = 'Person'">agent_photographer</xsl:when>
                <xsl:when test="$role = 'Contributor' and $name = 'Person'">agent_contributor</xsl:when>
                <xsl:when test="$role = 'Contributor' and $name = 'Corporate'">agent_contributor</xsl:when>
                <xsl:when test="$role = 'Publisher' and $name = 'Person'">agent_publisher</xsl:when>
                <xsl:when test="$role = 'Publisher' and $name = 'Corporate'">agent_publisher</xsl:when>
                <xsl:otherwise><xsl:value-of select="concat('agent_unknown:', $role, '_', $name)"/></xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:if test="$agent_key != ''">
            <xsl:call-template name="appendJsonObject">
                <xsl:with-param name="key"><xsl:value-of select="$agent_key"/></xsl:with-param>
                <xsl:with-param name="val">
                    <xsl:value-of select="//*[@rdf:about=$nameArk]/mads:authoritativeLabel"/>
                </xsl:with-param>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>
