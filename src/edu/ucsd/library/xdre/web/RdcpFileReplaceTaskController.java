package edu.ucsd.library.xdre.web;

import java.io.OutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import edu.ucsd.library.xdre.ingest.fileReplace.RdcpFileReplaceConfig;
import edu.ucsd.library.xdre.ingest.fileReplace.TransferredFile;
import edu.ucsd.library.xdre.ingest.RdcpFileReplaceHandler;
import edu.ucsd.library.xdre.ingest.RdlShareHandler;
import edu.ucsd.library.xdre.ingest.RdcpFileTransfer;
import edu.ucsd.library.xdre.ingest.fileReplace.ArkFileMapping;
import edu.ucsd.library.xdre.ingest.rdlshare.RdlShareApiClient;
import edu.ucsd.library.xdre.ingest.rdlshare.RdlShareApiDownloader;
import edu.ucsd.library.xdre.utils.Constants;
import edu.ucsd.library.xdre.utils.DAMSClient;

/**
 * Class RdcpFileReplaceTaskController perform task for RDCP file replace
 * @author lsitu
 *
 */
public class RdcpFileReplaceTaskController implements Controller {
    private static Logger log = Logger.getLogger(RdcpFileReplaceTaskController.class);

    private static boolean processing = false;

    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {

        // The relative path of the project folder in dams-staging that is setup for file replacement
        String path = request.getParameter("path");

        JSONObject result = new JSONObject();
        JSONArray transferredFiles = new JSONArray();
        result.put("transferredFiles", transferredFiles);

        Path projectPath = null;

        try {
            projectPath = Paths.get(Constants.DAMS_STAGING).resolve(path);

            Path configPath = projectPath.resolve(RdcpFileTransfer.REPORT_FOLDER).resolve(RdcpFileTransfer.CONFIG_FILE);
            if (configPath.toFile().exists()) {

                RdcpFileReplaceHandler handler = performUploadTask(projectPath.toFile().getAbsolutePath());

                int reportsSize = handler.getReports().size();

                if (reportsSize > 0) {

                    int errorSize = handler.getErrorReports().size();
                    String resultMessage = errorSize > 0 ? "failed (" + errorSize + " out of " + reportsSize + " failure). "
                            : "succeeded.";

                    resultMessage = "RDCP " + projectPath.toFile().getName() + " file replace " + resultMessage;
                    result.put("status", errorSize == 0 ? true : false);
                    result.put("message", resultMessage);

                    String[] headers = RdcpFileReplaceHandler.reportHeaders;
                    for (String[] report : handler.getReports()) {
                        JSONObject transferredFile = new JSONObject();
                        transferredFiles.add(transferredFile);

                        for (int i = 0; i < headers.length; i++) {
                            transferredFile.put(headers[i], report[i]);
                        }
                    }
                } else {
                    result.put("status", false);
                    result.put("message", "No transferred files found.");
                }
            } else {
                result.put("status", false);
                result.put("message", "Configuration file " + configPath.toFile().getAbsolutePath() + " is not found for project " + path);
            }
        } catch (Exception e) {

            String error = "Error RDCP file replacement for project " + projectPath.toFile().getName();
            log.error(error, e);

            result.put("status", false);
            result.put("message", error + ": " + e.getMessage());
        } finally {
            response.setContentType("application/json");

            try (OutputStream out = response.getOutputStream();) {
                out.write(result.toJSONString().getBytes());
            }
        }

        return null;
    }

    /**
     * Retrieve transferred files and perform the file replacement task.
     * @param projectPath - the path to the project folder setup for file replacement on dams-staging
     * @return
     * @throws Exception
     */
    public static RdcpFileReplaceHandler performUploadTask(String projectPath) throws Exception {

        RdlShareApiClient apiClient = null;
        DAMSClient damsClient = null;
        List<TransferredFile> transferredFiles = null;

        RdcpFileReplaceHandler handler = null;
        boolean successful = false;

        RdcpFileReplaceConfig config = null;

        if (assignUploadTask()) {

            try {
                Path configPath = Paths.get(projectPath)
                        .resolve(RdcpFileTransfer.REPORT_FOLDER)
                        .resolve(RdcpFileTransfer.CONFIG_FILE);

                if (configPath.toFile().exists()) {
                    try {
                        config = new RdcpFileReplaceConfig(configPath.toFile());
                    } catch (Exception e) {
                        log.error("Error load configuration file", e);
                        throw new Exception("Error load configuration file", e);
                    }
                } else {
                    throw new Exception("Configuration file " + configPath.toFile().getAbsolutePath() + " is not found for projetct " + projectPath);
                }

                damsClient = new DAMSClient(Constants.DAMS_STORAGE_URL);

                Path arkMappingPath = Paths.get(projectPath, RdcpFileTransfer.REPORT_FOLDER, RdcpFileReplaceHandler.arkMappingFile);
                List<ArkFileMapping> arkFileMappings = RdcpFileReplaceHandler.getArkFileMap(arkMappingPath.toFile().getAbsolutePath());

                if (config.needRdlShareDownload()) {
                    apiClient = new RdlShareApiClient(config);

                    RdlShareApiDownloader apiDownloader = new RdlShareApiDownloader(projectPath, apiClient, arkFileMappings);
                    transferredFiles = new ArrayList<TransferredFile>(apiDownloader.downloadAttachments());
                    handler = new RdlShareHandler(config, projectPath, transferredFiles, apiClient, damsClient, arkFileMappings);
                } else {
                    // Lookup files from the /Active_Files directory
                    transferredFiles = RdcpFileTransfer.retrieveActiveFiles(RdcpFileTransfer.getDownloadPath(projectPath));
                    handler = new RdcpFileReplaceHandler(config, projectPath, transferredFiles, damsClient, arkFileMappings);
                }

                log.info("RDCP file replacement: " + transferredFiles.size() + " files transferred for project " + projectPath + ".");

                // perform ingest the attachments/files to dams
                try {
                    successful = handler.replaceFiles();
                } catch (Exception ex) {
                    String error = "File replacement process failed to start: " + ex.getMessage();

                    log.error(error, ex);

                    throw ex;
                }
            } catch (Exception ex) {
                String error = "RDCP file replacement error: " + ex.getMessage();

                log.error(error, ex);

                // notify process error occurs
                if (config != null) {
                    String[] emails = config.getDistributionEmails().toArray(new String[config.getDistributionEmails().size()]);
                    notifyProcessFailure(emails, error);
                }

                throw ex;
            } finally {
                endUploadTask(); // release the RDCP upload process

                if(apiClient != null) {
                    apiClient.close();
                }

                if(damsClient != null) {
                    damsClient.close();
                }

                // generate the final report
                if (handler != null) {
                    if (transferredFiles.size() > 0) {
                        // write reports to rdcp staging
                        handler.writeReports();

                        if (!successful) {
                            // notify failure status
                            handler.emailNotify();
                        }
                    } else {
                        // notify if reaching the configured interval of days with no files received.
                        handler.notifyFilesTransferredExpected();
                    }
                }
            }
        } else {
            throw new Exception("RDCP file replacement process is already running ...");
        }

        return handler;
    }

    /*
     * Ensure that only one processes will run for RDCP file replace.
     * This will reject other request when a process is running.
     * @return
     */
    private static synchronized boolean assignUploadTask() {
        if (processing == true) {
            log.warn("RDCP File Replace process is already started ...");
            return false;
        } else {
            processing = true;

            log.info("RDCP File Replace process is started ...");
        }
        return processing;
    }

    /*
     * Reset the flag for RDCP file replace process.
     */
    private static synchronized void endUploadTask() {
        if (processing == true) {
            processing = false;

            log.info("RDCP File Replace process is ending ...");
        }
    }

    /*
     * Notify failure of the file replace process.
     * @param title
     * @param body
     */
    private static void notifyProcessFailure(String[] emails, String errorMessage) {
        String errorTitle = RdcpFileReplaceHandler.getEmailSubject();

        try {
            RdcpFileReplaceHandler.sendMail(emails, errorTitle, errorMessage, null);
        } catch (Exception ex) {
            log.error("RDCP File Replace send mail failure", ex);
        }
    }
}
