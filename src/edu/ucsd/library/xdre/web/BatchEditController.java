package edu.ucsd.library.xdre.web;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import edu.ucsd.library.xdre.tab.BatchEditExcelSource;
import edu.ucsd.library.xdre.utils.DAMSClient;


 /**
 * Class BatchEditController, the model for the BatchEdit view
 *
 * @author lsitu@ucsd.edu
 */
public class BatchEditController implements Controller {

    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String message = request.getParameter("message");

        String indexPriorityValue = request.getParameter("indexPriority");
        int indexPriority = indexPriorityValue != null ? Integer.parseInt(indexPriorityValue) : DAMSClient.PRIORITY_DEFAULT;

        // initiate column name and control values for validation
        String validateTemplate = request.getServletContext().getRealPath("files/xls_standard_input_template.xlsx");
        try {
            BatchEditExcelSource.initControlValues(new File( validateTemplate));
        } catch (Exception e) {
            e.printStackTrace();
        }

        HttpSession session = request.getSession();
        session.setAttribute("user", request.getRemoteUser());
        Map dataMap = new HashMap();

        message = !StringUtils.isBlank(message) ? message : (String)session.getAttribute("message");
        session.removeAttribute("message");

        dataMap.put("indexPriorities", DAMSClient.getIndexPriorityOptions());
        dataMap.put("prioritySelected", indexPriority);
        dataMap.put("message", message);

        return new ModelAndView("batchEdit", "model", dataMap);
    }
}
