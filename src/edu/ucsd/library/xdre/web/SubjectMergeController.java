package edu.ucsd.library.xdre.web;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import edu.ucsd.library.xdre.utils.DAMSClient;

/**
 * SubjectMergeController: merge duplicate authority records.
 * @author lsitu
 */
public class SubjectMergeController implements Controller {    
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        String message = "";
        Map dataMap = new HashMap();        

        HttpSession session = request.getSession();
        message = (String)session.getAttribute("message");
        session.removeAttribute("message");


        String sourceOption = request.getParameter("source");
        String mergeOption = request.getParameter("mergeOption");
        String excelSrcPath = request.getParameter("filesPath");

        String indexPriorityValue = request.getParameter("indexPriority");
        int indexPriority = indexPriorityValue != null ? Integer.parseInt(indexPriorityValue) : DAMSClient.PRIORITY_DEFAULT;

        dataMap.put("indexPriorities", DAMSClient.getIndexPriorityOptions());
        dataMap.put("prioritySelected", indexPriority);
        dataMap.put("message", (message == null ? "" : message));
        dataMap.put("mergeOption", mergeOption);
        dataMap.put("source", sourceOption);
        dataMap.put("filesPath", excelSrcPath == null ? "" : excelSrcPath);

        return new ModelAndView("subjectMerge", "model", dataMap);
    }
}
