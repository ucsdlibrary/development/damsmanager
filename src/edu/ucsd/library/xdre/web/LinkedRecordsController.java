package edu.ucsd.library.xdre.web;

import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import edu.ucsd.library.xdre.statistic.analyzer.Statistics;
import edu.ucsd.library.xdre.utils.Constants;
import edu.ucsd.library.xdre.utils.DAMSClient;
import edu.ucsd.library.xdre.utils.DamsURI;

/**
 * Class SearchLinkedRecordsController to generate report for linked records of an ARK
 * @author lsitu
 *
 */
public class LinkedRecordsController implements Controller {
    private static Logger log = Logger.getLogger(LinkedRecordsController.class);

    public static final int LEVEL_ONE = 1;
    public static final int LEVEL_TWO = 2;
    public static final int LEVEL_THREE = 3;

    public static final String PREDICATE_MADS_COMPONENT_LIST = "http://library.ucsd.edu/ark:/20775/bd23898848";
    public static final String PREDICATE_DAMS_RELATIONSHIP = "http://library.ucsd.edu/ark:/20775/bd1297727c";
 
    public static String[] topLevelPredicates = {PREDICATE_MADS_COMPONENT_LIST, PREDICATE_DAMS_RELATIONSHIP};

    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String ark = request.getParameter("ark");
        boolean exportOnly = request.getParameter("export") != null;

        DAMSClient damsClient = null;

        Map<String, Object> dataMap = new HashMap<>();
        Map<String, String> results = new TreeMap<>();
        if (StringUtils.isNotBlank(ark)) {
            try {
                String arkUrlBase = Constants.DAMS_ARK_URL_BASE + (Constants.DAMS_ARK_URL_BASE.endsWith("/") ? "" : "/");
                String arkUrl = arkUrlBase + Constants.ARK_ORG + "/" + ark.substring(ark.lastIndexOf("/") + 1).trim();
                damsClient = new DAMSClient(Constants.DAMS_STORAGE_URL);

                // direct linked records lookup: collection, object, component etc.
                lookupLinkedRecords(damsClient, arkUrl, results, LEVEL_ONE);

                // indirect linked records lookup with mads:ComponentList and dams:relationship predicates
                for (String pre : topLevelPredicates) {
                    lookupLinkedRecords(damsClient, arkUrl, results, LEVEL_TWO, pre);
                }

                // Lookup records lined to mads:componentList/mads:elementList recursively
                lookupLinkedRecords(damsClient, arkUrl, results, LEVEL_THREE);

                if (exportOnly) {
                    // export result in CSV format
                    StringBuilder reportBuilder = new StringBuilder();
                    reportBuilder.append("Type,ARK\n");

                    for (String key : results.keySet()) {
                        addCsvReport(reportBuilder, results.get(key), key.split("\\|")[0]);
                    }

                    String fileName = "linked_records-" + ark.substring(ark.lastIndexOf("/") + 1) + ".csv";
                    OutputStream out = response.getOutputStream();
                    response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
                    response.setContentType("text/csv");
                    out.write(reportBuilder.toString().getBytes());
                    out.close();
                    return null;
                } else {
                    dataMap.put("results", results);
                }
            } catch (Exception e){
                e.printStackTrace();
                dataMap.put("message", "Error: " + e.getMessage());
                log.error("Error: " + e.getMessage(), e);
            } finally {
                if (damsClient != null)
                    damsClient.close();
            }
        } else {
            dataMap.put("results", results);
        }

        dataMap.put("ark", ark);

        return new ModelAndView("linkedRecords", "model", dataMap);
    }

    /**
     * Lookup linked records
     * @param damsClient
     * @param ark
     * @param linkedRecords
     * @param level
     * @return
     * @throws Exception
     */
     public static void lookupLinkedRecords(DAMSClient damsClient, String ark, Map<String, String> linkedRecords, int level)
             throws Exception {
         lookupLinkedRecords(damsClient, ark, linkedRecords, level, null);
     }

   /**
    * Lookup linked records
    * @param damsClient
    * @param ark
    * @param linkedRecords
    * @param level
    * @param topLevelPredicate
    * @return
    * @throws Exception
    */
    public static void lookupLinkedRecords(DAMSClient damsClient, String ark, Map<String, String> linkedRecords,
            int level, String topLevelPredicate) throws Exception {
        String sparql = buildSparqlWithArk(ark, level, topLevelPredicate);
        List<Map<String, String>> records = damsClient.sparqlLookup(sparql);

        boolean blankNodeFound = false;

        for (Map<String, String> recordMap : records) {
            String type = recordMap.get("type") != null ? recordMap.get("type") : "";
            String objectUrl = recordMap.get("sub");
            if(objectUrl.startsWith("http")) {
                String urlBase = "https://" + Constants.CLUSTER_HOST_NAME + ".ucsd.edu/dc/";

                if (StringUtils.isBlank(type)) {
                    objectUrl = DamsURI.toParts(objectUrl, null).getObject();

                    // retrieve record type
                    Document doc = damsClient.getRecord(objectUrl);
                    type = doc.selectSingleNode("/rdf:RDF/*").getName();
                }

                if (type.contains("Collection")) {
                    objectUrl = urlBase + "collection" + objectUrl.substring(objectUrl.lastIndexOf("/"));
                } else if (type.contains("Object")) {
                    objectUrl = urlBase + "object" + objectUrl.substring(objectUrl.lastIndexOf("/"));
                } else if (type.contains("Component")) {
                    // change component url to object URL
                    type = "dams:Object";
                    int arkOrgIdx = objectUrl.lastIndexOf(Constants.ARK_ORG);
                    String[] parts = objectUrl.substring(arkOrgIdx + Constants.ARK_ORG.length() + 1).split("/");
                    objectUrl = urlBase + "object/" + parts[0];
                }

                type = type.indexOf(":") > 0 ? type.substring(type.indexOf(":") + 1) : type;

                if (!linkedRecords.containsValue(objectUrl)) {
                    linkedRecords.put(type + "|" + objectUrl, objectUrl);
                }
            } else {
                blankNodeFound = true;
            }
        }

        // recursive lookup for records linked to mads:componentList/mads:elementList
        if (level >= 3 && blankNodeFound) {
            lookupLinkedRecords(damsClient, ark, linkedRecords, level + 1);
        }
    }

    /* 
     * Build SPARQL to search for linked records
     * @param ark, the ark url
     * @param level, the level of SPARQL lookup
     * @return
     */
    public static String buildSparqlWithArk(String ark, int level) {
        return buildSparqlWithArk(ark, level, null);
    }

    /* 
     * Build SPARQL to search for linked records
     * @param ark, the ark url
     * @param level, the level of SPARQL lookup
     * @param topLevelPredicate, top level predicate
     * @return
     */
    public static String buildSparqlWithArk(String ark, int level, String topLevelPredicate) {
        String labelQuery = "?sub rdf:type ?_bn . ?_bn rdf:label ?type";
        String pre = StringUtils.isBlank(topLevelPredicate) ? "?p1"
                : topLevelPredicate.startsWith("?") ? topLevelPredicate
                : "<" + topLevelPredicate + ">";

        // build 1 level sparql: direct linked records with class type
        if (level == 1) {
            return "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "
                        + "SELECT ?sub ?type WHERE { ?sub " + pre + " <" + ark + "> . " + labelQuery + " }";
        } else if (level == 2) {
            // build 2 levels sparql for elements with blank nodes limited by top level predicate.
            String whereClause = "?sub " + pre + " ?o1 . ?o1 ?p2 " + "<" + ark + ">";

            return "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "
                        + "SELECT ?sub WHERE { " + whereClause + " }";
        } else {
            // build 3 or more levels sparql for rdf:componentList/rdf:ElementList only
            String predFirst = "<http://library.ucsd.edu/ark:/20775/bd6724396t>"; // Predicate rdf#first
            String predRest = "<http://library.ucsd.edu/ark:/20775/bd54274567>";  // Predicate rdf#resr
            String whereClause = "?sub " + pre + " ?o1";
            for (int i = 2; i <= level; i++) {
                String sVar = "?o" + (i - 1);
                String oVar = "?o" + i;
                if (i == level) {
                    whereClause +=  " . " + sVar + " " + predFirst + " <" + ark + ">";
                } else {
                    whereClause +=  " . " + sVar + " " + predRest + " " + oVar;
                }
            }

            return "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "
                        + "SELECT ?sub WHERE { " + whereClause + " }";
        }
    }

    /*
     * Build report in CSV format
     * @param reportBuilder
     * @param arkUrl
     */
    private void addCsvReport(StringBuilder reportBuilder, String ark, String type) {
        reportBuilder.append(Statistics.escapeCsv(type) + ", " + Statistics.escapeCsv(ark) + "\n");
    }
}
