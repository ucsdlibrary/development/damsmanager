package edu.ucsd.library.xdre.web;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

/**
 * Controller for ingest status report page.
 * @author lsitu
 *
 */
public class IngestStatusController implements Controller{

    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String submissionId = request.getParameter("submissionId");

        Map<String, Object> model = new HashMap<>();
        model.put("submissionId", submissionId);

        return new ModelAndView("ingestStatus", "model", model);
    }
}
