package edu.ucsd.library.xdre.web;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import edu.ucsd.library.xdre.collection.BatchExportHandler;
import edu.ucsd.library.xdre.collection.BatchRdfMetadataExportHandler;
import edu.ucsd.library.xdre.collection.CollectionHandler;
import edu.ucsd.library.xdre.utils.Constants;
import edu.ucsd.library.xdre.utils.DAMSClient;
import edu.ucsd.library.xdre.utils.RequestOrganizer;

/**
 * Controller to perform collection metadata export.
 * @author lsitu
 *
 */
public class CollectionExportController implements Controller{
    private static Logger log = Logger.getLogger( CollectionExportController.class );

    @Override
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DAMSClient damsClient = null;    

        request.setCharacterEncoding("UTF-8");
        String collectionId = request.getParameter("category");
        String exportFormat = request.getParameter("exportFormat");

        String environment = Constants.CLUSTER_HOST_NAME.equals("library") ? "Production" : Constants.CLUSTER_HOST_NAME.contains("test")
                ? "Staging" : Constants.CLUSTER_HOST_NAME.contains("qa") ? "QA" : Constants.CLUSTER_HOST_NAME;

        CollectionHandler handler = null;

        try {

            response.setContentType("text/html");

            damsClient = new DAMSClient(Constants.DAMS_STORAGE_URL);
            damsClient.setUser(request.getRemoteUser());

            String submissionId = RequestOrganizer.getProcessId(request.getRemoteUser());

            String appUrl = "https://" + (Constants.CLUSTER_HOST_NAME.indexOf("localhost")>=0 ? "localhost:8443" :
                Constants.CLUSTER_HOST_NAME.indexOf("lib-ingest")>=0 ? Constants.CLUSTER_HOST_NAME+".ucsd.edu:8443" :
                    Constants.CLUSTER_HOST_NAME+".ucsd.edu");
            String logLink = appUrl + "/damsmanager/downloadLog.do?submissionId=" + submissionId;

            log.info("Exporting collection " + collectionId + " for user " + damsClient.getUser() + " ...");

            Document doc = damsClient.getRecord(collectionId);
            String colTitle = doc.valueOf("/rdf:RDF/*/dams:title//mads:authoritativeLabel");

            File rdfFile = BatchExportHandler.getRdfFile("" + submissionId);

            OutputStream fileOut = null;
            try {
                exportFormat = StringUtils.isBlank(exportFormat) ? "RDF/XML-ABBREV" : exportFormat;
                if (exportFormat.equalsIgnoreCase("csv") || exportFormat.equalsIgnoreCase("excel")) {
                    // Handling Excel/CSV format object metadata export
                    handler = new BatchExportHandler(damsClient, collectionId, exportFormat, true);
                } else {
                    // Handling RDF/XML format object metadata export
                    fileOut = new FileOutputStream(rdfFile);
                    handler = new BatchRdfMetadataExportHandler(damsClient, collectionId, exportFormat, true, fileOut);
                }
                handler.setSubmissionId(submissionId);
                handler.execute();
            } finally {
                CollectionHandler.close(fileOut);
            }

            File destFile = rdfFile;
            if (exportFormat.equalsIgnoreCase("csv")) {
                destFile = BatchExportHandler.getCsvFile("" + submissionId);
            } else if (exportFormat.equalsIgnoreCase("excel")) {
                destFile = BatchExportHandler.getExcelFile("" + submissionId);
            }

            log.info("Collection " + collectionId + " exported for user " + damsClient.getUser() + ": "
                    + destFile.getAbsolutePath());

            Map<String, Object> dataMap = new HashMap<>();

            dataMap.put("environment", environment);
            dataMap.put("clusterHostName", Constants.CLUSTER_HOST_NAME);
            dataMap.put("logLink", logLink);
            dataMap.put("fileName", destFile.getName());
            dataMap.put("format", exportFormat);
            dataMap.put("category", appUrl + "/dc/collection/" + collectionId);
            dataMap.put("title", colTitle);

            return new ModelAndView("collectionExport", "model", dataMap);
        } catch (Exception ex) {
            log.error(ex);
            throw ex;
        } finally {
            if(damsClient != null)
                damsClient.close();
            if(handler != null) {
                handler.release();
                handler = null;
            }
        }
    }
}
