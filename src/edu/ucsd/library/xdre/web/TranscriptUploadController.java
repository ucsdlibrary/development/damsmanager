package edu.ucsd.library.xdre.web;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import edu.ucsd.library.xdre.utils.Constants;


 /**
 * Class TranscriptUploadController, the model for transcript file upload
 * @author lsitu
 */
public class TranscriptUploadController implements Controller {

	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String message = request.getParameter("message");
		String filesPath = request.getParameter("filesPath");

		if(message == null)
			message = "";
		if(filesPath == null)
			filesPath = "";

		Map dataMap = new HashMap();

		List<String> ingestFiles = new ArrayList<String>();
		filesPath = filesPath.trim();
		
		if (filesPath.length() > 0) {
			String[] filesPaths = filesPath.split(";");
			File tmpFile = null;
			for(int j=0; j<filesPaths.length; j++) {
				tmpFile = new File(Constants.DAMS_STAGING + File.separatorChar + filesPaths[j]);
				File[] files = tmpFile.listFiles();
				for (File file : files) {
					if (file.isFile() && !file.isHidden()) {
						String filePath = file.getPath().replace('\\', '/');
						ingestFiles.add(filePath.substring(filePath.indexOf(filesPaths[j])));
					}
				}
			}
		}

		Collections.sort(ingestFiles);

		message = !StringUtils.isBlank(message) ? message : (String)request.getSession().getAttribute("message");
		request.getSession().removeAttribute("message");
		
		dataMap.put("message", message);
		dataMap.put("filesPath", filesPath);
		dataMap.put("files", ingestFiles);
		dataMap.put("arkName", Constants.DEFAULT_ARK_NAME);

		return new ModelAndView("transcriptUpload", "model", dataMap);
	}
}
