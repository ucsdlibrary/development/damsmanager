package edu.ucsd.library.xdre.web;

/**
 * Class SubjectOverlayController, the controller for the SubjectOverlay
 *
 * @author lsitu
 */
public class SubjectOverlayController extends SubjectImportController {
    @Override
    protected String getView() {
        return "subjectOverlay";
    }
}
