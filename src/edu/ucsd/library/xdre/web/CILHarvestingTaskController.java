package edu.ucsd.library.xdre.web;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import edu.ucsd.library.xdre.harvesting.CilApiClient;
import edu.ucsd.library.xdre.harvesting.CilApiDownloader;
import edu.ucsd.library.xdre.harvesting.CilHarvesting;
import edu.ucsd.library.xdre.harvesting.FieldMappings;
import edu.ucsd.library.xdre.utils.Constants;
import edu.ucsd.library.xdre.utils.DAMSClient;

/**
 * Class CILHarvestingController perform task for CIL harvesting
 * @author lsitu
 *
 */
public class CILHarvestingTaskController implements Controller {
    private static Logger log = Logger.getLogger(CILHarvestingTaskController.class);

    private static final char CSV_DELIEMITER = ',';
    private static boolean harvesting = false;

    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
        OutputStream out = null;
        try {
            out = response.getOutputStream();

            // lastModified to control source json download with modified date after.
            String lastModifiedParam = request.getParameter("lastModified");
            // The designated date for the harvest folder
            String harvestDateParam = request.getParameter("harvestDate");

            // Records with date modified after lastModified
            Date lastModified = null;
            if (!StringUtils.isBlank(lastModifiedParam )) {
                lastModified = new SimpleDateFormat(CilApiDownloader.DATE_FORMAT).parse(lastModifiedParam );
            }

            // Set harvest date for the ingest folder
            Date harvestDate = Calendar.getInstance().getTime();
            if (!StringUtils.isBlank(harvestDateParam)) {
                harvestDate = new SimpleDateFormat(CilApiDownloader.DATE_FORMAT).parse(harvestDateParam);
            }

            List<String> sourceJsonsAdded = performHarvestingTask(harvestDate, lastModified);

            if (sourceJsonsAdded.size() > 0) {
                out.write(("Successfully converted " + sourceJsonsAdded.size() + " JSON source files:\n" + String.join("\n", sourceJsonsAdded)).getBytes("UTF-8"));
            } else {
                out.write(("No JSON source records are processed.").getBytes("UTF-8"));
            }
        }catch (Exception e){
            e.printStackTrace();
            out.write(("Error convert JSON source: " + e.getMessage()).getBytes("UTF-8"));
        }finally {
            if(out != null)
                out.close();
        }
        return null;
    }

    /**
     * Download JSON source from CIL REST API and perform metadata conversion.
     * @return
     * @throws Exception
     */
    public static List<String> performHarvestingTask(Date dateHarvest, Date lastModified) throws Exception {
        
        CilApiClient cilApiClient = null;
        List<String> sourceJsonsAdded = new ArrayList<>();

        if (assignHarvestTask()) {
            try {
                cilApiClient = new CilApiClient();

                CilApiDownloader cilApiDownloader = new CilApiDownloader(cilApiClient, dateHarvest, lastModified);

                // Backup the inventory file.
                backupCilInventory(cilApiDownloader.getHarvestDirectory());

                // Initiate inventory for records that were booked for processed
                List<String> inventory = getInventory(cilApiDownloader.getHarvestDirectory());
                cilApiDownloader.setInventory(inventory);

                sourceJsonsAdded = cilApiDownloader.download();

                performHarvestingTask(cilApiDownloader.getHarvestDirectory(), sourceJsonsAdded, cilApiDownloader.getHarvestLabel());

                // Append processed records to inventory
                writeInventory(cilApiDownloader.getHarvestDirectory(), sourceJsonsAdded);

                // Log source files harvested
                cilApiDownloader.logMessage("Harvested " + sourceJsonsAdded.size() + " CIL JSON records:\n" + String.join("\n", sourceJsonsAdded));
            } catch (Exception ex) {
                ex.printStackTrace();
                log.error("CIL harvesting failed: " + ex.getMessage());
                throw ex;
            } finally {
                endHarvestTask(); // release the CIL harvest process

                if(cilApiClient != null) {
                    cilApiClient.close();
                }
            }
        } else {
            throw new Exception("CIL havesting process is already running ...");
        }

        return sourceJsonsAdded;
    }

    /*
     * Method to ensure that no other processes will run while harvest started.
     * This will reject all other process when a process is running.
     * @return
     */
    private static synchronized boolean assignHarvestTask() {
        if (harvesting == true) {
            log.warn("CIL Harvesting task is already started ...");
            return false;
        } else {
            harvesting = true;

            log.info("CIL Harvesting task is started ...");
        }
        return harvesting;
    }

    /*
     * Reset the flag for CIL harvesting process.
     */
    private static synchronized void endHarvestTask() {
        if (harvesting == true) {
            harvesting = false;

            log.info("CIL Harvesting task is ended ...");
        }
    }

    /**
     * Function to lookup json files added and perform processing.
     * @return
     * @throws Exception
     */
    public synchronized static List<String> performHarvestingTask(String harvestDirectory, List<String> sourceJsons,
            String harvestLabel) throws Exception{
        DAMSClient damsClient = null;
        List<String> sourceJsonsAdded = new  ArrayList<>();

        try {
            damsClient = new DAMSClient(Constants.DAMS_STORAGE_URL);

            if (sourceJsons.size() > 0) {
                log.info("Start processing " + sourceJsons.size() + " CIL JSON source files in the batch ...");

                sourceJsonsAdded.addAll(sourceJsons);

                // process the source JSON
                InputStream mappingsInput = null;

                // retrieve the CIL Processing and Mapping Instructions file content
                String cilMappingFile = StringUtils.isNotBlank(Constants.CIL_HARVEST_MAPPING_FILE)
                        ? Constants.CIL_HARVEST_MAPPING_FILE : CilHarvesting.CIL_HARVEST_MAPPING_FILE;
                if (new File(cilMappingFile).exists()) {
                    mappingsInput = new FileInputStream(cilMappingFile);
                } else {
                    // use the default CIL Processing and Mapping Instructions.xlsx file provided in the code
                    mappingsInput = CILHarvestingTaskController.class.getResourceAsStream(cilMappingFile);
                }

                String message = "";
                File destMetadataFile = null;
                File destSubjectsFile = null;
                try(InputStream mappingsIn = mappingsInput;) {
                    FieldMappings fieldMapping = new FieldMappings(mappingsIn);
                    CilHarvesting cilHarvesting = new CilHarvesting(
                            fieldMapping.getFieldMappings(),
                            fieldMapping.getConstantFields(),
                            sourceJsonsAdded);

                    String metadataProcessedDir = CilHarvesting.getMetadataProcessedDirectory(harvestDirectory);

                    String csvObjectString = cilHarvesting.toCSV(getDams42JsonXsl());
                    destMetadataFile = new File (metadataProcessedDir, harvestLabel + "_" + CilHarvesting.EXCEL_OBJECT_INPUT_FILENAME);
                    writeContent(destMetadataFile.getAbsolutePath(), csvObjectString);

                    // Export subject headings
                    String csvSubjectString = cilHarvesting.getSubjectHeadingsCsv();
                    destSubjectsFile = new File (metadataProcessedDir, harvestLabel + "_" + CilHarvesting.EXCEL_SUBJECT_HEADINGS_FILENAME);
                    writeContent(destSubjectsFile.getAbsolutePath(), csvSubjectString);

                    message = "Successfully converted " + sourceJsonsAdded.size() + " CIL JSON source files: "
                            + destMetadataFile.getAbsolutePath();

                    log.info(message);
                } catch (Exception e) {
                    e.printStackTrace();
                    message = "Failed to convert " + sourceJsonsAdded.size() + " CIL JSON source files: "
                            + (destMetadataFile != null ? destMetadataFile.getAbsolutePath() : "");
                    log.error(message, e);
                    throw new Exception(message);
                }

             // notify DDOM users by email for the status of CIL harvesting
                try {
                    String[] emails = Constants.CIL_HARVEST_NOTIFY_EMAILS.split("\\,");
                    String sender = Constants.MAILSENDER_DAMSSUPPORT;

                    DAMSClient.sendMail(sender, emails, "CIL Harvesting Status Report", message, "text/html", "smtp.ucsd.edu");
                } catch (Exception e) {
                    e.printStackTrace();
                    log.error("Failed to send mail for CIL harvesting: " + e.getMessage());
                }
            } else {
                log.info("No CIL JSON source files were detected to be added.");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            log.error("CIL harvesting failed: " + ex.getMessage());
            throw ex;
        } finally {
            if(damsClient != null)
                damsClient.close();
        }

        return sourceJsonsAdded;
    }

    /*
     * Get the inventory file
     * @param harvestDirectory
     * @return
     */
    private static File getInventoryFile(String harvestDirectory) {
        return new File(CilHarvesting.getDocumentsDirectory(harvestDirectory), CilHarvesting.INVENTORY_FILENAME);
    }

    /*
     * Get backup file for the inventory file
     * @param harvestTag
     * @return
     */
    private static File getBackupInventoryFile() {
        return new File(Constants.CIL_BACKUP_DIR, CilHarvesting.INVENTORY_FILENAME);
    }

    /*
     * Generate the inventory list
     * @param harvestDirectory
     * @return
     * @throws FileNotFoundException
     * @throws IOException
     */
    private static List<String> getInventory(String harvestDirectory) throws FileNotFoundException, IOException {
        List<String> inventory = new ArrayList<>();

        File inventoryFile = getInventoryFile(harvestDirectory);
        if (inventoryFile.exists()) {
            try (Reader reader = new FileReader(inventoryFile);
                     BufferedReader br = new BufferedReader(reader)) {
                String line;
                while ((line = br.readLine()) != null) {
                    String[] tokens = line.split("" + CSV_DELIEMITER);
                    if (tokens.length > 1 && StringUtils.isNotBlank(tokens[1]) && !inventory.contains(tokens[1].trim())) {
                        inventory.add(tokens[1].trim());
                    }
                }
            }
        }
        return inventory;
    }

    /*
     * Backup CIL inventory file
     * @param harvestDirectory
     * @param harvestLabel
     * @throws IOException
     */
    private static void backupCilInventory(String harvestDirectory) {
        try {
            log.info("CIL Harvest backing up invertory file 1-file_inventory.csv ...");

            File inventoryFile = getInventoryFile(harvestDirectory);
            File inventoryBackupFIle = getBackupInventoryFile();

            Files.copy(Paths.get(inventoryFile.getAbsolutePath()), Paths.get(inventoryBackupFIle.getAbsolutePath()), REPLACE_EXISTING);
        } catch (Exception e) {
            log.error("Error backing up invertory file 1-file_inventory.csv", e);
        }
    }

    /*
     * Write CIL records processed in the batch to inventory
     * @param harvestDirectory
     * @param jsonSourceFiles
     * @throws FileNotFoundException
     * @throws IOException
     */
    private static void writeInventory(String harvestDirectory, List<String> jsonSourceFiles) throws FileNotFoundException, IOException {
        SimpleDateFormat dataFormat = new SimpleDateFormat("M/d/yyyy");

        File inventoryFile = getInventoryFile(harvestDirectory);
        try (FileWriter fw = new FileWriter(inventoryFile, true);) {
            for (String sourceFile : jsonSourceFiles) {
                String fileName = new File(sourceFile).getName();

                fw.append(dataFormat.format(Calendar.getInstance().getTime()) + CSV_DELIEMITER);
                fw.append(fileName.replace(".json", "") + CSV_DELIEMITER);
                fw.append(CSV_DELIEMITER + "\n");
            }
        }
    }

    /**
     * Retrieve the dams42Json XSL as InputStream from configuration if there's any,
     * or use the default provided in source code
     * @return
     * @throws FileNotFoundException
     */
    public static String getDams42JsonXsl() throws FileNotFoundException {
        // retrieve the dams42json xsl content
        String dams42jsonXsl = StringUtils.isNotBlank(Constants.DAMS42JSON_XSL_FILE) 
                ? Constants.DAMS42JSON_XSL_FILE : CilHarvesting.DAMS42JSON_XSL_FILE;
        if (!(new File(dams42jsonXsl).exists())) {
            // use the default dams42json.xsl file provided in the source code
            dams42jsonXsl = CILHarvestingTaskController.class.getResource(CilHarvesting.DAMS42JSON_XSL_FILE).getPath();
        }
        return dams42jsonXsl;
    }

    /**
     * Write content to file
     * @param filePath
     * @param content
     * @return
     * @throws FileNotFoundException
     * @throws IOException
     */
    private static File writeContent(String filePath, String content) throws FileNotFoundException, IOException {
        byte[] buf = new byte[4096];
        File destFile = new File(filePath);

        try (InputStream in = new ByteArrayInputStream(content.getBytes("UTF-8"));
                OutputStream out = new FileOutputStream(destFile)) {
            int bytesRead = 0;
            while((bytesRead = in.read(buf)) > 0) {
                out.write(buf, 0, bytesRead);
            }
        }

        return destFile;
    }
}
