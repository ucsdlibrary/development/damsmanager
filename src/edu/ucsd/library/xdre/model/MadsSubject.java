package edu.ucsd.library.xdre.model;

import org.dom4j.Element;

public class MadsSubject extends Subject {
    public MadsSubject(String id, String type, String term, String elementName, String exactMatch, String closeMatch,
            String variant, String hiddenVariant) {
        super(id, type, term, elementName, exactMatch, closeMatch, variant, hiddenVariant);
    }

    public Element serialize(){
         return buildSubject( madsNS );
    }
}
