package edu.ucsd.library.xdre.model;

import org.dom4j.Element;

public class DamsSubject extends Subject {
    public DamsSubject(String id, String type, String term, String elementName, String exactMatch, String closeMatch,
            String variant, String hiddenVariant) {
        super(id, type, term, elementName, exactMatch, closeMatch, variant, hiddenVariant);
    }

    public Element serialize(){
        return buildSubject( damsNS );
    }
}
