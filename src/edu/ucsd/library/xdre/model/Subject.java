package edu.ucsd.library.xdre.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.dom4j.Branch;
import org.dom4j.Document;
import org.dom4j.DocumentFactory;
import org.dom4j.Element;
import org.dom4j.Namespace;
import org.dom4j.Node;
import org.dom4j.QName;

import edu.ucsd.library.xdre.tab.TabularRecordBasic;

/**
 * Abstract base class for subject headings
 * @author lsitu
 */
public abstract class Subject extends DAMSResource {
    public static final String MULTI_VALUE_DELIMITER = "\\|";

    public static final String DAMS = "dams";
    public static final String MADS = "mads";
    public static final String RDF = "rdf";

    public static final String AUTHORITATIVE_LABEL = "authoritativeLabel";
    public static final String HAS_EXACT_EXTERNAL_AUTHORITY = "hasExactExternalAuthority";
    public static final String HAS_CLOSE_EXTERNAL_AUTHORITY = "hasCloseExternalAuthority";
    public static final String HAS_VARIANT = "hasVariant";
    public static final String VARIANT = "Variant";
    public static final String VARIANT_LABEL = "variantLabel";
    public static final String HAS_HIDDEN_VARIANT = "hasHiddenVariant";

    public static final String ABOUT = "about";
    public static final String RESOURCE = "resource";
    public static final String COLLECTION = "Collection";
    public static final String ELEMENT = "Element";
    public static final String ELEMENT_LIST = "elementList";
    public static final String ELEMENT_VALUE = "elementValue";
    public static final String PARSETYPE = "parseType";

    public static final String AUTHORITY = "Authority";
    public static final String LANGUAGE = "Language";
    public static final String COMPLEX_SUBJECT = "ComplexSubject";

    public static final String RDF_ABOUT = "rdf:about";
    public static final String RDF_RESOURCE = "rdf:resource";
    public static final String MADS_AUTHORITATIVE_LABEL = "mads:authoritativeLabel";
    public static final String XML_LANG = "xml:lang";

    public static final String[] elementsToOverlay = {
            MADS + ":" + AUTHORITATIVE_LABEL,
            MADS + ":" + HAS_EXACT_EXTERNAL_AUTHORITY,
            MADS + ":" + HAS_CLOSE_EXTERNAL_AUTHORITY,
            MADS + ":" + HAS_VARIANT,
            MADS + ":" + HAS_HIDDEN_VARIANT,
            MADS + ":" + ELEMENT_LIST};

    protected String type  = null;
    protected String elementName  = null;
    protected String term = null;
    protected String exactMatch = null;
    protected String closeMatch = null;
    protected String variant = null;
    protected String hiddenVariant = null;
    protected Document document = null;

    public Subject(String id, String type, String term, String elementName, String exactMatch, String closeMatch,
            String variant, String hiddenVariant) {
        super(id, type);
        this.term = term;
        this.elementName = elementName;
        this.exactMatch = exactMatch;
        this.closeMatch = closeMatch;
        this.variant = variant;
        this.hiddenVariant = hiddenVariant;
    }

    public Document getDocument() {
        return document;
    }

    public void setDocument(Document document) {
        this.document = document;
    }

    public String getElementName() {
        return elementName;
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public String getExactMatch() {
        return exactMatch;
    }

    public void setExactMatch(String exactMatch) {
        this.exactMatch = exactMatch;
    }

    public String getCloseMatch() {
        return closeMatch;
    }

    public void setCloseMatch(String exactMatch) {
        this.closeMatch = exactMatch;
    }

    public String getVariant() {
        return variant;
    }

    public void setVariant(String variant) {
        this.variant = variant;
    }

    public String getHiddenVariant() {
        return hiddenVariant;
    }

    public void setHiddenVariant(String hiddenVariant) {
        this.hiddenVariant = hiddenVariant;
    }

    protected Element buildSubject(Namespace typeNS)
    {
        // setup subject
        Element subjectElem = null;
        if (document == null) {
            // Import new subjects
            document = new DocumentFactory().createDocument();
            Element rdf = TabularRecordBasic.createRdfRoot (document);
            subjectElem = rdf.addElement(new QName(getType(), typeNS));
            subjectElem.addAttribute(new QName(ABOUT, rdfNS), getId());
        } else {
            subjectElem = (Element)document.selectSingleNode("//*[@" + RDF_ABOUT + "]");
            subjectElem.setQName(new QName(getType(), typeNS));

            // Clear elements that need to overlay
            clearElements(subjectElem);
        }

        if (StringUtils.isNotBlank( getTerm() )) {
            // mads:authoritativeLabel
            Element el = subjectElem.addElement( new QName(AUTHORITATIVE_LABEL, madsNS));
            setElementText(el, getTerm());

            // exclude mads:Language and mads:ComplexSubject for mads:elementList
            if (!(getType().equalsIgnoreCase(LANGUAGE) || getType().equalsIgnoreCase(COMPLEX_SUBJECT) || getType().equalsIgnoreCase(AUTHORITY))) {
                el = subjectElem.addElement( new QName(ELEMENT_LIST, madsNS) );
                el.addAttribute( new QName(PARSETYPE, rdfNS), COLLECTION );
                el = el.addElement(new QName(getElementName() + ELEMENT, typeNS)).addElement(new QName(ELEMENT_VALUE, madsNS));

                setElementText(el, getTerm());
            }
        }

        // Multi-value field mads:hasExactExternalAuthority
        if ( StringUtils.isNotBlank( getExactMatch() ) )
        {
            for (String val : getValues(getExactMatch())) {
                Element elem = subjectElem.addElement( new QName(HAS_EXACT_EXTERNAL_AUTHORITY, madsNS) );
                elem.addAttribute( new QName(RESOURCE, rdfNS), val );
            }
        }

        // Multi-value field mads:hasCloseExternalAuthority
        if ( StringUtils.isNotBlank( getCloseMatch() ) )
        {
            for (String val : getValues(getCloseMatch())) {
                Element elem = subjectElem.addElement( new QName(HAS_CLOSE_EXTERNAL_AUTHORITY, madsNS) );
                elem.addAttribute( new QName(RESOURCE, rdfNS), val );
            }
        }

        // Multi-value field mads:hasVariant
        if (StringUtils.isNotBlank( getVariant() )) {
            for (String val : getValues(getVariant())) {
                Element el = subjectElem.addElement( new QName(HAS_VARIANT, madsNS))
                    .addElement( new QName(VARIANT, madsNS))
                    .addElement( new QName(VARIANT_LABEL, madsNS));

                setElementText(el, val);
            }
        }

        // Multi-value field mads:hasHiddenVariant
        if (StringUtils.isNotBlank( getHiddenVariant() )) {
            for (String val : getValues(getHiddenVariant())) {
                Element el = subjectElem.addElement( new QName(HAS_HIDDEN_VARIANT, madsNS));

                setElementText(el, val);
            }
        }

        return subjectElem.getDocument().getRootElement();
    }

    /*
     * Clear all elements that need overlay
     */
    private void clearElements(Branch b) {
        for (String elemName : elementsToOverlay) {
            List<Node> nodes = b.selectNodes(elemName);
            for (Node node : nodes) {
                node.detach();
            }
        }
    }

    /*
     * Get values from delimited string value
     * @param val
     */
    private List<String> getValues(String val) {
        List<String> values = new ArrayList<>();
        String[] es = val.split(MULTI_VALUE_DELIMITER);
        for (String e : es ) {
            if (StringUtils.isNotBlank(e)) {
                values.add(e.trim());
            }
        }
        return values;
    }

    /*
     * Set element text value with xml:lang when needed
     * @param elem
     * @param text
     */
    private void setElementText(Element elem, String val) {
    	String elemVal = val;
        if (val.indexOf("\"@") > 0 && val.startsWith("\"")) {
            int idx = val.lastIndexOf("\"@");
            elem.addAttribute(XML_LANG, elemVal.substring(idx + 2));
            elemVal = val.substring(1, idx);
        }

        elem.setText(elemVal);
    }
}
