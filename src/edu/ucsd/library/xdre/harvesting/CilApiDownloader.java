package edu.ucsd.library.xdre.harvesting;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import edu.ucsd.library.xdre.utils.Constants;

/**
 * Class CilApiDownloader to download JSON source and content files from CIL API
 * @author lsitu
 */
public class CilApiDownloader {
    private static Logger log = Logger.getLogger(CilApiDownloader.class);

    public static final String DATE_FORMAT = "yyyy-MM-dd";
    public static final String MONTH_FORMAT = "yyyy-MM";

    public static String CIL_ID_PREFIX = "CIL_";

    private static int MAX_BATCH_SIZE = 500;
    private static String PUBLIC_IDS_PATH = "public_ids?";

    private List<String> cilIds = null;
    private String harvestDirectory = null;
    private String harvestLabel = "";
    private List<String> inventory;


    private CilApiClient cilApiClient = null;

    public CilApiDownloader(CilApiClient cilApiClient, Date dateHarvest, Date lastModified)
            throws Exception {
        this.cilApiClient = cilApiClient;

        this.inventory = new ArrayList<>();

        if (dateHarvest == null)
            dateHarvest = Calendar.getInstance().getTime();

        harvestLabel = createHarvestLabel();

        File harvestDir = new File(Constants.CIL_HARVEST_DIR, CilHarvesting.CIL_HARVEST_FOLDER);

        ensureHarvestDirectories(harvestDir.getAbsolutePath());

        harvestDirectory = harvestDir.getAbsolutePath() + File.separatorChar;

        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
        logMessage("CIL harvest " + harvestDirectory
                + (lastModified == null ? " initiation." : " with lastModifiedDate: " + dateFormat.format(lastModified)));

        cilIds = searchCilIds(lastModified);

        List<String> idsToExclude = searchCilIdsExcluded();

        String excludeMessage = "Total records excluded " + idsToExclude.size() + ": " + String.join(", ", idsToExclude);
        logMessage(excludeMessage);
        log.info(excludeMessage);

        cilIds.removeAll(idsToExclude);

        Collections.sort(cilIds);
    }

    private List<String> searchCilIdsExcluded() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.MONTH, -3);
        calendar.set(Calendar.DATE, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        Date lastModified = calendar.getTime();

        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
        logMessage("CIL harvest excluding records with modified date starting from " + dateFormat.format(lastModified) + " ...");

        return searchCilIds(lastModified);
    }

    /**
     * Download JSON source files and content files
     * @return CIL objects retrieved
     * @throws Exception
     */
    public List<String> download() throws Exception {
        logMessage("CIL JSON source files found: " + cilIds.size());

        List<String> sourceJsonFiles = new ArrayList<>();
        for (String cilId : cilIds) {
            if (!inventory.contains(cilId.replace(CIL_ID_PREFIX, ""))) {
                try {
                    MetadataSource metadataSource = new MetadataSource(cilId, cilApiClient, harvestLabel);

                    logMessage("Downloading object " + cilId + " from " + metadataSource.getUri());

                    String jsonFile = metadataSource.download(harvestDirectory);
                    sourceJsonFiles.add(jsonFile);

                    logMessage("JSON source " + metadataSource.getUri() + " downloaded: " + jsonFile);
                } catch (Exception ex) {
                    String error = "Error in downloading JSON source " + cilId;
                    log.error(error, ex);
                    logMessage(error + ": " + ex.getMessage());
                    throw ex;
                }
            }
        }
        return sourceJsonFiles;
    }

    /**
     * Set inventory for this download
     * @param inventory
     */
    public void setInventory(List<String> inventory) {
        this.inventory = inventory;
    }

    /**
     * Get the root harvest directory for the current download
     * @return
     */
    public String getHarvestDirectory() {
        return harvestDirectory;
    }

    /**
     * Loop through to search CIL IDs by lastModified date
     * @param lastModified
     * @return
     * @throws Exception
     */
    private List<String> searchCilIds(Date lastModified) throws Exception {
        List<String> ids = new ArrayList<>();
        int start = 0;
        long totalFound = getTotalHits(lastModified);

        while (start < totalFound) {
            ids.addAll(getIdBatch(start, MAX_BATCH_SIZE, lastModified));
            start += MAX_BATCH_SIZE;
        }

        return ids;
    }

    /**
     * Retrieve the number of cil object since lastModifiedDate
     * @param damsClient
     * @param modifiedDate
     * @return
     * @throws Exception
     */
    private long getTotalHits(Date lastModified) throws Exception {
        String queryString = buildQueryString(0, 0, lastModified);
        String url = ContentFile.ensureUrlBaseFormat(Constants.CIL_HARVEST_API) + PUBLIC_IDS_PATH + queryString;

        JSONObject result = (JSONObject)JSONValue.parse(cilApiClient.getContentBodyAsString(url));
        return (long)((JSONObject)result.get("hits")).get("total");
    }

    /**
     * Retrieve the CIL IDs in batch
     * @param damsClient
     * @param start
     * @param size
     * @param modifiedDate
     * @return
     * @throws Exception
     */
    private List<String> getIdBatch(int start, int size, Date lastModified) throws Exception {
        List<String> ids = new ArrayList<>();

        String queryString = buildQueryString(start, size, lastModified);
        String url = Constants.CIL_HARVEST_API + PUBLIC_IDS_PATH + queryString;
        JSONObject result = (JSONObject)JSONValue.parse(cilApiClient.getContentBodyAsString(url));
        JSONArray itemsArr = (JSONArray)((JSONObject)result.get("hits")).get("hits");
        for (int i=0; i< itemsArr.size(); i++) {
            ids.add((String) ((JSONObject)itemsArr.get(i)).get("_id"));
        }

        return ids;
    }

    /**
     * Build the query string for CIL API
     * @param start
     * @param size
     * @param lastModified
     * @return
     * @throws UnsupportedEncodingException 
     */
    private String buildQueryString(int start, int size, Date lastModified) throws UnsupportedEncodingException {
        StringBuilder urlBuilder = new StringBuilder();
        urlBuilder.append(URLEncoder.encode("from", "UTF-8"));
        urlBuilder.append("=");
        urlBuilder.append(URLEncoder.encode("" + start, "UTF-8"));
        urlBuilder.append("&");
        urlBuilder.append(URLEncoder.encode("size", "UTF-8"));
        urlBuilder.append("=");
        urlBuilder.append(URLEncoder.encode("" + size, "UTF-8"));
        if (lastModified != null) {
            urlBuilder.append("&");
            urlBuilder.append(URLEncoder.encode("lastModified", "UTF-8"));
            urlBuilder.append("=");
            urlBuilder.append(URLEncoder.encode("" + lastModified.getTime()/1000, "UTF-8"));
        }

        return urlBuilder.toString();
    }

    /**
     * Get cilId list
     * @return
     */
    public List<String> getCilIds() {
        return cilIds;
    }

    /*
     * Create folders for CIL harvest if not exist.
     * @param harvestDir
     */
    private void ensureHarvestDirectories(String harvestDir) throws IOException {
        File harvestRoot = new File(harvestDir);
        if (!harvestRoot.exists()) {
            harvestRoot.mkdirs();
        }

        String[] folders = {CilHarvesting.DOCUMENTS_FOLDER, CilHarvesting.METADATA_SOURCE_FOLDER, CilHarvesting.CONTENT_FILES_FOLDER, CilHarvesting.METADATA_PROCESSED_FOLDER};
        for (String folder : folders) {
            File file = new File(harvestDir, folder);
            if (!file.exists()) {
                file.mkdir();
            }
        }
    }

    /**
     * Get harvest label
     * @return
     */
    public String getHarvestLabel() {
        return harvestLabel;
    }

    /**
     * Append message to the CIL download log
     * @param message
     * @throws IOException
     */
    public void logMessage(String message) throws IOException {
        ContentFile.logMessage(harvestDirectory, harvestLabel, message);
    }

    /**
     * Create label with monthly format for this harvest process.
     * @return
     */
    public static String createHarvestLabel() {
        Date date = Calendar.getInstance().getTime();

        SimpleDateFormat dateFormat = new SimpleDateFormat(MONTH_FORMAT);
        return dateFormat.format(date);
    }
}
