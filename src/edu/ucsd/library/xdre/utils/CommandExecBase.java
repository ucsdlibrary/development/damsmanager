package edu.ucsd.library.xdre.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

/**
 * Utility class to execute a command.
 * @author lsitu
 **/
public class CommandExecBase {
    private static Logger log = Logger.getLogger(CommandExecBase.class);

    protected String command = null;

    /**
     * Constructor
     * @param command Full path to the locally-installed command/tool
    **/
    public CommandExecBase( String command ) {
        this.command = command;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    /**
     * Execute a command and output the result if there's any
     * @param cmd
     * @return
     * @throws Exception
     */
    public String exec(List<String> cmd) throws Exception {
        log.info("Executing command: " + String.join(" ", cmd));

        Process proc = null;
        StringBuffer log = new StringBuffer();

        // Execute the command
        ProcessBuilder pb = new ProcessBuilder(cmd);
        pb.redirectErrorStream(true);
        proc = pb.start();

        try ( InputStream in = proc.getInputStream();
                Reader reader = new InputStreamReader(in);
                BufferedReader buf = new BufferedReader(reader); ) {

            for ( String line = null; (line=buf.readLine()) != null; )
                log.append( line + "\n" );

            // Wait for the process to exit
            int status = proc.waitFor();

            if ( status == 0 ) {
                return log.toString().trim();
            } else {
                // Output error messages
                try ( InputStream errIn = proc.getErrorStream();
                        Reader errReader = new InputStreamReader(errIn);
                        BufferedReader errBuf = new BufferedReader(errReader); ) {

                    for ( String line = null; (line=errBuf.readLine()) != null; )
                        log.append( line + "\n" );

                    throw new Exception("Error execute command " + String.join(" ", cmd) + "."
                            + " Exit code " + status + ":\n" + log.toString() );
                }
            }
        } catch ( Exception ex ) {
            throw new Exception( log.toString(), ex );
        } finally {
            if(proc != null){
                proc.destroy();
                proc = null;
            }
        }
    }

    /**
     * Create file in dams filestore
     * @param oid
     * @param cid
     * @param fid
     * @return
     */
    public File createArkFile(String oid, String cid, String fid) {
        // Local file support only
        String ark = DAMSClient.stripID(oid);
        String compId = StringUtils.isBlank(cid) ? "0-" : cid + "-";
        String fsDir = Constants.FILESTORE_DIR + "/" + DAMSClient.pairPath(ark);
        String fName = Constants.ARK_ORG + "-" + ark + "-" + compId + fid;

        return new File(fsDir, fName);
    }
}
