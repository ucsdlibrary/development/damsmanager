package edu.ucsd.library.xdre.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

/**
 * Utility class to create audio/video derivatives with FFMPEG.
 * @author lsitu
 **/
public class FFMPEGConverter extends CommandExecBase
{
	/**
	 * Construct a default FFMPEGConverter object with command ffmpeg.
	 * 
	**/
	public FFMPEGConverter() {
		this( "ffmpeg");
	}
	
	/**
	 * Constructor for FFMPEGConverter object.
	 * @param command Full path to the locally-installed ffmpeg
	**/
	public FFMPEGConverter( String command ) {
		super(command);
	}

	/**
	 * method to embed metadata
	 * @param oid
	 * @param cid
	 * @param mfid
	 * @param dfid
	 * @param params
	 * @param metadata
	 * @return
	 * @throws Exception
	 */
	public File metadataEmbed(String oid, String cid, String mfid, String dfid, String params,
			Map<String, String> metadata) throws Exception {
		return createDerivative(oid, cid, mfid, dfid, params, metadata);
	}

	/**
	 * Create derivative
	 * @param oid
	 * @param cid
	 * @param mfid
	 * @param dfid
	 * @param params
	 * @return
	 * @throws Exception
	 */
	public File createDerivative(String oid, String cid, String mfid, String dfid, String params) throws Exception {
		return createDerivative(oid, cid, mfid, dfid, params);
	}

	/**
	 * Create derivative with embedded metadata
	 * @param oid
	 * @param cid
	 * @param mfid
	 * @param dfid
	 * @param params
	 * @param metadata
	 * @return
	 * @throws Exception
	 */
	public File createDerivative(String oid, String cid, String mfid, String dfid, String params,
			Map<String, String> metadata) throws Exception {
		File src = createArkFile(oid, cid, mfid);
		File dst =  createArkFile(oid, cid, dfid);

		if( !src.exists() ) {
			// XXX Implementation to retrieve master file to local disk???
			throw new Exception ("Master file " + src.getPath() + " doesn't exists.");
		}

		// Create the directory in dams staging to hold the temporary files created by ffmpeg for ingest
		File tmpDir = new File(Constants.DAMS_STAGING + "/darry/ffmpeg");
		if(!tmpDir.exists()){
			if(new File(Constants.DAMS_STAGING + "/darry").exists()) {
				tmpDir.mkdir();
			} else {
				tmpDir.mkdirs();
			}
		}

		dst = File.createTempFile("ffmpeg_tmp", oid+"-"+dst.getName(), tmpDir);
		boolean succssful = createDerivative( src, dst, params, metadata );
		if ( !succssful ) {
			if(dst != null && dst.exists()){
				// Cleanup temp files
				try {
					dst.delete();
				} catch ( Exception e ) {
					e.printStackTrace();
				}
				dst = null;
			}
		}
		return dst;
	}

	/**
	 * Create derivative
	 * @param src
	 * @param dst
	 * @param params
	 * @return
	 * @throws Exception
	 */
	public boolean createDerivative( File src, File dst, String params ) throws Exception{
		return createDerivative( src, dst, params );
	}

	/**
	 * Create derivative
	 * @param src
	 * @param dst
	 * @param params
	 * @return metadata
	 * @return
	 * @throws Exception
	 */
	public boolean createDerivative( File src, File dst, String params,
			Map<String, String> metadata ) throws Exception{
		// Build the ffmpeg command to create derivative
		List<String> cmd = new ArrayList<>();
		cmd.add( command );
		cmd.add( "-i" );
		cmd.add( src.getAbsolutePath());
		if (StringUtils.isNotBlank(params))
			cmd.addAll(Arrays.asList(params.split(" ")));

		if (metadata != null) {
			for (String fieldName : metadata.keySet()) {
				cmd.add("-metadata");
				cmd.add(fieldName + "=" + metadata.get(fieldName));
			}
		}
			
		cmd.add( dst.getAbsolutePath() );

		exec(cmd);
		return dst.exists();
	}
}
