package edu.ucsd.library.xdre.utils;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.UUID;

import javax.servlet.http.HttpSession;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import edu.ucsd.library.xdre.utils.Submission.Status;

/**
 * Class RequestOrganizer, a utility class for operation request
 *
 * @author lsitu@ucsd.edu
 */
public class RequestOrganizer {
	public static final int MAX_TASKS = 500;
	public static final String PROGRESS_PERCENTAGE_ATTRIBUTE = "progressPercentage";
	private static Map<String, Submission> TaskMap = new TreeMap<String, Submission>();
	private static List<String> ignoreParams = Arrays.asList(
			"progressId", "progress", "sid", "formId","activeButton", "message");

	public static Thread getReferenceServlet(String processId){
		Thread worker = null;
		Submission submission = getSubmission(processId);
		if(submission != null)
			worker = submission.getWorker();
		return worker;
	}
	
	public static Submission getSubmission(String processId){
		return TaskMap.get(processId);
	}

	/**
	 * Add submission to the tasks list
	 * @param submission
	 */
	public synchronized static void addSubmission(Submission submission) {
		TaskMap.put(submission.getSubmissionId(), submission);
	}

	public static Submission setReferenceServlet(String user, String processId,
			Map<String, String[]> params, Thread servlet) throws Exception{
		synchronized(TaskMap) {
			// find duplicate submission that is active with the same set of parameters
			Submission submission = findSubmission(params);
			if(submission != null) {
				String error = "WARNING: Ingest request ignored! Another process with ID " 
						+ submission.getSubmissionId() + " submitted by user " + submission.getOwner()
						+ " is already running:\n" + printParams(params);
				throw new Exception(error);
			}

			// remove processes that are older than a month
			cleanupTasks();

			submission = new Submission(servlet, processId, user, params);
			submission.setStatus(Status.progressing);
			TaskMap.put(processId, submission);

			return submission;
		}
	}

	/*
	 * Print parameters in JSON format
	 * @param params
	 * @return
	 */
	private static String printParams(Map<String, String[]> params) {
		JSONObject json = new JSONObject();
		for (String key : params.keySet()) {
			String value = "";
			JSONArray vals = new JSONArray();
			for (String v : params.get(key)) {
				vals.add(v);
			}
			json.put(key, vals);
		}

		return json.toJSONString();
	}

	public static void clearSession(HttpSession session){
		synchronized (session){
			try{
				if(session.getAttribute("status") != null)
			        session.removeAttribute("status");
			    if(session.getAttribute("result") != null)
			    	session.removeAttribute("result");
			    if(session.getAttribute("log") != null)
			    	session.removeAttribute("log");
			}catch(IllegalStateException e){
				//e.printStackTrace();
			}
		}
	}
	
	public static boolean addLogMessage (String processId, String message){
		if (TaskMap.containsKey(processId)) {
			getSubmission(processId).logMessage(message);
			return true;
		}
		return false;
	  }

	public static String getLogMessage (String processId){
		if (TaskMap.containsKey(processId)) {
			return getSubmission(processId).getLog().toString();
		}
		return "";
	 }
	
	public static boolean addResultMessage (String processId, String message){
		Submission submission = getSubmission(processId);
		if(submission != null){
			submission.setStatus(Status.done);
			submission.setMessage(message);
			submission.setEndDate(Calendar.getInstance().getTime());
			return true;
		}
		return false;
	 }

	public static String getResultMessage (String processId){
		if (TaskMap.containsKey(processId)) {
			return getSubmission(processId).getResultMessage();
		}
		return "";
	 }
	
	public static String getProgressPercentage(String processId){
		if (TaskMap.containsKey(processId)) {
			return "" + getSubmission(processId).getPercentage();
		}
		return "0";
	}
	
	public static void setProgressPercentage(String processId, int percent) {
		if (TaskMap.containsKey(processId)) {
			getSubmission(processId).setPercentage(percent);
		}
	}

	/**
	 * Create a unique process ID
	 * @param user
	 * @return
	 */
	public static String getProcessId(String user) {
		return user + "_" + UUID.randomUUID().toString();
	}

	/**
	 * Check whether an active process with the same set of parameters exists to avoid duplicate submission.
	 * @param user
	 * @param params
	 * @return
	 */
	public static Submission findSubmission(Map<String, String[]> params) {
		// cleanup application specific parameters
		cleanupParams(params);

		for (String key : TaskMap.keySet()) {
			Submission submission = TaskMap.get(key);
			Thread t = submission.getWorker();
			Map<String, String[]> p = submission.getParams();

			if (p.size() > 0 && p.size() == params.size() && submission.getStatus() != null
					&& submission.getStatus().equals(Status.progressing)
					&& t != null && t.isAlive() && !t.isInterrupted()) {
				for (String k : p.keySet()) {
					if (!ignoreParams.contains(k) && (!params.containsKey(k) || !paramEquals(p.get(k), params.get(k)))) {
						return null;
					}
				}
				return submission;
			}
		}
		return null;
	}

	/*
	 * Remove application specific parameters that are added
	 * @param params
	 */
	private static void cleanupParams(Map<String, String[]> params) {
		for (String key : ignoreParams) {
			if (params.containsKey(key)) {
				params.remove(key);
			}
		}
	}

	/*
	 * compare equivalence of two parameter value arrays
	 * @param a
	 * @param b
	 * @return
	 */
	private static boolean paramEquals(String[] a, String[] b) {
		if (a == b || a != null && b != null && a.length == b.length) {
			for (String val : a) {
				boolean valMatch = false;
				for (String v : b) {
					if (val == v || val != null && v != null && val.equals(v)) {
						valMatch = true;
						break;
					}
				}

				if (!valMatch) {
					return false;
				}
			}
			return true;
		}

		return false;
	}

	/*
	 * Keep process history for a month only.
	 */
	private static void cleanupTasks() {
		if (TaskMap.size() > MAX_TASKS) {
			for (String key : TaskMap.keySet()) {
				Submission submission = TaskMap.get(key);

				Date endDate = submission.getEndDate();
				Calendar cal = Calendar.getInstance();
				cal.add(Calendar.MONTH, -1);
	
				if (endDate.before(cal.getTime()) && (submission.getStatus() != Status.progressing)) {
					TaskMap.remove(key);
				}
			}
		}
	}
}
