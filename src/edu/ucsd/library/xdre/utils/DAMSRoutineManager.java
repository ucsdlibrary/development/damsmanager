package edu.ucsd.library.xdre.utils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import edu.ucsd.library.xdre.ingest.fileReplace.RdcpFileReplaceRunner;
import edu.ucsd.library.xdre.statistic.analyzer.Statistics;
import edu.ucsd.library.xdre.web.CILHarvestingTaskController;
import edu.ucsd.library.xdre.web.StatsCollectionsReportController;
import edu.ucsd.library.xdre.web.StatsQuantityAnalyzerController;
import edu.ucsd.library.xdre.web.StatsRdcpDownloadController;
import edu.ucsd.library.xdre.web.StatsRdcpUsageController;
import edu.ucsd.library.xdre.web.StatsWeblogAnalyzerController;

public class DAMSRoutineManager{
	private static Logger logger = Logger.getLogger(DAMSRoutineManager.class);
	private static DAMSRoutineManager worker = null;
	private static int SCHEDULED_HOUR = 1;

	private Date cilHarvestingStartedTime = null;

	private DAMSRoutineManager(){}
	
	public void start(){
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, 1);
		calendar.set(Calendar.HOUR_OF_DAY, SCHEDULED_HOUR);
		calendar.set(Calendar.MINUTE, 50);
		calendar.set(Calendar.SECOND, 0);
		Date scheduledTime = calendar.getTime();
		Timer timer = new Timer();
		timer.scheduleAtFixedRate(new DamsRoutine(), scheduledTime, 1000*60*60*24);

		// Nightly RDCP file replacement process
		calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, 1);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 1);
		calendar.set(Calendar.SECOND, 0);
		scheduledTime = calendar.getTime();
		timer.scheduleAtFixedRate(new RdcpFileReplaceRunner(), scheduledTime, 1000*60*60*24);

		//Caching collection report
		calendar = Calendar.getInstance();
		calendar.add(Calendar.MINUTE, 2);
		timer = new Timer();
		timer.schedule(new StatisticsTask(), calendar.getTime());
	}
	
	class StatisticsTask extends TimerTask{

		public void run() {
			// DAMS collection reports
			try{
				logger.info("DAMS Mananger generating collections report ... ");
				StatsCollectionsReportController.generateReport();
				logger.info("Collections report generated on " + Statistics.getDatabaseDateFormater().format(Calendar.getInstance().getTime()) + ".");
			}catch(Exception e){
				e.printStackTrace();
				logger.error("Failed to generate collections report on " + Statistics.getDatabaseDateFormater().format(Calendar.getInstance().getTime()) + ".");
			}

			// Cache stats RDCP usage
			cacheStatsRdcpUsage();

			// Cache stats RDCP file download
			cacheStatsRdcpDownload();
		}
		
	}

	class DamsRoutine extends TimerTask{
		
		public DamsRoutine(){}
		
		public void run(){

			int dateField = Calendar.HOUR_OF_DAY;
			long logAvailableTime = 30 * 24 * 60 * 60 * 10000;
			
			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());
			int hour = cal.get(dateField);
			logger.info("DAMS Manager scheduled task running at " + hour + " o'clock ... " );

			//Cleanup temp files
			File tmpDirFile = new File(Constants.TMP_FILE_DIR);
			if(tmpDirFile.exists()){
				File[] tmpFiles = tmpDirFile.listFiles();
				String fName = null;
				File tmp = null;
				for(int i=0; i<tmpFiles.length; i++){
					tmp = tmpFiles[i];
					if(tmp.isFile() && !tmp.isHidden()){
						fName = tmp.getName();
						if(fName.startsWith("tmpRdf_") || fName.startsWith("tmp_")){
							if((System.currentTimeMillis() - tmp.lastModified()) > logAvailableTime)
								tmp.delete();
						}else if(fName.startsWith("damslog-") || fName.startsWith("_tmp_damslog-") || fName.startsWith("_tmp_export-") || fName.startsWith("export-")){
							if((System.currentTimeMillis() - tmp.lastModified()) > logAvailableTime)
								tmp.delete();
						}
					}
				}
			}else
				logger.error("Error: directory " +  tmpDirFile.getAbsolutePath() + " doesn't exist.");
			
			Calendar sCal = Calendar.getInstance();
			sCal.add(Calendar.DATE, -1);
			Calendar eCal = Calendar.getInstance();
			SimpleDateFormat dFormat = Statistics.getDatabaseDateFormater();
			try{	
				StatsWeblogAnalyzerController.analyzeWeblog(sCal.getTime(), eCal.getTime(), false);
				logger.info("DAMS Statistics Weblog analyzed for period " + dFormat.format(sCal.getTime()) + " to " + dFormat.format(eCal.getTime()) + ".");
			}catch(Exception e){
				e.printStackTrace();
				logger.error(" DAMS Statistics failed to analyze Weblog for period " + dFormat.format(sCal.getTime()) + " to " + dFormat.format(eCal.getTime()) + ".");
			}

			// DAMS statistics quantity analyzing
			try{
				StatsQuantityAnalyzerController.statsAnalyze(cal.getTime(), true, null);
				logger.info("DAMS quantity statistics generated on " +  Statistics.getDatabaseDateFormater().format(cal.getTime()) + ".");
			}catch(Exception e){
				e.printStackTrace();
				logger.error("Failed to generate DAMS quantity statistics on " +  Statistics.getDatabaseDateFormater().format(cal.getTime()) + ".");
			}

			// DAMS collection reports
			try{
				StatsCollectionsReportController.generateReport();
				logger.info("Collections report generated on " +  Statistics.getDatabaseDateFormater().format(cal.getTime()) + ".");
			}catch(Exception e){
				e.printStackTrace();
				logger.error("Failed to generate collections report on " +  Statistics.getDatabaseDateFormater().format(cal.getTime()) + ".");
			}

			// CIL harvest (quarterly)
			if(cal.get(Calendar.DAY_OF_MONTH) == 1){
				// Perform CIL harvest only on Jan 1, April 1, July 1, and Oct 1 of each year
				int quarterRemainder = Calendar.getInstance().get(Calendar.MONTH) % 3;
				logger.info("Month index: " + Calendar.getInstance().get(Calendar.MONTH) + "; Quarter remainder (%3): " + quarterRemainder + "; CIL Harvest Enabled: " + Constants.CIL_HARVEST_ENABLED + ".");
				if (quarterRemainder == 0 && StringUtils.isNotBlank(Constants.CIL_HARVEST_ENABLED) && Boolean.parseBoolean(Constants.CIL_HARVEST_ENABLED.trim())) {
					try {
						cilHarvestingStartedTime = Calendar.getInstance().getTime();
						logger.info("DAMS Mananger start CIL Harvesting task on " +  Statistics.getDatabaseDateFormater().format(cilHarvestingStartedTime) + "...");
		
						CILHarvestingTaskController.performHarvestingTask(cilHarvestingStartedTime, null);
		
						logger.info("DAMS Mananger exits CIL Harvesting task at " + Statistics.getDatabaseDateFormater().format(Calendar.getInstance().getTime()) + ".");
					} catch(Exception e) {
						e.printStackTrace();
						logger.error("CIL Harvesting process failed: " + Statistics.getDatabaseDateFormater().format(Calendar.getInstance().getTime()) + ".");
					}
				}

			}

			// Nightly refresh stats RDCP usage
			cacheStatsRdcpUsage();

			// Nightly refresh stats RDCP file download
			cacheStatsRdcpDownload();

			// clear authority records cache for nightly refreshing
			try {
				DAMSRepository.getRepository().clearCache();
			} catch (Exception e) {
				e.printStackTrace();
				logger.error("Failed to clear authority cache on " +  dFormat.format(cal.getTime()) + ".");
			}
		}
	}

	/*
	 * Cache stats RDCP usage
	 */
	private void cacheStatsRdcpUsage() {
		try {
			logger.info("DAMS Mananger generating statatistics for RDCP usage ... ");
			StatsRdcpUsageController.STATS_ITEMS_SUMMARIES = StatsRdcpUsageController.buildStatsSummaries(null, null, true);
			logger.info("Stats RDCP usage generated on " +  Statistics.getDatabaseDateFormater().format(Calendar.getInstance().getTime()) + ".");
		} catch(Exception e) {
			e.printStackTrace();
			logger.error("Failed to generate stats RDCP usage on " +  Statistics.getDatabaseDateFormater().format(Calendar.getInstance().getTime()) + ".");
		}
	}

	/*
	 * Cache stats RDCP download
	 */
	private void cacheStatsRdcpDownload() {
		try {
			logger.info("DAMS Mananger generating statatistics for RDCP download ... ");
			StatsRdcpDownloadController.STATS_DOWNLOAD_SUMMARIES = StatsRdcpDownloadController.buildStatsDownloadSummaries(null, null, true);
			logger.info("Stats RDCP download generated on " +  Statistics.getDatabaseDateFormater().format(Calendar.getInstance().getTime()) + ".");
		} catch(Exception e) {
			e.printStackTrace();
			logger.error("Failed to generate stats RDCP download on " +  Statistics.getDatabaseDateFormater().format(Calendar.getInstance().getTime()) + ".");
		}
	}

	public static synchronized boolean startRoutine(){
		worker = new DAMSRoutineManager();
		worker.start();
		return true;
	}
}
