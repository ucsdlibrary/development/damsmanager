package edu.ucsd.library.xdre.utils;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;

/**
 * A submitted request for processing
 * @author lsitu
 *
 */
public class Submission {
		public static enum Status {done, progressing, error, interrupted};
		private Thread worker = null; //Servlet, worker thread
		private String submissionId = null; //Id of the submission
		private String owner = null; //Owner of the submission
		private Status status = null; //Status of the submission
		private Date submittedDate = null; //Time submitted
		private Date startDate = null; //Time the process started.
		private Date endDate = null; //Time the process ended
		private String requestInfo = null;
		private String message = null;
		private String statusMessage = null;
		private String resultMessage = null;
		private int percentage = 0;
		private Map<String, String[]> params = null;
		private StringBuilder log = new StringBuilder();

		public Submission (Thread worker, String submissionId, String owner, Map<String, String[]> params) {
			this.worker = worker;
			this.submissionId = submissionId;
			this.owner = owner;
			this.params = params;
			this.startDate = Calendar.getInstance().getTime();
		}

		public Thread getWorker() {
			return worker;
		}

		public void setWorker(Thread worker) {
			this.worker = worker;
		}

		public String getSubmissionId() {
			return submissionId;
		}

		public void setSubmissionId(String submissionId) {
			this.submissionId = submissionId;
		}

		public String getOwner() {
			return owner;
		}

		public void setOwner(String owner) {
			this.owner = owner;
		}

		public Status getStatus() {
			return status;
		}

		public void setStatus(Status status) {
			this.status = status;
			if (status != Status.progressing) {
				endDate = Calendar.getInstance().getTime();
			}
		}

		public Date getSubmittedDate() {
			return submittedDate;
		}

		public void setSubmittedDate(Date submittedDate) {
			this.submittedDate = submittedDate;
		}

		public Date getStartDate() {
			return startDate;
		}

		public void setStartDate(Date startDate) {
			this.startDate = startDate;
		}

		public Date getEndDate() {
			return endDate;
		}

		public void setEndDate(Date endDate) {
			this.endDate = endDate;
		}

		public String getRequestInfo() {
			return requestInfo;
		}

		public void setRequestInfo(String requestInfo) {
			this.requestInfo = requestInfo;
		}

		public String getMessage() {
			return message;
		}

		public void setMessage(String message) {
			this.message = message;
		}

		public String getStatusMessage() {
			return statusMessage;
		}

		public void setStatusMessage(String statusMessage) {
			this.statusMessage = statusMessage;
		}

		public String getResultMessage() {
			return resultMessage;
		}

		public void setResultMessage(String resultMessage) {
			this.resultMessage = resultMessage;
		}

		public int getPercentage() {
			return percentage;
		}

		public void setPercentage(int percentage) {
			this.percentage = percentage;
		}

		public Map<String, String[]> getParams() {
			return params;
		}

		public void setParams(Map<String, String[]> params) {
			this.params = params;
		}

		public StringBuilder getLog() {
			return log;
		}

		public void logMessage(String message) {
			statusMessage = message;
			log.append(message);
		}
}
