package edu.ucsd.library.xdre.utils;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.lang3.StringUtils;

/**
 * Utility class to handle ARK files
 * @author lsitu
 *
 */
public class ArkFile extends DamsURI {

    private String fileStoreDir;

    public ArkFile(String object, String component, String fileName) {
        this(Constants.FILESTORE_DIR, object, component, fileName);
    }

    /**
     * Constructor
     * @param fileStoreDir
     * @param object
     * @param component
     * @param fileName
     */
    public ArkFile(String fileStoreDir, String object, String component, String fileName) {
        super(object, component, fileName);
        this.fileStoreDir = fileStoreDir;
    }

    public String getFileStoreDir() {
        return fileStoreDir;
    }

    public void setFileStoreDir(String fileStoreDir) {
        this.fileStoreDir = fileStoreDir;
    }

    /**
     * Get the Path of the the ark file in local filestore
     * @return
     */
    public Path getLocalStoreFile() {
        String ark = object.substring(object.lastIndexOf("/") + 1);

        String arkFileName = getDams4FileName(ark, component, fileName);
        return Paths.get(Constants.FILESTORE_DIR, DAMSClient.pairPath(ark), arkFileName );
    }

    /**
     * parse ark file URI to an ArkFile instance
     * @param fileUri
     * @return
     * @throws Exception
     */
    public static ArkFile fromArkFileUri(String fileUri) throws Exception {
        DamsURI damsURI = DamsURI.toParts(fileUri, DamsURI.FILE);

        return new ArkFile(damsURI.getObject(), damsURI.getComponent(), damsURI.getFileName());
    }

    /**
     * Convert ark file URI to ark file in local filestore
     * @param fileUri
     * @return
     * @throws Exception
     */
    public static Path getLocalStoreFile(String fileUri) throws Exception {
        ArkFile arkFile = fromArkFileUri(fileUri);

        return arkFile.getLocalStoreFile();
    }

    /**
     * Get local filestore ark filename
     * @param oid
     * @param cid
     * @param fid
     * @return
     */
    public static String getDams4FileName(String oid, String cid, String fid) {
        String compIndex = StringUtils.isNotBlank(cid) ? cid : "0";
        String ark = oid.substring(oid.lastIndexOf("/") + 1);

        return Constants.ARK_ORG + "-" + ark + "-" + compIndex + "-" + fid;
    }

	/*
	 * Get ARK URL in DAMS
	 * @param ark
	 * @return
	 */
	public static String getArkUrl(String ark) {
		return getArkUrl(ark, "", "");
	}

    /*
     * Get ARK URL in DAMS
     * @param objectArk
     * @param cid
     * @param fid
     * @return
     */
    public static String getArkUrl(String objectArk, String cid, String fid) {
        String arkUrlBase = Constants.DAMS_ARK_URL_BASE
                + (Constants.DAMS_ARK_URL_BASE.endsWith("/") ? "" : "/");
        String ark = objectArk.substring(objectArk.lastIndexOf("/") + 1);
        String componentPart = (StringUtils.isNotBlank(cid) ? "/" + cid : "");
        String filePart = StringUtils.isNotBlank(fid) ? "/" + fid : "";

        return arkUrlBase  + Constants.ARK_ORG + "/" + ark + componentPart + filePart;
    }
}
