package edu.ucsd.library.xdre.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.CellRangeAddress;

/**
 * ExcelReader that uses Apache POI to read Excel files.
 * @author lsitu
**/
public class ExcelReader
{
    protected int lastRow;
    protected int currRow;

    private Workbook book;
    private Sheet sheet;
    private List<String> headers;

    private Map<Integer, Map<String, String>> data;

    /**
     * Create Excel reader with Excel file on disk.
     * @throws Exception 
    **/
    public ExcelReader( File f ) throws Exception
    {
        try(InputStream in = new FileInputStream(f)) {
            init(in);
        }
    }

    /**
     * Create Excel reader with InputStream
     * @throws Exception 
    **/
    public ExcelReader( InputStream in )
        throws Exception
    {
        init(in);
    }

    private void init(InputStream in) throws Exception {
        data = new TreeMap<>();

        this.book = WorkbookFactory.create(in);

        // use the the first sheet
        this.sheet = book.getSheetAt(0);

        this.lastRow = sheet.getLastRowNum();
        if ( this.lastRow == 0 ) {
            this.lastRow = sheet.getPhysicalNumberOfRows() - 1;
        }

        // parse headers
        if ( lastRow > 0 ) {
            headers = new ArrayList<>();
            Row firstRow = sheet.getRow(currRow);

            if (firstRow.getLastCellNum() == 1) {
                // skip terminology row to the next headings row
                firstRow = sheet.getRow(++currRow);
            }

            for ( int i = 0; i < firstRow.getLastCellNum(); i++ ) {
                String header = firstRow.getCell(i).getStringCellValue();
                headers.add( header );
            }
        }

        while ( currRow++ < lastRow ) {
            data.put(currRow, parseRow( currRow ));
        }
    }

    /**
     * Parse a row of data and return it as a Map
     * @param n
    **/
    protected Map<String,String> parseRow( int n )
    {
        Row row = sheet.getRow(n);
        Map<String,String> values = new HashMap<>();

        if (row != null)  {
            for ( int i = 0; i < headers.size(); i++ ) {
                String header = headers.get(i);

                String value = null;
                if ( i < (row.getLastCellNum() + 1) ) {
                    Cell cell = row.getCell(i);
                    if ( cell != null ) {
                        value = getCellValue(cell, n, i);
                    }
                }

                if ( value != null && !value.trim().equals("") ) {
                    try {
                        value = new String(value.trim().getBytes("UTF-8"));
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }

                values.put(header, value);
            }
        }

        return values;
    }

    public List<String> getHeaders() {
		return headers;
	}

	public void setHeaders(List<String> headers) {
		this.headers = headers;
	}

	public Map<Integer, Map<String, String>> getData() {
		return data;
	}

	public void setData(Map<Integer, Map<String, String>> data) {
		this.data = data;
	}

	/*
     * Retrieve cell value from merged cells if exists
     * @param cell
     * @param rowNum
     * @param colNum
     * @return
     */
    private String getCellValue(Cell cell, int rowNum, int colNum) {

        // handle merged cell
        for (int i = 0; i < sheet.getNumMergedRegions(); i++) {
            CellRangeAddress region = sheet.getMergedRegion(i);

            int frIndex = region.getFirstRow();
            int fcIndex = region.getFirstColumn();

            if ( rowNum >= frIndex && rowNum <= region.getLastRow() && colNum >= fcIndex && colNum <= region.getLastColumn()) {
                // retrieve merged cell value
                cell = sheet.getRow(frIndex).getCell(fcIndex);
                break;
            }
        }

        cell.setCellType(Cell.CELL_TYPE_STRING);
        return cell.toString();
    }
}
