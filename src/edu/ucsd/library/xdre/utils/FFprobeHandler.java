package edu.ucsd.library.xdre.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Utility class for FFprobe command execution.
 * @author lsitu
 **/
public class FFprobeHandler extends CommandExecBase {

    /**
     * default constructor
     * 
    **/
    public FFprobeHandler() {
        this( "ffprobe");
    }

    /**
     * Constructor
     * @param command Full path to the locally-installed ffprobe
    **/
    public FFprobeHandler( String command ) {
        super(command);
    }

    /**
     * Process to get the duration video thumbnail creation
     * @param oid
     * @param cid
     * @param fid
     * @param params
     * @return
     * @throws Exception
     */
    public String getDuration( String oid, String cid, String fid, String params )
            throws Exception{

        File src = createArkFile(oid, cid, fid);

        List<String> cmd = new ArrayList<>();

        cmd.add(command);
        cmd.addAll(Arrays.asList(params.split(" ")));

        cmd.add(src.getAbsolutePath());

        return exec(cmd);
    }
}