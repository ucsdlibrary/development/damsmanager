package edu.ucsd.library.xdre.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.naming.InitialContext;
import javax.naming.NameNotFoundException;
import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.commons.lang3.StringUtils;

/**
 * Constants used by the DAMS Manager
 * 
 * @author Longshou Situ (lsitu@ucsd.edu)
 */
public class Constants {
	public static final String SERVICE = "service";
	public static final String SOURCE = "source";
	public static final String ALTERNATE = "alternate";
	public static final String IMAGE = "image";

	public static final String IMPORT_MODE_ADD = "add";
	public static final String IMPORT_MODE_ALL = "all";
	public static final String IMPORT_MODE_SAMEPREDICATES = "samePredicates";
	public static final String IMPORT_MODE_DESCRIPTIVE = "descriptive";
	public static final String IMPORT_MODE_DELETE = "delete";

	public static final int TIMEOUT = 30000;

	public static String DEFAULT_ARK_NAME = "";
	public static String ARK_ORG = "";
	public static String DAMS_ARK_URL_BASE = "";
	public static String DAMS_CLR_URL_BASE = "";
	public static String DAMS_CLR_IMG_DIR = "";
	public static String DAMS_CLR_THUMBNAILS_DIR = "";
	public static String DAMS_CLR_SOURCE_DIR = "";
	
	public static String DAMS_STORAGE_URL = "";
	public static String DAMS_STORAGE_USER = "";
	public static String DAMS_STORAGE_PWD = "";
	
	public static String CLUSTER_HOST_NAME = "";
	public static String TMP_FILE_DIR = "";
	public static String DAMS_STAGING = "";
	public static String SOLR_URL_BASE = "";
	public static String DEFAULT_TRIPLESTORE = "";
	public static String DEFAULT_DERIVATIVES = "";
	
	public static String MAILSENDER_DAMSSUPPORT = "";
	
	public static String FILESTORE_DIR = "";
	public static String ZOOMIFY_BASE_DIR = "";
	public static String DERIVATIVES_LIST = "";
	public static String VIDEO_SIZE = "";
	public static String FFMPEG_COMMAND = "";
	public static String FFMPEG_VIDEO_PARAMS = "";
	public static String FFMPEG_AUDIO_PARAMS = "";
	public static String FFPROBE_COMMAND = "";
	public static String FFPROBE_PARAMS_DURATION = "";
	public static String EXIFTOOL_COMMAND = "exiftool";
	public static String IMAGEMAGICK_COMMAND = "convert";
	public static String IMAGEMAGICK_PARAMS = "";
	public static String COLLECTION_THUMBNAILS_SIZE = "150x150";
	public static String COLLECTION_IMAGE_SIZE = "1024x1024";

	public static String ZOOMIFY_COMMAND = "";

	public static String WATERMARK_COMMAND = "";
	//path to the image watermark file used for watermarking
	public static String WATERMARK_IMAGE = "";
	//derivative name/use pairs for watermarking
	public static Map<String, String> WATERMARKED_DERIVATIVES = null;

	public static String BATCH_ADDITIONAL_FIELDS = "";

	// Related resource type and displayLabel mapping
	public static Map<String, String> RELATED_RESOURCE_LABEL_MAPPINGS = new HashMap<>();

	//Alma API URL and key
	public static String ALMA_API_BIB_URL = "";
	public static String ALMA_API_KEY = "";

	/* begin stats declaration*/
	public static String CURATOR_ROLE ="";
	public static DataSource DAMS_DATA_SOURCE = null;
	public static String STATS_WEBLOG_DIR = "";
	public static String STATS_SE_PATTERNS = "";
	public static String STATS_SE_DATA_LOCATION = "";
	public static String STATS_IP_FILTER = "";
	public static String STATS_QUERY_IP_FILTER = "";
	/* end stats declaration*/
	
	//Namespace prefix
	public static String NS_PREFIX = "";
	public static Map<String, String> NS_PREFIX_MAP = null;

	// ffmpeg parameters for embedded metadata
	public static Map<String, String> FFMPEG_EMBED_PARAMS = new HashMap<>();

	// CIL harvesting enabled
	public static String CIL_HARVEST_ENABLED = "";
	// CIL harvesting base directory
	public static String CIL_HARVEST_DIR = "";
	// Directory for CIL inventory backup
	public static String CIL_BACKUP_DIR = "";
	// CIL metadata mapping instructions (Excel file)
	public static String CIL_HARVEST_MAPPING_FILE = "";
	// CIL harvesting emails for notification
	public static String CIL_HARVEST_NOTIFY_EMAILS = "";
	// CIL REST API URL base
	public static String CIL_HARVEST_API = "";
	// CIL REST API user
	public static String CIL_HARVEST_API_USER = "";
	// CIL REST API password
	public static String CIL_HARVEST_API_PWD = "";
	// CIL content URL base
	public static String CIL_CONTENT_URL_BASE = "";

	//Location for DAMS data migration CSV and content files export
	public static String DAMS_DATA_MIGRATION_EXPORTS_DIR = "";
	//Location for DAMS data migration XSL files
	public static String DAMS_DATA_MIGRATION_XSL_DIR = "";
	// dams42json conversion xsl file
	public static String DAMS42JSON_XSL_FILE = "";

	// rdcp file replacement root directory postfix for prod, staging, and QA
	public static String RDCP_UPLOAD_ROOT_POSTFIX = "";

	/*
	 * Create data source from configuration according to the current settings in Tomcat resource
	 * @reference http://commons.apache.org/proper/commons-dbcp/api-2.7.0/index.html 
	 * @param props
	 * @return
	 */
	public static DataSource createDataSource(Properties props) {

		BasicDataSource ds = new BasicDataSource();

		String dirverClassName = props.getProperty("db.driverClassName");
		ds.setDriverClassName(dirverClassName);
		ds.setUrl(props.getProperty("db.url"));
		ds.setUsername(props.getProperty("db.username"));
		ds.setPassword(props.getProperty("db.pwd"));

		String validationQuery = props.getProperty("db.validationQuery");
		if (StringUtils.isNotBlank(validationQuery)) {
			ds.setValidationQuery(validationQuery.trim());
		}

		String maxWaitMillis = props.getProperty("db.maxWaitMillis");
		if (StringUtils.isNotBlank(maxWaitMillis)) {
			ds.setMaxWaitMillis(Long.parseLong(maxWaitMillis.trim()));
		}

		String maxIdle = props.getProperty("db.maxIdle");
		if (StringUtils.isNotBlank(maxIdle)) {
			ds.setMaxIdle(Integer.parseInt(maxIdle.trim()));
		}

		String maxActive = props.getProperty("db.maxActive");
		if (StringUtils.isNotBlank(maxActive)) {
			ds.setMaxTotal(Integer.parseInt(maxActive.trim()));
		}

		String testOnBorrow = props.getProperty("db.testOnBorrow");
		if (StringUtils.isNotBlank(testOnBorrow)) {
			ds.setTestOnBorrow(Boolean.parseBoolean(testOnBorrow.trim()));
		}

		String testOnReturn = props.getProperty("db.testOnReturn");
		if (StringUtils.isNotBlank(testOnReturn)) {
			ds.setTestOnReturn(Boolean.parseBoolean(testOnReturn.trim()));
		}

		String removeAbandoned = props.getProperty("db.removeAbandoned");
		if (StringUtils.isNotBlank(removeAbandoned)) {
			ds.setRemoveAbandonedOnBorrow(Boolean.parseBoolean(removeAbandoned.trim()));
		}

		String removeAbandonedTimeout = props.getProperty("db.removeAbandonedTimeout");
		if (StringUtils.isNotBlank(removeAbandonedTimeout)) {
			ds.setRemoveAbandonedTimeout(Integer.parseInt(removeAbandonedTimeout.trim()));
		}

		String logAbandoned = props.getProperty("db.logAbandoned");
		if (StringUtils.isNotBlank(logAbandoned)) {
			ds.setLogAbandoned(Boolean.parseBoolean(logAbandoned.trim()));
		}

		return ds;
	}

	/*
	 * Retrieve properties from environment variables.
	 * @param props
	 * @param propName
	 * @param prefix
	 */
	public static Properties propertiesFromEnv() {
		Properties props = new Properties();
		String prefix = "APPS_DM";                          //env variable prefix
		
		addProperty(props, "xdre.damsRepo", prefix);        //DAMS repository REST API servlet URL
		addProperty(props, "xdre.damsRepo.user", prefix);   //DAMS repository user
		addProperty(props, "xdre.damsRepo.pwd", prefix);    //DAMS repository password
		addProperty(props, "xdre.clusterHostName", prefix); //Cluster host URL

		addProperty(props, "xdre.staging", prefix);         //DAMS staging area
		addProperty(props, "xdre.tmpFileDir", prefix);      //Directory to write temp files
		addProperty(props, "xdre.defaultDerivatives", prefix);// Directory to write temp files

		addProperty(props, "xdre.solrBase", prefix);         //Default derivatives
		addProperty(props, "xdre.ark.urlBase", prefix);      //ARK URL nase

		addProperty(props, "xdre.clr.urlBase", prefix);      //Collection URL base
		addProperty(props, "xdre.clr.imgDir", prefix);       //Collection image dir
		addProperty(props, "xdre.clr.thumbnailsDir", prefix);//Collection thumbnails dir
		addProperty(props, "xdre.clr.sourceDir", prefix);    //Collection image source dir

		addProperty(props, "xdre.ark.name", prefix);         //ARK name
		addProperty(props, "xdre.ark.orgCode", prefix);      //ARK orgCode

		addProperty(props, "mail.support", prefix);          //Support mail

		addProperty(props, "fs.localStore.baseDir", prefix); //Local filestore base dir for DAMS3 files renaming
		addProperty(props, "zoomify.tiles.baseDir", prefix); //Zoomify tile base dir for add component renaming
		addProperty(props, "derivatives.list", prefix);      //DAMS4/dams3 derivative list

		addProperty(props, "xdre.ffmpeg", prefix);           //FFMPEG command
		addProperty(props, "ffmpeg.video.params", prefix);   //FFMPEG MP4 video derivative creation parameters string
		addProperty(props, "ffmpeg.audio.params", prefix);   //FFMPEG MP3 audio derivative creation parameters string
		addProperty(props, "xdre.ffprobe", prefix);          //FFPROBE command
		addProperty(props, "ffprobe.params.duration", prefix);   //FFprobe params to get the duration for video thumbnail creation
		addProperty(props, "video.size", prefix);            //video size
		addProperty(props, "exiftool.command", prefix);      //ExifTool command

		addProperty(props, "imageMagick.command", prefix);   //ImageMagick command
		addProperty(props, "imageMagick.params", prefix);    //ImageMagick params

		addProperty(props, "xdre.clr.thumbnailsSize", prefix);//Collection thumbnails size
		addProperty(props, "xdre.clr.imgSize", prefix);      //Collection image size

		addProperty(props, "zoomfy.command", prefix);        //Zoomify command

		addProperty(props, "watermark.command", prefix);     //Watermark command
		addProperty(props, "watermark.image", prefix);       //Watermark image file
		addProperty(props, "watermark.derivative.list", prefix);//Derivative name/use pairs for watermarking

		addProperty(props, "xdre.relatedreource.mapping", prefix);//Related resource type and label mapping

		addProperty(props, "batch.additional.fields", prefix);//Additional fields for batch overlay validation

		addProperty(props, "alma.api.bibs.url", prefix);       // Alma API bib url
		addProperty(props, "alma.api.key", prefix);           // Alma API key

		addProperty(props, "ns.prefix", prefix);              //Namespace prefix

		addProperty(props, "dams.weblog.dir", prefix);        //Weblog location
		addProperty(props, "dams.stats.se.patterns", prefix); //Search engine, crawlers patterns excluding from dams statistics
		addProperty(props, "dams.stats.se.data", prefix);     //Search engine, crawlers data location
		addProperty(props, "dams.stats.ip.filter", prefix);   //IP filter to exclude hits from collecting
		addProperty(props, "dams.stats.query.ip.filter", prefix); //IP query filter to exclude hits from search result
		addProperty(props, "dams.curator.role", prefix);      //DAMS super user role
		addProperty(props, "ffmpeg.embed.params", prefix);    //ffmpeg parameters for embedded metadata

		addProperty(props, "cil.harvest.enabled", prefix);    //CIL harvest enabled
		addProperty(props, "cil.harvest.dir", prefix);        //CIL harvest base dir
		addProperty(props, "cil.backup.dir", prefix);         //Directory for CIL inventory backup
		addProperty(props, "cil.harvest.metadata.mapping.xslt", prefix);//Location of the CIL mapping file
		addProperty(props, "cil.harvest.emails", prefix);     //CIL harvesting emails for notification
		addProperty(props, "cil.harvest.api", prefix);        //CIL REST API URL base
		addProperty(props, "cil.harvest.api.user", prefix);   //CIL REST API user
		addProperty(props, "cil.harvest.api.pwd", prefix);    //CIL REST API password
		addProperty(props, "cil.content.urlBase", prefix);    //CIL content URL base

		addProperty(props, "dams.data.migration.exports.dir", prefix); //Location for DAMS data migration CSV and content files export
		addProperty(props, "dams.data.migration.xsl.dir", prefix);     //Location for DAMS data migration XSL files
		addProperty(props, "dams.dams42json.xsl", prefix);    //Location of dams42json xsl

		// rdcp files replacement
		addProperty(props, "rdcp.upload.dir.postfix", prefix);  //rdcp file replacement root directory postfix

		//// database connection properties
		addProperty(props, "db.driverClassName", prefix);     // database driver class
		addProperty(props, "db.url", prefix);                 // database connection URL
		addProperty(props, "db.username", prefix);            // database username
		addProperty(props, "db.pwd", prefix);                 // database password

		// Following are optional database connection properties
		addProperty(props, "db.validationQuery", prefix);     // connection validation query
		addProperty(props, "db.maxWaitMillis", prefix);       // connection max await milliseconds
		addProperty(props, "db.maxIdle", prefix);             // connection pooling max idle connections
		addProperty(props, "db.maxActive", prefix);           // connection pooling max active connections
		addProperty(props, "db.testOnBorrow", prefix);        // connection pooling testOnBorrow flag
		addProperty(props, "db.testOnReturn", prefix);        // connection pooling testOnReturn flag
		addProperty(props, "db.removeAbandoned", prefix);     // connection pooling removeAbandoned flag
		addProperty(props, "db.removeAbandonedTimeout", prefix);// connection pooling removeAbandonedTimeout flag
		addProperty(props, "db.logAbandoned", prefix);        // connection pooling logAbandoned flag

		return props;
	}

	private static void addProperty(Properties props, String propName, String prefix) {
		// Env variable convenient: APPS_[JIRA code][purpose] APPS_DM_DERIVATIVES_LIST
		String val = System.getenv((prefix + "_" + propName).replace(".", "_").toUpperCase());

		if (val != null) {
			props.put(propName, val);
		}
	}

	static {
		InputStream in = null;
		Properties props = null;

		try {
			// load config from JNDI
			InitialContext ctx = new InitialContext();

			try {
				String appHome = (String) ctx.lookup("java:comp/env/damsmanager/home");
				File f = new File(appHome, "damsmanager.properties" );

				if (f.exists()) {
					// Use local configuration file damsmanager.propertie if exist,
					// otherwise retrieve from environment settings.
					in = new FileInputStream(f);
					props = new Properties();
					props.load( in );
				} else {
					props = propertiesFromEnv();
				}
			} catch (NameNotFoundException ex) {
				// retrieve properties from environment settings
				props = propertiesFromEnv();
			}

			//DAMS repository REST API servlet URL
			DAMS_STORAGE_URL = props.getProperty("xdre.damsRepo");
			
			//DAMS repository user
			DAMS_STORAGE_USER = props.getProperty("xdre.damsRepo.user");
			
			//DAMS repository password
			DAMS_STORAGE_PWD = props.getProperty("xdre.damsRepo.pwd");
			
			//Cluster host URL
			CLUSTER_HOST_NAME = props.getProperty("xdre.clusterHostName");
			
			//DAMS staging area
			DAMS_STAGING = props.getProperty("xdre.staging");
			
			// Directory to write temp files
			TMP_FILE_DIR = props.getProperty("xdre.tmpFileDir");

			//SOLR UR
			DEFAULT_DERIVATIVES = props.getProperty("xdre.defaultDerivatives");
			
			//Default derivatives
			SOLR_URL_BASE = props.getProperty("xdre.solrBase");
			
			//ARK URL nase
			DAMS_ARK_URL_BASE = props.getProperty("xdre.ark.urlBase");
			
			// Collection URL base
			DAMS_CLR_URL_BASE = props.getProperty("xdre.clr.urlBase");

			//Collection image dir
			DAMS_CLR_IMG_DIR = props.getProperty("xdre.clr.imgDir");

			//Collection thumbnails dir
			DAMS_CLR_THUMBNAILS_DIR = props.getProperty("xdre.clr.thumbnailsDir");

			//Collection image source dir
			DAMS_CLR_SOURCE_DIR = props.getProperty("xdre.clr.sourceDir");
			
			//ARK name
			DEFAULT_ARK_NAME = props.getProperty("xdre.ark.name");
			//ARK orgCode
			ARK_ORG = props.getProperty("xdre.ark.orgCode");
			
			//Support mail
			MAILSENDER_DAMSSUPPORT = props.getProperty("mail.support");
			
			//Local filestore base dir for DAMS3 files rename to DAMS4 naming convention only
			FILESTORE_DIR = props.getProperty("fs.localStore.baseDir");
			
			//Zoomify tiles base dir for add component rename only
			ZOOMIFY_BASE_DIR = props.getProperty("zoomify.tiles.baseDir");
			
			//DAMS4/dams3 derivative list
			DERIVATIVES_LIST = props.getProperty("derivatives.list");
			
			// FFMPEG command
			FFMPEG_COMMAND = props.getProperty("xdre.ffmpeg");
			
			// FFMPEG MP4 video derivative creation parameters string
			FFMPEG_VIDEO_PARAMS = props.getProperty("ffmpeg.video.params");

			// FFMPEG MP3 audio derivative creation parameters string
			FFMPEG_AUDIO_PARAMS = props.getProperty("ffmpeg.audio.params");

			// FFPROBE command
			FFPROBE_COMMAND = props.getProperty("xdre.ffprobe");
			// FFprobe params to get the duration for video thumbnail creation
			FFPROBE_PARAMS_DURATION = props.getProperty("ffprobe.params.duration");

			VIDEO_SIZE = props.getProperty("video.size");

			//ExifTool command
			EXIFTOOL_COMMAND = props.getProperty("exiftool.command");

			//ImageMagick command
			IMAGEMAGICK_COMMAND = props.getProperty("imageMagick.command");
			
			//ImageMagick params
			IMAGEMAGICK_PARAMS = props.getProperty("imageMagick.params");

			//Collection thumbnails size
			COLLECTION_THUMBNAILS_SIZE = props.getProperty("xdre.clr.thumbnailsSize");

			//Collection image size
			COLLECTION_IMAGE_SIZE = props.getProperty("xdre.clr.imgSize");

			//Zoomify command
			ZOOMIFY_COMMAND = props.getProperty("zoomfy.command");

			//Watermark command
			WATERMARK_COMMAND = props.getProperty("watermark.command");
			//Watermark image file
			WATERMARK_IMAGE = props.getProperty("watermark.image");
			//Derivative name/use pairs for watermarking
			WATERMARKED_DERIVATIVES = new HashMap<>();
			String derivativeList = props.getProperty("watermark.derivative.list");
			if (derivativeList != null) {
				String[] ders = derivativeList.split(",");
				for (String der : ders) {
					String[] pair = der.split("\\:");
					WATERMARKED_DERIVATIVES.put(pair[0].trim(), pair[1].trim());
				}
			}

			// Related resource type and displayLabel mapping 
			String relatedResourceMappingValue = props.getProperty("xdre.relatedreource.mapping");
			String[] relatedResourceMappingPairs = relatedResourceMappingValue.split(";");
			for (String pair : relatedResourceMappingPairs) {
				String[] token = pair.split("\\|");
				RELATED_RESOURCE_LABEL_MAPPINGS.put(token[0].trim().toLowerCase(), token[1].trim());
			}

			//Additional fields for batch overlay validation
			BATCH_ADDITIONAL_FIELDS = props.getProperty("batch.additional.fields");

			// Alma API
			ALMA_API_BIB_URL = props.getProperty("alma.api.bibs.url");
			ALMA_API_KEY = props.getProperty("alma.api.key");

			// Namespace prefix
			NS_PREFIX = props.getProperty("ns.prefix");
			NS_PREFIX_MAP = new HashMap<String, String>();
			if(NS_PREFIX != null){
				String[] nsPrefixes = NS_PREFIX.split(",");
				for(int i=0; i<nsPrefixes.length; i++){
					String nsPrefix = nsPrefixes[i].trim();
					NS_PREFIX_MAP.put(nsPrefix, props.getProperty("ns.prefix." + nsPrefix));
				}
			}
			
			// Retrieve the default triplestore
			DAMSClient damsClient = new DAMSClient(DAMS_STORAGE_URL);		
			DEFAULT_TRIPLESTORE = damsClient.defaultTriplestore(); //Default triplestore

			// Weblog location
			STATS_WEBLOG_DIR = props.getProperty("dams.weblog.dir");
			// Search engine, crawlers patterns to be excluded from dams statistics
			STATS_SE_PATTERNS = props.getProperty("dams.stats.se.patterns");
			// Search engine, crawlers data location
			STATS_SE_DATA_LOCATION = props.getProperty("dams.stats.se.data");
			// IP filter to exclude hits from collecting
			STATS_IP_FILTER = props.getProperty("dams.stats.ip.filter");
			// IP query filter to exclude hits from search result
			STATS_QUERY_IP_FILTER = props.getProperty("dams.stats.query.ip.filter");
			// DAMS super user role
			CURATOR_ROLE = props.getProperty("dams.curator.role");

			// retrieve or create data source for DAMS statistics
			try {
				DAMS_DATA_SOURCE = (DataSource) ctx.lookup("java:comp/env/jdbc/dams");
				if (DAMS_DATA_SOURCE == null) {
					DAMS_DATA_SOURCE = createDataSource(props);
				}
			} catch (NameNotFoundException ex) {
				// create datasource from properties configurations
				DAMS_DATA_SOURCE = createDataSource(props);
			}

			// ffmpeg parameters for embedded metadata
			String ffmpegEmbedParams = props.getProperty("ffmpeg.embed.params");
			String[] formats = ffmpegEmbedParams.split(";");
			for (String format : formats) {
				String[] token = format.split("\\|");
				FFMPEG_EMBED_PARAMS.put(token[0].trim(), token[1].trim());
			}

			// CIL harvesting enabled
			CIL_HARVEST_ENABLED = props.getProperty("cil.harvest.enabled");
			// CIL harvest base dir
			CIL_HARVEST_DIR = props.getProperty("cil.harvest.dir");
			// Directory for CIL inventory backup
			CIL_BACKUP_DIR = props.getProperty("cil.backup.dir");
			// Location of the CIL mapping file
			CIL_HARVEST_MAPPING_FILE = props.getProperty("cil.harvest.metadata.mapping.xslt");
			// CIL harvesting emails for notification
			CIL_HARVEST_NOTIFY_EMAILS = props.getProperty("cil.harvest.emails");
			// CIL REST API URL base
			CIL_HARVEST_API = props.getProperty("cil.harvest.api");
			// CIL REST API user
			CIL_HARVEST_API_USER = props.getProperty("cil.harvest.api.user");
			// CIL REST API password
			CIL_HARVEST_API_PWD = props.getProperty("cil.harvest.api.pwd");
			// CIL content URL base
			CIL_CONTENT_URL_BASE = props.getProperty("cil.content.urlBase");

			//Location for DAMS data migration CSV and content files export
			DAMS_DATA_MIGRATION_EXPORTS_DIR = props.getProperty("dams.data.migration.exports.dir");
			//Location for DAMS data migration XSL files
			DAMS_DATA_MIGRATION_XSL_DIR = props.getProperty("dams.data.migration.xsl.dir");
			// Location of dams42json xsl
			DAMS42JSON_XSL_FILE = props.getProperty("dams.dams42json.xsl");

			// rdcp file replacement root directory postfix for prod, staging, and QA
			RDCP_UPLOAD_ROOT_POSTFIX = props.getProperty("rdcp.upload.dir.postfix");
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			if(in != null){
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				in = null;
			}
		}
	}
}
