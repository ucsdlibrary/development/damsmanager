package edu.ucsd.library.xdre.collection;

import static edu.ucsd.library.xdre.tab.TabularRecordBasic.addAttribute;
import static edu.ucsd.library.xdre.tab.TabularRecordBasic.addElement;
import static edu.ucsd.library.xdre.tab.TabularRecordBasic.addMadsElement;
import static edu.ucsd.library.xdre.tab.TabularRecordBasic.addTextElement;
import static edu.ucsd.library.xdre.tab.TabularRecordBasic.damsNS;
import static edu.ucsd.library.xdre.tab.TabularRecordBasic.madsNS;
import static edu.ucsd.library.xdre.tab.TabularRecordBasic.rdfNS;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.dom4j.Branch;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Node;

import edu.ucsd.library.xdre.utils.ArkFile;
import edu.ucsd.library.xdre.utils.Constants;
import edu.ucsd.library.xdre.utils.DAMSClient;
import edu.ucsd.library.xdre.utils.DamsURI;
import edu.ucsd.library.xdre.utils.ZoomifyTilesConverter;

/**
 * Class AddComponentsHandler to add component with file to existing objects,
 * which need object restructuring and local filestore ARK files renaming.
 * @author lsitu
 */
public class TranscriptUploadHandler extends CollectionHandler{
    private static Logger log = Logger.getLogger(TranscriptUploadHandler.class);

    public static final String COMPONENT_TITLE = "Component file";
    public static final String DIGITAL_FILE = "Digital file";
    public static final String TRANSCRIPT_TITLE = "Transcript";

    // For transcript file
    private boolean transcript = false;

    private int count = 0;
    private int failedCount = 0;

    private int processIndex = 0;
    private String[] processNames = {"Restructure object metadata", "Rename ARK files", "Update object metadata", "Ingest component file", "Refresh SOLR index"};
    private boolean[] status = new boolean[processNames.length];
    private StringBuilder[] messages = new StringBuilder[processNames.length];
    private StringBuilder arkReport = new StringBuilder();

    private Map<String, String> filesMap = null;
    private Map<String, String> titlesMap = new HashMap<>();

    /**
     * Constructor for AddComponentsHandler
     * @param damsClient
     * @throws Exception
     */
    public TranscriptUploadHandler(DAMSClient damsClient, Map<String, String> filesMap) throws Exception{
        super(damsClient, null);
        this.filesMap = filesMap;
    }

    public boolean isTranscript() {
        return transcript;
    }

    public void setTranscript(boolean transcript) {
        this.transcript = transcript;
    }

    public Map<String, String> getTitlesMap() {
		return titlesMap;
	}

	public void setTitlesMap(Map<String, String> titlesMap) {
		this.titlesMap = titlesMap;
	}

	/**
     * Procedure for adding components with files
     */
    public boolean execute() throws Exception {

        String message = null;
        String subjectURI = null;

        // add ark report headers
        toArkReport (arkReport, transcript ? "Transcript ARK" : "Component File ARK", "Component Title", transcript ? "Transcript file" : "Component file", "Outcome", "Event date", "Error Description");

        for(int i=0; i<itemsCount && !interrupted; i++) {
            count++;

            message = "";
            for (int k = 0; k <messages.length; k++) {
                messages[k] = new StringBuilder();
            }

            String f = items.get(i);
            subjectURI = filesMap.get(f);

            // the component file to add
            File file = new File(Constants.DAMS_STAGING, f);
            String compFileName = file.getName();

            String compTitle = titlesMap.get(f);
            compTitle = StringUtils.isNotBlank(compTitle) ? compTitle : transcript ? TRANSCRIPT_TITLE : file .getName();

            setStatus("Preparing object " + subjectURI  + " (" + (i + 1) + " of total " + itemsCount + ") ... " );

            Map<Path, Path> fileRenameMap = new HashMap<>();
            Map<Path, Path> zoomifyRenameMap = new HashMap<>();

            Document doc = null;
            Node compParentNode = null;

            try {
                DamsURI compFileURI = DamsURI.toParts(subjectURI, null);
                String oid = compFileURI.getObject();

                int compCid = Integer.parseInt(compFileURI.getComponent()); // index of the component to add
                int compOrder = 2;                                      // order of the component to add

                String progressMessage = "Processing component file " + compFileName + " for object " + oid + "  ...";
                setStatus(progressMessage);
                log("log", progressMessage);

                /////// First step: restructure object
                processIndex = 0;

                if (damsClient.exists(oid, null, null)) {
                    try {
                        doc = damsClient.getRecord(oid);

                        Node objectNode = doc.selectSingleNode("/rdf:RDF/dams:Object");

                        if (transcript && hasTranscript(objectNode, oid + "/" + compCid)) {
                            message = handleError(oid, "Transcript component " + oid + "/" + compCid
                                            + " exists", null);

                            continue;
                        }

                        List<Node> components = objectNode.selectNodes("//dams:Component");
                        if (components.size() > 0) {
                            // complex object: check for the existence component, which will be change to the next sibling node
                            Node compExisted = objectNode.selectSingleNode("//dams:Component[@rdf:about='" + oid + "/" + compCid + "']");
                            if (compExisted != null) {
                                compParentNode = compExisted.getParent().getParent();
 
                                compOrder = Integer.parseInt(compExisted.valueOf("dams:order"));
                            } else {
                                // check for the existence of the preceding sibling component
                                String preCompUri = oid + "/" + (compCid - 1);
                                Node preNode = objectNode.selectSingleNode("//dams:Component[@rdf:about='" + preCompUri + "']");
                                if (preNode == null) {
                                    message = handleError(subjectURI, "Preceding sibling component " + preCompUri + " doesn't exists", null);

                                    continue;
                                }

                                compParentNode = preNode.getParent().getParent();

                                // Component order: the next order of the preceding sibling component
                                compOrder = Integer.parseInt(preNode.valueOf("dams:order"));
                            }

                            List<Node> siblings = compParentNode.selectNodes("dams:hasComponent/dams:Component");

                            // restructure component index: increase component index by 1
                            for (Node component : components) {
                                restructureComponent(component, fileRenameMap, zoomifyRenameMap, siblings, compCid);
                            }
                        } else {
                            // simple object
                            compOrder = compCid;
                            compParentNode = objectNode;
 
                            // restructure files in simple object to component
                            int restructureCompCid = compCid == 1 ? 2 : 1;
                            restructureObject(objectNode, fileRenameMap, zoomifyRenameMap, restructureCompCid);
                        }

                        // add component
                        addComponent((Element)compParentNode, oid + "/" + compCid, compTitle, compOrder);

                    } catch (Exception ex) {
                        message = handleError(oid, "Error restructuring object metadata", ex);

                        continue;
                    }
                }

                /////// Second step: rename ark files in local filestore
                status[processIndex] = true;
                messages[processIndex].append(damsDateFormat.format(new Date()));
                processIndex++;

                boolean successful = true;
                StringBuilder errorBuilder = new StringBuilder();

                if (renameArkFiles(oid, fileRenameMap, errorBuilder)) {

                    /////// Third step: update object metadata
                    status[processIndex] = true;
                    messages[processIndex].append(damsDateFormat.format(new Date()));
                    processIndex++;

                    try {
                        if (!damsClient.updateObject(oid, doc.asXML(), Constants.IMPORT_MODE_ALL)) {
                            successful = false;
                            message = handleError(oid, "Update object metadata failed", null);
                        }
                    } catch (Exception ex) {
                        successful = false;
                        message = handleError(oid, "Update object metadata failed", ex);
                    }
                } else {
                    successful = false;
                    message = errorBuilder.toString();
                }

                if (successful) {

                    /////// Fourth step: ingest component file and create derivative
                    status[processIndex] = true;
                    messages[processIndex].append(damsDateFormat.format(new Date()));
                    processIndex++;

                    String fid = compFileURI.getFileName();
                    String cid = "" + compCid;

                    Map<String, String> params = DAMSClient.toFileIngestParams(oid, cid, fid, file);
                    params.put("local", file.getAbsolutePath());

                    // file use: image files => image-source, video files => video-source, audio files => audio-source, .txt file => data-service
                    String use = transcript ? "document-service" : isImage(fid, null) ? "image-source" : isAudio(fid, null) ?
                            "audio-source" : isVideo(fid, null) ? "video-source" : fid.toLowerCase().endsWith(".txt") ? "data-service" : null; 
                    if (StringUtils.isNotBlank(use)) {
                        params.put("use", use);
                    }

                    if (damsClient.uploadFile(params, false)) {
                        progressMessage = "Ingested component file (" + compFileName + ") to " + subjectURI  + " ... ";
                        setStatus(progressMessage);
                        log("log", progressMessage);

                        // create derivatives for images and document PDFs
                        if( isDerivativesRequired(fid, null) ){
                            try {
                                // rename zoomify tiles for master image files
                                if ( zoomifyRenameMap.size() > 0 ) {
                                    renameZoomifyDirectories(oid, zoomifyRenameMap, errorBuilder);
                                }

                                successful = FileUploadHandler.createDerivate(damsClient, subjectURI, null);

                                if (successful) {
                                    logMessage( "Created derivatives for " + subjectURI + " (" + damsClient.getRequestURL() + ").");
                                } else {
                                    status[processIndex] = false;
                                    messages[processIndex].append("Derivative creation failed.");
                                }
                            } catch(Exception e) {
                                successful = false;
                                status[processIndex] = false;

                                message = e.getMessage();
                                log.error(e);
                                logError(message);
                            }
                        }

                        if (successful) {

                            /////// Final step: update solr
                            status[processIndex] = true;
                            messages[processIndex].append(exeResult ? damsDateFormat.format(new Date()) : message);
                            processIndex++;

                            if(!updateSOLR(oid)) {
                                message = handleError(subjectURI, "SOLR index failed", null);
                                status[processIndex] = false;
                            } else {
                                status[processIndex] = true;
                            }
                        }
                    } else {
                        message = handleError(subjectURI, "Failed to ingest component file " + compFileName, null);
                    }
                }
            } catch (Exception ex) {

                message = handleError(subjectURI, "Failed to ingest component " + compFileName, ex);
            } finally {
                if (!status[processIndex]) {
                    failedCount++;
                    exeResult = false;
                }

                messages[processIndex].append(exeResult ? damsDateFormat.format(new Date()) : message);

                // Generate ark report
                toArkReport(
                        arkReport,
                        subjectURI,
                        (StringUtils.isNotBlank(compTitle) ? compTitle : "[No Component Title]"), 
                        compFileName,
                        status[processIndex] ? "successful" : "failed", 
                        damsDateFormat.format(new Date()),
                        status[processIndex] ? "" : "[" + processNames[processIndex] + "] " + message);
            }

            try{
                Thread.sleep(10);
            } catch (InterruptedException e1) {
                exeResult = false;
                failedCount++;
                interrupted = true;
                logError("Add component process canceled for " + subjectURI + " ("  + compFileName + "): "
                        + e1.getMessage() + ". ");
                setStatus("Cancelling ...");
                clearSession();
            }

            setProgressPercentage( ((i + 1) * 100) / itemsCount);
        }

        return exeResult;
    }

    /**
     * Execute result of the add components process
     */
    public String getExeInfo() {
        if(exeResult)
            exeReport.append("Files upload succeeded: \n ");
        else
            exeReport.append("Files upload failed (" + failedCount + " of total " + count + " failed): \n ");    

        exeReport.append("Total files found " + itemsCount + " (Number of files processed: " + count + "). \n");

        exeReport.append(errorReport.toString());

        String exeInfo = exeReport.toString();
        log("log", exeInfo);

        // write the ark report to file
        if (arkReport.length() > 0) {
            File arkReportFile = getArkReportFile();
            try (OutputStream out = new FileOutputStream(arkReportFile)) {
                out.write(arkReport.toString().getBytes());
            } catch (IOException ex) {
                log.error("Error writting ark report: ", ex);
            }
        }

        return exeInfo;
    }

    /*
     * Restructure component when component index is larger than component index to add
     * @param component
     * @param fileRenameMap
     * @param zoomifyRenameMap
     * @param siblingNodes
     * @param transIndex
     * @throws Exception
     */
    private void restructureComponent(Node component, Map<Path, Path> fileRenameMap, Map<Path, Path> zoomifyRenameMap,
            List<Node> siblingNodes, int transIndex) throws Exception {
        Node rdfAbout = component.selectSingleNode("@rdf:about");

        DamsURI damsURI = DamsURI.toParts(rdfAbout.getStringValue(), null);

        // update component uri to use the next component index
        int cIdx = Integer.parseInt(damsURI.getComponent());
        if (cIdx >= transIndex) {
            rdfAbout.setText(damsURI.getObject() + "/" + ++cIdx);

            // update the order of sibling components
            if (siblingNodes.contains(component)) {
                Node orderNode = component.selectSingleNode("dams:order");
                int order = Integer.parseInt(orderNode.getText());
                orderNode.setText(String.valueOf(order + 1));
            }

            // get component ark files to rename
            List<Node> fileNodes = component.selectNodes("dams:hasFile/dams:File");
            reorganizeFiles(fileNodes, fileRenameMap, zoomifyRenameMap);
        }
    }

    /*
     * restructure simple object to complex object
     * and move all files to the new component created
     * @param objectNode
     * @param fileRenameMap
     * @param zoomifyRenameMap
     * @param compIndex
     * @throws Exception
     */
    private void restructureObject(Node objectNode, Map<Path, Path> fileRenameMap, Map<Path, Path> zoomifyRenameMap, int compIndex) throws Exception {
        String oid = objectNode.valueOf("@rdf:about");

        Element restructureComp = addComponent((Element)objectNode, oid + "/" + compIndex, transcript ? DIGITAL_FILE : COMPONENT_TITLE, compIndex);

        List<Node> fileNodes = objectNode.selectNodes("dams:hasFile/dams:File");
        reorganizeFiles(fileNodes, fileRenameMap, zoomifyRenameMap);

        // move all file to the new component
        for (Node fileNode : fileNodes) {
            restructureComp.add(fileNode.getParent().detach());
        }
    }

    /*
     * Rename ARK files to for all restructured components
     * @param oid
     * @param fileRenameMap
     * @param tmpRenameMap
     * @param errorBuilder
     * @return
     */
    private boolean renameArkFiles(String oid, Map<Path, Path> fileRenameMap, StringBuilder errorBuilder) {
        boolean successful = true;
        Map<Path, Path> tmpRenameMap = new HashMap<>();

        for (Path existingFile : fileRenameMap.keySet()) {
            try {
                // rename existing file to tmp file to avoid naming conflicts
                if (existingFile.toFile().exists()) {
                    Path tmpPath = Paths.get(existingFile.getParent().toFile().getAbsolutePath(), "_" + existingFile.getFileName());
                    Files.move(existingFile, tmpPath, StandardCopyOption.REPLACE_EXISTING);
                    tmpRenameMap.put(tmpPath, fileRenameMap.get(existingFile));
                } else {
                    successful = false;
                    String error = "Failed to rename ark file " + existingFile.toFile().getName() + " (file not found!)";
                    errorBuilder.append(handleError(oid, error, null));
                }
            } catch (Exception ex) {
                successful = false;
                errorBuilder.append(handleError(oid, "Failed to rename file: " + existingFile.toFile().getName(), ex));

                break;
            }
        }

        if (successful) {
            // check all ark files to avoid overwriting
            for (Path tmpFile : tmpRenameMap.keySet()) {
                 if (tmpRenameMap.get(tmpFile).toFile().exists()) {
                     errorBuilder.append(handleError(oid, "Renaming conflicts: ARK file " + tmpRenameMap.get(tmpFile) + " exists!", null));
                     successful = false;
                 }
            }

            if (successful) {
                for (Path tmpFile : tmpRenameMap.keySet()) {
                    // rename temporary ark files to their new ark filenames
                    try {
                        Files.move(tmpFile, tmpRenameMap.get(tmpFile), StandardCopyOption.ATOMIC_MOVE);
                    } catch (Exception ex) {
                        successful = false;
                        errorBuilder.append(handleError(oid, "Failed to rename ARK file to " + tmpRenameMap.get(tmpFile), ex));

                        break;
                    }
                }
            }
        }

        if (!successful) {
            try {
                restoreArkFiles(tmpRenameMap);
            } catch (Exception ex) {
                errorBuilder.append(handleError(oid, "Error occurred during restoring ARK files", ex));
            }
        }

        return successful;
    }

    /*
     * Rename zoomify tile directories for image components that need to be restructured.
     * @param oid
     * @param zoomifyRenameMap
     * @param errorBuilder
     * @return
     */
    private boolean renameZoomifyDirectories(String oid, Map<Path, Path> zoomifyRenameMap, StringBuilder errorBuilder) {
        boolean successful = true;
        Map<Path, Path> tmpRenameMap = new HashMap<>();
        for (Path existingTilesDir : zoomifyRenameMap.keySet()) {
            try {
                // rename existing zoomify tile directories to temporary directories to avoid naming conflicts
                if (existingTilesDir.toFile().exists()) {
                    Path tmpPath = Paths.get(existingTilesDir.getParent().toFile().getAbsolutePath(), "_" + existingTilesDir.getFileName());
                    Files.move(existingTilesDir, tmpPath, StandardCopyOption.REPLACE_EXISTING);
                    tmpRenameMap.put(tmpPath, zoomifyRenameMap.get(existingTilesDir));
                } else {
                    successful = false;
                    String error = "Failed to rename zoomify tiles directory " + existingTilesDir + " (directory not found!)";
                    errorBuilder.append(handleError(oid, error, null));
                }
            } catch (Exception ex) {
                successful = false;
                errorBuilder.append(handleError(oid, "Failed to rename zoomify tiles directory: " + existingTilesDir, ex));

                break;
            }
        }

        if (successful) {
            // check all zoomify tile directories to avoid overwriting
            for (Path tmpTilesDir : tmpRenameMap.keySet()) {
                 if (tmpRenameMap.get(tmpTilesDir ).toFile().exists()) {
                     errorBuilder.append(handleError(oid, "Renaming conflicts: zoomify tiles directory " + tmpRenameMap.get(tmpTilesDir ) + " exists!", null));
                     successful = false;
                 }
            }

            if (successful) {
                for (Path tmpTilesDir : tmpRenameMap.keySet()) {
                    // rename temporary zoomify tile directories to the designate directories
                    try {
                        Files.move(tmpTilesDir, tmpRenameMap.get(tmpTilesDir), StandardCopyOption.ATOMIC_MOVE);
                    } catch (Exception ex) {
                        successful = false;
                        errorBuilder.append(handleError(oid, "Failed to rename zoomify tiles directory to " + tmpRenameMap.get(tmpTilesDir), ex));

                        break;
                    }
                }
            }
        }

        // Restore zoomify tile directories on error.
        if (!successful) {
            try {
                restoreArkFiles(tmpRenameMap);
            } catch (Exception ex) {
                errorBuilder.append(handleError(oid, "Error occurred during restoring zoomify tiles directories", ex));
            }
        }

        return successful;
    }

    /*
     * Restore ark filenames
     * @param tmpRenameMap
     * @throws IOException
     */
    private void restoreArkFiles(Map<Path, Path> tmpRenameMap) throws IOException {
        // rename failure: revert tmp files back to their original filenames
        for (Path tmpFile : tmpRenameMap.keySet()) {
            String tmpFileName = tmpFile.getFileName().toString();
            Path origPath = Paths.get(tmpFile.getParent().toString(), tmpFileName.substring(1));

            if (!origPath.toFile().exists()) {
                Files.move(tmpFile, origPath, StandardCopyOption.REPLACE_EXISTING);
            }
        }
    }

    /*
     * Build error message
     * @param subject
     * @param errorTitle
     * @param ex
     * @return
     */
    private String handleError(String subject, String errorTitle, Exception ex) {
        exeResult = false;
        String error ="";

        if (ex != null) {
            error = ex.getMessage();
            log.error(errorTitle, ex);
        }

        String message = errorTitle + " for " + subject
                + (StringUtils.isNotBlank(error) ? ": " + error : ".");
        messages[processIndex].append(message);

        setStatus(message);
        logError(message);
        errorReport.append(message + "\n");

        return message;
    }

    /*
     * Reorganize ARK files and zoomify tiles for the file nodes
     * @param fileNodes
     * @param fileRenameMap
     * @param zoomifyRenameMap
     */
    private static void reorganizeFiles(List<Node> fileNodes, Map<Path, Path> fileRenameMap, Map<Path, Path> zoomifyRenameMap)
            throws Exception {
        for (Node fileNode : fileNodes) {
            // rename file uri
            Node fRdfAbout = fileNode.selectSingleNode("@rdf:about");
            String origFileUri = fRdfAbout.getStringValue();
            DamsURI damsURI = DamsURI.toParts(origFileUri, null);
            String cid = damsURI.getComponent();
            int cidx = StringUtils.isNotBlank(cid) ? Integer.parseInt(cid) : 0;
            damsURI.setComponent("" + ++cidx);
            fRdfAbout.setText(damsURI.toString());

            // book local files that need to rename
            Path existingFile = ArkFile.getLocalStoreFile(origFileUri);
            Path newFile = ArkFile.getLocalStoreFile(damsURI.toString());
            fileRenameMap.put(existingFile, newFile);

            String fid = damsURI.getFileName();
            if ( isImage(fid, fileNode.valueOf("dams:use")) && fid.startsWith("1.") ) {
                // book zoomify tiles directory that need to rename
                DamsURI origDamsURI = DamsURI.toParts(origFileUri, null);
                Path existingTilesDir = ZoomifyTilesConverter.getZoomifyTilesDirectory(origDamsURI.getObject(), origDamsURI.getComponent());
                if (existingTilesDir.toFile().exists()) {
                    Path newTilesDir = ZoomifyTilesConverter.getZoomifyTilesDirectory(damsURI.getObject(), damsURI.getComponent());
                    zoomifyRenameMap.put(existingTilesDir, newTilesDir);
                }
            }
        }
    }

    /**
     * Create dams:Component element
     * @param b
     * @param uri
     * @param title
     * @param order
     * @return - the Component
     */
    public static Element addComponent(Branch b, String uri, String title, int order) {
        Element e = addElement(b, "hasComponent", damsNS, "Component", damsNS);
        addAttribute(e, "about", rdfNS, uri);

        Element t = addElement(e, "title", damsNS, "Title", madsNS);
        addElement(t,"authoritativeLabel",madsNS).setText(title);
        Element el = addElement(t,"elementList",madsNS);
        addAttribute( el, "parseType", rdfNS, "Collection" );
        addMadsElement(el, "MainTitle",  title);

        addTextElement(e, "order", damsNS, String.valueOf(order));

        return e;
    }

    /**
     * Check for existence of the transcript component
     * @param objectNode
     * @return
     */
    public static boolean hasTranscript(Node objectNode, String componentUri) {
        String xpath = "//dams:Component[@rdf:about='" + componentUri + "']";
        Node node = objectNode.selectSingleNode(xpath);

        if (node != null) {
            String title = node.valueOf("dams:title//mads:authoritativeLabel");
            if (StringUtils.isNotBlank(title) && TRANSCRIPT_TITLE.equalsIgnoreCase(title)) {
                return true;
            }
        }

        return false;
    }
}
