package edu.ucsd.library.xdre.collection;

import java.util.ArrayList;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.Node;

import edu.ucsd.library.xdre.utils.DAMSClient;

/**
 * 
 * SubjectMergeHandler: Merge authority records.
 * @author lsitu@ucsd.edu
 */
public abstract class SubjectMergeHandlerBase extends CollectionHandler{
    public static final String EXCEL_HEADER_ARK_TO_USE = "Ark to use";
    public static final String EXCEL_HEADER_REPLACE_ARKS = "replace ARKS";

    protected int count = 0;
    protected int failedCount = 0;

    protected List<List<String>> replaceArksList = new ArrayList<List<String>>();
    protected Document[] authorityDocuments = null;

    protected StringBuilder arkReport = new StringBuilder();

    /**
     * Constructor
     * @param damsClient
     * @param authorityArk
     * @throws Exception
     */
    public SubjectMergeHandlerBase(DAMSClient damsClient) throws Exception{
        super(damsClient);
    }

    /**
     * Set authority ARKs to use
     * @param items
     */
    @Override
    public void setItems(List<String> items) {
        super.setItems(items);

        authorityDocuments = new Document[items.size()];
    }

    /**
     * Get the list of replace ARKs
     * @return
     */
    public List<List<String>> getReplaceArksList() {
        return replaceArksList;
    }

    /**
     * Set the list of replace ARKs
     * @param replaceArksList
     */
    public void setReplaceArksList(List<List<String>> replaceArksList) {
        this.replaceArksList = replaceArksList;
    }

    /**
     * Sum items in the list
     * @param itemsList
     * @return
     */
    public static int getItemsCount(List<List<String>> itemsList) {
        int count = 0;
        for (List<String> items : itemsList) {
            count += items.size();
        }
        return count;
    }

    /**
     * Retrieve the model type with ns prefix like mads:Topic.
     * @param node
     * @return
     */
    public static String getModelName(Node node) {
        if (node != null) {
            String xPath = node.selectSingleNode("//*[@rdf:about]").getPath();
            return xPath.substring(xPath.lastIndexOf("/") + 1);
        }
        return "[Unknown Model]";
    }

    /**
     * Retrieve the label of a node.
     * @param node
     * @return
     */
    public static String getSubjectLabel(Node node) {
        return node == null ? "[Unknown Label]" : node.valueOf("//mads:authoritativeLabel");
    }

    /**
     * Retrieve URI of a node.
     * @param node
     * @return
     */
    public static String getSubjectUri(String subject, Node node) {
        return node == null ? subject : node.valueOf("//@rdf:about");
    }

    /**
     * Grab the ARKs from the ARK URL list for report
     * @param arkUrls
     * @return
     */
    public static String toArksString(List<String> arkUrls) {
        List<String> arks = new ArrayList<>();
        for (String arkUrl : arkUrls) {
            arks.add(arkUrl.substring(arkUrl.lastIndexOf("/") + 1).trim());
        }

        return String.join("|", arks);
    }

}
