package edu.ucsd.library.xdre.collection;

import static edu.ucsd.library.xdre.web.LinkedRecordsController.LEVEL_ONE;
import static edu.ucsd.library.xdre.web.LinkedRecordsController.LEVEL_TWO;
import static edu.ucsd.library.xdre.web.LinkedRecordsController.LEVEL_THREE;
import static edu.ucsd.library.xdre.web.LinkedRecordsController.lookupLinkedRecords;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.dom4j.Document;

import edu.ucsd.library.xdre.utils.ArkFile;
import edu.ucsd.library.xdre.utils.DAMSClient;
import edu.ucsd.library.xdre.utils.DAMSRepository;
import edu.ucsd.library.xdre.web.LinkedRecordsController;

/**
 * 
 * SubjectMergeReportHandler: generate report for the merging records.
 * @author lsitu@ucsd.edu
 */
public class SubjectMergeReportHandler extends SubjectMergeHandlerBase {
    private static Logger log = Logger.getLogger(SubjectMergeReportHandler.class);

    public static String SPARQL_PREFIX = "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "
            + "PREFIX dams: <http://library.ucsd.edu/ontology/dams#>";

    private DAMSRepository damsRepository = null;

    /**
     * Constructor
     * @param damsClient
     * @throws Exception
     */
    public SubjectMergeReportHandler(DAMSClient damsClient) throws Exception{
        this(damsClient, null);
    }

    /**
     * Constructor
     * @param damsClient
     * @param authorityArk
     * @throws Exception
     */
    public SubjectMergeReportHandler(DAMSClient damsClient, String authorityArk)
            throws Exception{
        super(damsClient);
        //this.authorityArk = authorityArk;
        this.damsRepository = DAMSRepository.getRepository();
    }

    /**
     * Procedure to perform merging subjects
     */
    public boolean execute() throws Exception {
        String subjectURI = null;
        String subjectLabel = null;
        String subjectType = null;
        List<String> replaceArks = null;

        int curIndex = 0;
        int totalItems = getItemsCount(replaceArksList);

        logMessage ("[ARK]   -   [Label]   -   [Subject type]   -   [Records matched]"
                + "   -   [Linked records count]   -   [Linked records]   -   [Note]");
        toArkReport (arkReport, "ARK", "Label", "Subject type", "Records matched", "Linked records count", "Linked records",  "Note");

        Document authorityDoc = null;
        try {

            for(int i=0; i<itemsCount && !interrupted; i++) {
                count++;

                String authorityArk = items.get(i);
                replaceArks = replaceArksList.get(i);

                authorityDoc = damsClient.getRecord(authorityArk);
                authorityDocuments[i] = authorityDoc;

                String authorityLabel = getSubjectLabel(authorityDoc);
                String authorityType = getModelName(authorityDoc);

                // report for the authority to use
                String arkURI = ArkFile.getArkUrl(authorityArk, "", "");
                Collection<String> records = lookupLinkingRecords(damsClient, arkURI);
                List<String> matches = getSubjectsMatched(arkURI, authorityType, authorityLabel);

                // build the report
                String message = "To use in DAMS";
                appendToReport(authorityArk, authorityLabel, authorityType, matches, records, message);

                setStatus("Processing report for authority " + authorityArk + " (" + (i+1) + " of " + itemsCount + ") ... " );

                for(int j = 0; j < replaceArks.size() && !interrupted; j++) {
                    curIndex++;

                    String subject = replaceArks.get(j);

                    setProgressPercentage( (curIndex * 100) / totalItems);


                    Collection<String> subRecords = new ArrayList<>();
                    List<String> subMatches = new ArrayList<>();

                    try {
                        Document subjectDoc = damsClient.getRecord(subject);
                        subjectURI = getSubjectUri(subject, subjectDoc);
                        subjectType = getModelName(subjectDoc);
                        subjectLabel = getSubjectLabel(subjectDoc);

                        subMatches = getSubjectsMatched(subjectURI, subjectType, subjectLabel);

                        subRecords = lookupLinkingRecords(damsClient, subjectURI);

                        setStatus("Done report for " + subjectURI + ": Linking records count " + subRecords.size() + ".");
                    } catch (Exception ex) {
                        failedCount++;
                        exeResult = false;
                        String errorTitle = "Failed to report " + subjectURI + " .";
                        log.error(errorTitle, ex);
                        message = errorTitle + ": " + ex.getMessage();
                    }

                    // build the report
                    message = "To be replaced by " + authorityArk + " '" + authorityLabel + "'.";
                    appendToReport(subject, subjectLabel, subjectType, subMatches, subRecords, message);

                    setProgressPercentage( (curIndex * 100) / totalItems);

                    try{
                        Thread.sleep(100);
                    } catch (InterruptedException e1) {
                        exeResult = false;
                        interrupted = true;
                        logError("Report canceled on " + subjectURI  + ".");
                        setStatus("Canceled");
                        clearSession();
                    }
                }
            }

        } finally {
            // export the ark report to file
            File arkReportFile = getArkReportFile();
            try (FileOutputStream out = new FileOutputStream(arkReportFile);) {
                out.write(arkReport.toString().getBytes());
            }
        }

        return exeResult;
    }

    /*
     * Retrieve subjects that matched the subject type and label
     * @param subjectURI
     * @param subjectType
     * @param subjectLabel
     * @return
     * @throws Exception
     */
    private List<String> getSubjectsMatched(String subjectURI, String subjectType, String subjectLabel) throws Exception {
    	List<String> matches = damsRepository.findAuthority(subjectType, StringEscapeUtils.unescapeJava(subjectLabel));
        if (matches != null && matches.contains(subjectURI)) {
            matches.remove(matches.indexOf(subjectURI));
        }

        return matches == null ? new ArrayList<String>() : matches;
    }

    /*
     * Build the report for subject
     * @param subject
     * @param subjectLabel
     * @param subjectType
     * @param subMatches
     * @param linkingRecords
     * @param message
     */
    private void appendToReport(String subject, String subjectLabel, String subjectType, List<String> subMatches,
            Collection<String> linkingRecords, String message) {

        logMessage ("* " + subject
                         + " - "+ subjectLabel
                         + " - " + subjectType
                         + " - " + (subMatches == null || subMatches.size() == 0 ? "n/a" : String.join(" | ", subMatches))
                         + " - " + linkingRecords.size()
                         + " - " + (linkingRecords.size() == 0 ? "" : toArksString(new ArrayList<String>(linkingRecords)))
                         + " - " + message);

        toArkReport (arkReport,
                     subject,
                     subjectLabel,
                     subjectType,
                     (subMatches == null || subMatches.size() == 0 ? "n/a" : String.join(" | ", subMatches)),
                     linkingRecords.size() + "",
                     (linkingRecords.size() == 0 ? "" : toArksString(new ArrayList<String>(linkingRecords))),
                     message);

    }

    /*
     * lookup records that are linking to the authority record.
     * @param arkUrI
     * @return
     * @throws Exception
     */
    public static Collection<String> lookupLinkingRecords(DAMSClient damsClient, String subjectUrI)
            throws Exception {

        Map<String, String> linkingRecords = new HashMap<>();
        lookupLinkedRecords(damsClient, subjectUrI, linkingRecords, LEVEL_ONE);

        // indirect linked records lookup with mads:ComponentList and dams:relationship predicates
        for (String pre : LinkedRecordsController.topLevelPredicates) {
            lookupLinkedRecords(damsClient, subjectUrI, linkingRecords, LEVEL_TWO, pre);
        }

        // Lookup records lined to mads:componentList/mads:elementList recursively
        lookupLinkedRecords(damsClient, subjectUrI, linkingRecords, LEVEL_THREE);

        return linkingRecords.values();
    }

    /**
     * Execution result message
     */
    public String getExeInfo() {
        int replaceArksCount = getItemsCount(replaceArksList);

        for (int i = 0; i < items.size(); i++) {
            String authorityArk = items.get(i);
            List<String> replaceArks = replaceArksList.get(i);

            Document authDoc = authorityDocuments[i];
            String authorityLabel = getSubjectLabel(authDoc);
            String authorityType = getModelName(authDoc);

            String reportLabel = "merging authorities [" + String.join(" | ", replaceArks) + "] to "
                    + authorityArk + " [" + authorityLabel + ", " + authorityType + "].\n";
            if(exeResult)
                exeReport.append("Successfully created report for " + reportLabel);
            else
                exeReport.append("Failed to create report for " + reportLabel);
        }

        if (failedCount > 0){
            exeReport.append(failedCount + " out of " + replaceArksCount + " replace arks failed.");
        }

        // report for download
        String downloadPath = "/damsmanager/downloadLog.do?submissionId=" + submissionId
            + "&file=" + getArkReportFile().getName();
        String downloadMessage = "\nThe report is ready for <a href=\"" + downloadPath + "\">download</a>.\n";

        exeReport.append(downloadMessage);

        String exeInfo = exeReport.toString();
        log("log", exeInfo);
        return exeInfo;
    }
}
