package edu.ucsd.library.xdre.collection;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Workbook;

import edu.ucsd.library.xdre.tab.RDFExcelConvertor;
import edu.ucsd.library.xdre.utils.Constants;
import edu.ucsd.library.xdre.utils.DAMSClient;
import edu.ucsd.library.xdre.utils.RDFStore;

/**
 * 
 * BatchExportHandler: export metadata in Excel/CSV format
 * @author lsitu@ucsd.edu
 */
public class BatchExportHandler extends BatchRdfMetadataExportHandler {
    private static Logger log = Logger.getLogger(BatchExportHandler.class);

    public static String DAMS42JSON_EXPORT_XSL_FILE = "/resources/dams42json-export.xsl";

    protected String visibility = null;
    protected String sortField = null;
    protected RDFExcelConvertor convertor = null;

    /**
     * Constructor
     * @param damsClient
     * @param collectionId
     * @param format
     * @param out
     * @throws Exception
     */
    public BatchExportHandler(DAMSClient damsClient, String collectionId, String format, OutputStream out)
            throws Exception{
        this(damsClient, collectionId, format, false, out);
    }

    /**
     * Constructor
     * @param damsClient
     * @param collectionId
     * @param format
     * @throws Exception
     */
    public BatchExportHandler(DAMSClient damsClient, String collectionId, String format, boolean components)
            throws Exception{
        this(damsClient, collectionId, format, components, null);
    }

    /**
     * Constructor
     * @param damsClient
     * @param collectionId
     * @param format
     * @param components
     * @param out
     * @throws Exception
     */
    public BatchExportHandler(DAMSClient damsClient, String collectionId, String format, boolean components,
            OutputStream out) throws Exception{
        super(damsClient, collectionId, format, components, out);

        this.convertor = new RDFExcelConvertor(getDams42JsonExportXsl());
    }

    /**
     * Procedure to populate the RDF metadata
     */
    public boolean execute() throws Exception {

        String subjectId = null;

        long start = System.currentTimeMillis();

        itemsCount = items.size();
        for (int i = 0; i < itemsCount; i++) {
            count++;
            subjectId = items.get(i);

            // Reset the RDFStore for the current record
            rdfStore = new RDFStore();

            try {
                setStatus("Processing export for subject " + subjectId  + " (" + (i+1) + " of " + itemsCount + ") ... " ); 

                processRdf(damsClient.getFullRecord(subjectId, true).asXML());

                String visibility = getVisibility(subjectId);
                try (OutputStream out = new ByteArrayOutputStream();) {
                    rdfStore.write(out, "RDF/XML-ABBREV");
                    convertor.addRdfSource(out.toString(), (exportByArk(subjectId) ? subjectId : null), visibility, isCollection(subjectId));
                }

                logMessage("Metadata exported for subject " + subjectId + ".");
            } catch (Exception e) {
                exeResult = false;
                failedCount++;
                e.printStackTrace();
                logError("Metadata export failed (" +(i+1)+ " of " + itemsCount + "): " + e.getMessage());
            }
            setProgressPercentage( ((i + 1) * 100) / itemsCount);

            try{
                Thread.sleep(10);
            } catch (InterruptedException e1) {
                failedCount++;
                logError("Metadata export interrupted: " + subjectId  + ". Error: " + e1.getMessage() + ".");
                setStatus("Canceled");
                clearSession();
                break;
            }
        }

        long end = System.currentTimeMillis();
        log.info("Total elapsed time for converting rdf to json: " + (end - start)/1000 + " seconds");

        File destFile = format.equalsIgnoreCase("excel") ? getExcelFile(submissionId) : getCsvFile(submissionId);
        try (OutputStream outDest = new FileOutputStream(destFile)) {

            logMessage("Converting to " + format.toUpperCase() + " format ...");

            if (StringUtils.isNotBlank(sortField)) {
                convertor.sortObjectBy(sortField);
            }

            if (format.equalsIgnoreCase("excel")) {
                // Batch export in excel format
                Workbook workbook = convertor.convert2Excel(true);
                workbook.write(outDest);
            } else {
                // Batch export in CSV format
                outDest.write(convertor.convert2CSV(true).getBytes("UTF-8"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            exeResult = false;
            logError("Export " + format.toUpperCase() + " format failed: " + e.getMessage());
        }

        log.info("Total elapsed time for converting JSON to " + format.toUpperCase() + " format: " + (System.currentTimeMillis() - end)/1000 + " seconds");

        return exeResult;
    }

    /**
     * Get visibility of the object/collection: not needed for batch export.
     * @param oid
     * @return
     * @throws Exception 
     */
    protected String getVisibility(String oid) throws Exception {
        return null;
    }

    /*
     * Flag to ensure collection records are always exported by ARK to avoid other nested collections are selected. 
     * @param oid
     * @return
     * @throws Exception
     */
    protected boolean exportByArk(String oid) throws Exception {
        return exportByArks ? exportByArks : isCollection(oid);
    }

    /*
     * Flag for collection record. 
     * @param oid
     * @return
     * @throws Exception
     */
    protected boolean isCollection(String oid) throws Exception {
        return collectionArks.contains(oid.substring(oid.lastIndexOf("/") + 1));
    }

    /**
     * Sort objects by the field
     * @param sortField
     */
    public void setSortField(String sortField) {
        this.sortField = sortField;
    }

    /**
     * Retrieve the dams42Json-export XSL as InputStream from provided in source code
     * @return
     * @throws FileNotFoundException
     */
    public String getDams42JsonExportXsl() throws FileNotFoundException {
        return getClass().getResource(DAMS42JSON_EXPORT_XSL_FILE).getPath();
    }

    /**
     * CSVl export filename
     * @param submissionId
     */
    public static File getCsvFile(String submissionId) {
        return new File(Constants.TMP_FILE_DIR, toolName + "-" + submissionId + ".csv");
    }

    /**
     * Excel export filename
     * @param submissionId
     */
    public static File getExcelFile(String submissionId) {
        return new File(Constants.TMP_FILE_DIR, toolName + "-" + submissionId + ".xlsx");
    }
}
