package edu.ucsd.library.xdre.collection;

import static edu.ucsd.library.xdre.model.Subject.ELEMENT_LIST;
import static edu.ucsd.library.xdre.model.Subject.HAS_CLOSE_EXTERNAL_AUTHORITY;
import static edu.ucsd.library.xdre.model.Subject.HAS_EXACT_EXTERNAL_AUTHORITY;
import static edu.ucsd.library.xdre.model.Subject.HAS_VARIANT;
import static edu.ucsd.library.xdre.model.Subject.MADS;
import static edu.ucsd.library.xdre.model.Subject.RDF_RESOURCE;
import static edu.ucsd.library.xdre.model.Subject.VARIANT;
import static edu.ucsd.library.xdre.model.Subject.VARIANT_LABEL;
import static edu.ucsd.library.xdre.model.Subject.XML_LANG;
import static edu.ucsd.library.xdre.model.Subject.elementsToOverlay;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.Node;

import edu.ucsd.library.xdre.tab.SubjectTabularEditRecord;
import edu.ucsd.library.xdre.tab.SubjectTabularRecord;
import edu.ucsd.library.xdre.utils.DAMSClient;

/**
 * 
 * SubjectOverlayHandler: Overlay metadata in DAMS4 RDF/XML format for subjects.
 * @author lsitu
 */
public class SubjectOverlayHandler extends SubjectImportHandler {
    public static final String XPATH_SUBJECT_ELEMENT = "/rdf:RDF/*";

    public SubjectOverlayHandler(DAMSClient damsClient, List<SubjectTabularRecord> subjects) throws Exception {
        super(damsClient, subjects);
    }

    /**
     * Determine whether SOLR update needed for link records or not
     * @param subject
     * @return
     * @throws Exception 
     */
    @Override
    protected boolean needLinkedRecordsIndex(SubjectTabularRecord subject) throws Exception {
        return needLinkedRecordsIndex((SubjectTabularEditRecord)subject);
    }

    //@Override
    protected String getToolName() {
        return "Heading overlay";
    }

    public static boolean needLinkedRecordsIndex(SubjectTabularEditRecord subject) throws Exception {
        SubjectTabularEditRecord record = (SubjectTabularEditRecord)subject;
        Document originalDoc = record.getDocument();
        Document overlayDoc = record.toRDFXML();

        // Subject type change
        String subjectTypeOrigin = originalDoc.selectSingleNode(XPATH_SUBJECT_ELEMENT).getName();
        String subjectTypeOverlay = overlayDoc.selectSingleNode(XPATH_SUBJECT_ELEMENT).getName();
        if (!subjectTypeOrigin.equals(subjectTypeOverlay)) {
            return true;
        }

        // If exactMatch changes, so it must be something changed that need solr update for linked records.
        String xpath = XPATH_SUBJECT_ELEMENT + "/" + MADS + ":" +  HAS_EXACT_EXTERNAL_AUTHORITY + "/@" + RDF_RESOURCE;
        if (!isElementChanged(originalDoc, overlayDoc, xpath)) {
            return true;
        }

        // Elements change that need solr update for linked records.
        for (String elem : elementsToOverlay) {
            if (!elem.endsWith(HAS_EXACT_EXTERNAL_AUTHORITY)) {
                xpath = XPATH_SUBJECT_ELEMENT + "/";

                // Variant path: mads:hasVariant/mads:Variant/mads:variantLabel
                xpath += elem.endsWith(HAS_VARIANT) ? elem + "/" + MADS + ":" + VARIANT + "/" + MADS + ":" + VARIANT_LABEL
                        : elem.endsWith(HAS_CLOSE_EXTERNAL_AUTHORITY) || elem.endsWith(HAS_EXACT_EXTERNAL_AUTHORITY) ? elem + "/@" + RDF_RESOURCE
                        : elem;
                if (!(elem.endsWith(HAS_EXACT_EXTERNAL_AUTHORITY) || elem.endsWith(ELEMENT_LIST))
                        && isElementChanged(originalDoc, overlayDoc, xpath)) {

                    return subject.isLinkingsUpdate();
                }
            }
        }

        return false;
    }

    /**
     * Determine whether XPath elements from two documents are changed or not
     * @param document1
     * @param document2
     * @param xpath
     * @return
     */
    public static boolean isElementChanged(Document document1, Document document2, String xpath) {
        List<Node> nodes1 = document1.selectNodes(xpath);
        List<Node> nodes2 = document2.selectNodes(xpath);

        if (nodes1.size() != nodes2.size()) {
            return true;
        }

        // Compare element values
        List<String> values1 = getNodesValue(nodes1);
        List<String> values2 = getNodesValue(nodes2);
        Collections.sort(values1);
        Collections.sort(values2);

        if (!String.join("\t", values1).equals(String.join("\t", values2))) {
            return true;
        }

        DocumentHelper.sort(nodes1, ".");
        DocumentHelper.sort(nodes2, ".");

        for (int i = 0; i  < nodes1.size(); i++) {
            Node node1 = nodes1.get(i);
            Node node2 = nodes2.get(i);

            if (node1 instanceof Element && node2 instanceof Element) {
                // Compare element attributes

                // xml:lang
                String xmlLang1 = getAttributeValue((Element)node1, XML_LANG);
                String xmlLang2 = getAttributeValue((Element)node2, XML_LANG);

                return !xmlLang1.equals(xmlLang2);
            }
        }

        return false;
    }

    /*
     * Get attribute value from an element
     * An alternate to Attribute.getAttributeValue(String name), which may return null.
     * @param element
     * @param attribute
     * @return
     */
    private static String getAttributeValue(Element element, String attribute) {
        for (Iterator<Attribute> it = element.attributeIterator(); it.hasNext();) {
            Attribute att = it.next();
            if (attribute.endsWith(att.getName())) {
                return att.getValue();
            }
        }

        return "";
    }

    /*
     * Get string values from list of nodes
     * @param nodes
     * @return
     */
    private static List<String> getNodesValue(List<Node> nodes) {
        List<String> values = new ArrayList<>();
        for (Node node : nodes) {
            values.add(node instanceof Attribute ? node.getStringValue() : node.getText());
        }
        return values;
    }
}
