package edu.ucsd.library.xdre.collection;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Node;
import org.dom4j.XPath;
import org.dom4j.io.SAXReader;

import edu.ucsd.library.xdre.tab.RDFExcelConvertor;
import edu.ucsd.library.xdre.utils.Constants;
import edu.ucsd.library.xdre.utils.DAMSClient;

/**
 * 
 * BatchExportHandler: export metadata in Excel/CSV format
 * @author lsitu@ucsd.edu
 */
public class DamsDataMigrationHandler extends BatchExportHandler {
    private static Logger log = Logger.getLogger(DamsDataMigrationHandler.class);

    private static final String[] REQUIRED_FIELDS = {"source_identifier","model","visibility","parents"};
    private static final String[] GROUP_FIELDS = {"title","title_subtitle","title_alternative","title_alternative_geospatial"};

    private static final String CSV_FOLDER = "csv";
    private static final String CONTENT_FILES_FOLDER = "files";
    private static final String FILE_HEADER_PREFIX = "use:";

    private static final String MODEL_PARAM = "model";
    private static final String PATH_PARAM = "path";

    private static final String MIGRATION_OPTION_IMPORT = "import";
    private static final String MIGRATION_OPTION_MIGRATE = "migrate";

    private static final String DAMS42JSON_MIGRATION_XSL_FILE = "dams42json-migration.xsl";

    private static final String COMET_VISIBILITY_OPEN = "open";
    private static final String COMET_VISIBILITY_CAMPUS = "campus";
    private static final String COMET_VISIBILITY_METADATA_ONLY = "metadata_only";
    private static final String COMET_VISIBILITY_COMET = "comet";

    private static final String DAMS_ROLE_PUBLIC = "public";
    private static final String DAMS_ROLE_LOCAL = "local";

    private static final String PERMISSION_DISCOVER = "discover";
    private static final String PERMISSION_READ = "read";
    private static final String PERMISSION_EDIT = "edit";
    private static final String[] PERMISSION_TYPES = {PERMISSION_DISCOVER, PERMISSION_READ, PERMISSION_EDIT};

    protected String migrationOption = null;
    protected String objectType = "";
    protected String projectName = "";
    protected String collectionArk = "";

    /**
     * Constructor
     * @param damsClient
     * @param collectionId
     * @param format
     * @param components
     * @param out
     * @throws Exception
     */
    public DamsDataMigrationHandler(DAMSClient damsClient, String collectionId, String format,
            String migrationOption, String objectType, String projectName) throws Exception{
        super(damsClient, collectionId, format, true);

        this.migrationOption = migrationOption;
        this.objectType = objectType;
        this.projectName = projectName;

        // Just use the first collection URL for now if there are more than one collections
        String collId = StringUtils.isNotBlank(collectionId) ? collectionId.split(",")[0] : "";
        this.collectionArk = collId.length() > 0 ? collId.substring(collId.lastIndexOf("/") + 1) : "";

        Map<String, String> params = new HashMap<>();
        params.put(MODEL_PARAM, objectType);

        // Full path/key pattern of content files: [MOUNT POINT]/files/[COLLECTION ID]/[ARK FILENAME]
        String bucketPrefix = Paths.get(Constants.DAMS_DATA_MIGRATION_EXPORTS_DIR).toFile().getName();
        String fileKeyPrefix = bucketPrefix + "/" + CONTENT_FILES_FOLDER + (collectionArk.length() > 0 ? "/" + collectionArk : "");
        params.put(PATH_PARAM, fileKeyPrefix);

        this.convertor = new RDFExcelConvertor(null, getDams42JsonExportXsl(collectionArk), REQUIRED_FIELDS, GROUP_FIELDS, params);
    }

    /**
     * Procedure to migrate the RDF metadata for bulkrax import
     */
    public boolean execute() throws Exception {
       boolean result = super.execute();
       if (migrationOption.equalsIgnoreCase(MIGRATION_OPTION_IMPORT) || migrationOption.equalsIgnoreCase(MIGRATION_OPTION_MIGRATE)) {
           File csvFile = getCsvFile(submissionId);

           // For the migrate option: deposit the CSV file to staging area for auto-ingest.
           if (migrationOption.equalsIgnoreCase(MIGRATION_OPTION_MIGRATE)) {
             // The CSV file name will be parsed for workflow project name and bulkrax importer name: [PROJECT_NAME]@[IMPORTER_NAME]@[SUBMISSION_ID].csv
             int objectsSize = items.size() - getCollections().size();
             String csvNameForCollections = "collection" + (getCollections().size() > 1 ? "s" : "") + "-" + collectionArk;
             String csvNameForArks = "arks-" + items.get(0).substring(items.get(0).lastIndexOf("/") + 1);
             String importerName = (collectionArk.length() > 0 ? csvNameForCollections : csvNameForArks) + "-" + objectsSize;
             String importCsvFileName = projectName + "@" + importerName + "@ " + submissionId + ".csv";

             // Deposit the CSV and content files to staging area for bulkrax import
             Path exportCsvPath = Paths.get(Constants.DAMS_DATA_MIGRATION_EXPORTS_DIR, CSV_FOLDER, importCsvFileName);
             FileUtils.copyFile(csvFile, exportCsvPath.toFile());
           }

           // Parse the CSV to extract content files and copy them to staging area
           try (Reader in = new FileReader(csvFile);) {
               CSVFormat csvFormat = CSVFormat.DEFAULT.builder()
                   .setHeader(convertor.getHeaders().toArray(new String[0]))
                   .setSkipHeaderRecord(true)
                   .build();

               Iterable<CSVRecord> records = csvFormat.parse(in);

               List<String> fileHeaders = getFileHeaders(convertor.getHeaders());
               if (fileHeaders.size() > 0) {
                   for (CSVRecord record : records) {
                       copyFilesToStaging(fileHeaders, record);
                   }
               }
           }
       }
       return result && exeResult;
    }

    /**
     * Get visibility of the object/collection
     * @param oid
     * @return
     * @throws Exception
     */
    protected String getVisibility(String oid) throws Exception {
        // Retrieve permissions from damsrepo fedora API: /dams/fedora/objects/[oid]/datastreams/rightsMetadata/content
        String rightsMetadataXml = damsClient.getContentBodyAsString(getRightsMetadataUrl(oid));

        try (InputStream in = new ByteArrayInputStream(rightsMetadataXml.getBytes("UTF-8"))) {
            return getVisibility(new SAXReader().read(in));
        }
    }

    /**
     * Get visibility of object/collection from the document
     * @param doc
     * @return
     * @throws Exception
     */
    public static String getVisibility(Document doc) throws Exception {
        String visibility = COMET_VISIBILITY_COMET;

        Map<String, Set<String>> permissions = getPermissions(doc);

        Set<String> discoverGroups = permissions.get(PERMISSION_DISCOVER);
        Set<String> readGroups = permissions.get(PERMISSION_READ);
        if (discoverGroups.contains(DAMS_ROLE_PUBLIC) && readGroups.contains(DAMS_ROLE_PUBLIC)) {
            return COMET_VISIBILITY_OPEN;
        } else if (discoverGroups.contains(DAMS_ROLE_PUBLIC)) {
            return COMET_VISIBILITY_METADATA_ONLY;
        } else if (discoverGroups.contains(DAMS_ROLE_LOCAL) && readGroups.contains(DAMS_ROLE_LOCAL)) {
            return COMET_VISIBILITY_CAMPUS;
        }
        return visibility;
    }

    /*
     * Get permission groups from the document
     * @param doc
     * @return
     */
    private static Map<String, Set<String>> getPermissions(Document doc) {
        Map<String, Set<String>> permissionGroups = new HashMap<>();

        for (String permissionType : PERMISSION_TYPES) {
            permissionGroups.put(permissionType, getGroups(doc, permissionType));
        }
        return permissionGroups;
    }

    /*
     * Get the group values of the permission type
     * @param doc
     * @param type
     * @return
     */
    private static Set<String> getGroups(Document doc, String permissionType) {
        Set<String> groups = new HashSet<>();

        String xpathExpr = "/v1:rightsMetadata/v1:access[@type='" + permissionType + "']/v1:machine/v1:group";
        XPath xpath = createXPath(doc, xpathExpr);
        List<Node> groupNodes = xpath.selectNodes(doc);
        for (Node groupNode : groupNodes) {
            groups.add(groupNode.getText());
        }
        return groups;
    }

    /*
     * Create XPath with namespaces
     * @param doc
     * @param xpathExpr
     * @return
     */
    private static XPath createXPath(Document doc, String xpathExpr) {
        XPath xpath = DocumentHelper.createXPath(xpathExpr);
        Map<String, String> namespaces = new HashMap<String, String>();
        namespaces.put("v1", doc.getRootElement().getNamespaceURI());
        xpath.setNamespaceURIs(namespaces);

        return xpath;
    }

    /**
     * The URL for rights metadata retrieval.
     * @param oid
     * @return
     */
    public String getRightsMetadataUrl(String oid) {
        return Constants.DAMS_STORAGE_URL.replace("/api", "/fedora/objects/") + oid.substring(oid.lastIndexOf("/") + 1) + "/datastreams/rightsMetadata/content";
    }

    /*
     * Copy files in the CSV record to staging area.
     * @param fileHeaders
     * @param record
     * @throws IOException
     */
    private void copyFilesToStaging(List<String> fileHeaders, CSVRecord record) throws IOException {
        for (String fileHeader : fileHeaders) {
            String filePath = record.get(fileHeader);
            if (StringUtils.isNotBlank(filePath)) {
                // Exclude the root folder/mount point. 
                String arkFilePath = filePath.substring(filePath.indexOf("/") + 1);
                try {
                    copyArkFileToStagingArea(arkFilePath);
                } catch (Exception e) {
                    exeResult = false;
                    failedCount++;
                    e.printStackTrace();
                    logError("Failed to copy file" + filePath  +  " to staging area: " + e.getMessage());
                }
            }
        }
    }

    /*
     * Copy the ark file from local filestore to export location
     * @param arkFilePath
     * @throws IOException
     */
    private void copyArkFileToStagingArea(String arkFilePath) throws IOException {
        String arkFileName = arkFilePath.substring(arkFilePath.lastIndexOf("/") + 1);
        String ark = arkFilePath.split("-")[1];
        Path localStoreFile = Paths.get(Constants.FILESTORE_DIR, DAMSClient.pairPath(ark), arkFileName );

        // Staging area content files path follow pattern: [MOUNT POINT]/files/[COLLECTION ID]/[ARK FILENAME]
        String stagingAreaRelativeDir = arkFilePath.indexOf("/") > 0 ? arkFilePath.substring(0, arkFilePath.lastIndexOf("/")) : "";
        Path exportFileDir = Paths.get(Constants.DAMS_DATA_MIGRATION_EXPORTS_DIR, stagingAreaRelativeDir);
        if (!exportFileDir.toFile().exists()) {
            Files.createDirectories(exportFileDir);
        }
        Path exportFilePath = Paths.get(exportFileDir.toFile().getAbsolutePath(), arkFileName);

        FileUtils.copyFile(localStoreFile.toFile(), exportFilePath.toFile(), true);
    }

    /*
     * Get headers for files
     * @param headers
     * @return
     */
    private List<String> getFileHeaders(List<String> headers) {
        List<String> fileHeaders = new ArrayList<>();
        for (String header : headers) {
            if (header != null && header.trim().startsWith(FILE_HEADER_PREFIX)) {
                fileHeaders.add(header.trim());
            }
        }
        return fileHeaders;
    }

    /**
     * Retrieve the dams42Json-migration XSL as InputStream from provided in source code
     * The xsl for a collection with name that starts with the collection ark will be returned if exist.
     * @return
     * @throws FileNotFoundException
     */
    private String getDams42JsonExportXsl(String collectionArk) throws FileNotFoundException {
        String xsl = Constants.DAMS_DATA_MIGRATION_XSL_DIR + collectionArk + "_" + DAMS42JSON_MIGRATION_XSL_FILE;
        if (!(new File(xsl).exists())) {
            xsl = Constants.DAMS_DATA_MIGRATION_XSL_DIR + DAMS42JSON_MIGRATION_XSL_FILE;
        }

        log.info("Applied XSL for converting " + (collectionArk != null && collectionArk.length() > 0 ? "collection " + collectionArk : "") + ": " + xsl + ".");

        return xsl;
    }
}
