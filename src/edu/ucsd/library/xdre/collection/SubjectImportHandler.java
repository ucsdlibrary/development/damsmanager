package edu.ucsd.library.xdre.collection;

import static edu.ucsd.library.xdre.collection.SubjectMergeReportHandler.lookupLinkingRecords;
import static edu.ucsd.library.xdre.imports.RDFDAMS4ImportTsHandler.getModelLabel;
import static edu.ucsd.library.xdre.model.Subject.DAMS;
import static edu.ucsd.library.xdre.model.Subject.MADS_AUTHORITATIVE_LABEL;
import static edu.ucsd.library.xdre.model.Subject.RDF_ABOUT;
import static edu.ucsd.library.xdre.tab.TabularEditRecord.toArkUrl;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.Node;

import edu.ucsd.library.xdre.tab.SubjectTabularRecord;
import edu.ucsd.library.xdre.utils.Constants;
import edu.ucsd.library.xdre.utils.DAMSClient;
import edu.ucsd.library.xdre.utils.DAMSRepository;

/**
 * 
 * SubjectImportHandler: Import metadata in DAMS4 RDF/XML format for subjects.
 * @author lsitu
 */
public class SubjectImportHandler extends CollectionHandler{
    private static Logger log = Logger.getLogger(SubjectImportHandler.class);

    protected DAMSRepository damsRepository = null;

    protected List<String> recordsIngested = new ArrayList<String>();
    protected List<String> ingestFailed = new ArrayList<String>();
    protected List<String> linkingsUpdateFailed = new ArrayList<String>();
    protected List<String> recordsIndexed = new ArrayList<String>();
    protected StringBuilder arkReport = new StringBuilder();

    protected List<SubjectTabularRecord> subjects = null;

    protected int failedCount = 0;

    /**
     * Constructor
     * @param damsClient
     * @param rdf
     * @param mode
     * @throws Exception
     */
    public SubjectImportHandler(DAMSClient damsClient) throws Exception {
        this(damsClient, new ArrayList<SubjectTabularRecord>());
    }

    /**
     * Constructor
     * @param damsClient
     * @param rdf
     * @param mode
     * @throws Exception
     */
    public SubjectImportHandler(DAMSClient damsClient, List<SubjectTabularRecord> subjects) throws Exception {
        super(damsClient, null);
        this.subjects = subjects;
        this.damsRepository = DAMSRepository.getRepository();
    }

    /*
     * Internal tool name for logging
     */
    protected String getToolName() {
        return "Heading import";
    }

    /**
     * Procedure to populate the subject headings RDF metadata
     */
    @Override
    public boolean execute() throws Exception {
        String message = "";
    
        String oid = null;

        int subSize = subjects.size();

        // initiate ARK report header
        toArkReport(arkReport, "ARK", "Subject type", "Subject term", "Outcome", "Event date", "Note");

        try {
            for(int i = 0 ; i < subjects.size(); i++) {
                boolean succeeded = false;

                SubjectTabularRecord subject  = subjects.get(i);
                oid = subject.recordID();

                Document doc = subject.toRDFXML();
                String term = doc.valueOf("//*[@" + RDF_ABOUT + "]/" + MADS_AUTHORITATIVE_LABEL);

                setStatus("Processing " + getToolName().toLowerCase() + " for record '" + term + "' (" + (i+1) + " of " + subSize + ") ... " );

                String subjectUri = doc.valueOf("//@rdf:about");
                try {
                    if (!(subjectUri.startsWith("http") && subjectUri.indexOf("/ark:/") > 0)) {
                        // Assign ARK
                        oid = damsClient.mintArk(Constants.DEFAULT_ARK_NAME);
                        subjectUri = toArkUrl(oid);
                        doc.selectSingleNode("//@" + RDF_ABOUT).setText(subjectUri);
                    }

                    if (damsClient.exists(oid, "", "")) {
                        succeeded = damsRepository.updateAuthorityRecord(doc);
                    } else {
                        succeeded = damsRepository.createAuthorityRecord(doc) != null;
                    }

                    if (!succeeded) {
                        failedCount++;
                        exeResult = false;

                        message = getToolName() + " for " + oid + " (" + term  + ") failed (" + (i + 1) + " of " + subSize + ").";
                        logMessage( message ); 
                        log.error("RDF: \n" + doc.asXML());
                    } else {
                        message = getToolName() + " for " + oid + " (" + term  + ") succeeded (" + (i + 1) + " of " + subSize + "). ";
                        logMessage(message); 
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    exeResult = false;

                    ingestFailed.add(oid + " (" + term + ")");
                    message = getToolName() + " failed for " + oid + " (" + term  + "): " + e.getMessage();
                    logMessage( message  + " (" + (i + 1) + " of " + subSize + ")."); 
                }

                Collection<String> linkingRecords = null;
                if (succeeded) {
                    try{
                        // Update SOLR for the record.
                        if (updateSolr && !updateSOLR(oid)) {
                            failedCount++;
                            exeResult = false;
                            succeeded = false;
                            solrFailed.add(oid);
                            message = "SOLR index for " + oid + " (" + damsClient.getRequestURL() + ") failed.";
                            logMessage(message);
                        } else if (needLinkedRecordsIndex(subject)) {
                            linkingRecords = lookupLinkingRecords(damsClient, subjectUri);

                            boolean reIndexNeeded = false;
                            for (String rec : linkingRecords) {
                                // Linking predicate update for subject type change
                                if (subject.isSubjectTypeModified()) {
                                    Document obj = damsClient.getRecord(rec);
                                    if (updatePredicate(obj, doc) && !damsClient.updateObject(rec, obj.asXML(), Constants.IMPORT_MODE_ALL)) {
                                        succeeded = false;
                                        exeResult = false;
                                        linkingsUpdateFailed.add(rec);
                                        message = "Linking record " + subjectUri  + " of authoritity " + rec  + " (" + damsClient.getRequestURL() + ") failed.";
                                        logMessage(message);
                                    }
                                }

                                // Update linking records in solr
                                if (succeeded && (!recordsIndexed.contains(rec) || reIndexNeeded)) {
                                    if (updateSolr && !updateSOLR(rec)) {
                                        succeeded = false;
                                        exeResult = false;
                                        solrFailed.add(rec);
                                        message = "SOLR index of " + rec  + " linking  to " + subjectUri + " (" + damsClient.getRequestURL() + ") failed.";
                                        logMessage(message);
                                    } else if (!recordsIndexed.contains(rec)) {
                                        recordsIndexed.add(rec);
                                    }
                                }
                            }
                        }

                        if (succeeded) {
                            recordsIngested.add(oid + " (" + term + ")");
                        }
                    } catch(Exception e) {
                        e.printStackTrace();
                        exeResult = false;
                        succeeded = false;
                        message = "SOLR Index failed for " + oid;
                        logMessage(message);
                    }
                }

                setProgressPercentage( ((i + 1) * 100) / subSize);

                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    interrupted = true;
                    exeResult = false;
                    failedCount++;
                    ingestFailed.add(oid + " (" + term + ")");
                    message = getToolName() + " interrupted during processing subject " + oid  + ". \n Error: " + e.getMessage() + "\n";
                    setStatus("Canceled");
                    clearSession();
                    log.error(message);

                    logMessage("Client Cancled - " + damsDateFormat.format(new Date()));
                }

                // ark report for the authority record
                String linkingsMessage = linkingRecords != null ? "Total " + linkingRecords.size() + " linking record(s) affected." : "";
                String resultMessage = succeeded ? linkingsMessage : message;
                authorityReport(doc, oid, term, (exeResult ? "Success" : "Failed"), resultMessage);
            }
        } finally {
            // export ark report to file
            File arkReportFile = getArkReportFile();
            try (FileOutputStream out = new FileOutputStream(arkReportFile);) {
                out.write(arkReport.toString().getBytes());
            }
        }

        return exeResult;
    }

    /**
     * Determine whether SOLR update needed for link records or not
     * @param subject
     * @return
     * @throws Exception 
     */
    protected boolean needLinkedRecordsIndex(SubjectTabularRecord subject) throws Exception {
        return subject.isLinkingsUpdate();
    }

    /*
     * Build ark report
     * @param record
     * @param oid
     * @param term
     * @param outcome
     */
    private void authorityReport(Node record, String oid, String term, String outcome, String message) {
        Node subElem = record.selectSingleNode("//*[@" + RDF_ABOUT + "]");
        String modelLabel = getModelLabel(subElem);
        String ark = oid.substring(oid.indexOf("/") + 1);

        toArkReport(arkReport, ark, modelLabel, term, outcome, damsDateFormat.format(new Date()), message);
    }

    /*
     * Update object for linking predicates
     * @param object
     * @param authority
     * @return
     * @throws Exception
     */
    private boolean updatePredicate(Document object, Document authority) throws Exception {
        String authUri = authority.selectSingleNode("//@" + RDF_ABOUT).getStringValue();
        List<Node> nodes = object.selectNodes("//*[@rdf:resource='" + authUri + "']");

        boolean updated = false;
        for (Node node : nodes) {
            String currPredName = node.getName();
            String modelName = authority.selectSingleNode("//*[@" + RDF_ABOUT + "]").getName();
            String newPredName = getPredicateName(currPredName, modelName);

            if (!currPredName.equals(newPredName)) {
                node.setName(DAMS + ":" + newPredName);
                updated = true;
            }
        }
        return updated;
    }

    /*
     * Get linking predicate of the authority record
     * predicate - current predicate
     * modelName - the model name
     * @return the predicate name
     */
    private String getPredicateName( String predicate, String modelName ) {
        String rightsHolder = "rightsHolder";
        String name = "Name";

        if (predicate.endsWith("role")) {
            return predicate;
        } else if (predicate.indexOf(rightsHolder) >= 0) {
            String preSubfix = modelName.equals(name) ? name : modelName.replace(name, "");
            return rightsHolder + preSubfix;
        }

        return modelName.substring(0, 1).toLowerCase() + modelName.substring(1);
    }

    /**
     * Return the action name
     * @param isOverlay
     * @return
     */
    public static String getActionName(boolean isOverlay) {
        return isOverlay ? "overlay" : "import";
    }

    /**
     * Execution result message
     */
    @Override
    public String getExeInfo() {
        String action = getActionName(getToolName().toLowerCase().indexOf("overlay") >= 0);

        if (exeResult) {
            exeReport.append("Successfully "+ action + " " + recordsIngested.size() + " heading" + (subjects.size() > 1 ? "s" : "") + ":\n");
            for (String record : recordsIngested) {
                exeReport.append("* " + record + "\n");
            }
        } else {
            exeReport.append(getToolName() + " failed - " + failedCount + " of " + subjects.size() + " failed:\n");
            if (ingestFailed.size() > 0) {
                exeReport.append("Error with the following record(s): \n");
                for (String record : ingestFailed) {
                    exeReport.append("* " + record + "\n");
                }
            }

            if (linkingsUpdateFailed.size() > 0) {
                exeReport.append("Failed to update predicate for the following linking record(s): \n");
                for (String record : linkingsUpdateFailed) {
                    exeReport.append("* " + record + "\n");
                }
            }

            if (solrFailed.size() > 0) {
                exeReport.append("Failed to update SOLR for the following record(s): \n");
                for (String record : solrFailed) {
                    exeReport.append("* " + record + "\n");
                }
            }

            if (recordsIngested.size() > 0) {
                exeReport.append("\nThe following record " + (recordsIngested.size() > 0 ? "s are ": " is ") + action + " successfully: \n");

                for (String record : recordsIngested) {
                    exeReport.append("* " + record + "\n");
                }
            }
        }

        if (recordsIndexed.size() > 0) {
            exeReport.append("\nTotal " + recordsIndexed.size() + " linking record(s) affected.\n");
        }

        log("log", "\n______________________________________________________________________________________________");

        String exeInfo = exeReport.toString();
        logMessage(exeInfo);

        return exeInfo;
    }
}
