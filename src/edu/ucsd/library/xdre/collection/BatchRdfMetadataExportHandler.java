package edu.ucsd.library.xdre.collection;

import java.io.File;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.hp.hpl.jena.rdf.model.Resource;

import edu.ucsd.library.xdre.utils.Constants;
import edu.ucsd.library.xdre.utils.DAMSClient;
import edu.ucsd.library.xdre.utils.RDFStore;

/**
 * 
 * BatchRdfMetadataExportHandler: export RDF/XML metadata in batch
 * @author lsitu@ucsd.edu
 */
public class BatchRdfMetadataExportHandler extends MetadataExportHandler {
    private static Logger log = Logger.getLogger(BatchRdfMetadataExportHandler.class);

    protected static final String toolName = "BatchExport";

    // Flag for export with list of ARKs
    protected boolean exportByArks = false;

    protected Set<String> collectionArks = new HashSet<>();

    /**
     * Constructor
     * @param damsClient
     * @param collectionId
     * @param format
     * @param out
     * @throws Exception
     */
    public BatchRdfMetadataExportHandler(DAMSClient damsClient, String collectionId, String format, OutputStream out)
            throws Exception{
        this(damsClient, collectionId, format, false, out);
    }

    /**
     * Constructor
     * @param damsClient
     * @param collectionId
     * @param format
     * @param components
     * @throws Exception
     */
    public BatchRdfMetadataExportHandler(DAMSClient damsClient, String collectionId, String format, boolean components,
            OutputStream out) throws Exception{
        super(damsClient, format, out);
        this.components = components;
        initHandler(collectionId);
    }

    private void initHandler(String collectionId) throws Exception{
        rdfStore = new RDFStore();
        items = new ArrayList<>();

        // handling items in the collection list
        if (StringUtils.isNotBlank(collectionId)) {
            String[] collections = collectionId.split("\\,");
            for (int i = 0; i< collections.length; i++) {
                if (collections[i] != null && collections[i].trim().length() > 0) {
                    String collArk = collections[i].substring(collections[i].lastIndexOf("/") + 1).trim();
                    collectionArks.add(collArk);

                    List<String> objs = damsClient.listObjects(collArk);
                    for (String obj : objs) {
                        if (!items.contains(obj)) {
                            items.add(obj);
                        }
                    }
                }
            }

            // Exclude collections from the export list
            this.items.removeAll(damsClient.listCollections().values());
        }
    }

    /**
     * Collections to export
     * @return
     */
    public Set<String> getCollections() {
        return collectionArks;
    }

    public boolean isExportByArks() {
        return exportByArks;
	}

    public void setExportByArks(boolean exportByArks) {
        this.exportByArks = exportByArks;
    }

    public void addItems(Collection<String> items) {
        List<String> itemsToAdd = new ArrayList<String>(items);
        itemsToAdd.removeAll(this.items);
        this.items.addAll(itemsToAdd);
    }

    public String getFileUri() {
        return fileUri;
    }

    public void setFileUri(String fileUri) {
        this.fileUri = fileUri;
    }

    /**
     * Procedure to populate the RDF metadata
     */
    public boolean execute() throws Exception {

        String subjectId = null;

        itemsCount = items.size();
        for (int i = 0; i < itemsCount; i++) {
            count++;
            subjectId = items.get(i);
            try {
                setStatus("Processing export for subject " + subjectId  + " (" + (i+1) + " of " + itemsCount + ") ... " ); 

                processRdf(damsClient.getMetadata(subjectId, "xml"));

                logMessage("Metadata exported for subject " + subjectId + ".");
            } catch (Exception e) {
                exeResult = false;
                failedCount++;
                e.printStackTrace();
                logError("Metadata export failed (" +(i+1)+ " of " + itemsCount + "): " + e.getMessage());
            }
            setProgressPercentage( ((i + 1) * 100) / itemsCount);

            try{
                Thread.sleep(10);
            } catch (InterruptedException e1) {
                exeResult = false;
                failedCount++;
                logError("Metadata export interrupted: " + subjectId  + ". Error: " + e1.getMessage() + ".");
                setStatus("Canceled");
                clearSession();
                break;
            }
        }

        rdfStore.write(out, format);

        return exeResult;
    }

    /*
     * Process the RDF/XML
     * @param rdf
     * @throws Exception
     */
    protected void processRdf(String rdf) throws Exception {
        RDFStore iStore =  new RDFStore();
        iStore.loadRDFXML(rdf);

        // exclude components from export
        if (!components) {
            trimStatements(iStore);
            // Merge the resources
            List<Resource> resIds = iStore.listURIResources();
            for(Iterator<Resource> it=resIds.iterator(); it.hasNext();){
                Resource res = it.next();
                rdfStore.merge(iStore.querySubject(res.getURI()));
            }
        } else {
            rdfStore.mergeObjects(iStore);
        }
    }

    /**
     * Execution result message
     */
    public String getExeInfo() {
        exeReport.append((format.startsWith("RDF/XML")?"RDF/XML":format.toUpperCase()) + " metadata export ");
        if(exeResult)
            exeReport.append(" is ready" + (fileUri!=null?" for <a href=\"" + fileUri + "\">download</a>":"") + ":\n");
        else
            exeReport.append("failed (" + failedCount + " of " + count + " failed): \n ");    
        exeReport.append("- Total items found " + itemsCount + ". \n- Number of items processed " + count + ".");
        String exeInfo = exeReport.toString();
        logMessage(exeInfo);
        return exeInfo;
    }

    public static File getRdfFile(String submissionId) {
        return new File(Constants.TMP_FILE_DIR, toolName + "-" + submissionId + "-rdf.xml");
    }
}
