package edu.ucsd.library.xdre.collection;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.json.simple.JSONObject;

import edu.ucsd.library.xdre.utils.DAMSClient;

/**
 * 
 * SubjectMergeHandler: Merge authority records.
 * @author lsitu@ucsd.edu
 */
public class SubjectMergeHandler extends SubjectMergeHandlerBase {
    private static Logger log = Logger.getLogger(SubjectMergeHandler.class);

    private int solrFailedCount = 0;

    private boolean[] mergeStatus = null;

    /**
     * Constructor
     * @param damsClient
     * @param authorityArk
     * @throws Exception
     */
    public SubjectMergeHandler(DAMSClient damsClient)
            throws Exception{
        super(damsClient);
    }

    /**
     * Set authority ARKs to use
     * @param items
     */
    @Override
    public void setItems(List<String> items) {
        super.setItems(items);

        mergeStatus = new boolean[items.size()];
    }

    /**
     * Procedure to perform merging subjects
     */
    public boolean execute() throws Exception {

        String subjectURI = null;
        String subjectLabel = null;
        String subjectType = null;
        List<String> replaceArks = null;

        int curIndex = 0;
        int totalItems = getItemsCount(replaceArksList);

        try {
            logMessage ("Subject merging status:\n[Subject]   -   [Merged To]   -   [Status]   -   [Timestamp]"
                    + "   -   [Linked records]   -   [Result message]");
            toArkReport (arkReport, "ARK", "Label", "Subject type", "Outcome", "Event date", "Linked records", "Result message");

            for(int i = 0; i < itemsCount && !interrupted; i++) {
                count++;

                String authorityArk = items.get(i);
                replaceArks = replaceArksList.get(i);

                Document authorityDoc = damsClient.getRecord(authorityArk);
                authorityDocuments[i] = authorityDoc;

                String authorityLabel = getSubjectLabel(authorityDoc);

                boolean mergeSucceeded = true;
                for(int j = 0; j < replaceArks.size() && !interrupted; j++) {
                    curIndex++;

                    String subject = replaceArks.get(j);

                    setStatus("Processing merging subject " + subject +  " to authority " + authorityArk
                            + " (" + curIndex + " of " + totalItems + ") ... " );

                    setProgressPercentage( (curIndex * 100) / totalItems);

                    boolean successful = false;
                    String message = "";
                    String effectedRecords = "";

                    Document subjectDoc = damsClient.getRecord(subject);
                    subjectURI = getSubjectUri(subject, subjectDoc);
                    subjectType = getModelName(subjectDoc);
                    subjectLabel = getSubjectLabel(subjectDoc);

                    try {

                        // Merge subject
                        String[] subsToMerge = { subject };
                        JSONObject result = damsClient.mergeRecords(authorityArk, subsToMerge);
                        message = (String)result.get("message");
                        effectedRecords = (String)result.get("effectedRecords");

                        message = "Merged subject " + subject + " to " + authorityArk + "('" + authorityLabel + "'):" + message.substring(message.lastIndexOf(":") + 1);
                        setStatus(message);

                        successful = true;
                    } catch (Exception ex) {
                        failedCount++;
                        exeResult = false;
                        String errorTitle = "Failed to merge " + subjectURI + " to " + authorityArk;
                        log.error(errorTitle, ex);
                        message = errorTitle + ": " + ex.getMessage();
                    }

                    if (updateSolr && effectedRecords != null && effectedRecords.length() > 0) {
                        String[] recordsToIndex = effectedRecords.split("\\|");

                        for (String record : recordsToIndex) {
                            record = record != null ? record.trim() : "";
                            if (record.length() > 0 && !updateSOLR(record)) {
                                solrFailedCount++;
                                exeResult = false;
                                successful = false;
                            }
                        }

                        if (solrFailedCount > 0) {
                            String solrFailedMessage = "\nThe following record" + (solrFailed.size() > 1 ? "s are " : " is ") + " failed to push for SOLR update:\n";
                            solrFailedMessage += String.join("\n", solrFailed);

                            message += solrFailedMessage;
                        }
                    }

                    logMessage ("* " + subjectURI
                                     + " - "+ subjectLabel
                                     + " - " + subjectType
                                     + " - " + (successful ? "successful" : "failed")
                                     + " - " + damsDateFormat.format(System.currentTimeMillis())
                                     + " - " + effectedRecords == null ? "" : toArksString(Arrays.asList(effectedRecords.split("\\|")))
                                     + " - " + message);

                    toArkReport (arkReport,
                                 subjectURI,
                                 subjectLabel, 
                                 subjectType,
                                 successful ? "successful" : "failed",
                                 damsDateFormat.format(System.currentTimeMillis()),
                                 effectedRecords == null ? "" : toArksString(Arrays.asList(effectedRecords.split("\\|"))),
                                 message);

                    setProgressPercentage( (curIndex * 100) / totalItems);

                    try{
                        Thread.sleep(100);
                    } catch (InterruptedException e1) {
                        failedCount++;
                        exeResult = false;
                        interrupted = true;
                        logError("Subject merge canceled on " + subjectURI  + ".");
                        setStatus("Canceled");
                        clearSession();
                    }

                    mergeSucceeded = !successful ? false : mergeSucceeded;
                }

                mergeStatus[i] = mergeSucceeded;
            }

        } finally {
            // export ark report to file
            File arkReportFile = getArkReportFile();
            try (FileOutputStream out = new FileOutputStream(arkReportFile);) {
                out.write(arkReport.toString().getBytes());
            }
        }

        return exeResult;
    }

    /**
     * Execution result message
     */
    public String getExeInfo() {
        int replaceArksCount = SubjectMergeHandler.getItemsCount(replaceArksList);

        for (int i = 0; i < items.size(); i++) {
            String authorityArk = items.get(i);
            List<String> replaceArks = replaceArksList.get(i);

            Document authDoc = authorityDocuments[i];
            String authorityLabel = getSubjectLabel(authDoc);
            String authorityType = getModelName(authDoc);
 
            String reportLabel = "authorities [" +String.join(", ", replaceArks)+ "] to "
                + authorityArk + " [" + authorityLabel + ", " + authorityType + "]";

            if(mergeStatus[i])
                exeReport.append("Successfully merged " + reportLabel + ".\n ");
            else
                exeReport.append("Failed to merge " + reportLabel + ".\n ");
        }

        if (failedCount > 0){
            exeReport.append(failedCount + " out of " + replaceArksCount+ " replace arks failed.");
        }

        // report for download
        String downloadPath = "/damsmanager/downloadLog.do?submissionId=" + submissionId
            + "&file=" + getArkReportFile().getName();
        String downloadMessage = "\nHere is the ARK report <a href=\"" + downloadPath + "\">download</a>.\n";

        exeReport.append(downloadMessage);

        String exeInfo = exeReport.toString();
        log("log", exeInfo);

        return exeInfo;
    }
}
