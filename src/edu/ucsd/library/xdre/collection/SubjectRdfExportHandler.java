package edu.ucsd.library.xdre.collection;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.Node;

import edu.ucsd.library.xdre.utils.Constants;
import edu.ucsd.library.xdre.utils.DAMSClient;
import edu.ucsd.library.xdre.utils.RDFStore;

/**
 * 
 * SubjectRdfExportHandler: export subject headings in RDF format
 * @author lsitu@ucsd.edu
 */
public class SubjectRdfExportHandler extends BatchRdfMetadataExportHandler {
    private static Logger log = Logger.getLogger(SubjectRdfExportHandler.class);

    // Headings that need to export
    protected static final String[] HEADING_ELEMENNTS = {
            "anatomy",
            "commonName",
            "cruise",
            "culturalContext",
            "lithology",
            "scientificName",
            "series",
            "authority",
            "complexSubject", 
            "conferenceName",
            "corporateName",
            "familyName",
            "genreForm",
            "geographic",
            "language",
            "name",
            "occupation", 
            "personalName",
            "temporal",
            "topic",
            "role",
            "rightsHolder",
            "rightsHolderName",
            "rightsHolderCorporate",
            "rightsHolderPersonal",
            "rightsHolderFamily",
            "rightsHolderConference"
    };

    // List to hold authority records passing from user input
    protected List<String> headings = new ArrayList<>();

    /**
     * Constructor
     * @param damsClient
     * @param collectionId
     * @param format
     * @param  out
     * @throws Exception
     */
    public SubjectRdfExportHandler(DAMSClient damsClient, String collectionId, String format,
            OutputStream out) throws Exception{
        super(damsClient, collectionId, format, out);
    }

    /**
     * Override to keep headings in [headings] list. 
     * While [items] list is used to hold object records from collection.
     * @param items
     */
    @Override
    public void addItems(Collection<String> items) {
        this.headings.addAll(items);
    }

    /**
     * Retrieve all headings linking to an object
     * @param oid
     * @throws UnsupportedEncodingException
     * @throws IOException
     * @throws Exception
     */
    protected void loadObjectHeadings(String oid) throws UnsupportedEncodingException, IOException, Exception {
        List<String> headingElems = Arrays.asList(HEADING_ELEMENNTS);
        Document doc = damsClient.getRecord(oid);

        RDFStore iStore =  null;
        List<Node> resNodes = doc.selectNodes("//*[@rdf:resource]");
        for (Node res : resNodes) {

            String elemName = res.getName();
            String resUri = res.valueOf("@rdf:resource");
            if (headingElems.contains(elemName) && !headings.contains(resUri)
                    && resUri.contains("/ark:/" + Constants.ARK_ORG)) {

                iStore =  new RDFStore();
                iStore.loadRDFXML(damsClient.getMetadata(resUri, "xml"));
                rdfStore.mergeObjects(iStore);

                headings.add(resUri);
            }
        }
    }

    protected void loadHeadingRecords() {
        itemsCount = headings.size();
        for (int i = 0; i < itemsCount ; i++) {
            count++;
            String sid = headings.get(i);

            setStatus("Looking up authority records " + sid  + " (" + (i+1) + " of " + itemsCount + ") ... " );

            try {
                processRdf(damsClient.getMetadata(sid, "xml"));

                logMessage("Successfully retrieved authority record " + sid + ".");
                setProgressPercentage( ((i + 1) * 100) / itemsCount);

                try{
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    exeResult = false;
                    failedCount++;
                    logError("Export interrupted while retrieving authority record " + sid  + ": " + e.getMessage() + ".");
                    setStatus("Canceled");
                    clearSession();
                    break;
                }

            } catch (Exception e) {
                exeResult = false;
                failedCount++;
                e.printStackTrace();
                logError("Failed to retrieve authority record " + sid + " (" +(i+1)+ " of " + itemsCount + "): " + e.getMessage());
            }
        }
    }

    /**
     * Procedure to export subject headings
     */
    public boolean execute() throws Exception {
        // Export headings only
        loadHeadingRecords();

        String objId = null;
        itemsCount = items.size();
        for (int i = 0; i < itemsCount; i++) {
            count++;
            objId = items.get(i);

            setStatus("Looking up authority records in " + objId  + " (" + (i+1) + " of " + itemsCount + ") ... " ); 
            try {
                loadObjectHeadings(objId);

                logMessage("Successfully retrieved authority records in " + objId + ".");
                setProgressPercentage( ((i + 1) * 100) / itemsCount);

                try{
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    exeResult = false;
                    failedCount++;
                    logError("Export interrupted while retrieving authority records in " + objId  + ": " + e.getMessage() + ".");
                    setStatus("Canceled");
                    clearSession();
                    break;
                }

            } catch (Exception e) {
                exeResult = false;
                failedCount++;
                e.printStackTrace();
                logError("Failed to retrieve authority records in " + objId + " (" +(i+1)+ " of " + itemsCount + "): " + e.getMessage());
            }
        }

        rdfStore.write(out, format);
 
        return exeResult;
    }

    /**
     * Build result message
     */
    public String getExeInfo() {
        exeReport.append((format.startsWith("RDF/XML")?"RDF/XML":format.toUpperCase()) + " subject headings export ");
        if(exeResult)
            exeReport.append(" is ready" + (fileUri!=null?" for <a href=\"" + fileUri + "\">download</a>":"") + ":\n");
        else
            exeReport.append("failed (" + failedCount + " of " + count + " failed): \n ");    

        exeReport.append("- Total " + (itemsCount == 0 ? headings.size() : itemsCount) + " records found for subject headings export."
                + "\n- Number of records processed " + count + ".");

        String exeInfo = exeReport.toString();
        logMessage(exeInfo);

        return exeInfo;
    }
}
