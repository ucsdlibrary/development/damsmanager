package edu.ucsd.library.xdre.collection;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Workbook;

import edu.ucsd.library.xdre.tab.RDFExcelConvertor;
import edu.ucsd.library.xdre.utils.Constants;
import edu.ucsd.library.xdre.utils.DAMSClient;
import edu.ucsd.library.xdre.utils.RDFStore;

/**
 * 
 * SubjectExportHandler: export subject headings in Excel/CSV format
 * @author lsitu@ucsd.edu
 */
public class SubjectExportHandler extends SubjectRdfExportHandler {
    private static Logger log = Logger.getLogger(SubjectExportHandler.class);

    // Required fields for subject headings
    private static final String[] headingRequiredFields = {"ARK","subject type", "subject term"};
    // Group fields for for subject headings
    private static final String[] headingGroupFields = {};
    // The XSL file used to convert subject heading records
    private static final String DAMS42JSON_SUBJECT_EXPORT_XSL_FILE = "/resources/dams42json-subject-export.xsl";

    protected RDFExcelConvertor convertor = null;

    /**
     * Constructor
     * @param damsClient
     * @param collectionId
     * @param format
     * @param  out
     * @throws Exception
     */
    public SubjectExportHandler(DAMSClient damsClient, String collectionId, String format) throws Exception{
        super(damsClient, collectionId, format, null);

        convertor = new RDFExcelConvertor(null, getDams42JsonExportXsl(),
                headingRequiredFields, headingGroupFields);
    }

    /**
     * Retrieve the dams42json-subject-export.xsl XSL as InputStream from provided in source code
     * for subject headings conversion
     * @return
     * @throws FileNotFoundException
     */
    public String getDams42JsonExportXsl() throws FileNotFoundException {
        return getClass().getResource(DAMS42JSON_SUBJECT_EXPORT_XSL_FILE).getPath();
    }

    /**
     * Procedure to export subject headings
     */
    public boolean execute() throws Exception {

        long start = System.currentTimeMillis();

        // Export headings only
        loadHeadingRecords();

        // Add headings metadata to the RDFExcelConvertor
        try (OutputStream out = new ByteArrayOutputStream();) {
            rdfStore.write(out, "RDF/XML-ABBREV");
            convertor.addRdfSource(out.toString(), null);
        } catch (Exception e) {
            e.printStackTrace();
            exeResult = false;
            logError("Failed to export authority records in " + format.toUpperCase() + " format: " + e.getMessage());
        }

        String objId = null;
        itemsCount = items.size();
        for (int i = 0; i < itemsCount; i++) {
            count++;
            objId = items.get(i);

            setStatus("Looking up authority records in " + objId  + " (" + (i+1) + " of " + itemsCount + ") ... " ); 
            try {
                // Reset the RDFStore for headings linking in the current object
                rdfStore = new RDFStore();
                loadObjectHeadings(objId);

                if (rdfStore.listURISubjects().size() > 0) {
                    try (OutputStream out = new ByteArrayOutputStream();) {
                        rdfStore.write(out, "RDF/XML-ABBREV");
                        convertor.addRdfSource(out.toString(), null);
                    }
                }

                logMessage("Successfully exported authority records linked in " + objId + ".");
                setProgressPercentage( ((i + 1) * 100) / itemsCount);

                try{
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    exeResult = false;
                    failedCount++;
                    logError("Export interrupted while exporting authority records linked in " + objId  + ": " + e.getMessage() + ".");
                    setStatus("Canceled");
                    clearSession();
                    break;
                }

            } catch (Exception e) {
                e.printStackTrace();
                exeResult = false;
                failedCount++;
                logError("Failed to export authority records linked in " + objId + " (" +(i+1)+ " of " + itemsCount + "): " + e.getMessage());
            }
        }

        long end = System.currentTimeMillis();
        log.info("Total elapsed time for converting rdf to json: " + (end - start)/1000 + " seconds");

        File destFile = format.equalsIgnoreCase("excel") ? getExcelFile(submissionId) : getCsvFile(submissionId);
        try (FileOutputStream outDest = new FileOutputStream(destFile);) {
            logMessage("Converting to " + format.toUpperCase() + " format ...");

            if (format.equalsIgnoreCase("excel")) {
                // Export in excel format
                Workbook workbook = convertor.convert2Excel(true);
                workbook.write(outDest);
            } else {
                // Export in CSV format
                outDest.write(convertor.convert2CSV(true).getBytes("UTF-8"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            exeResult = false;
            logError("Export authority records in " + format.toUpperCase() + " format failed: " + e.getMessage());
        }

        log.info("Total elapsed time for converting JSON to " + format.toUpperCase() + " format: " + (System.currentTimeMillis() - end)/1000 + " seconds");

        return exeResult;
    }

    /**
     * CSVl export filename
     * @param submissionId
     */
    public static File getCsvFile(String submissionId) {
        return new File(Constants.TMP_FILE_DIR, toolName + "-" + submissionId + ".csv");
    }

    /**
     * Excel export filename
     * @param submissionId
     */
    public static File getExcelFile(String submissionId) {
        return new File(Constants.TMP_FILE_DIR, toolName + "-" + submissionId + ".xlsx");
    }
}
