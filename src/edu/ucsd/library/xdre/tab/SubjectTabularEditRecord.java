package edu.ucsd.library.xdre.tab;

import static edu.ucsd.library.xdre.model.Subject.RDF_ABOUT;

import java.util.Map;

import org.dom4j.Document;
import org.dom4j.Element;

import edu.ucsd.library.xdre.model.Subject;

/**
 * class SubjectTabularEditRecord for the subject overlay
 * @author lsitu
**/
public class SubjectTabularEditRecord extends SubjectTabularRecord
{
    private Document document = null;

    public SubjectTabularEditRecord( Document document, Map<String,String> data )
    {
        super( data );
        this.document = document;
        this.id = data.get("ark");

        this.linkingsUpdate = true;
    }

    /**
     * Get original document of the overlay subject
     * @return
     */
    public Document getDocument() {
       return document;
    }

    @Override
    protected Element createSubject( Map<String,String> data, String ark ) throws Exception
    {
        Subject subject = buildSubject(data, ark);
        subject.setDocument((Document) document.clone());

        String subjectType = document.selectSingleNode("//*[@" + RDF_ABOUT + "]").getName();
        subjectTypeModified = subjectType.equals(subject.getType());

        return subject.serialize();
    }
}
