package edu.ucsd.library.xdre.tab;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.dom4j.Document;

import edu.ucsd.library.xdre.utils.DAMSClient;

/**
 * class SubjectEditExcelSource for headings overlay.
 * @author lsitu
**/
public class SubjectEditExcelSource extends SubjectExcelSource
{
    public static final String[] ADDITIONAL_OVERLAY_SUBJECTS = {
            "mads:Authority",
            "mads:Language",
            "Subject:Complex",
            "Subject:Name"
    };

    protected DAMSClient damsClient = null;

    public SubjectEditExcelSource(File f, List<String> validFields, DAMSClient damsClient) throws IOException,
            InvalidFormatException {
        super(f, validFields);
        this.damsClient = damsClient;
    }

    @Override
    public Record nextRecord() throws Exception
    {
        Record rec = null;
        if (cache != null)
        {
            rec = new SubjectTabularEditRecord( getRecord(cache), cache );
        }
        else if ( currRow < lastRow )
        {
            cache = parseRow( ++currRow );
            rec = new SubjectTabularEditRecord( getRecord(cache), cache );
        }
        cache = null;
        return rec;
    }

    /*
     * Retrieve the RDF/XML document for the record
     * @param cache
     * @return
     * @throws Exception
     */
    private Document getRecord(Map<String, String> cache) throws Exception {
        String ark = cache.get("ark");
        if (StringUtils.isNotBlank(ark) && damsClient.exists(ark, "", "")) {
            return damsClient.getRecord(ark);
        }
        throw new Exception("Authority record (" + ark + ") for heading overlay doesn't exist!");
    }
}
