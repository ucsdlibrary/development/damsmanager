package edu.ucsd.library.xdre.tab;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.dom4j.Document;
import org.dom4j.Element;
import org.json.simple.JSONObject;

import edu.ucsd.library.xdre.model.DamsSubject;
import edu.ucsd.library.xdre.model.MadsSubject;
import edu.ucsd.library.xdre.model.Subject;

/**
 * class SubjectTabularRecord: A bundle of tabular data, key-value map for the subject
 * @author lsitu
**/
public class SubjectTabularRecord extends TabularRecordBasic
{
    public static final String ARK = "ARK";
    public static final String SUBJECT_TYPE = "subject type";
    public static final String SUBJECT_TERM = "subject term";
    public static final String EXACT_MATCH = "exactMatch";
    public static final String CLOSE_MATCH = "closeMatch";
    public static final String VARIANT = "variant";
    public static final String HIDDEN_VARIANT = "hiddenVariant";

    public static final String[] ALL_FIELDS_FOR_SUBJECTS = { "ARK", SUBJECT_TYPE, SUBJECT_TERM, EXACT_MATCH, CLOSE_MATCH, VARIANT, HIDDEN_VARIANT };

    protected String id = null;
    // Flag for linking records SOLR update
    protected boolean linkingsUpdate = false;
    // Flag for subject type change that may need linking predicate update
    protected boolean subjectTypeModified = false;

    public SubjectTabularRecord ( Map<String,String> data )
    {
        super( data );

        assignId();
    }

    /*
     * Assign ID for the record
     */
    private void assignId() {
        String ark = data.get("ark");
        this.id = StringUtils.isNotBlank(ark) ? ark.trim() : "ARK_" + counter++;
    }

    @Override
    public String recordID()
    {
        return id;
    }

    /**
     * Return the flag for SOLR update of linking records
     */
    public boolean isLinkingsUpdate() {
        return linkingsUpdate;
    }

    /**
     * Set the flag for SOLR update of linking records
     * @param linkingsUpdate
     */
    public void setLinkingsUpdate(boolean linkingsUpdate) {
        this.linkingsUpdate = linkingsUpdate;
    }

    /**
     * Return the flag for subject type change
     */
    public boolean isSubjectTypeModified() {
		return subjectTypeModified;
	}

    /**
     * Set the flag for subject type change
     * @param subjectTypeModified
     */
	public void setSubjectTypeModified(boolean subjectTypeModified) {
		this.subjectTypeModified = subjectTypeModified;
	}

	/**
     * Convert the record to RDF/XML.
     * @throws Exception 
    **/
    public Document toRDFXML() throws Exception
    {
        // get previously-assigned ark
        String ark = data.get("ark");

        // object metadata
        Element subjectElem = createSubject( data, ark );

        return subjectElem.getDocument();
    }

 
    /**
     * fields with the key mapping for the subject structure
     * @throws Exception 
    **/
    protected Element createSubject( Map<String,String> data, String ark ) throws Exception
    {
        return buildSubject( data, ark).serialize();
    }

    /*
     * Build the Subject instance from the key mapping data structure for subject
     * @param data
     * @param ark
     * @return
     * @throws Exception
     */
    protected Subject buildSubject( Map<String,String> data, String ark ) throws Exception
    {
        String elemName = null;
        String subjectType = data.get(SUBJECT_TYPE.toLowerCase());
        if (subjectType == null) {
            // Subject type is required.
            throw new Exception("Subject type is missing: " + new JSONObject(data).toJSONString());
        }

        subjectType = subjectType.substring(subjectType.indexOf(":") + 1);
        String subjectName = toCamelCase(subjectType);
        if ( subjectType.equalsIgnoreCase("genre") )
            subjectName = "GenreForm";
        else if (subjectType.equalsIgnoreCase("Complex"))
            subjectName = "ComplexSubject";
        if ( subjectType.equalsIgnoreCase("personal name") )
            elemName = "FullName";

        String exactMatch = (String)data.get(EXACT_MATCH.toLowerCase());
        String closeMatch = (String)data.get(CLOSE_MATCH.toLowerCase());
        String subjectTerm = (String)data.get(SUBJECT_TERM.toLowerCase());
        String variant = (String)data.get(VARIANT.toLowerCase());
        String hiddenVariant = (String)data.get(HIDDEN_VARIANT.toLowerCase());

        Subject subject = null;
        String uri = StringUtils.isBlank(ark) ? id : TabularEditRecord.toArkUrl(ark);
        switch (subjectType)
        {
            case "conference name":
                elemName = "Name";
            case "corporate name":
                elemName = "Name";
            case "family name":
                elemName = "Name";
            case "personal name":
            case "genre":
            case "geographic":
            case "occupation":
            case "temporal":
            case "topic":
            case "Language":
            case "Authority":
            case "Complex":
            case "Name":
                // mads subject: conference name, corporate name, family name, personal name, genre, geographic, occupation, temporal, topic etc.
                elemName = StringUtils.isBlank(elemName) ? subjectName : elemName;
                subject = new MadsSubject(uri, subjectName, subjectTerm, elemName, exactMatch, closeMatch,
                        variant, hiddenVariant);
                break;
            default:
                // dams subject: scientific name, common name, culturalContext, lithology, series, cruise etc.
                elemName = StringUtils.isBlank(elemName) ? subjectName : elemName;
                subject = new DamsSubject(uri, subjectName, subjectTerm, elemName, exactMatch, closeMatch,
                        variant, hiddenVariant);
                break;
        }

        return subject;
    }
}
