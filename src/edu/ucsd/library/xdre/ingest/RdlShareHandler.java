package edu.ucsd.library.xdre.ingest;

import java.io.File;
import java.util.List;

import org.apache.log4j.Logger;

import edu.ucsd.library.xdre.ingest.fileReplace.ArkFileMapping;
import edu.ucsd.library.xdre.ingest.fileReplace.RdcpFileReplaceConfig;
import edu.ucsd.library.xdre.ingest.fileReplace.TransferredFile;
import edu.ucsd.library.xdre.ingest.rdlshare.RdlShareApiClient;
import edu.ucsd.library.xdre.ingest.rdlshare.RdlShareAttachment;
import edu.ucsd.library.xdre.utils.DAMSClient;

/**
 * Utility class to handle rdcp file replacement with RDL-Share attachments
 * @author lsitu
 *
 */
public class RdlShareHandler extends RdcpFileReplaceHandler {
    private static Logger log = Logger.getLogger(RdlShareHandler.class);

    private RdlShareApiClient apiClient = null;

    /**
     * Constructor
     * @param attachments
     * @param apiClient
     * @param damsClient
     * @throws Exception 
     */
    public RdlShareHandler(RdcpFileReplaceConfig config, String projectPath, List<TransferredFile> transferredFiles,
            RdlShareApiClient apiClient, DAMSClient damsClient, List<ArkFileMapping> arkFileMappings) throws Exception {
        super(config, projectPath, transferredFiles, damsClient, arkFileMappings);

        this.apiClient = apiClient;
    }

    /**
     * Function to replace files in dams with rdl-share attachments
     * @return
     * @throws Exception
     */
    @Override
    public boolean replaceFiles() throws Exception {
        boolean transferStatus = false;
        boolean replaceStatus = false;

        for (TransferredFile transferedFile : transferredFiles) {
            transferStatus = false;
            replaceStatus = false;

            String transferMessage = "";
            String fileReplaceMessage = "";
 
            RdlShareAttachment attachment = (RdlShareAttachment)transferedFile;
            String fileName = attachment.getFileName();

            String fileStatus = transferedFile.isIgnore() ? "Ignore" : transferedFile.isError() ? "Error" : transferedFile.isInvalid() ? "Invalid" : transferedFile.isRedundant() ? "Redundant" : "Valid";
            log.info("Processing file replacement for attachment " + fileName + " (" + fileStatus + ")" + " ...");

            try {
                String attCreatedDate = dateFormat.format(attachment.getCreatedAt());
                if (attachment.isError()) {
                    // report attachments that are failed while downloading
                    transferMessage = "Unable to download " + fileName + " submitted on " + attCreatedDate;
                } else if (attachment.isInvalid()) {
                    // report attachments that have invalid filename pattern
                    transferMessage = "File does not match established naming convention.";
                    if (!apiClient.getRecipients().contains(attachment.getSender())) {
                        transferMessage += " (sent from unknow account " + attachment.getSender() + ")";
                    }
                } else if (!attachment.isIgnore()) {

                    if (!attachment.getChecksumCrc32().equalsIgnoreCase(attachment.getLocalChecksumCrc32())) {
                        transferMessage = "Checksum comparison failed for " + fileName
                                + " submitted on " + attCreatedDate;

                        File corruptedFile = RdcpFileTransfer.getDownloadFile(projectPath, fileName);
                        if (corruptedFile.exists()) {
                            // delete the corrupted file
                            corruptedFile.delete();
                        }

                        continue;
                    }

                    transferStatus = true;
                    try {
                        processFile(attachment);
                    } catch (Exception ex) {
                        transferStatus = false;
                        transferMessage = ex.getMessage();
                    }
                }
            } finally {
                 // delete attachments unless it's failed for file replacement
                 if (attachment.isInvalid() || attachment.isIgnore()
                        || !attachment.getChecksumCrc32().equalsIgnoreCase(attachment.getLocalChecksumCrc32())) {
                    fileReplaceMessage = (fileReplaceMessage.length() > 0 ? fileReplaceMessage + "; " : "") + deleteAttachment(attachment);
                 }

                // generate report for attachments with download error or invalid.
                if (attachment.isError() || attachment.isInvalid()) {
                    reports.add(generateReport(attachment, transferStatus, transferMessage, replaceStatus, fileReplaceMessage));
                }
            }
        }

        // now replace the archive file in dams
        String fileReplaceMessage = "";
        for (ArkFileMapping arkFileMapping : arkFileMappings) {
            replaceStatus = false;
            fileReplaceMessage = "";

            if (arkFileMapping.needArchiveFile() && arkFileMapping.getArchiveFile() != null) {
                try {
                    replaceStatus = replaceFile(arkFileMapping);
                } catch (Exception ex) {
                    fileReplaceMessage = ex.getMessage();

                    log.error(fileReplaceMessage, ex);
                } finally {
                    // cleanup TarGzip intermediate files
                    arkFileMapping.getArchiveFile().cleanUp();

                    // delete attachment and generate reports
                    for (TransferredFile transferredFile : arkFileMapping.getTransferredFiles()) {
                        RdlShareAttachment attachment = (RdlShareAttachment)transferredFile;
                        // delete attachment unless it's failed during file replace
                        if (replaceStatus) {
                            fileReplaceMessage = deleteAttachment((RdlShareAttachment)transferredFile);
                        }

                        // generate report for the attachment
                        reports.add(generateReport(attachment, true, "", replaceStatus, fileReplaceMessage));
                    }
                }
            } else if (!arkFileMapping.needArchiveFile() && arkFileMapping.getTransferredFiles().size() > 0){
                // Upload single files
                List<TransferredFile> transferredFiles = arkFileMapping.getTransferredFiles();
                for (int i = 0; i < transferredFiles.size(); i++) {
                    replaceStatus = false;
                    fileReplaceMessage = "";

                    TransferredFile transferredFile = transferredFiles.get(i);
                    if (transferredFile.isError() || transferredFile.isInvalid() || transferredFile.isIgnore()) {
                        continue;
                    }

                    try {
                        if (i == transferredFiles.size() - 1) {
                            // Upload file
                            replaceStatus = replaceFile(arkFileMapping);
                        } else {
                            // Multiple files for the same ark: don't need to replace
                            transferredFile.setRedundant(true);
                            fileReplaceMessage = "Redundant of " + transferredFiles.get(transferredFiles.size() -1).getFileName() + ".";
                        }
                    } catch (Exception e) {
                        fileReplaceMessage = e.getMessage();
                    } finally {
                        // delete attachments unless it's failed for file replacement
                        if (transferredFile.isRedundant() || replaceStatus) {
                            fileReplaceMessage = (fileReplaceMessage.length() > 0 ? fileReplaceMessage + "; " : "") + deleteAttachment((RdlShareAttachment)transferredFile);
                        }

                        // generate report for the attachment
                        reports.add(generateReport(transferredFiles.get(i), true, "", replaceStatus, fileReplaceMessage));

                        // delete the redundant file
                        if (transferredFile.isRedundant()) {
                           File redundantFile = RdcpFileTransfer.getDownloadFile(projectPath, transferredFile.getFileName());
                            if (redundantFile.exists()) {
                                redundantFile.delete();
                            }
                        }
                    }
                }
            }
        }

        return errorReports.size() == 0;
    }

    /*
     * Remove the attachment from rdl-share
     * @param attachment
     * @return
     */
    private String deleteAttachment(RdlShareAttachment attachment) {
        String fileReplaceMessage = "";
        try {
            if (!apiClient.deleteAttachment(attachment.getMessageUrl())) {
                fileReplaceMessage = "Warn: delete rdl-share attachment failure.";
            }
        } catch (Exception ex) {
            fileReplaceMessage = "Warn: delete rdl-share attachment error: " + ex.getMessage();

            log.error(fileReplaceMessage, ex);
        }
        return fileReplaceMessage;
    }
}
