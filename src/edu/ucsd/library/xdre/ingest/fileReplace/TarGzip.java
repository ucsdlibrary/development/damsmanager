package edu.ucsd.library.xdre.ingest.fileReplace;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorOutputStream;
import org.apache.log4j.Logger;

/**
 * Utility class to handle GZip files.
 * @author lsitu
 *
 */
public class TarGzip extends ArchiveFile {
    private static Logger log = Logger.getLogger(TarGzip.class);

    /**
     * 
     * @param gzipFile
     * @throws IOException
     */
    public TarGzip(File gzipFile) throws IOException {
        super(gzipFile);
    }

    @Override
    public void compress(File destFile) throws FileNotFoundException, IOException {
        // create a TarArchiveOutputStream object
        try (FileOutputStream destOut = new FileOutputStream(destFile);
                BufferedOutputStream bufOut = new BufferedOutputStream(destOut);
                GzipCompressorOutputStream gzipOut = new GzipCompressorOutputStream(bufOut);
                TarArchiveOutputStream tarOut = new TarArchiveOutputStream(gzipOut);) {

            compress(tarOut);

            log.info("Successfully created .tgz file: " + destFile + ".");
        }
    }

    @Override
    protected void decompress() throws IOException {
        try (InputStream fIn = new FileInputStream(archivedFile);
                GzipCompressorInputStream gzipIn = new GzipCompressorInputStream(fIn);
                TarArchiveInputStream tarIn = new TarArchiveInputStream(gzipIn)) {

            decompress(tarIn);
        }
    }
}
