package edu.ucsd.library.xdre.ingest.fileReplace;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Calendar;
import java.util.List;
import java.util.TimerTask;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import edu.ucsd.library.xdre.ingest.RdcpFileReplaceHandler;
import edu.ucsd.library.xdre.ingest.RdcpFileTransfer;
import edu.ucsd.library.xdre.statistic.analyzer.Statistics;
import edu.ucsd.library.xdre.utils.Constants;
import edu.ucsd.library.xdre.web.RdcpFileReplaceTaskController;

/**
 * Runner for RDCP file replacement
 * @author lsitu
 *
 */
public class RdcpFileReplaceRunner extends TimerTask {
    private static Logger logger = Logger.getLogger(RdcpFileReplaceRunner.class);

    public static final String RDCP_STAGING = "rdcp-staging";

    @Override
    public void run() {
        Path rdcpStaging = Paths.get(Constants.DAMS_STAGING, RDCP_STAGING);
        File[] folders = rdcpStaging.toFile().listFiles();
        for (File folder : folders ) {
            // Match the project folder setup for each environment by the postfix of the folder name:
            // development (QA), staging (test), prod (production)
            if (folder.isDirectory() && StringUtils.isNotBlank(Constants.RDCP_UPLOAD_ROOT_POSTFIX)
                && folder.getName().toLowerCase().endsWith(Constants.RDCP_UPLOAD_ROOT_POSTFIX)) {
                File[] projectFolders = folder.listFiles();
                for (File projectFolder : projectFolders) {
                    if (projectFolder.getName().endsWith(RdcpFileTransfer.UPLOAD_PATH_POSTFIX)) {
                        Path configPath = Paths.get(projectFolder.getAbsolutePath())
                                .resolve(RdcpFileTransfer.REPORT_FOLDER)
                                .resolve(RdcpFileTransfer.CONFIG_FILE);
                        if (configPath.toFile().exists()) {
                            RdcpFileReplaceHandler handler = null;

                            try {
                                logger.info("DAMS Mananger start RDCP file replacement task for project " + projectFolder.getName() + " at " +  Statistics.getDatabaseDateFormater().format(Calendar.getInstance().getTime()) + "...");
                                // cleanup backup files
                                RdcpFileTransfer.cleanupBackupFiles(projectFolder.getAbsolutePath());

                                // perform file replacement task 
                                handler = RdcpFileReplaceTaskController.performUploadTask(projectFolder.getAbsolutePath());
                            } catch (Exception e) {
                                logger.error("Error RDCP file replacement for project " + projectFolder.getName(), e);
                            } finally {
                                if (handler != null) {
                                    List<String[]> reports = handler.getReports();
                                    if (reports.size() > 0) {

                                        List<String[]> errors = handler.getErrorReports();
                                        StringBuilder resultBuilder = new StringBuilder();
                                        resultBuilder.append(errors.size() > 0 ? "failed (" + errors.size() + " out of " + reports.size() + " failure):\n"
                                                : "succeeded. Total " + reports.size() + " attachments are processed.");
                                        for (String[] error : errors) {
                                            resultBuilder.append(String.join("\t", error) + "\n");
                                        }

                                        logger.info("RDCP file replacement for project " + projectFolder.getName() + " " + resultBuilder.toString());
                                    }
                                }

                                logger.info("DAMS Mananger finished RDCP file replacement task for " + projectFolder.getName() + " at " + Statistics.getDatabaseDateFormater().format(Calendar.getInstance().getTime()) + ".");
                            }
                        }
                    }
                }
            }
        }
    }

}
