package edu.ucsd.library.xdre.ingest.fileReplace;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;
import java.util.Map;

import edu.ucsd.library.xdre.ingest.RdcpFileTransfer;

/**
 * Class TransferredFile: the data structure for files transferred from data provider
 *
 */
public class TransferredFile implements Comparable<TransferredFile> {
    protected String fileName;           // filename
    protected long fileSize;             // file size
    protected String checksum;           // checksum sha256
    protected String checksumCrc32;      // checksum from attachment
    protected String localChecksumCrc32 = ""; // local file checksum
    protected Date createdAt;            // attachment created date
    protected File file;                 // the source file
    protected boolean ignore = false;    // duplicate attachment
    protected boolean invalid = false;   // invalid filename pattern
    protected boolean error = false;     // error download the attachment
    protected boolean redundant = false; // redundant attachment that is replaced by newer version
    protected boolean archived = false;  // the file is added to a archive file or not

    public TransferredFile(File file) throws FileNotFoundException, IOException {
        this.file = file;
        if (file != null) {
            this.fileName = file.getName();
            this.fileSize = file.length();
            this.checksum = "";
            this.localChecksumCrc32 = checksumCrc32(file);
            this.checksumCrc32 = localChecksumCrc32;
            this.createdAt = new Date(file.lastModified());
        }
    }

    public TransferredFile(String fileName, long fileSize, String checksum,
            String checksumCrc32, Date createdAt) {
        this.fileName = fileName;
        this.fileSize = fileSize;
        this.checksum = checksum;
        this.checksumCrc32 = checksumCrc32;
        this.createdAt = createdAt;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public long getFileSize() {
        return fileSize;
    }

    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }

    public String getChecksum() {
        return checksum;
    }

    public void setChecksum(String checksum) {
        this.checksum = checksum;
    }

    public String getLocalChecksumCrc32() {
        return localChecksumCrc32;
    }

    public void setLocalChecksumCrc32(String localChecksumCrc32) {
        this.localChecksumCrc32 = localChecksumCrc32;
    }

    public String getChecksumCrc32() {
        return checksumCrc32;
    }

    public void setChecksumCrc32(String checksumCrc32) {
        this.checksumCrc32 = checksumCrc32;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public boolean isIgnore() {
        return ignore;
    }

    public void setIgnore(boolean ignore) {
        this.ignore = ignore;
    }

    public boolean isInvalid() {
        return invalid;
    }

    public void setInvalid(boolean invalid) {
        this.invalid = invalid;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public boolean isRedundant() {
        return redundant;
    }

    public void setRedundant(boolean redundant) {
        this.redundant = redundant;
    }

	public boolean isArchived() {
        return archived;
    }

    public void setArchived(boolean archived) {
        this.archived = archived;
    }

    public String checksumCrc32(File file) {
        try {
            return RdcpFileTransfer.checksumCrc32(file.getAbsolutePath());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }


    /**
     * Find duplicate file that need to be ignored by its filename pattern.
     * @param filesMap - Existing files
     * @return - the TransferredFile that is duplicate
     */
    public TransferredFile dedupTransferredFileByDate(Map<String, TransferredFile> filesMap) {
        TransferredFile duplicateFile = null;
        String targetFileName = fileName.substring(fileName.indexOf(RdcpFileTransfer.DATE_DELIMITER) + 1);
        if (filesMap.containsKey(targetFileName)) {
            TransferredFile existing = filesMap.get(targetFileName);
            Date existingDate = RdcpFileTransfer.getDateFromFileName(existing.getFileName());
            Date date = RdcpFileTransfer.getDateFromFileName(fileName);

            if (existingDate.before(date)) {
                // replace the existing attachment with the one with the latest date tag
                filesMap.put(targetFileName, this);

                // ignore the existing attachment
                existing.setIgnore(true);
                duplicateFile = existing;
            } else {
                // ignore the attachment
                setIgnore(true);
                duplicateFile = this;
            }
        } else {
            filesMap.put(targetFileName, this);
        }

        return duplicateFile;
    }

    public int compareTo(TransferredFile o) {
        return this.createdAt.compareTo(o.createdAt) ;
    }
}
