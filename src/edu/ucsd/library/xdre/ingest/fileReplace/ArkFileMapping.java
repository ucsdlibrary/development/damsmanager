package edu.ucsd.library.xdre.ingest.fileReplace;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import edu.ucsd.library.xdre.ingest.RdcpFileTransfer;

/**
 * The model for ARK files mapping
 * @author lsitu
 *
 */
public class ArkFileMapping {
    public static final String ARCHIVE_FORMAT_TARBALL = "TARBALL";
    public static final String ARCHIVE_FORMAT_ZIP = "ZIP";
    public static final String ARCHIVE_FORMAT_NONE = "NONE";

    public String[] ARCHIVE_FORMATS = {ARCHIVE_FORMAT_TARBALL, ARCHIVE_FORMAT_ZIP};

    private String arkFileUri = null;      // the ark file uri that should be mapped to.
    private String fileNameSyntax = null;  // the syntax of a valid filename.
    private String archiveFormat = null;      // flag for GZIP processing.

    private ArchiveFile archiveFile = null; // the compressed ArchiveFile object (TarGzip, Zip) of the tar ball ARK file.

    private Pattern fileNamePattern = null; // the compiled pattern of the fileNameSyntax

    private List<TransferredFile> transferredFiles = new ArrayList<>(); // The files added to the ArchiveFile

    /**
     * Constructor
     * @param arkFileUri
     * @param fileNameSyntax
     * @param needGzip
     */
    public ArkFileMapping(String arkFileUri, String fileNameSyntax, String archiveFormat) {
        this.arkFileUri = arkFileUri;
        this.fileNameSyntax = fileNameSyntax;
        this.archiveFormat = archiveFormat;
        this.fileNamePattern = Pattern.compile(fileNameSyntax);
    }

    public String getArkFileUri() {
        return arkFileUri;
    }

    public void setArkFileUri(String arkFileUri) {
        this.arkFileUri = arkFileUri;
    }

    public String getFileNameSyntax() {
        return fileNameSyntax;
    }

    public void setFileNameSyntax(String fileNameSyntax) {
        this.fileNameSyntax = fileNameSyntax;
    }

    public String getArchiveFormat() {
        return archiveFormat;
    }

    public void setArchiveFormat(String archiveFormat) {
        this.archiveFormat = archiveFormat;
    }

    public boolean needArchiveFile() {
        return archiveFormat != null && Arrays.asList(ARCHIVE_FORMATS).contains(archiveFormat.toUpperCase());
    }

    public ArchiveFile getArchiveFile() {
        return archiveFile;
    }

    public void setArchiveFile(ArchiveFile archiveFile) {
        this.archiveFile = archiveFile;
    }

    public List<TransferredFile> getTransferredFiles() {
        return transferredFiles;
    }

    public void addTransferredFile(TransferredFile transferredFile) {
        transferredFiles.add(transferredFile);
    }

    /**
     * Get the source filename that is mapped for dams ingest
     * @param fileName
     * @return
     */
    public String getSourceFileName(String fileName) {
        Matcher matcher = fileNamePattern.matcher(fileName);
        if (this.needArchiveFile() || matcher.matches()) {
            return fileName;
        }
        return fileName.substring(fileName.indexOf(RdcpFileTransfer.DATE_DELIMITER) + 1);
    }

    /**
     * Determine whether a file name of an attachment is valid or not
     * @param fileName
     * @return
     */
    public boolean isFileNameValid(String fileName) {
        Matcher matcher = fileNamePattern.matcher(fileName);
        if (matcher.matches()) {
            return true;
        }

        // single file replace with date pattern
        return RdcpFileTransfer.getDateFromFileName(fileName) != null
                && fileNameSyntax.endsWith(getSourceFileName(fileName));
    }
}
