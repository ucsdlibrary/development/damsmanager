package edu.ucsd.library.xdre.ingest.fileReplace;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;

/**
 * The model of configuration for RDCP file replacement
 * @author lsitu
 *
 */
public class RdcpFileReplaceConfig {
    public static final int DEFAULT_NOTIFY_INTERVAL = 7;
    public static final String RDL_SHARE = "RDL-Share";

    // file location: rdl-share | /Active_Files folder
    private String fileLocation = null;

    // rdl-share url base
    private String rdlShareUriBase = null;

    // rdl-share API key
    private String rdlShareApiKey = null;

    // password of the API key. Normally x
    private String rdlSharePassword = null;

    // rdl-share recipients
    private List<String> rdfShareRecipients = new ArrayList<>();

    // email distribution list for notification
    private List<String> distributionEmails = new ArrayList<>();

    // Notify interval in days
    private int notifyInterval = DEFAULT_NOTIFY_INTERVAL;

    /**
     * Constructor
     * @param config
     * @throws FileNotFoundException
     * @throws IOException
     */
    public RdcpFileReplaceConfig(File config) throws FileNotFoundException, IOException {
        Properties props = new Properties();
        props.load( new FileInputStream(config) );

        this.fileLocation = props.getProperty("rdcp.file.replace.location"); // RDL-Share | Active_Files

        this.rdlShareUriBase = props.getProperty("rdl.share.url");

        this.rdlShareApiKey = props.getProperty("rdl.share.apiKey");

        String pwd = props.getProperty("rdl.share.password");
        this.rdlSharePassword = StringUtils.isNotBlank(pwd) ? pwd.trim() : "x";

        this.rdfShareRecipients = getDelimitedValues(props.getProperty("rdl.share.recipients"), ",");

        this.distributionEmails = getDelimitedValues(props.getProperty("rdcp.file.replace.emails"), ",");

        String notifyIntervalValue = props.getProperty("rdcp.file.replace.fail.notify.interval");
        if (StringUtils.isNotBlank(notifyIntervalValue)) {
            // Notify interval in days. Default is 7.
            this.notifyInterval = Integer.parseInt(notifyIntervalValue);
        }
    }

    public String getFileLocation() {
        return fileLocation;
    }

    public void setFileLocation(String fileLocation) {
        this.fileLocation = fileLocation;
    }

    public boolean needRdlShareDownload() {
        return StringUtils.isBlank(fileLocation) || fileLocation.equalsIgnoreCase(RDL_SHARE);
    }

    public String getRdlShareUriBase() {
        return rdlShareUriBase;
    }

    public void setRdlShareUriBase(String rdlShareUriBase) {
        this.rdlShareUriBase = rdlShareUriBase;
    }

    public String getRdlShareApiKey() {
        return rdlShareApiKey;
    }

    public void setRdlShareApiKey(String rdlShareApiKey) {
        this.rdlShareApiKey = rdlShareApiKey;
    }

    public String getRdlSharePassword() {
        return rdlSharePassword;
    }

    public void setRdlSharePassword(String rdlSharePassword) {
        this.rdlSharePassword = rdlSharePassword;
    }

    public List<String> getRdfShareRecipients() {
        return rdfShareRecipients;
    }

    public void setRdfShareRecipients(List<String> rdfShareRecipients) {
        this.rdfShareRecipients = rdfShareRecipients;
    }

    public List<String> getDistributionEmails() {
        return distributionEmails;
    }

    public void setDistributionEmails(List<String> emails) {
        this.distributionEmails = emails;
    }

    private static List<String> getDelimitedValues(String value, String delimiter) {
        List<String> vals = new ArrayList<>();

        for (String val : value.split("\\" + delimiter)) {
            if (StringUtils.isNotBlank(val)) {
                vals.add(val.trim());
            }
        }
        return vals;
    }

    public int getNotifyInterva() {
        return notifyInterval;
    }

    public void setNotifyInterva(int notifyInterval) {
        this.notifyInterval = notifyInterval;
    }
}
