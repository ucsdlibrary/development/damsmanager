package edu.ucsd.library.xdre.ingest.fileReplace;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileAttribute;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.compress.archivers.ArchiveEntry;
import org.apache.commons.compress.archivers.ArchiveInputStream;
import org.apache.commons.compress.archivers.ArchiveOutputStream;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import edu.ucsd.library.xdre.utils.Constants;

/**
 * Base class to handle archive files.
 * @author lsitu
 *
 */
public abstract class ArchiveFile {
    private static Logger log = Logger.getLogger(ArchiveFile.class);

    protected static final int BUF_SIZE = 4096;

    protected File archivedFile = null;
    protected Path decompressedFolderPath = null;
    protected List<File> filesAdded = new ArrayList<>();

    /**
     * 
     * @param gzipFile
     * @throws IOException
     */
    public ArchiveFile(File archivedFile) throws IOException {
        this.archivedFile = archivedFile;
        this.decompressedFolderPath = Files.createTempDirectory(Paths.get(Constants.TMP_FILE_DIR),
                "rdcp_file_replace", new FileAttribute[] {});
        decompress();
    }

    /**
     * Add a file to the archive source
     * @param srcFile
     * @throws IOException
     */
    public void addFile(File srcFile) throws IOException {
        File destFolder = getDestinationFolder(decompressedFolderPath.toFile());
        File destFile = Paths.get(destFolder.getAbsolutePath(), srcFile.getName()).toFile();
        FileUtils.copyFile(srcFile, destFile);
        filesAdded.add(srcFile);
    }

    /*
     * Get the folder that contains files
     * @param srcDir
     * @return
     */
    private File getDestinationFolder(File srcDir) {
        File destFolder = srcDir;
        File[] files = srcDir.listFiles();
        for (File file : files) {
            if (file.isDirectory() && file.listFiles().length > 0) {
                destFolder = getDestinationFolder(file);
                break;
            }
        }
        return destFolder;
    }

    /**
     * Get the list of files added to the ArchiveFile object
     * @return
     */
    public List<File> getFilesAdded() {
        return filesAdded;
    }

    public Path getDecompressedFolderPath() {
        return decompressedFolderPath;
    }

    /**
     * Cleanup the GZIP workspace
     * @throws IOException
     */
    public void cleanUp() throws IOException {
        FileUtils.deleteDirectory(decompressedFolderPath.toFile());
    }

    /*
     * Archive the source files recursively
     * @param baseDir
     * @param src
     * @param archiveOut
     * @throws IOException
     */
    protected void archiveFiles(File baseDir, File src, ArchiveOutputStream archiveOut) throws IOException {

        if (src.isFile()) {
            log.debug("Adding File: " + baseDir.toURI().relativize(src.toURI()).getPath());

            try (FileInputStream srcIn = new FileInputStream(src);
                    BufferedInputStream sourceStream = new BufferedInputStream(srcIn, BUF_SIZE);) {

                // create entry for each file, and files to the tar with relative path without including the entire path
                ArchiveEntry entry = createEntry(baseDir, src, archiveOut);

                // put the tar entry, and write file to the tar
                archiveOut.putArchiveEntry(entry);

                int bytesRead = -1;
                byte[] buf = new byte[BUF_SIZE];
                while ((bytesRead = sourceStream.read(buf, 0, BUF_SIZE)) != -1) {
                    archiveOut.write(buf, 0, bytesRead);
                }

                archiveOut.closeArchiveEntry();
            }
        } else {

            if (src.listFiles() != null) {
                if (src.listFiles().length == 0)
                    log.warn("Adding Empty Folder: " + baseDir.toURI().relativize(src.toURI()).getPath());

                // create entry for the folder, which is a relative path without including the entire path
                ArchiveEntry entry = createEntry(baseDir, src, archiveOut);

                archiveOut.putArchiveEntry(entry);
                archiveOut.closeArchiveEntry();

                for (File file : src.listFiles()) {
                    archiveFiles(baseDir, file, archiveOut);
                }
            }
        }
    }

    /*
     * Create entry for the related ArchiveOutputStream
     * @param baseDir
     * @param src
     * @param archiveOut
     * @return
     */
    protected ArchiveEntry createEntry(File baseDir, File src, ArchiveOutputStream archiveOut) {
        String entryName = baseDir
                .getParentFile().toURI().relativize(src.toURI())
                .getPath();

        log.debug("Creating archive entry " + entryName + " for " + src.getAbsolutePath() + "=>" + entryName);

        return archiveOut instanceof TarArchiveOutputStream ?
                new TarArchiveEntry(src, entryName) : new ZipArchiveEntry(src, entryName);
    }

    /**
     * Compress the ArchiveFile to a ArchiveOutputStream.
     * @param archiveOut
     * @throws IOException
     */
    protected void compress(ArchiveOutputStream archiveOut) throws IOException {
        // recursively open the source folder and get a list of files.
        File srcDir = decompressedFolderPath.toFile();
        if (!srcDir.exists()) {
            log.error("Source directory " + srcDir  + " does not exist.");
            throw new FileNotFoundException("Source directory " + srcDir  + " does not exist.");
        }

        for (File file : srcDir.listFiles()) {
            archiveFiles(file, file, archiveOut);
        }
    }

    /**
     * Decompressed from the ArchiveInputStream
     * @param destDir
     * @param archiveIn
     * @throws IOException
     */
    protected void decompress(ArchiveInputStream archiveIn) throws IOException {
        File destDir = decompressedFolderPath.toFile();

        ArchiveEntry entry;
        while ((entry = archiveIn.getNextEntry()) != null) {
            File file = new File(destDir, entry.getName());

            // create directory for directory entry
            if (entry.isDirectory()) {
                if (!file.exists()) {
                    file.mkdirs();
                }
            } else {
                if (!file.getParentFile().exists()) {
                    file.getParentFile().mkdirs();
                }

                try (FileOutputStream archiveOut = new FileOutputStream(file);){
                    IOUtils.copy(archiveIn, archiveOut); 
                }
            }
        }

        log.info("Successful decompressed file " + archivedFile.getAbsolutePath() + " to " + destDir.getAbsolutePath());
    }

    /**
     * Compress the files to the destination file
     * @param destFile - the destination compressed file
     * @throws FileNotFoundException
     * @throws IOException
     */
    public abstract void compress(File destFile) throws FileNotFoundException, IOException;

    /**
     * Decompress the archive file
     * @throws IOException
     */
    protected abstract void decompress() throws IOException;
}
