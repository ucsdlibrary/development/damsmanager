package edu.ucsd.library.xdre.ingest.fileReplace;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.compress.archivers.ArchiveInputStream;
import org.apache.commons.compress.archivers.zip.ZipArchiveInputStream;
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;
import org.apache.log4j.Logger;

/**
 * Utility class to handle Zip files.
 * @author lsitu
 *
 */
public class Zip extends ArchiveFile {
    private static Logger log = Logger.getLogger(Zip.class);

    public Zip(File zipFile) throws IOException {
        super(zipFile);
    }

    @Override
    public void compress(File destFile) throws FileNotFoundException, IOException {
        // create a ZipArchiveOutputStream object
        try (FileOutputStream destOut = new FileOutputStream(destFile);
                BufferedOutputStream bufOut = new BufferedOutputStream(destOut);
                ZipArchiveOutputStream zipOut = new ZipArchiveOutputStream(bufOut);) {

            compress(zipOut);

            log.info("Successfully created zip file: " + destFile.getAbsolutePath() + ".");
        }
    }

    @Override
    protected void decompress() throws IOException {
        try (InputStream fIn = new FileInputStream(archivedFile);
                ArchiveInputStream archiveIn = new ZipArchiveInputStream(fIn)) {

            decompress(archiveIn);
        }
    }
}
