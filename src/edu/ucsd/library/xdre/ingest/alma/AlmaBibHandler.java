package edu.ucsd.library.xdre.ingest.alma;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.security.auth.login.LoginException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.collections4.ListUtils;
import org.apache.http.client.ClientProtocolException;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.Namespace;
import org.dom4j.Node;
import org.dom4j.QName;
import org.dom4j.XPath;
import org.dom4j.io.SAXReader;

/**
 * Handle Alma MARC/XML
 * @author lsitu
 *
 */
public class AlmaBibHandler {
    private static Logger log = Logger.getLogger(AlmaBibHandler.class);

    public static final Namespace NS_MARC21 = new Namespace("", "http://www.loc.gov/MARC21/slim");
    private static final Namespace NS_XSI = new Namespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
    private static final String MARC21_XSD = "http://www.loc.gov/standards/marcxml/schema/MARC21slim.xsd";
    private static final String MODS_V3 = "http://www.loc.gov/mods/v3";

    private static final String MARC21_SLIM_TO_MODS_XSL = "/resources/MARC21slim2MODS.xsl";

    private static Map<String, String> MARC_MODS_NAMESPACES = null;

    private AlmaApiClient apiClient;

    public AlmaBibHandler() throws LoginException, IOException {
        this(new AlmaApiClient());
    }

    public AlmaBibHandler(AlmaApiClient apiClient) throws LoginException, IOException {
        this.apiClient = apiClient;
    }

    /**
     * Retrieve bib records from Alma API and encode in MARC/XML format
     * @param ids - list of mms_id
     * @return MARC/XML document with namespace http://www.loc.gov/MARC21/slim 
     * @throws ClientProtocolException
     * @throws LoginException
     * @throws IOException
     * @throws ParseException 
     * @throws DocumentException 
     */
    public Document getMarcXml(List<String> ids) throws ClientProtocolException,
            LoginException, IOException, ParseException, DocumentException {
        Element root = DocumentHelper.createDocument().addElement(new QName("collection", NS_MARC21));
        root.addAttribute(new QName("schemaLocation", NS_XSI), NS_MARC21.getURI() + " " + MARC21_XSD);

        List<List<String>> idsBlocks = ListUtils.partition(ids, 100);
        for (List<String> idsBlock : idsBlocks) {
            Document bibs = apiClient.retrieveRecords(idsBlock);
            List<Node> records = bibs.selectNodes("//record");

            for (Node record : records) {
                Node node = ensureMarc21Namespace(record.detach());

                root.add(node);
            }
        }
        return root.getDocument();
    }

    /*
     * Ensure MARC21 SLIM namespace apply to all elements to avoid empty namespace added by dom4j
     * @return
     */
    private Node ensureMarc21Namespace(Node node)
            throws UnsupportedEncodingException, IOException, DocumentException {

        // Simply replace the record node with the namespace works while DOM manipulation won't. 
        String marcXml = node.asXML().replace("<record>", "<record xmlns=\"" + NS_MARC21.getURI() + "\">");
        try (InputStream in = new ByteArrayInputStream(marcXml.getBytes("UTF-8"));) {
            return new SAXReader().read(in).getRootElement();
        }
    }

    /**
     * Retrieve mms_id from MARCXML record
     * @param node
     * @return
     */
    public static String getMmsId(Node node) {
        return createXPath("marc:controlfield[@tag=001]").valueOf(node);
    }

    /**
     * Get MARC/MODS namespace map
     * @return
     */
    public synchronized static Map<String, String> getMarcModsNamespaces() {
        if (MARC_MODS_NAMESPACES == null) {
            MARC_MODS_NAMESPACES = new HashMap<String, String>();
            MARC_MODS_NAMESPACES.put("marc", AlmaBibHandler.NS_MARC21.getURI());
            MARC_MODS_NAMESPACES.put("mods", MODS_V3);
        }
        return MARC_MODS_NAMESPACES;
    }

    /**
     * Create XPath for MARC/MODS elements selection
     * @param xpathExpression
     * @return
     */
    public static XPath createXPath(String xpathExpression) {
        XPath xPath = DocumentHelper.createXPath(xpathExpression);
        xPath.setNamespaceURIs(getMarcModsNamespaces());
        return xPath;
    }

    /**
     * Convert MARC/XML to MODS
     * @param mmsId the mms_id
     * @param marcXml
     * @return
     * @throws IOException
     * @throws TransformerException
     */
    public static String convertToMods(String mmsId, String marcXml) throws IOException, TransformerException {
        Map<String, String> params = new HashMap<>();
        params.put("roger", mmsId);

        try (InputStream xmlInput = new ByteArrayInputStream(marcXml.getBytes("UTF-8"));) {
            URL xslUrl = AlmaBibHandler.class.getResource(MARC21_SLIM_TO_MODS_XSL);
            return xslConvert(xslUrl, xmlInput, params);
        }
    }

    /**
     * Convert xml source to other formats with xsl transformation
     * @param xslIn
     * @param rdfIn
     * @param params
     * @return
     * @throws TransformerException
     * @throws IOException 
     */
    public static String xslConvert(URL xslUrl, InputStream rdfInput, Map<String, String> params)
            throws TransformerException, IOException {
        try (OutputStream out = new ByteArrayOutputStream();) {
            Source xslSrc = new StreamSource(xslUrl.getPath());
            TransformerFactory transFact = TransformerFactory.newInstance();
            Templates xslTemp = transFact.newTemplates(xslSrc);
            Transformer trans = xslTemp.newTransformer();
            Source xmlSource = new StreamSource(rdfInput);
            StreamResult result = new StreamResult(out);

            if (params != null && params.size() > 0) {
                for (String key : params.keySet()) {
                    trans.setParameter(key, params.get(key));
                }
            }
            trans.setOutputProperty("encoding", "UTF8");
            trans.setOutputProperty(OutputKeys.INDENT, "yes");
            trans.transform(xmlSource, result);

            return out.toString();
        }
    }
}
