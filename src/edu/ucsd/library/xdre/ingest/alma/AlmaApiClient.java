package edu.ucsd.library.xdre.ingest.alma;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

import javax.security.auth.login.LoginException;

import org.apache.http.Header;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.util.EntityUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.io.SAXReader;

import edu.ucsd.library.xdre.utils.Constants;
import edu.ucsd.library.xdre.utils.HttpClientBase;

/**
 * AlmaApiClient to download bib MARC/XML records from Alma API
 * @author lsitu
 * 
 */
public class AlmaApiClient extends HttpClientBase {

    private static final String ADDITIONAL_API_PARAMS = "view=full&expand=None";

    private String apiKey = null;

    /**
     * Construct AlmaApiClient object
     * @throws IOException 
     * @throws LoginException 
     */
    public AlmaApiClient() throws LoginException, IOException {
        this(Constants.ALMA_API_BIB_URL, Constants.ALMA_API_KEY);
    }

    /**
     * Construct ApiClient object using basic url
     * 
     * @param urlBase
     * @param apiKey
     * @throws IOException
     * @throws LoginException
     */
    public AlmaApiClient(String urlBase, String apiKey) throws IOException, LoginException {
        super(urlBase, null, null);
        this.apiKey = apiKey;
    }

    /**
     * Construct AlmaApiClient object using damsmanager properties
     * @param props
     * @throws IOException
     * @throws LoginException
     */
    public AlmaApiClient(Properties props) throws IOException, LoginException {
        this((String)props.get("alma.api.bibs.url"), (String)props.get("alma.api.key"));
    }

    /**
     * Retrieve MARC/XML from Alma API
     * @param ids - list of mms_id
     * @return Document
     * @throws ClientProtocolException
     * @throws LoginException
     * @throws IOException
     * @throws DocumentException
     */
    public Document retrieveRecords(List<String> ids)
            throws DocumentException, ClientProtocolException, LoginException, IOException {
        HttpGet get = new HttpGet(getRequestUrl(ids));
        SAXReader saxReader = new SAXReader();

        Document bibs = null;
        try {
            int status = execute(get);
            if (status < 300) {
                try (InputStream in = response.getEntity().getContent();) {
                    bibs = saxReader.read(in);
                }
            }else{
                handleError("");
            }
        }finally{
            get.releaseConnection();
        }
        return bibs;
    }

    /*
     * Build the request url
     * @param ids
     * @return
     */
    private String getRequestUrl(List<String> ids) {
        StringBuilder builder = new StringBuilder();
        builder.append(this.storageURL)
            .append("?" + ADDITIONAL_API_PARAMS)
            .append("&apikey=" + apiKey)
            .append("&mms_id=" + String.join(",", ids));

        return builder.toString();
    }

    /**
     * Handle error response.
     * @param format
     * @throws IOException 
     * @throws LoginException 
     */
    public void handleError(String format) throws IOException, LoginException {
        int status = response.getStatusLine().getStatusCode();
        Header[] headers = response.getAllHeaders();
        for (int i = 0; i < headers.length; i++) {
            log.debug(headers[i] + "; ");
        }

        String reqInfo = request.getMethod() + " " + request.getURI();
        String respContent = EntityUtils.toString(response.getEntity());

        log.error(reqInfo + ": " + respContent);
        if (status == 401) {
            // 401 - unauthorized access
            throw new LoginException(reqInfo + ": " + respContent);
        } else {
            // Other error status codes
            throw new IOException(reqInfo + ": " + respContent);
        }
    }
}
