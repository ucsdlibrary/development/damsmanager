package edu.ucsd.library.xdre.ingest;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.CRC32;
import java.util.zip.CheckedInputStream;

import org.apache.log4j.Logger;

import edu.ucsd.library.xdre.ingest.fileReplace.TransferredFile;

/**
 * Utility class for RDCP file transfer support
 * @author lsitu
 *
 */
public class RdcpFileTransfer {
    private static Logger log = Logger.getLogger(RdcpFileTransfer.class);

    public static final String DATE_PATTERN = "yyyy-MM-dd";
    public static final String REPORT_DATE_PATTERN = "yyyy-MM-dd'T'HH:mm:ss";

    public static final String UPLOAD_FOLDER = "Active_Files";
    public static final String UPLOAD_PATH_POSTFIX = "File_Replacement";
    public static final String BACKUP_FOLDER = "Backup_Files";
    public static final String REPORT_FOLDER = "File_Replacement_Documents";
    public static final String CONFIG_FILE = "config.properties";
    public static final String DATE_DELIMITER = "_";

    /**
     * Get the date value from attachment filename
     * @param fileNameKey
     * @return
     */
    public static Date getDateFromFileName(String fileNameKey) {
        SimpleDateFormat dateFormatTag = new SimpleDateFormat(DATE_PATTERN);

        String[] pair = fileNameKey.split(DATE_DELIMITER);
        if (pair.length > 1) {
            try {
                return dateFormatTag.parse(pair[0]);
            } catch (Exception e) {
                log.warn("Invalid date prefix for attachment " + fileNameKey);
            }
         }

        return null;
    }

    /*
     * Get the corresponding source filename with no date prefix.
     * @param fileName
     * @return
     */
    public static String getSourceFileName(String fileName) {
        return fileName.substring(fileName.indexOf(RdcpFileTransfer.DATE_DELIMITER) + 1);
    }

    /**
     * Directory for rdcp files download
     * @return
     */
    public static String getDownloadPath(String projectPath) {
        Path downloadPath = Paths.get(projectPath, UPLOAD_FOLDER);
        return downloadPath.toFile().getAbsolutePath() + File.separatorChar;
    }

    /**
     * Directory for rdcp download files backup
     * @return
     */
    public static String getBackupPath(String projectPath) {
        Path backupPath = Paths.get(projectPath, BACKUP_FOLDER);
        return backupPath.toFile().getAbsolutePath() + File.separatorChar;
    }

    /**
     * Directory for rdcp report
     * @return
     */
    public static String getReportPath(String projectPath) {
        Path reportPath = Paths.get(projectPath, REPORT_FOLDER);
        return reportPath.toFile().getAbsolutePath() + File.separatorChar;
    }

    /**
     * Get the report file from rdcp staging
     * @param fileName
     * @return
     */
    public static File getReportFile(String projectPath, String fileName) {
        File reportDir = Paths.get(projectPath, REPORT_FOLDER).toFile();
        if (!reportDir.exists()) {
            reportDir.mkdirs();
        }

        return Paths.get(projectPath, REPORT_FOLDER, fileName).toFile();
    }

    /**
     * Move the file to Backup_Files folder.
     * @throws IOException 
     */
    public static void backupFile(String projectPath, String file) throws IOException {
        Path source = Paths.get(file);
        Path target = Paths.get(getBackupPath(projectPath), source.toFile().getName());
        Files.move(source, target, REPLACE_EXISTING);
    }

    /**
     * Delete files in Backup_Files folder if more than 30 days.
     */
    public static void cleanupBackupFiles(String projectPath) {
        Calendar cal = Calendar.getInstance(); 
        cal.setTime(new Date()); 
        cal.add(Calendar.DATE, -30);
        Date deleteDate = cal.getTime();

        try {
            Path backupPath = Paths.get(getBackupPath(projectPath));

            File[] files = backupPath.toFile().listFiles();

            log.info("RDCP file replacement backup " + backupPath.toFile().getAbsolutePath()
                    + ": total " + (files == null ? null : files.length) + " files.");

            for (File file : files) {
                Date lastModifiedDate = new Date(file.lastModified());
                if (lastModifiedDate.before(deleteDate)) {
                    log.info("Deleting file " + file.getAbsolutePath() + ".");

                    file.delete();
                }
            }
        } catch (Exception ex) {
            log.error("Error cleaning up backup files in " + projectPath + ".", ex);
        }
    }

    /**
     * Directory for files download from rdl-share api
     * @return
     */
    public static File getDownloadFile(String projectPath, String fileName) {
        return Paths.get(getDownloadPath(projectPath), fileName).toFile();
    }

    /**
     * Extract crc32 checksum
     * @param file
     * @return
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static String checksumCrc32(String file) throws FileNotFoundException, IOException {
        try(InputStream is =  new FileInputStream(file);
            CheckedInputStream cis =  new CheckedInputStream( is, new CRC32() );) {

            // read data
            byte[] buf = new byte[8192];
            while((cis.read(buf)) >= 0) { ; }

            String checksumCrc32 = Long.toString(cis.getChecksum().getValue(), 16);
            while ( checksumCrc32.length() < 8 ) { checksumCrc32 = "0" + checksumCrc32; }

            return checksumCrc32;
        }
    }

    /**
     * Lookup active files from a directory path on local disk
     * @param activeFilesPath
     * @return List<TransferredFile>
     */
    public static List<TransferredFile> retrieveActiveFiles(String activeFilePath)
            throws FileNotFoundException, IOException {
        List<TransferredFile> transferredFiles = new ArrayList<>();

        File[] activeFiles = new File(activeFilePath).listFiles();
        if (activeFiles != null) {
            Map<String, TransferredFile> filesMap = new HashMap<>();

            for (int i = 0; i < activeFiles.length; i++) {
                File file = activeFiles[i];
                if (file.isFile() && !file.isHidden()) {
                    TransferredFile transferredFile = new TransferredFile(file);
                    transferredFiles.add(transferredFile);

                    // Check duplicate file basing on date pattern on the filename.
                    transferredFile.dedupTransferredFileByDate(filesMap);
                }
            }
        }

        Collections.sort(transferredFiles);

        return transferredFiles;
    }
}
