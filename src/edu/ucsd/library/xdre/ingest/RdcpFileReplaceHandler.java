package edu.ucsd.library.xdre.ingest;

import static edu.ucsd.library.xdre.collection.CollectionHandler.escapeCsvValue;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.dom4j.Document;

import edu.ucsd.library.xdre.collection.FileUploadHandler;
import edu.ucsd.library.xdre.ingest.fileReplace.ArchiveFile;
import edu.ucsd.library.xdre.ingest.fileReplace.ArkFileMapping;
import edu.ucsd.library.xdre.ingest.fileReplace.RdcpFileReplaceConfig;
import edu.ucsd.library.xdre.ingest.fileReplace.TarGzip;
import edu.ucsd.library.xdre.ingest.fileReplace.TransferredFile;
import edu.ucsd.library.xdre.ingest.fileReplace.Zip;
import edu.ucsd.library.xdre.utils.ArkFile;
import edu.ucsd.library.xdre.utils.Constants;
import edu.ucsd.library.xdre.utils.DAMSClient;
import edu.ucsd.library.xdre.utils.DamsURI;
import edu.ucsd.library.xdre.utils.ExcelReader;

/**
 * Utility class to handle rdcp file replacement
 * @author lsitu
 *
 */
public class RdcpFileReplaceHandler {
    private static Logger log = Logger.getLogger(RdcpFileReplaceHandler.class);

    public static String FILENAME_HEADER = "File Name";
    public static String ARKFILE_HEADER = "ARK File";
    public static String CONTAINER_HEADER = "Container";

    public static final String arkMappingFile = "rdcp-arkfile-mapping.xls";

    public static final String reportFileName = "file_replace_report.csv";
    public static final String[] reportHeaders = {"File Transfer Timestamp", "File Name", "Checksum (SHA256)",
        "File Size", "Transfer Status", "Transfer Error Description", "Checksum (CRC-32)", "File Replace Status",
        "File Replace Error Description"};

    protected static final String successStatus = "Success";
    protected static final String failureStatus = "Failure";

    protected SimpleDateFormat dateFormat = new SimpleDateFormat(RdcpFileTransfer.REPORT_DATE_PATTERN);

    protected RdcpFileReplaceConfig config = null;
    protected String projectPath = null;
    protected DAMSClient damsClient = null;

    protected List<TransferredFile> transferredFiles = null;

    protected List<ArkFileMapping> arkFileMappings = new ArrayList<>();
    protected List<String[]> reports = new ArrayList<>();
    protected List<String[]> errorReports = new ArrayList<>();

    /**
     * Constructor
     * @param attachments
     * @param apiClient
     * @param damsClient
     * @throws Exception 
     */
    public RdcpFileReplaceHandler(RdcpFileReplaceConfig config, String projectPath, List<TransferredFile> transferredFiles, DAMSClient damsClient, List<ArkFileMapping> arkFileMappings) throws Exception {
        this.config = config;
        this.projectPath = projectPath;
        this.transferredFiles = transferredFiles;
        this.damsClient = damsClient;
        this.arkFileMappings = arkFileMappings;
    }

    /**
     * Function to replace files in dams
     * @return
     * @throws Exception
     */
    public boolean replaceFiles() throws Exception {
        boolean transferStatus = false;

        for (TransferredFile transferredFile : transferredFiles) {
            transferStatus = false;

            String transferMessage = "";
            String fileReplaceMessage = "";
            String fileName = transferredFile.getFileName();

            log.info("Processing file replacement for attachment " + fileName + " ...");

            try {
                if (!transferredFile.isIgnore()) {
                    transferStatus = true;

                    try {
                        processFile(transferredFile);
                    } catch (Exception ex) {
                        transferStatus = false;
                        transferMessage = ex.getMessage();
                    }
                }
            } finally {
                // generate report for attachments with download error or invalid.
                if (transferredFile.isError() || transferredFile.isInvalid()) {
                    reports.add(generateReport(transferredFile, transferStatus, transferMessage, false, fileReplaceMessage));
                } 

                // delete files unless it's failed for file replacement
                if ((transferredFile.isInvalid() || transferredFile.isIgnore()) && transferredFile.getFile() != null) {
                    transferredFile.getFile().delete();
                }
            }
        }

        // now replace the files in dams
        boolean replaceStatus = false;
        String fileReplaceMessage = "";
        for (ArkFileMapping arkFileMapping : arkFileMappings) {
            if (arkFileMapping.needArchiveFile() && arkFileMapping.getArchiveFile() != null) {
                try {
                    replaceStatus = replaceFile(arkFileMapping);
                } catch (Exception ex) {
                    fileReplaceMessage = ex.getMessage();
    
                    log.error(fileReplaceMessage, ex);
                } finally {
                    // cleanup intermediate files generated from Tarball, Zip.
                    arkFileMapping.getArchiveFile().cleanUp();

                    for (TransferredFile transferredFile : arkFileMapping.getTransferredFiles()) {
                        // generate report for the TransferedFile
                        reports.add(generateReport(transferredFile, true, "", replaceStatus, fileReplaceMessage));
                    }
                }
            } else if (!arkFileMapping.needArchiveFile() && arkFileMapping.getTransferredFiles().size() > 0) {
                // Upload single files
                List<TransferredFile> transferredFiles = arkFileMapping.getTransferredFiles();
                for (int i = 0; i < transferredFiles.size(); i++) {
                    replaceStatus = false;
                    fileReplaceMessage = "";

                    TransferredFile transferredFile = transferredFiles.get(i);
                    if (transferredFile.isError() || transferredFile.isInvalid() || transferredFile.isIgnore()) {
                        continue;
                    }

                    try {
                        if (i == transferredFiles.size() - 1) {
                            // Upload file
                            replaceStatus = replaceFile(arkFileMapping);
                        } else {
                            // Multiple files for the same ark: don't replace
                            transferredFile.setRedundant(true);
                            fileReplaceMessage = "Redundant of " + transferredFiles.get(transferredFiles.size() -1).getFileName() + ".";
                        }
                    } catch (Exception e) {
                        fileReplaceMessage = e.getMessage();
                    } finally {
                        // generate report for the attachment
                        reports.add(generateReport(transferredFiles.get(i), true, "", replaceStatus, fileReplaceMessage));

                        // delete the redundant file
                        if (transferredFile.isRedundant()) {
                            transferredFile.getFile().delete();
                        }
                    }
                }
            }
        }
        return errorReports.size() == 0;
    }

    /**
     * Process the file transferred from data provider
     * @param transferredFile
     * @return
     * @throws Exception
     */
    protected ArkFileMapping processFile(TransferredFile transferredFile) throws Exception {
        String fileReplaceMessage = "";

        String fileName = transferredFile.getFileName();
        File srcFile = RdcpFileTransfer.getDownloadFile(projectPath, fileName);
        transferredFile.setFile(srcFile);

        // map transferred file to the ark file in dams
        ArkFileMapping arkFileMapping = getArkFileMapping(arkFileMappings, fileName);

        if (arkFileMapping == null || StringUtils.isBlank(arkFileMapping.getArkFileUri())) {
            if (arkFileMapping == null) {
                // Invalid file with no filename pattern matched for ARK file in dams.
                transferredFile.setInvalid(true);
                fileReplaceMessage = "File does not match established naming convention.";
            } else {
                fileReplaceMessage = "No ARK file mapping found for file " + fileName;
            }

            throw new Exception(fileReplaceMessage);
        }

        arkFileMapping.addTransferredFile(transferredFile);
        String arkFile = arkFileMapping.getArkFileUri();

        // handle single ark file that don't need tarball processing
        if (!arkFileMapping.needArchiveFile()) {
            // check for file extension
            int fileExtIdx = fileName.lastIndexOf(".");
            int arkFileExtIdx = arkFile.lastIndexOf(".");
            if (fileExtIdx > 0 && arkFileExtIdx > 0
                    && !arkFile.endsWith(fileName.substring(fileExtIdx))) {
                fileReplaceMessage = "ARK file extension doesn't match the transfered filename: "
                        + fileName + " -> " + arkFile;
            }
        } else {
            try {
                // processing archive file
                ArchiveFile archiveFile = arkFileMapping.getArchiveFile();
                if (archiveFile == null) {
                    DamsURI damsURI = DamsURI.toParts(arkFile, DamsURI.FILE);
                    boolean arkFileExist = damsClient.exists(damsURI.getObject(), damsURI.getComponent(), damsURI.getFileName());
                    if (arkFileExist) {
                        // Download the tar ball to build ArchiveFile object that can perform compress, decompress, and add files operations.
                        String arkFileName = ArkFile.getDams4FileName(damsURI.getObject(), damsURI.getComponent(), damsURI.getFileName());
                        File archiveTmpFile = Paths.get(Constants.TMP_FILE_DIR, arkFileName).toFile();
                        archiveTmpFile.deleteOnExit();
                        damsClient.download(damsURI.getObject(), damsURI.getComponent(), damsURI.getFileName(), archiveTmpFile.getAbsolutePath());

                        archiveFile = ArkFileMapping.ARCHIVE_FORMAT_TARBALL.equalsIgnoreCase(arkFileMapping.getArchiveFormat()) ?
                                new TarGzip(archiveTmpFile) : new Zip(archiveTmpFile);
                        arkFileMapping.setArchiveFile(archiveFile);
                    } else {
                        fileReplaceMessage = "ARK File doesn't exist in DAMS: " + arkFile + ".";
                    }
                }

                if (archiveFile != null) {
                    // add the file to the archive file
                    archiveFile.addFile(srcFile);
                    transferredFile.setArchived(true);
                } else {
                    fileReplaceMessage = "Unable to process archive file: " + arkFile + ".";
                }
            } catch (Exception ex) {
                fileReplaceMessage = "File replacement error: " + ex.getMessage();

                log.error(fileReplaceMessage, ex);
            }
        }

        if (StringUtils.isNotBlank(fileReplaceMessage)) {
            throw new Exception(fileReplaceMessage);
        }

        return arkFileMapping;
    }

    /**
     * Replace the RDCP file represented by the ArkFileMapping object
     * @param arkFileMapping
     * @throws Exception
     */
    protected boolean replaceFile(ArkFileMapping arkFileMapping) throws Exception {
        String fileReplaceMessage = "";
        boolean replaceStatus = false;
        try {
            String arkFile = arkFileMapping.getArkFileUri();
            DamsURI damsURI = DamsURI.toParts(arkFile, DamsURI.FILE);

            File srcFile = null;
            if (arkFileMapping.needArchiveFile()) {
                // Retrieve the source filename from dams
                String srcFileName = retrieveSourceFileName(arkFileMapping.getArkFileUri());

                String localStoreFileName = ArkFile.getDams4FileName(damsURI.getObject(), damsURI.getComponent(), damsURI.getFileName());
                srcFileName = StringUtils.isNotBlank(srcFileName) ? srcFileName : localStoreFileName;
                srcFile = Paths.get(RdcpFileTransfer.getDownloadPath(projectPath), srcFileName).toFile();
                arkFileMapping.getArchiveFile().compress(srcFile);
            } else {
                if (arkFileMapping.getTransferredFiles().size() == 0) {
                    fileReplaceMessage = "File replace error: source file missing for " + arkFile;
                }

                // Single file replace: use the latest file
                List<TransferredFile> transferredFiles = arkFileMapping.getTransferredFiles();
                srcFile = transferredFiles.get(transferredFiles.size() - 1).getFile();
            }

            // perform file replace
            replaceStatus = replaceFile(arkFileMapping.getArkFileUri(), srcFile.getAbsolutePath());
            if (!replaceStatus) {
                fileReplaceMessage = "File replace failed for " + arkFile + ".";
            } else {
                try {
                    // upload succeeded: move the file to the Backup_Files folder
                    RdcpFileTransfer.backupFile(projectPath, srcFile.getAbsolutePath());
                    if (arkFileMapping.getArchiveFile() != null) {
                        // move all source files added to the Backup_Files folder
                        for (File src : arkFileMapping.getArchiveFile().getFilesAdded()) {
                            RdcpFileTransfer.backupFile(projectPath, src.getAbsolutePath());
                        }
                    }
                } catch (Exception ex) {
                    replaceStatus = false;
                    fileReplaceMessage = "Backup file error: " + srcFile;

                    log.error(fileReplaceMessage, ex);
                }
            }
        } catch (Exception ex) {
            fileReplaceMessage = "File replace error: " + ex.getMessage();

            log.error(fileReplaceMessage, ex);
        }

        // Throw exception for error that need to report.
        if (StringUtils.isNotBlank(fileReplaceMessage)) {
            throw new Exception(fileReplaceMessage);
        }
 
        return replaceStatus;
    }

    /**
     * Get reports for all files
     * @return
     */
    public List<String[]> getReports() {
        return reports;
    }

    /**
     * Get the error reports
     * @return
     */
    public List<String[]> getErrorReports() {
        return errorReports;
    }

    /**
     * Retrieve the ArkFileMapping by attachment filename
     * @param arkFileMappings
     * @param fileName
     * @return
     */
    public static ArkFileMapping getArkFileMapping(List<ArkFileMapping> arkFileMappings, String fileName) {
        for (ArkFileMapping mapping : arkFileMappings) {
            // find the first ArkFileMapping that match the filename
            if (mapping.isFileNameValid(fileName)) {
                return mapping;
            }
        }

        return null;
    }

    /*
     * Retrieve the original source filename from dams
     * @param arkUrI
     * @return
     * @throws Exception
     */
    protected String retrieveSourceFileName(String arkFileUrl) throws Exception {
        DamsURI damsURI = DamsURI.toParts(arkFileUrl, DamsURI.FILE);
        Document doc = damsClient.getRecord(damsURI.getObject());
        return doc.valueOf("//*[@rdf:about='" + arkFileUrl + "']/dams:sourceFileName");
    }

    /*
     * Replace the file in the dams filestore
     * @param arkFile
     * @param srcFile
     * @return
     * @throws Exception
     */
    protected boolean replaceFile(String arkFile, String srcFile) throws Exception {
        FileUploadHandler handler = null;

        try {
            // Use relative path to dams_staging
            String srcFileOnStaging = srcFile.substring(new File(Constants.DAMS_STAGING).getAbsolutePath().length() + 1);

            Map<String, String> filesMap = new HashMap<>();
            filesMap.put(srcFileOnStaging, arkFile);
            handler = new FileUploadHandler(damsClient, filesMap);
            handler.setItems(Arrays.asList(filesMap.keySet().toArray(new String[filesMap.size()])));
            return handler.execute();
        } finally {
            if (handler != null) {
                try {
                    handler.release();
                } catch (Exception ex) {
                    log.warn("RDCP file replace FileUploadHandler release error!", ex);
                }
            }
        }
    }

    /*
     * Generate report for the status of an attachment.
     * @param attachment
     * @param status
     * @param message
     * @return
     */
    protected String[] generateReport(TransferredFile transferredFile,
            boolean transferStatus, String transferMessage, boolean ingestStatus, String fileReplaceMessage) {
        long sizeInMb = transferredFile.getFileSize() / (1024*1024);
        SimpleDateFormat dateFormat = new SimpleDateFormat(RdcpFileTransfer.REPORT_DATE_PATTERN);

        String[] report = {
                escapeCsvValue(dateFormat.format(new Date())),
                escapeCsvValue(transferredFile.getFileName()),
                escapeCsvValue("'" + transferredFile.getChecksum()),      // enforce text display in Excel with single quote
                escapeCsvValue((sizeInMb == 0 ? 1 : sizeInMb) + " mb"),
                escapeCsvValue(transferStatus ? successStatus : failureStatus),
                escapeCsvValue(transferMessage),
                escapeCsvValue("'" + transferredFile.getChecksumCrc32()), // enforce text display in Excel with single quote
                escapeCsvValue(!transferStatus ? "" : ingestStatus ? successStatus : failureStatus),
                escapeCsvValue(fileReplaceMessage) };

        if (!(transferStatus && ingestStatus)) {
            errorReports.add(report);

            String error = !transferStatus ? transferMessage : fileReplaceMessage;
            log.error("RDCP File Replacement for " + transferredFile.getFileName() + " failed: " + error + ".");
        }

        return report;
    }

    /**
     * Retrieve the source filename -> ARK file mapping from configuration file
     * @return
     * @throws Exception
     */
    public static List<ArkFileMapping> getArkFileMap(String arkMappingFile) throws Exception {
        List<ArkFileMapping> mappings = new ArrayList<>();

        if (!new File(arkMappingFile).exists()) {
            throw new FileNotFoundException("RDCP ARK File mapping configuration file is not found: "
                    + arkMappingFile);
        }

        try (InputStream mappingsInput = new FileInputStream(arkMappingFile);) {

            ExcelReader excelReader = new ExcelReader(mappingsInput);
            Map<Integer, Map<String, String>> data = excelReader.getData();

            for (Map<String, String> d : data.values()) {
                String fileName = d.get(FILENAME_HEADER);
                String arkFileUri = d.get(ARKFILE_HEADER);
                String fileFormat = d.get(CONTAINER_HEADER);

                if (StringUtils.isNotBlank(fileName) && StringUtils.isNotBlank(arkFileUri)) {
                    ArkFileMapping arkFileMapping = new ArkFileMapping(arkFileUri, fileName, fileFormat);
                    mappings.add(arkFileMapping );
                }
            }
        }
        return mappings;
    }

    /**
     * Write report to rdcp staging
     * @return
     * @throws FileNotFoundException
     * @throws IOException
     */
    public void writeReports() throws FileNotFoundException, IOException {

        File reportFile = RdcpFileTransfer.getReportFile(projectPath, reportFileName);

        StringBuilder reporBuilder = new StringBuilder();

        if (reportFile.length() == 0) {
            // append the column headers only if the report file is first created
            reporBuilder.append(String.join(",", reportHeaders) + "\n");
        }

        for (String[] item : reports) {
            reporBuilder.append(String.join(",", item) + "\n");
        }

        // append report to the file in rdcp staging
        appendToReport(reportFile.getAbsolutePath(), reporBuilder.toString());
    }

    /**
     * Append content to the end of the file starting with new line
     * @param filePath
     * @param content
     * @return
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static File appendToReport(String filePath, String content)
            throws FileNotFoundException, IOException {
        byte[] buf = new byte[4096];
        File destFile = new File(filePath);

        try (InputStream in = new ByteArrayInputStream(content.getBytes("UTF-8"));
                RandomAccessFile randomAccessFile = new RandomAccessFile(filePath, "rw")) {
            long endCharIndex = randomAccessFile.length() - 1;

            if (endCharIndex > 0) {
                randomAccessFile.seek(endCharIndex);
                byte bt = randomAccessFile.readByte();

                if (bt != "\n".codePointAt(0) && bt != "\r".charAt(0)) {
                    randomAccessFile.seek(randomAccessFile.length());
                    randomAccessFile.write("\n".getBytes("UTF-8"));
                }
            }

            randomAccessFile.seek(randomAccessFile.length());

            int bytesRead = 0;
            while((bytesRead = in.read(buf)) > 0) {
                randomAccessFile.write(buf, 0, bytesRead);
            }
        }

        return destFile;
    }

    /**
     * Notify by mail
     * @param failureReports
     * @param reportFile
     */
    public void emailNotify() {
        try {
            String reportFile = RdcpFileTransfer.getReportFile(projectPath, reportFileName).getAbsolutePath();
            String[] mailAttachments = { reportFile };

            String mailSubject = getEmailSubject();

            StringBuilder mailBodyBuilder = new StringBuilder();

            String errorInfo = "Error with the following " + errorReports.size()
                    + " file" + (errorReports.size() == 1 ? "" : "s") + ": \n";
            mailBodyBuilder.append(errorInfo);

            for (String[] failureItem : errorReports) {
                String errorPhase = (failureItem[4].equalsIgnoreCase(failureStatus) ? "File Transfer" : "File Replace");
                errorInfo = failureItem[0] + " " + failureItem[1] + " " + errorPhase + ": "
                        + failureItem[failureItem.length - 1] + "\n";
                mailBodyBuilder.append(errorInfo);
            }

            String[] emails = config.getDistributionEmails().toArray(new String[config.getDistributionEmails().size()]);
            sendMail(emails, mailSubject, mailBodyBuilder.toString(), mailAttachments);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Failed to send email for RDCP File Replace:\n" + e.getMessage());
        }
    }

    /**
     * Notify files transferred expected if no files received with the configured notify interval (e.g. 7 days) reached.
     * @throws Exception
     * @return
     */
    public void notifyFilesTransferredExpected() throws Exception {
        if (transferredFiles.size() == 0 && notifyNeeded()) {
            String mailSubject = getEmailSubject(projectPath);
            String mailBody = "No files transferred in " + config.getNotifyInterva() + " days interval!";
            String[] emails = config.getDistributionEmails().toArray(new String[config.getDistributionEmails().size()]);

            log.info("Notify " + String.join(", ", emails) + ": " + mailBody);
            sendMail(emails, mailSubject, mailBody, null);
        }
    }

    /*
     * Checking for notification when no files transferred within the configured interval.
     * @return
     */
    protected boolean notifyNeeded() {
        boolean notifyNeeded = false;
        File reportFile = RdcpFileTransfer.getReportFile(projectPath, reportFileName);

        if (reportFile.exists()) {
            try {
                SimpleDateFormat dateFormat = new SimpleDateFormat(RdcpFileTransfer.DATE_PATTERN);

                // Calculate dates passed bassing on the last modified time of the report and send mails when the configured interval in days reached.
                Calendar lastModified = Calendar.getInstance();
                lastModified.setTimeInMillis(reportFile.lastModified());

                Date lastModifiedDate = dateFormat.parse(dateFormat.format(lastModified.getTime()));
                Date today = dateFormat.parse(dateFormat.format(Calendar.getInstance().getTime()));

                long diffMillis = Math.abs(today.getTime() - lastModifiedDate.getTime());
                long diffDays = TimeUnit.DAYS.convert(diffMillis, TimeUnit.MILLISECONDS);

                if (diffDays > 0 && (diffDays % config.getNotifyInterva()) == 0) {
                    notifyNeeded = true;
                    log.info("Notify needed: " + diffDays + " days no files received, configured notify interval " + config.getNotifyInterva());
                }
            } catch (ParseException e) {
                log.warn("Error parsing report lastModified time " + reportFile.lastModified(), e);
            }
        }
        return notifyNeeded;
    }

    /**
     * Send mails to report the status of the rdcp file replacement process
     * @param title
     * @param body
     * @param attachments
     * @throws Exception
     */
    public static void sendMail(String[] emails, String title, String body, String[] attachments) throws Exception {
        String sender = Constants.MAILSENDER_DAMSSUPPORT;

        DAMSClient.sendMail(sender, emails, title,
                body, "text/plain", "smtp.ucsd.edu", attachments);
    }

    /**
     * Error email notification title for rdcp file replacement
     * @return
     */
    public static String getEmailSubject() {
        return getEmailSubject(null);
    }

    /**
     * Error email notification title for rdcp file replacement
     * @param projectPath
     * @return
     */
    public static String getEmailSubject(String projectPath) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(RdcpFileTransfer.REPORT_DATE_PATTERN);

        String hostName = Constants.CLUSTER_HOST_NAME;
        String envPrefix = hostName.equalsIgnoreCase("library") ? "" :
                hostName.equalsIgnoreCase("librarytest") ? "Staging: " : 
                hostName.equalsIgnoreCase("qa") ? "QA: " : hostName + ": ";

        String projectInfo = StringUtils.isNotBlank(projectPath) ? " for " + Paths.get(projectPath).getFileName() : "";
        return envPrefix + "RDCP File Replacement Alert" + projectInfo + ", " + dateFormat.format(new Date());
    }

    /**
     * Get transfered files
     * @return
     */
    public List<TransferredFile> getTransferredFiles() {
        return transferredFiles;
    }
}
