package edu.ucsd.library.xdre.ingest.rdlshare;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.security.auth.login.LoginException;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.ClientProtocolException;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import edu.ucsd.library.xdre.ingest.RdcpFileReplaceHandler;
import edu.ucsd.library.xdre.ingest.RdcpFileTransfer;
import edu.ucsd.library.xdre.ingest.fileReplace.ArkFileMapping;
import edu.ucsd.library.xdre.ingest.fileReplace.TransferredFile;

/**
 * Implement the rules to download attachments from rdl-share API.
 * @author lsitu
 *
 */
public class RdlShareApiDownloader {
    private static Logger log = Logger.getLogger(RdlShareApiDownloader.class);

    private String projectPath;
    private RdlShareApiClient apiClient;
    private List<ArkFileMapping> arkFileMappings;

    public RdlShareApiDownloader(String projectPath, RdlShareApiClient apiClient, List<ArkFileMapping> arkFileMappings) {
        this.projectPath = projectPath;
        this.apiClient = apiClient;
        this.arkFileMappings = arkFileMappings;

        File downloadPath = new File(RdcpFileTransfer.getDownloadPath(projectPath));
        if (!downloadPath.exists()) {
            downloadPath.mkdirs();
        }
    }

    /**
     * Download attachments from rdl-share to rdcp-staging
     * @return
     * @throws ClientProtocolException
     * @throws LoginException
     * @throws IOException
     * @throws ParseException 
     */
    public List<RdlShareAttachment> downloadAttachments() throws ClientProtocolException,
            LoginException, IOException, ParseException {

        // list of attachments that match the criteria will be ignored
        List<RdlShareAttachment> badAttachments = new ArrayList<>();

        // Retrieve attachments from rdl-share api
        Map<String, RdlShareAttachment> rdlShareAttachments = retrieveAttachments(badAttachments);

        // Use the latest attachment if date prefix exists in filename
        List<RdlShareAttachment> attachments = dedupAttachmentByDate(rdlShareAttachments, badAttachments);

        // Download the attachments
        for (RdlShareAttachment attachment : attachments) {
            String stagingFile = RdcpFileTransfer.getDownloadFile(projectPath, attachment.getFileName()).getAbsolutePath();

            int numOfTry = 0;
            String checksumcrc32 = "";
            do {
                try {
                    String attamentUrl = attachment.getDownloadUrl();
                    if (StringUtils.isBlank(attamentUrl)) {
                        attachment.setError(true);
                        log.error("Download attachment url missing for file " + attachment.getFileName() + "!");
                        break;
                    }

                    apiClient.downloadFile(attachment.getDownloadUrl(), stagingFile);

                    checksumcrc32 = RdcpFileTransfer.checksumCrc32(stagingFile);
                    if (checksumcrc32.equalsIgnoreCase(attachment.getChecksumCrc32())) {
                        // only retry download if checksum failed
                        break;
                    }
                } catch (Exception e) {
                    if (numOfTry > 0) {
                        attachment.setError(true);
                        log.error("Failed to download attachment " + attachment.getDownloadUrl(), e);
                    }
                }

            } while (numOfTry++ == 0);

            attachment.setLocalChecksumCrc32(checksumcrc32);
        }

        // bad attachment: add back to the attachment list for report purpose
        attachments.addAll(badAttachments);

        return attachments;
    }

    /*
     * Retrieve attachments from rdl-share api
     * @param badAttachments
     * @return
     * @throws ClientProtocolException
     * @throws LoginException
     * @throws IOException
     * @throws ParseException
     */
    private Map<String, RdlShareAttachment> retrieveAttachments(List<RdlShareAttachment> badAttachments)
            throws ClientProtocolException, LoginException, IOException, ParseException {
        Map<String, RdlShareAttachment> attachmentsMap = new HashMap<>();

        JSONObject resultJsonObj = apiClient.retrieveMessages();
        JSONArray messages = (JSONArray)resultJsonObj.get(RdlShareAttachment.MESSAGES_KEY);
        for (Object mObj : messages) {
            JSONObject message = (JSONObject)mObj;
            String messageUrl = (String)message.get(RdlShareAttachment.URL_KEY);
            String sender = (String)message.get(RdlShareAttachment.SENDER_KEY);
            JSONArray attachmentArr = (JSONArray)message.get(RdlShareAttachment.ATTACHMENTS_KEY);

            if (attachmentArr != null) {
                for (Object attachmentObj : attachmentArr) {

                    RdlShareAttachment attachment = RdlShareAttachment.fromMessage(sender,
                            messageUrl, (JSONObject)attachmentObj);

                    RdlShareAttachment dupAttachment = attachmentsMap.get(attachment.getFileName());

                    // check for duplicate attachments, only the latest attachment will be accepted.
                    if (dupAttachment != null) {
                        if (dupAttachment.getCreatedAt().before(attachment.getCreatedAt())) {
                            attachmentsMap.put(attachment.getFileName(), attachment);
                            dupAttachment.setIgnore(true);
                            badAttachments.add(dupAttachment);
                        } else {
                            attachment.setIgnore(true);
                            badAttachments.add(attachment);
                        }
                    } else {
                        attachmentsMap.put(attachment.getFileName(), attachment);
                    }
                }
            }
        }

        return attachmentsMap;
    }

    /*
     * Get the list of latest attachment by comparing the date prefix in filename if exists
     * @param attachmentsMap
     * @param badAttachments
     * @return
     */
    private List<RdlShareAttachment> dedupAttachmentByDate(
            Map<String, RdlShareAttachment> attachmentsMap, List<RdlShareAttachment> badAttachments) {

        // Use the attachment that were tagged with the latest timestamp if it exists
        Map<String, RdlShareAttachment> downloadMap = new HashMap<>();
        for (String fileNameKey : attachmentsMap.keySet()) {
            RdlShareAttachment attm = attachmentsMap.get(fileNameKey);

            ArkFileMapping arkFileMapping = RdcpFileReplaceHandler.getArkFileMapping(arkFileMappings, attm.getFileName());
            // only attachments from the known recipient accounts with correct date format will be valid
            if (arkFileMapping != null && apiClient.getRecipients().contains(attm.getSender().toLowerCase())) {
                // convert the download attachments map that holds all valid attachments for dedupTransferredFileByDate() call
                Map<String, TransferredFile> filesMap = new HashMap<String, TransferredFile>(downloadMap);
                TransferredFile duplicateFile = attm.dedupTransferredFileByDate(filesMap);
                downloadMap = convertMap(filesMap);

                if (duplicateFile != null) {
                    badAttachments.add((RdlShareAttachment)duplicateFile);
                }
            } else {
                // Invalid attachment: filename pattern doesn't match
                attm.setInvalid(true);
                badAttachments.add(attm);
            }
        }

        // sort attachment list by date created
        List<RdlShareAttachment> attachments = new ArrayList<>(downloadMap.values());
        Collections.sort(attachments);

        return attachments;
    }

    private Map<String, RdlShareAttachment> convertMap(Map<String, TransferredFile> filesMap) {
        Map<String, RdlShareAttachment> mapConverted = new HashMap<>();
        for (String key : filesMap.keySet()) {
            mapConverted.put(key, (RdlShareAttachment)filesMap.get(key));
        }
        return mapConverted;
    }
}
