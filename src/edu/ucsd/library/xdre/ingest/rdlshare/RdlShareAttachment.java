package edu.ucsd.library.xdre.ingest.rdlshare;

import java.text.ParseException;
import java.time.OffsetDateTime;
import java.util.Date;

import org.json.simple.JSONObject;

import edu.ucsd.library.xdre.ingest.fileReplace.TransferredFile;

/**
 * Class RdlShareAttachment: the data structure for rdl-share api attachments
 *
 */
public class RdlShareAttachment extends TransferredFile {
    public static final String MESSAGE_KEY = "message";
    public static final String MESSAGES_KEY = "messages";
    public static final String ATTACHMENTS_KEY = "attachments";
    public static final String URL_KEY = "url";
    public static final String FILENAME_KEY = "filename";
    public static final String FILESIZE_KEY = "size";
    public static final String CHECKSUM_KEY = "checksum";
    public static final String CRC32_CHECKSUM_KEY = "crc32";
    public static final String CREATED_AT_KEY = "created_at";
    public static final String SENDER_KEY = "sender";

    private String messageUrl;         // attachment message url
    private String downloadUrl;        // attachment download url
    private String fileName;           // filename
    private long fileSize;             // file size
    private String checksum;           // checksum sha256
    private String checksumCrc32;      // checksum from attachment
    private String localChecksumCrc32 = ""; // local file checksum
    private Date createdAt;            // attachment created date
    private String sender;
    private boolean ignore = false;    // duplicate attachment
    private boolean invalid = false;   // invalid filename pattern
    private boolean error = false;     // error download the attachment

    public RdlShareAttachment(String messageUrl, String downloadUrl, String fileName,
            long fileSize, String checksum, String checksumCrc32, Date createdAt, String sender) {
        super(fileName, fileSize, checksum, checksumCrc32, createdAt);
        this.messageUrl = messageUrl;
        this.downloadUrl = downloadUrl;
        this.fileName = fileName;
        this.fileSize = fileSize;
        this.checksum = checksum;
        this.checksumCrc32 = checksumCrc32;
        this.createdAt = createdAt;
        this.sender = sender;
    }

    public String getMessageUrl() {
        return messageUrl;
    }

    public void setMessageUrl(String messageUrl) {
        this.messageUrl = messageUrl;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public long getFileSize() {
        return fileSize;
    }

    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }

    public String getChecksum() {
        return checksum;
    }

    public void setChecksum(String checksum) {
        this.checksum = checksum;
    }

    public String getLocalChecksumCrc32() {
        return localChecksumCrc32;
    }

    public void setLocalChecksumCrc32(String localChecksumCrc32) {
        this.localChecksumCrc32 = localChecksumCrc32;
    }

    public String getChecksumCrc32() {
        return checksumCrc32;
    }

    public void setChecksumCrc32(String checksumCrc32) {
        this.checksumCrc32 = checksumCrc32;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public boolean isIgnore() {
        return ignore;
    }

    public void setIgnore(boolean ignore) {
        this.ignore = ignore;
    }

    public boolean isInvalid() {
        return invalid;
    }

    public void setInvalid(boolean invalid) {
        this.invalid = invalid;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    /**
     * Construct RdlShareAttachment object instance from the attachment JSON data
     * @param messageId
     * @param attachmentJson
     * @return
     * @throws ParseException 
     */
    public static RdlShareAttachment fromMessage(String sender, String messageId, JSONObject attachment)
            throws ParseException {
        String downloadUrl = (String)attachment.get(URL_KEY);
        String fileName = (String)attachment.get(FILENAME_KEY);
        long fileSize = (long)attachment.get(FILESIZE_KEY);
        String checksum = (String)attachment.get(CHECKSUM_KEY);
        String crc32Checksum = (String)attachment.get(CRC32_CHECKSUM_KEY);

        OffsetDateTime oDateTime = OffsetDateTime.parse((String)attachment.get(CREATED_AT_KEY));
        Date createdAt = Date.from(oDateTime.toInstant());

        return new RdlShareAttachment(messageId, downloadUrl, fileName, fileSize,
                checksum, crc32Checksum, createdAt, sender);
    }

    @Override
    public int compareTo(TransferredFile o) {
        return ((RdlShareAttachment)this).createdAt.compareTo(((RdlShareAttachment)o).createdAt) ;
    }
}
