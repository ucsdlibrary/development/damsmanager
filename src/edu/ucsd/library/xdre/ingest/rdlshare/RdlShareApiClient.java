package edu.ucsd.library.xdre.ingest.rdlshare;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import javax.activation.FileDataSource;
import javax.security.auth.login.LoginException;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.dom4j.DocumentException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import edu.ucsd.library.xdre.ingest.fileReplace.RdcpFileReplaceConfig;
import edu.ucsd.library.xdre.utils.HttpClientBase;

/**
 * RdlShareApiClient provide functionalities to handle the rdl-share API request
 * @author lsitu
 * 
 */
public class RdlShareApiClient extends HttpClientBase {

    public static final String INBOX = "inbox";
    public static final String ATTACHMENTS = "attachments";
    public static final String MESSAGE = "message";
    public static final String MESSAGES = "messages";
    public static final String DELETE_ATTACHMENTS = "delete_attachments";

    private List<String> recipients = new ArrayList<>();

    /**
     * Construct RdlShareClient object.
     * @param config
     * @throws IOException 
     * @throws LoginException 
     */
    public RdlShareApiClient(RdcpFileReplaceConfig config) throws LoginException, IOException {
        this(config.getRdlShareUriBase(), config.getRdlShareApiKey(),
                config.getRdlSharePassword(), config.getRdfShareRecipients());
    }

    /**
     * Construct RdlShareClient object.
     * @param urlBase
     * @param apiKey
     * @param password
     * @param recipients
     * @throws LoginException
     * @throws IOException
     */
    public RdlShareApiClient(String urlBase, String apiKey, String password, List<String> recipients)
            throws LoginException, IOException {
        super(urlBase, apiKey, password);
        this.recipients = recipients;
    }

    public List<String> getRecipients() {
        return recipients;
    }

    public void setRecipients(List<String> recipients) {
        this.recipients = recipients;
    }

    /**
     * Retrieve messages from rdl-share api
     * @return
     * @throws ClientProtocolException
     * @throws LoginException
     * @throws IOException
     * @throws IllegalStateException
     * @throws DocumentException
     */
    public JSONObject retrieveMessages()
            throws ClientProtocolException, LoginException, IOException {
        return retrieveMessages(-1);
    }

    /**
     * Retrieve messages from rdl-share api
     * @param sentInHours - with -1 for all messages
     * @return
     * @throws ClientProtocolException
     * @throws LoginException
     * @throws IOException
     * @throws IllegalStateException
     * @throws DocumentException
     */
    public JSONObject retrieveMessages(int sentInHours)
            throws ClientProtocolException, LoginException, IOException {
        String url = santirizeUrlBase(storageURL) + MESSAGES + "/" + INBOX + "?"
                + (sentInHours > 0 ? "sent_in_the_last=" + sentInHours : "");
        HttpGet get = new HttpGet(url);
        get.setHeader(HttpHeaders.ACCEPT, "application/json");
        get.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");

        JSONObject messages = null;
        InputStream in = null;
        Reader reader = null;
        int status = -1;

        try {
            status = execute(get);

            if (status < 300) {
                in = response.getEntity().getContent();
                reader = new InputStreamReader(in);
                messages = (JSONObject) JSONValue.parse(reader);
            }else{
                handleError("");
            }
        }finally{
            get.releaseConnection();
            close(in);
            close(reader);
        }
        return messages;
    }

    /**
     * Upload file to rdl-share api as attachment
     * @param srcFile
     * @return
     * @throws ClientProtocolException
     * @throws LoginException
     * @throws IOException
     */
    public String addAttachment(String srcFile) throws ClientProtocolException, LoginException, IOException {
        String attachmentsUrl = santirizeUrlBase(storageURL) + ATTACHMENTS;
        HttpPost postAttachments = new HttpPost(attachmentsUrl);

        String attachmentId = "";

        File attFile = new File(srcFile);
        String contentType = new FileDataSource(srcFile).getContentType();
        FileBody fileBody = new FileBody(attFile, contentType);
        MultipartEntity ent = new MultipartEntity();
        ent.addPart("file", fileBody);

        postAttachments.setEntity(ent);

        InputStream in = null;
        Reader reader = null;
        int status = -1;
        try {
            status = execute(postAttachments);
            if (status < 300) {
                HttpEntity en = response.getEntity();
                Header encoding = en.getContentEncoding();
                attachmentId = EntityUtils.toString(en, (encoding==null?"UTF-8":encoding.getValue()));

                linkToMessage(attFile.getName(), attachmentId);
            }else{
                handleError("");
            }
        }finally{
            postAttachments.releaseConnection();
            close(in);
            close(reader);
        }
        return attachmentId;
    }

    /*
     * Link the attachment to message
     * @param attachmentId
     * @throws ClientProtocolException
     * @throws LoginException
     * @throws IOException
     */
    private void linkToMessage(String fileName, String attachmentId) throws ClientProtocolException,
            LoginException, IOException {
        String messageUrl = santirizeUrlBase(storageURL) + MESSAGE;

        HttpPost postMessage = new HttpPost(messageUrl);
        postMessage.setEntity(createMessageAttachment(fileName, attachmentId, recipients));
        postMessage.setHeader( "Accept", "application/json" );

        int status = -1;
        try {
            status = execute(postMessage);
            if (status >= 300) {
                handleError("");
            }
        }finally{
            postMessage.releaseConnection();
        }

    }

    /*
     * Create StringEntity for message attachment
     * @param attachmentId
     * @return
     * @throws UnsupportedEncodingException
     */
    private HttpEntity createMessageAttachment(String fileName, String attachmentId, List<String> recipients)
            throws UnsupportedEncodingException {
        JSONObject message = new JSONObject();
        JSONObject messageContent = new JSONObject();

        JSONArray recipientsArr = new JSONArray();
        recipientsArr.addAll(recipients);
        messageContent.put("recipients", recipientsArr);

        messageContent.put("subject", "File " + fileName);
        messageContent.put("message", "File Upload");
        messageContent.put("send_email", false);
        messageContent.put("authorization", 3);

        JSONArray attachments = new JSONArray();
        attachments.add(attachmentId);
        messageContent.put(RdlShareAttachment.ATTACHMENTS_KEY, attachments);

        message.put(RdlShareAttachment.MESSAGE_KEY, messageContent);

        StringEntity en = new StringEntity(message.toJSONString());
        en.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));

        return en;
    }

    /**
     * Delete attachments from message
     * @param messageUrl
     * @return
     * @throws LoginException
     * @throws IOException
     */
    public boolean deleteAttachment(String messageUrl) throws LoginException, IOException {
        HttpDelete delete = new HttpDelete(messageUrl + "/" + DELETE_ATTACHMENTS);
        delete.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
        delete.setHeader( "Accept", "application/json" );

        int status = -1;
        try {
            status = execute(delete);

            if (status >= 300) {
                handleError("");
            }
        }finally{
            delete.releaseConnection();
        }

        return true;
    }

    /**
     * Handle error response.
     * @param format
     * @throws IOException 
     * @throws LoginException 
     */
    public void handleError(String format) throws IOException, LoginException {
        int status = response.getStatusLine().getStatusCode();
        Header[] headers = response.getAllHeaders();
        for (int i = 0; i < headers.length; i++) {
            log.debug(headers[i] + "; ");
        }

        String reqInfo = request.getMethod() + " " + request.getURI();
        String respContent = EntityUtils.toString(response.getEntity());

        log.error(reqInfo + ": " + respContent);
        if (status == 401) {
            // 401 - unauthorized access
            throw new LoginException(reqInfo + ": " + respContent);
        } else {
            // Other error status codes
            throw new IOException(reqInfo + ": " + respContent);
        }
    }

    private String santirizeUrlBase(String urlBase) {
        return urlBase + (urlBase.endsWith("/") ? "" : "/");
    }
}
