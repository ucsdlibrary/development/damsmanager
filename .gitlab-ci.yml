image: ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/openjdk:8-alpine
before_script:
  - apk add --no-cache apache-ant git unzip curl

variables:
  OUTPUT_DIR: dist
  WAR_FILE: $OUTPUT_DIR/damsmanager.war

.deploy_helper: &deploy_helper |
  deploy_to() {
    if test $# -lt 1 ; then
      echo "You must specify an environment to deploy the application to. example: staging"
      echo "And optionally, a branch or tag. example: my-new-branch"
      exit 1
    fi
    apk add --no-cache apache-ant git unzip curl sudo openssh-client
    eval $(ssh-agent)
    echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
    mkdir -p ~/.ssh
    chmod 700 ~/.ssh
    ssh-keyscan "$TOMCAT_SERVER" >> ~/.ssh/known_hosts
    chmod 644 ~/.ssh/known_hosts

    if [ "$CHAT_INPUT" != "" ] ; then
      git fetch & git checkout "$CHAT_INPUT"
    fi
    ant clean webapp
    curl -T $WAR_FILE "http://$TOMCAT_USER:$TOMCAT_PASSWORD@$TOMCAT_SERVER:8080/manager/text/deploy?path=/damsmanager&update=true"
    if [ "$1" = "production" ]; then
      ssh "$TOMCAT_USER@$TOMCAT_SERVER" /home/tomcat/bin/restarttomcat
    else
      chmod 600 $ANSIBLE_USER
      ssh -i $ANSIBLE_USER "ansible@$TOMCAT_SERVER" sudo systemctl restart tomcat 
    fi

    echo -e "section_start:$( date +%s ):chat_reply\r\033[0K\nDeploy Succeeded - $CI_JOB_URL\nsection_end:$( date +%s ):chat_reply\r\033[0K"
  }

stages:
  - unit_test
  - build
  - chatops
  - deploy

unit_test:
  stage: unit_test
  script: ant junit

build:
  stage: build
  script:
    - ant clean webapp

qa_deploy:
  extends: .tomcat-deploy
  variables:
    TOMCAT_USER: tomcat
    TOMCAT_SERVER: lib-hydratail-qa.ucsd.edu
    TOMCAT_PASSWORD: $TOMCAT_MANAGER_QA_PASSWORD
    SSH_PRIVATE_KEY: $TOMCAT_SSH_QA
  script:
    - deploy_to "qa" "$CHAT_INPUT"
  before_script:
    - *deploy_helper

staging_deploy:
  extends: .tomcat-deploy
  variables:
    TOMCAT_USER: tomcat
    TOMCAT_SERVER: lib-hydratail-staging.ucsd.edu
    TOMCAT_PASSWORD: $TOMCAT_MANAGER_STAGING_PASSWORD
    SSH_PRIVATE_KEY: $TOMCAT_SSH_STAGING
  script:
    - deploy_to "staging" "$CHAT_INPUT"
  before_script:
    - *deploy_helper

production_deploy:
  extends: .tomcat-deploy
  variables:
    TOMCAT_USER: tomcat
    TOMCAT_SERVER: lib-ingest.ucsd.edu
    TOMCAT_PASSWORD: $TOMCAT_MANAGER_PRODUCTION_PASSWORD
    SSH_PRIVATE_KEY: $TOMCAT_SSH_PRODUCTION
  script:
    - deploy_to "production" "$CHAT_INPUT"
  before_script:
    - *deploy_helper

ops_deploy:
  extends: .tomcat-deploy
  variables:
    TOMCAT_USER: tomcat
    TOMCAT_SERVER: lib-hydratail-ops.ucsd.edu
    TOMCAT_PASSWORD: $TOMCAT_MANAGER_OPS_PASSWORD
    SSH_PRIVATE_KEY: $TOMCAT_SSH_OPS
  script:
    - deploy_to "ops" "$CHAT_INPUT"
  before_script:
    - *deploy_helper

production_scheduled_deploy:
  extends: .tomcat-deploy
  stage: deploy
  only:
    - schedules
  variables:
    TOMCAT_USER: tomcat
    TOMCAT_SERVER: lib-ingest.ucsd.edu
    TOMCAT_PASSWORD: $TOMCAT_MANAGER_PRODUCTION_PASSWORD
    SSH_PRIVATE_KEY: $TOMCAT_SSH_PRODUCTION
    DEPLOY_REF: master
  script:
    - deploy_to "production" "$DEPLOY_REF"
  before_script:
    - *deploy_helper

# Template for deploying to staging or production
.tomcat-deploy:
  stage: chatops
  only: [chat]
