package edu.ucsd.library.xdre.tab;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.Node;
import org.junit.Test;

/**
 * Tests for RecordUtil class
 * @author lsitu
 *
 */
public class RecordUtilTest extends TabularRecordTestBasic {

    private final String UNIT_URI = "http://library.ucsd.edu/ark:/20775/bb6827300d";

    @Test
    public void testAddRightsCc0() throws Exception {
        Document doc = createDocumentRoot("objectId#1").getDocument();
        String copyrightJurisdiction = "";
        String access = "";
        String program = RecordUtil.programRDC;
        String[] copyrightOwners = null;
        Map<String, String> collections = new HashMap<>();
        RecordUtil.addRights(doc, UNIT_URI, collections, RecordUtil.copyrightCc0,
                copyrightJurisdiction, copyrightOwners, program, access, null, null);

        //// verify dams:Copyright
        List<Node> copyrightNodes = doc.selectNodes("//dams:Copyright");
        assertEquals(1, copyrightNodes.size());

        Node copyrightNode = copyrightNodes.get(0);;
        String copyrightStatus = copyrightNode.valueOf("dams:copyrightStatus");
        assertEquals(RecordUtil.copyrightCc0, copyrightStatus);

        String copyrightPurposeNote = copyrightNode.valueOf("dams:copyrightPurposeNote");
        assertEquals(RecordUtil.copyrightPurposeNoteCc0, copyrightPurposeNote);

        String copyrightNote = copyrightNode.valueOf("dams:copyrightNote");
        assertEquals(RecordUtil.copyrightNotePublicDomain, copyrightNote);

        // verify no copyright jurisdiction
        assertNull(copyrightNode.selectSingleNode("dams:copyrightJurisdiction"));

        //// verify dams:License
        List<Node> licenseNodes = doc.selectNodes("//dams:License");
        assertEquals(1, copyrightNodes.size());

        Node licenseNode = licenseNodes.get(0);
        String licenseNote = licenseNode.valueOf("dams:licenseNote");
        assertEquals(RecordUtil.accessCc0, licenseNote);

        String licenseURI = licenseNode.valueOf("dams:licenseURI");
        assertEquals(RecordUtil.creativeCommonsV0, licenseURI);

        String permissionType = licenseNode.valueOf("dams:permission/dams:Permission/dams:type");
        assertEquals("display", permissionType);
    }

    @Test
    public void testAddAcademicLiaisonProgramNote() throws Exception {
        Document doc = createDocumentRoot("objectId#1").getDocument();
        String copyrightJurisdiction = "";
        String access = "";
        String program = RecordUtil.programAL;
        String[] copyrightOwners = null;
        Map<String, String> collections = new HashMap<>();
        RecordUtil.addRights(doc, UNIT_URI, collections, RecordUtil.copyrightPublic,
                copyrightJurisdiction, copyrightOwners, program, access, null, null);

        //// verify local attribution note
        List<Node> programNoteNodes = doc.selectNodes("//dams:Note[dams:type='local attribution']");
        assertEquals(1, programNoteNodes.size());

        Node programNote = programNoteNodes.get(0);;
        String programNoteValue = programNote.valueOf("rdf:value");
        assertEquals(RecordUtil.programALnote, programNoteValue);

        String displayLabel = programNote.valueOf("dams:displayLabel");
        assertEquals("digital object made available by", displayLabel);
    }
}
