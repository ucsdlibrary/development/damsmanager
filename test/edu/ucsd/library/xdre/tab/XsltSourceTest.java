package edu.ucsd.library.xdre.tab;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import org.apache.commons.lang3.StringUtils;
import org.dom4j.Document;
import org.junit.Before;
import org.junit.Test;

/**
 * Tests for XsltSource class
 * @author lsitu
 *
 */
public class XsltSourceTest extends TabularRecordTestBasic {

    private static final String testId = "991000022469706535";

    private File xsltFile = null;
    private File modsExampleFile = null;

    /*
     * Test initiation
     * @throws Exception
     */
    @Before
    public void init() throws Exception {
        xsltFile = getResourceFile("mets2dams.xsl");
        modsExampleFile = getResourceFile("mods-example.xml");
    }

    @Test
    public void testMeds2DamsWithID() throws Exception {
        try (InputStream modsIn = new FileInputStream(modsExampleFile);) {
            XsltSource xsltSource = new XsltSource(xsltFile, testId, modsIn);
            Record record = xsltSource.nextRecord();
            
            assertEquals(testId, record.recordID());

            Document doc = record.toRDFXML();

            String nodeId = doc.valueOf("/rdf:RDF/*/@rdf:about | /*/@rdf:about");
            assertEquals("The RDF node ID doesn't match!", testId, nodeId);
        }
    }

    @Test
    public void testMeds2DamsWithNoID() throws Exception {
            XsltSource xsltSource = new XsltSource(xsltFile, modsExampleFile);
            Record record = xsltSource.nextRecord();

            assertNotEquals(testId, record.recordID());

            Document doc = record.toRDFXML();

            String nodeId = doc.valueOf("/rdf:RDF/*/@rdf:about | /*/@rdf:about");
            assertTrue("The default RDF node ID wasn't assigned!", StringUtils.isNotBlank(nodeId));
    }
}
