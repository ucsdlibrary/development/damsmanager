package edu.ucsd.library.xdre.tab;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.Charsets;
import org.dom4j.Document;
import org.dom4j.DocumentFactory;
import org.dom4j.Element;
import org.dom4j.Namespace;
import org.dom4j.Node;
import org.dom4j.QName;

import edu.ucsd.library.xdre.utils.Constants;

/**
 * Utility methods for testing TabularEditRecord
 * @author lsitu
 *
 */
public class TabularRecordTestBasic {
	protected File xlsInputTemplate = null;
	protected File xlsInputTestFile = null;

    protected void init() throws Exception {
        Constants.DAMS_STORAGE_URL = "http://localhost:8080/dams/api";
        Constants.DAMS_ARK_URL_BASE = "http://library.ucsd.edu/ark:";
        Constants.ARK_ORG = "20775";

        Map<String, String> nsPrefixes = new HashMap<>();
        nsPrefixes.put("rdf", "http://www.w3.org/1999/02/22-rdf-syntax-ns#");
        nsPrefixes.put("mads", "http://www.loc.gov/mads/rdf/v1#");
        nsPrefixes.put("dams", "http://library.ucsd.edu/ontology/dams#");

        Constants.NS_PREFIX_MAP = nsPrefixes;

        xlsInputTemplate = getResourceFile("xls_standard_input_template.xlsx");
    }

    protected static void addAttribute(Element e, String name, Namespace ns, String value)
    {
        e.addAttribute( new QName(name,ns), value );
    }

    protected Element createDocumentRoot(String uri) {
        Document doc = new DocumentFactory().createDocument();
        Element rdf = TabularRecord.createRdfRoot (doc);

        Element e = TabularRecord.addElement(rdf, "Object", TabularRecord.damsNS);

        addAttribute(e, "about", TabularRecord.rdfNS, uri);
        return e;
    }

    protected TabularEditRecord createdRecordWithOverlay(Map<String, String> data, Map<String, String> overlayData)
            throws Exception {
        return createdRecordWithOverlay(data, overlayData, new ArrayList<String>());
    }

    protected TabularEditRecord createdRecordWithOverlay(Map<String, String> data, Map<String, String> overlayData,
            List<String> eventUrls) throws Exception {
        String objUrl = TabularEditRecord.getArkUrl(data.get(TabularRecord.OBJECT_ID));
        data.put("ark", objUrl);
        Record record = new TabularRecord( data, null);
        Document doc = record.toRDFXML();
        if (eventUrls != null && eventUrls.size() > 0) {
            Element el = (Element)doc.selectSingleNode("//dams:Object");
            for (String eventUrl : eventUrls) {
                el.addElement(new QName("event", TabularRecord.damsNS))
                  .addAttribute(new QName("resource", TabularRecord.rdfNS), eventUrl);
            }
        }

        return createdRecordWithOverlay(doc, overlayData);
    }

    protected TabularEditRecord createdRecordWithOverlay(Document doc, Map<String, String> overlayData)
            throws Exception {
        return createdRecordWithOverlay(doc, overlayData, null);
    }

    protected TabularEditRecord createdRecordWithOverlay(Document doc, Map<String, String> objectData,
            Map<String, String> compData) throws Exception {
        TabularEditRecord editRecord = new TabularEditRecord(objectData, null, doc);

        if (compData != null && compData.size() > 0) {
            TabularEditRecord comp = new TabularEditRecord();
            comp.setData(compData);
            editRecord.addComponent(comp);
        }

        return editRecord;
    }

    protected TabularEditRecord createdRecordWithOverlay(Document doc, Map<String, String> objectData,
            Map<String, String> compData, Map<String, String> subcompData) throws Exception {
        TabularEditRecord editRecord = new TabularEditRecord(objectData, null, doc);

        TabularEditRecord comp = null;
        if (compData != null && compData.size() > 0) {
            // component
            comp = new TabularEditRecord();
            comp.setData(compData);
            editRecord.addComponent(comp);

            // sub-component
            if (subcompData != null && subcompData.size() > 0) {
                TabularEditRecord subcomp = new TabularEditRecord();
                subcomp.setData(subcompData);
                comp.addComponent(subcomp);
            }
        }

        return editRecord;
    }

    protected Map<String, String> createDataWithTitle(String oid, String title) {
        return createDataWithTitle(oid, title, "object");
    }

    protected Map<String, String> createDataWithTitle(String oid, String title, String level) {
        Map<String, String> data = new HashMap<>();
        data.put(TabularRecord.OBJECT_ID, oid);
        data.put(TabularRecord.OBJECT_COMPONENT_TYPE, level);
        data.put("title", title);

        return data;
    }

    protected File getResourceFile(String fileName) throws IOException {
        File resourceFile = new File(fileName);
        resourceFile.deleteOnExit();

        byte[] buf = new byte[4096];
        try(InputStream in = getClass().getResourceAsStream("/resources/" + fileName);
                FileOutputStream out = new FileOutputStream(resourceFile)) {

            int bytesRead = 0;
            while ((bytesRead = in.read(buf)) > 0) {
                out.write(buf, 0, bytesRead);
            }
        }
        return resourceFile;
    }

    protected InputStream getResource(String fileName) throws IOException {
        return getClass().getResourceAsStream("/resources/" + fileName);
    }

    protected String getResourceAsString(String fileName) throws IOException {
        byte[] buf = new byte[4096];
        try(InputStream in = getClass().getResourceAsStream("/resources/" + fileName);
                ByteArrayOutputStream out = new ByteArrayOutputStream();) {

            int bytesRead = 0;
            while ((bytesRead = in.read(buf)) > 0) {
                out.write(buf, 0, bytesRead);
            }
            return out.toString(Charsets.UTF_8.name());
        }
    }

    protected String getOverlayValue(String value) {
        return value + " overlay";
    }

    protected Map<String, String> createSubjectDataWithTerm(String oid, String type, String term) {
        Map<String, String> data = new HashMap<>();
        data.put(SubjectTabularRecord.ARK.toLowerCase(), oid);
        data.put(SubjectTabularRecord.SUBJECT_TYPE.toLowerCase(), type);
        data.put(SubjectTabularRecord.SUBJECT_TERM.toLowerCase(), term);

        return data;
    }

    protected SubjectTabularEditRecord createdSubjectWithOverlay(Map<String, String> data, Map<String, String> overlayData)
            throws Exception {
        Record record = new SubjectTabularRecord( data);
        Document doc = record.toRDFXML();

        SubjectTabularEditRecord editRecord = new SubjectTabularEditRecord(doc, overlayData);
        return editRecord;
    }

    protected boolean containsAttribute(List<Node> nodes, String attrName, String attrValue) {
        for (Node node : nodes) {
            if (node.selectSingleNode("@" + attrName).getStringValue().equals(attrValue)) {
                return true;
            }
        }
        return false;
    }
}
