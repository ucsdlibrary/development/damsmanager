package edu.ucsd.library.xdre.tab;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import edu.ucsd.library.xdre.utils.Constants;
import edu.ucsd.library.xdre.utils.DAMSClient;
import edu.ucsd.library.xdre.web.CollectionOperationController;

/**
 * Test methods for SubjectEditExcelSource class
 * @author lsitu
 *
 */
public class SubjectEditExcelSourceTest extends TabularRecordTestBasic {

    private File xlsInputTemplate = null;
    private File xlsInputTestFile = null;
    private SubjectEditExcelSource excelSource = null;

    @Before
    public void init() throws Exception {
        Constants.DAMS_STORAGE_URL = "http://localhost:8080/dams/api";
        Constants.DAMS_ARK_URL_BASE = "http://library.ucsd.edu/ark:";
        Constants.ARK_ORG = "20775";

        xlsInputTemplate = getResourceFile("xls_standard_input_template.xlsx");
        xlsInputTestFile = getResourceFile("xls_subject_edit_input_test.xlsx");
        SubjectEditExcelSource.initControlValues(xlsInputTemplate);

        DAMSClient damsClient = new DAMSClient(Constants.DAMS_STORAGE_URL);
        excelSource = new SubjectEditExcelSource(xlsInputTestFile, Arrays.asList(SubjectTabularRecord.ALL_FIELDS_FOR_SUBJECTS), damsClient);
    }

    @Test
    public void testReportInvalidHeader() throws Exception {
        assertEquals("Invalid header isn't reported!", 1, excelSource.getInvalidColumns().size());

        List<String> invalidHeaders = excelSource.getInvalidColumns();
        assertEquals("Invalid header 'invalid column' doesn't match!", "invalid column", invalidHeaders.get(0));
    }

    @Ignore
    public void testNextRecord() throws Exception {
        SubjectTabularRecord editRecord = (SubjectTabularRecord)excelSource.nextRecord();

        assertNotNull(editRecord);

        // record id
        assertEquals("Record ID doesn't match!", "xx00000001", editRecord.recordID());

        Map<String, String> data = editRecord.getData();

        // subject type
        assertEquals("Field value for 'subject type' doesn't match!", "Subject:personal name", data.get("subject type"));

        // subject term
        assertEquals("Field value for 'subject term' doesn't match!", "Subject Term", data.get("subject term"));

        // closeMatch
        assertEquals("Field value for 'closeMatch' doesn't match!", "http://viaf.org/viaf/close_match | http://vocab.getty.edu/aat/close_match", data.get("closematch"));

        // exactMatch
        assertEquals("Field value for 'exactMatch' doesn't match!", "http://id.worldcat.org/fast/exact_match", data.get("exactmatch"));

        // hiddenVariant
        assertEquals("Field value for 'hiddenVariant' doesn't match!", "Hidden Variant", data.get("hiddenvariant"));

        //vVariant
        assertEquals("Field value for 'variant' doesn't match!", "\"Variant\"@en", data.get("variant"));
    }

    @Ignore
    public void testSubjectTypeCvValidation() throws Exception {
        // Move to the second row for the invalid subject type
        excelSource.nextRecord();
        excelSource.nextRecord();

        // Validate invalid column is caught
        List<Map<String, String>> invalidCvs = excelSource.getInvalidValues();
        assertEquals("Invalid CV isn't caught!", 1, invalidCvs.size());
        assertEquals("Invalid subject type 'Subject:Invalid' isn't matched!", "Subject:Invalid", invalidCvs.get(0).get("subject type"));

        String errorReport = CollectionOperationController.getInvalidControlValues(invalidCvs, true);
        assertTrue("Invalid subject type 'Subject:Invalid' isn't reported!", errorReport.contains("Subject:Invalid"));

        // Move to the third row for the valid complex subject type
        excelSource.nextRecord();

        // Validate that complex subject type is caught but accepted for overlay
        assertEquals("Extra overlay subject type 'Subject:Complex' doesn't exist!", "Subject:Complex", invalidCvs.get(1).get("subject type"));

        errorReport = CollectionOperationController.getInvalidControlValues(invalidCvs, true);
        assertFalse("Extra overlay subject type 'Subject:Complex' isn't validated!", errorReport.contains("Subject:Complex"));
    }

}
