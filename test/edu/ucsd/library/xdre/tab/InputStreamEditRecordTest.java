package edu.ucsd.library.xdre.tab;

import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.*;
import static org.mockito.Mockito.mock;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.security.auth.login.LoginException;

import org.dom4j.Document;
import org.dom4j.Node;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import edu.ucsd.library.xdre.utils.Constants;
import edu.ucsd.library.xdre.utils.DAMSClient;

/**
 * Tests for InputStreamEditRecord class
 * @author lsitu
 *
 */
public class InputStreamEditRecordTest extends TabularRecordTestBasic {

    @Before
    public void init() throws LoginException, IOException {
        Constants.DAMS_STORAGE_URL = "http://localhost:8080/dams/api";
        Constants.DAMS_ARK_URL_BASE = "http://library.ucsd.edu/ark:";
        Constants.ARK_ORG = "20775";
        Constants.BATCH_ADDITIONAL_FIELDS = "Note:local attribution,collection(s),unit";
    }

    @Test
    public void testInitEditingOnce() throws Exception {

        // Initiate tabular record for edit
        String title = "Test object";
        String objUrl = TabularEditRecord.getArkUrl("zzxxxxxxxx");
        Map<String, String> data = createDataWithTitle(objUrl, title);

        TabularRecord tabRecord = new TabularRecord(data);
        tabRecord.setIgnoreCopyright(true);
        Document docOrig = tabRecord.toRDFXML();

        DAMSClient damsClient = mock(DAMSClient.class);
        when(damsClient.getRecord(any(String.class))).thenReturn(docOrig);

        String overlayTitle = "Test object Edited";
        Map<String, String> overlayData = createDataWithTitle(objUrl, overlayTitle);
        overlayData.put("copyrightstatus", "under copyright");

        // Create record with data overlay
        TabularEditRecord tabEditRecord = createdRecordWithOverlay(docOrig, overlayData);

        TabularEditRecord spyTabEditRecord = Mockito.spy(tabEditRecord);
        InputStreamEditRecord inputStreamRecord = new InputStreamEditRecord(spyTabEditRecord, damsClient);

        Document docEdited = inputStreamRecord.toRDFXML();

        // verify method editDocument() in TabularEditRecord will be called only once.
        verify(spyTabEditRecord, times(1)).editDocument();

        List<Node> copyrightNodes = docEdited.selectNodes("//dams:copyright");
        assertEquals("Copyright node not found!", 1, copyrightNodes.size());

        String copyrightStatus = docEdited.valueOf(("//dams:copyright/dams:Copyright/dams:copyrightStatus"));
        assertEquals("Copyright status doesn't match!", "under copyright", copyrightStatus);
    }
}
