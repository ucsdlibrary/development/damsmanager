package edu.ucsd.library.xdre.tab;

import static junit.framework.TestCase.fail;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.transform.TransformerException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.junit.Test;

/**
 * Test methods for RDFExcelConvertorTest class
 * @author lsitu
 *
 */
public class RDFExcelConvertorTest extends TabularRecordTestBasic {

    private static final String TEST_OBJECT_ARK = "zz00000001";
    private static final String CC_LICENSE ="cc license";

    @Test
    public void testJSONParseBackslashException() throws UnsupportedEncodingException, IOException {
        String jsonString = "{\"record_id\":[{\"title\":\"Test title backslash \\\"},{\"typeOfResource\":\"Still Image\"}]}";
        String error = "Error parse json: Unexpected character (t) at position 51.\n"
                + "Source: {\"record_id\":[{\"title\":\"Test title backslash \\\"},{\"typeOfResource\":\"Still Image\"}]}";
        try {
            RDFExcelConvertor.parseJson(jsonString);
            fail();
        } catch(TransformerException ex) {
            assertEquals(error, ex.getMessage());
        }
    }

    @Test
    public void testJsonParseWithBackslashEscaped() throws UnsupportedEncodingException, IOException, TransformerException {
        String jsonString = "{\"record_id\":[{\"title\":\"Test title backslash \\\\\"},{\"typeOfResource\":\"Still Image\"}]}";

        JSONObject json = RDFExcelConvertor.parseJson(jsonString);
        String expectedTitle = ((JSONObject)((JSONArray)json.get("record_id")).get(0)).get("title").toString();
        assertEquals("Test title backslash \\", expectedTitle);
    }

    @Test
    public void testAddRdfSource() throws IOException, TransformerException {
        String xsl = getClass().getResource("/resources/dams42json-export.xsl").getPath();
        String rdf = getResourceAsString("dams_object_zz00000001.xml");
        RDFExcelConvertor convertor = new RDFExcelConvertor(xsl);

        convertor.addRdfSource(rdf, "http://library.ucsd.edu/ark:/20775/zz00000001");

        List<List<Map<String,String>>> objectRecords = convertor.getObjectRecords();
        assertEquals(1, objectRecords.size());

        Map<String,String> objMetadata = objectRecords.iterator().next().get(0);

        //// Validate object level metadata
        assertEquals("Object title isn't matched!", "Test Object Title", objMetadata.get("Title"));
        assertEquals("Resource type isn't matched!", "data", objMetadata.get("Type of Resource"));
        assertEquals("Language isn't matched!", "zxx - No linguistic content @ xxll000001", objMetadata.get("Language"));
        assertEquals("Topic isn't matched!", "Test Topic @ xxtt000001", objMetadata.get("Subject:topic"));
        assertEquals("Local attribution note isn't matched!", "Local attribution note", objMetadata.get("Note:local attribution"));
        assertEquals("Public Source Download isn't matched!", "True", objMetadata.get("Public Source Download"));

        // Date creation fields
        assertEquals("Date creation isn't matched!", "2021-05", objMetadata.get("Date:creation"));
        assertEquals("Begin date isn't matched!", "2021-05-01", objMetadata.get("Begin date"));
        assertEquals("End date isn't matched!", "2021-05-25", objMetadata.get("End date"));

        // Copyright fields
        assertEquals("CopyrightJurisdiction isn't matched!", "us", objMetadata.get("copyrightJurisdiction"));
        assertEquals("CopyrightStatus isn't matched!", "Under copyright", objMetadata.get("copyrightStatus"));
        assertEquals("CopyrightNote isn't matched!", "Constraint(s) on Use: Copyright note.", objMetadata.get("copyrightNote"));
        assertEquals("CopyrightPurposeNote isn't matched!", "Use: Copyright purpose note.", objMetadata.get("copyrightPurposeNote"));

        // Collection(s) and Unit
        assertEquals("Collection isn't matched!", "Test Collection @ xxcc000001", objMetadata.get("collection(s)"));
        assertEquals("Unit isn't matched!", "Test Unit @ xxuu000001", objMetadata.get("unit"));

        //// Validate component level metadata
        Map<String,String> compMetadata = objectRecords.iterator().next().get(1);
        assertEquals("Component title isn't matched!", "Test Component Title", compMetadata.get("Title"));
        assertEquals("Component resource type isn't matched!", "data", compMetadata.get("Type of Resource"));
        assertEquals("Component note isn't matched!", "A test note", compMetadata.get("Note:note"));
        assertEquals("Component Public Source Download isn't matched!", "False", compMetadata.get("Public Source Download"));

        // File fields
        assertEquals("Component file name isn't matched!", "test-file.txt", compMetadata.get("File name"));
        assertEquals("Component file use isn't matched!", "data-service", compMetadata.get("File use"));
    }

    @Test
    public void testSortObjectByKey() throws UnsupportedEncodingException, IOException, TransformerException {
        String xsl = getClass().getResource("/resources/dams42json-export.xsl").getPath();
        RDFExcelConvertor convertor = new RDFExcelConvertor(xsl);
        List<List<Map<String,String>>> objectRecords = convertor.getObjectRecords();

        String shareAlike = "Attribution-ShareAlike";
        String attribution = "Attribution";
        String noDerivs = "Attribution-NoDerivs";
        objectRecords.add(createSimpleObjectRecordWithCCRights("00310", "Title 00310", shareAlike));
        objectRecords.add(createSimpleObjectRecordWithCCRights("00314", "Title 00314", noDerivs));
        objectRecords.add(createSimpleObjectRecordWithCCRights("00312", "Title 00312", attribution));
        objectRecords.add(createSimpleObjectRecordWithCCRights("00311", "Title 00311", shareAlike));

        convertor.getFieldCounts().put(CC_LICENSE, 1);
        convertor.sortObjectBy(CC_LICENSE);
        String csv = convertor.convert2CSV(true);
        int indexShareAlikeFirst = csv.indexOf(shareAlike);
        int indexShareAlikeLast = csv.indexOf(shareAlike);
        int indexNoDerivs = csv.indexOf(noDerivs);
        int indexattribution = csv.indexOf(attribution);
        assertTrue(indexShareAlikeFirst > indexattribution
            && indexShareAlikeLast > indexattribution
            && indexNoDerivs > indexattribution);
    }

    private List<Map<String,String>> createSimpleObjectRecordWithCCRights(String objId, String title, String ccLicense) {
        List<Map<String,String>> compDate = new ArrayList<>();
        Map<String, String> attrs = new HashMap<>();
        attrs.put("Object Unique ID", objId);
        attrs.put("Level", "Object");
        attrs.put("Title", title);
        attrs.put(CC_LICENSE, ccLicense);
        compDate.add(attrs);

        return compDate;
    }
}
