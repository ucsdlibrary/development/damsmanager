package edu.ucsd.library.xdre.tab;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.security.auth.login.LoginException;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Node;
import org.junit.Before;
import org.junit.Test;

import edu.ucsd.library.xdre.model.Subject;
import edu.ucsd.library.xdre.utils.Constants;

/**
 * Test methods for SubjectTabularEditRecord class
 * @author lsitu
 *
 */
public class SubjectTabularEditRecordTest extends TabularRecordTestBasic {

    @Before
    public void init() throws LoginException, IOException {
        Constants.DAMS_STORAGE_URL = "http://localhost:8080/dams/api";
        Constants.DAMS_ARK_URL_BASE = "http://library.ucsd.edu/ark:";
        Constants.ARK_ORG = "20775";

        Map<String, String> nsPrefixes = new HashMap<>();
        nsPrefixes.put("rdf", "http://www.w3.org/1999/02/22-rdf-syntax-ns#");
        nsPrefixes.put("mads", "http://www.loc.gov/mads/rdf/v1#");
        nsPrefixes.put("dams", "http://library.ucsd.edu/ontology/dams#");

        Constants.NS_PREFIX_MAP = nsPrefixes;
    }

    @Test
    public void testOverlaySubject() throws Exception {
        // Initiate subject tabular data for edit
        String type = "Subject:topic";
        String typeOverlay = "Subject:anatomy";
        String term = "Test term";
        String termOverlay = getOverlayValue(term);
        
        Map<String, String> data = createSubjectDataWithTerm("zzxxxxxxxx", type, term);
        Map<String, String> overlayData = createSubjectDataWithTerm("zzxxxxxxxx", typeOverlay, termOverlay);

        // Create record with data overlay
        SubjectTabularEditRecord testSubject = createdSubjectWithOverlay(data, overlayData);
        Document docEdited = testSubject.toRDFXML();

        // validate subject type is edited
        Node typeElem = docEdited.selectSingleNode("//dams:Anatomy");
        assertNotNull("Field value for 'subject type' doesn't match!", typeElem);

        // validate subject term is edited
        List<Node> termNodes = docEdited.selectNodes("//" + Subject.MADS_AUTHORITATIVE_LABEL);
        assertEquals("Subject term size doesn't match!", 1, termNodes.size());

        String actualResult = termNodes.get(0).getText();
        assertEquals("Field value for 'subject term' doesn't match!", termOverlay, actualResult);
    }

    @Test
    public void testOverlayWithMultiValueField() throws Exception {
        // Initiate subject tabular data for edit
        String type = "Subject:topic";
        String term = "Test term";
        Map<String, String> data = createSubjectDataWithTerm("zzxxxxxxxx", type, term);
        String closeMatch = "http://viaf.org/viaf/close_match";
        data.put("closematch", closeMatch);

        Map<String, String> overlayData = createSubjectDataWithTerm("zzxxxxxxxx", type, term);
        String closeMatchAddition = "http://vocab.getty.edu/aat/close_match";
        String closeMatchOverlay = closeMatch + " | " + closeMatchAddition;
        overlayData.put("closematch", closeMatchOverlay);

        // Create record with data overlay
        SubjectTabularEditRecord testSubject = createdSubjectWithOverlay(data, overlayData);
        Document docEdited = testSubject.toRDFXML();

        // validate closeMatch has two fields
        List<Node> closeMatchNodes = docEdited.selectNodes("//mads:" + Subject.HAS_CLOSE_EXTERNAL_AUTHORITY);
        assertEquals("Field size for 'closeMatch' doesn't match!", 2, closeMatchNodes.size());

        boolean attrExist = containsAttribute(closeMatchNodes, "rdf:resource", closeMatch);
        assertTrue("Field value for 'closeMatch' doesn't exist!", attrExist);

        attrExist = containsAttribute(closeMatchNodes, "rdf:resource", closeMatchAddition);
        assertTrue("Field value for 'closeMatch' doesn't exist!", attrExist);
    }

    @Test
    public void testOverlayWithXmlLang() throws Exception {
        // Initiate subject tabular data for edit
        String type = "Subject:topic";
        String term = "Test term";
        String termOverlay = "\"陈昭宏, 1942-\"@zh-Hans";

        Map<String, String> data = createSubjectDataWithTerm("zzxxxxxxxx", type, term);
        Map<String, String> overlayData = createSubjectDataWithTerm("zzxxxxxxxx", type, termOverlay);

        // Create record with data overlay
        SubjectTabularEditRecord testSubject = createdSubjectWithOverlay(data, overlayData);
        Document docEdited = testSubject.toRDFXML();

        // validate subject term is updated with xml:lang
        List<Node> termNodes = docEdited.selectNodes("//" + Subject.MADS_AUTHORITATIVE_LABEL);
        assertEquals("Field value for 'subject term' size doesn't match!", 1, termNodes.size());

        Node authNode = termNodes.get(0);
        // xml:lang
        String xmlLang = ((Element)authNode).attributeValue("xml:lang");
        assertEquals("Attribute xml:lang doesn't match!", "zh-Hans", xmlLang);
        // mads:authoritativeLabel
        assertEquals("Field value for 'subject term' doesn't match!", "陈昭宏, 1942-", authNode.getText());
    }

    @Test
    public void testOverlayComplexSubject() throws Exception {
        // Initiate subject tabular data for edit
        String type = "Subject:Complex";
        String term = "Term ComplexSubject";
        String termOverlay = getOverlayValue(term);

        Map<String, String> data = createSubjectDataWithTerm("zzxxxxxxxx", type, term);
        Map<String, String> overlayData = createSubjectDataWithTerm("zzxxxxxxxx", type, termOverlay);

        // Create complex subject with data overlay
        SubjectTabularEditRecord testSubject = createdSubjectWithOverlay(data, overlayData);
        Document docEdited = testSubject.toRDFXML();

        // Validate subject type is complex subject
        Node typeElem = docEdited.selectSingleNode("//mads:ComplexSubject");
        assertNotNull("Complex subject doesn't created!", typeElem);

        // Validate subject term for complex subject is overlay
        List<Node> termNodes = docEdited.selectNodes("//" + Subject.MADS_AUTHORITATIVE_LABEL);
        assertEquals("Complex subject term size doesn't match!", 1, termNodes.size());

        String actualResult = termNodes.get(0).getText();
        assertEquals("Complex subject term doesn't match!", termOverlay, actualResult);
    }
}
