package edu.ucsd.library.xdre.tab;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

/**
 * Tests for FilesChecker class
 * @author lsitu
 *
 */
@Ignore
public class FilesCheckerTest extends TabularRecordTestBasic {

    private final String BAD_TIMESTAMP_FILE = "audio-bad-timestamp.mp3";
    private final String EXIF_TOOL = "/usr/local/bin/exiftool";
 
    private List<String> fileNames = null;
    private Map<String, File> sourceFiles = null;

    @Before
    public void init() {
        fileNames = new ArrayList<>();
        sourceFiles = new TreeMap<>();
    }

    @Test
    public void testReportBadTimestamp() throws Exception {
        File audioFile = getResourceFile(BAD_TIMESTAMP_FILE);
        String fileName = audioFile.getName();
        fileNames.add(fileName);
        sourceFiles.put(fileName, audioFile);
        FilesChecker filesChecker = new FilesChecker(fileNames, sourceFiles);
        filesChecker.setCommand(EXIF_TOOL);

        assertFalse(filesChecker.filesValidate());
        Map<File, String> invalidFiles = filesChecker.getInvalidFiles();
        assertTrue(invalidFiles.size() > 0);

        assertTrue("Audio file with invalid timestamp is not caught.", invalidFiles.containsKey(audioFile));

        String error = invalidFiles.get(audioFile);
        assertEquals("Message for invalid timestamp file doesn't match.", "Error: Modify Date: 2020:07:03 18:06-07:00", error);
    }
}
