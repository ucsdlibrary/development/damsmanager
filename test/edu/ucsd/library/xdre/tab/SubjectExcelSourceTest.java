package edu.ucsd.library.xdre.tab;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

/**
 * Test methods for SubjectExcelSource class
 * @author lsitu
 *
 */
public class SubjectExcelSourceTest extends TabularRecordTestBasic {


    private SubjectExcelSource excelSource = null;

    @Before
    public void init() throws Exception {
        super.init();

        xlsInputTestFile = getResourceFile("subjectsfailed1.xlsx");
        SubjectExcelSource.initControlValues(xlsInputTemplate);

        excelSource = new SubjectExcelSource(xlsInputTestFile, Arrays.asList(SubjectTabularRecord.ALL_FIELDS_FOR_SUBJECTS));
    }

    @Test
    public void testProcessExcelWithEmptyRows() throws Exception {
        List<SubjectTabularRecord> subjects = new ArrayList<>();
        for (SubjectTabularRecord rec = null; (rec = (SubjectTabularRecord) excelSource.nextRecord()) != null;) {
            assertNotNull(rec);
            ((SubjectTabularRecord)rec).toRDFXML();
            subjects.add(rec);
        }

        // record id
        assertEquals("Subjects count doesn't match!", 14, subjects.size());
    }
}
