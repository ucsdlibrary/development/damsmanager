package edu.ucsd.library.xdre.tab;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.junit.Before;
import org.junit.Test;

import edu.ucsd.library.xdre.utils.Constants;

/**
 * Test methods for ExcelSource class
 * @author lsitu
 *
 */
public class ExcelSourceTest extends TabularRecordTestBasic {


    private ExcelSource excelSource = null;

    @Before
    public void init() throws Exception {
        super.init();

        // Related resource:IsSupplementTo - Publication@http://example.com/uri
        Constants.RELATED_RESOURCE_LABEL_MAPPINGS.put("is supplement to - publication", "Primary associated publication");
        Constants.RELATED_RESOURCE_LABEL_MAPPINGS.put("related", "Other resource");

        xlsInputTestFile = getResourceFile("xls_excel_input_test.xlsx");
        ExcelSource.initControlValues(xlsInputTemplate);

        excelSource = new ExcelSource(xlsInputTestFile, Arrays.asList(ExcelSource.IGNORED_FIELDS_FOR_OBJECTS));
    }

    @Test
    public void testReplaceControlChars() {
        String val = "Test control character\3.\\3";

        String result = ExcelSource.replaceControlChars(val);
        assertEquals("Value doesn't match!", "Test control character[END OF TEXT].[END OF TEXT]", result);
    }

    @Test
    public void testReplaceControlCharsWithNoControlCharacters() {
        String val = "Test Çatalhöyük \u00c7 \\t \\r \\n control character.";

        String result = ExcelSource.replaceControlChars(val);
        assertEquals("Value is not null!", null, result);
    }

    @Test
    public void testCaseInsensitivePublicSourceDownloadFlagValidation() {
        List<Map<String, String>> invalidValues = excelSource.getInvalidValues();

        assertEquals("Public Source Download flag should be case insensitive!", 0, invalidValues.size());
    }

    @Test
    public void testCaseReportInvalidPublicSourceDownloadFlag() throws Exception {
        excelSource.nextRecord();

        List<Map<String, String>> invalidValues = excelSource.getInvalidValues();
        assertEquals("Invalid Public Source Download flag not being caught!", 1, invalidValues.size());
    }

    @Test
    public void testNextRecord() throws Exception {
        TabularRecord record = (TabularRecord)excelSource.nextRecord();

        assertNotNull(record);
        // record id
        assertEquals("Record ID doesn't match!", "zzxxxxxxxx", record.recordID());

        Map<String, String> data = record.getData();

        // Related resource:IsSupplementTo - Publication
        assertEquals("Related resource:is supplement to - publication doesn't match!", "PREFIX<Related resource:is supplement to - publication@http://example.com/uri>",
                data.get("related resource:is supplement to - publication@11"));

        Document doc = record.toRDFXML();

        // validate dams:type
        assertEquals("dams:type doesn't match!", "is supplement to - publication",
                doc.valueOf("//dams:RelatedResource/dams:type"));

        // validate dams:prefix
        assertEquals("dams:prefix doesn't match!", "PREFIX",
                doc.valueOf("//dams:RelatedResource/dams:prefix"));

        // validate dams:description
        assertEquals("dams:description doesn't match!", "Related resource:is supplement to - publication",
                doc.valueOf("//dams:RelatedResource/dams:description"));

        // validate dams:uri
        assertEquals("dams:uri doesn't match!", "http://example.com/uri",
                doc.valueOf("//dams:RelatedResource/dams:uri/@rdf:resource"));

        // validate order
        assertEquals("dams:order doesn't match!", "11",
                doc.valueOf("//dams:RelatedResource/dams:order"));

        // validate displayLabel
        assertEquals("dams:displayLabel doesn't match!", "Primary associated publication",
                doc.valueOf("//dams:RelatedResource/dams:displayLabel"));

        // validate publicSourceDownload
        assertEquals("dams:publicSourceDownload doesn't match!", "TRUE",
                doc.valueOf("//dams:publicSourceDownload"));

        // Validate existing related resource with no ordering are still supported
        record = (TabularRecord)excelSource.nextRecord();

        assertNotNull(record);

        // record id
        assertEquals("Record ID doesn't match!", "zzxxxxxxxn", record.recordID());

        data = record.getData();

        // Related resource:related
        assertEquals("Related resource:related doesn't match!", "PREFIX<Related resource:related@http://example.com/uri>",
                data.get("related resource:related"));

        doc = record.toRDFXML();

        // validate dams:type
        assertEquals("dams:type doesn't match!", "related",
                doc.valueOf("//dams:RelatedResource/dams:type"));

        // validate dams:prefix
        assertEquals("dams:prefix doesn't match!", "PREFIX",
                doc.valueOf("//dams:RelatedResource/dams:prefix"));

        // validate dams:description
        assertEquals("dams:description doesn't match!", "Related resource:related",
                doc.valueOf("//dams:RelatedResource/dams:description"));

        // validate dams:uri
        assertEquals("dams:uri doesn't match!", "http://example.com/uri",
                doc.valueOf("//dams:RelatedResource/dams:uri/@rdf:resource"));

        // validate the default order exists
        assertEquals("Default dams:order doesn't match!", "25", 
                doc.valueOf("//dams:RelatedResource/dams:order"));

        // validate displayLabel
        assertEquals("dams:displayLabel doesn't match!", "Other resource",
                doc.valueOf("//dams:RelatedResource/dams:displayLabel"));
    }
}
