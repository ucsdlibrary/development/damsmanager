package edu.ucsd.library.xdre.tab;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.xml.transform.TransformerException;

import org.junit.Test;

/**
 * Test methods for RDFExcelConvertor class for data migration.
 * @author lsitu
 *
 */
public class RDFExcelConvertorMigrationTest extends TabularRecordTestBasic {
     @Test
    public void testAddRdfSource() throws IOException, TransformerException {
        String xsl = getClass().getResource("/resources/dams42json-migration.xsl").getPath();
        String rdf = getResourceAsString("dams_object_zz00000001.xml");
        RDFExcelConvertor convertor = new RDFExcelConvertor(xsl);

        convertor.addRdfSource(rdf, "http://library.ucsd.edu/ark:/20775/zz00000001");

        List<List<Map<String,String>>> objectRecords = convertor.getObjectRecords();
        assertEquals(1, objectRecords.size());

        //// Validate object metadata
        Map<String,String> objMetadata = objectRecords.iterator().next().get(0);
        assertEquals("Object model isn't matched!", "GenericObject", objMetadata.get("model"));
        assertEquals("Object source identifier isn't matched!", "zz00000001", objMetadata.get("source_identifier"));
        assertEquals("Object title isn't matched!", "Test Object Title", objMetadata.get("title"));

        // Collection
        assertEquals("Collection isn't matched!", "xxcc000001", objMetadata.get("parents"));

        //// Validate component metadata
        Map<String,String> compMetadata = objectRecords.iterator().next().get(1);
        assertEquals("Component model isn't matched!", "GenericObject", compMetadata.get("model"));
        assertEquals("Component source identifier isn't matched!", "zz00000001-1", compMetadata.get("source_identifier"));
        assertEquals("Component title isn't matched!", "Test Component Title", compMetadata.get("title"));
        assertEquals("Component parent isn't matched!", "zz00000001", compMetadata.get("parents"));

        //// Validate FileSet metadata
        Map<String,String> fileMetadata = objectRecords.iterator().next().get(2);
        assertEquals("File model isn't matched!", "FileSet", fileMetadata.get("model"));
        assertEquals("File source identifier isn't matched!", "zz00000001-1-1.txt", fileMetadata.get("source_identifier"));
        assertEquals("File title isn't matched!", "test-file.txt", fileMetadata.get("title"));
        assertEquals("File use isn't matched!", "20775-zz00000001-1-1.txt", fileMetadata.get("use:Service"));
    }
}
