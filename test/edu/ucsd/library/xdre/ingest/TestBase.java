package edu.ucsd.library.xdre.ingest;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.FileUtils;

/**
 * Abstract base class for test
 * @author lsitu
 */
public abstract class TestBase {

    /*
     * Create a temp file
     * @param content
     * @return
     * @throws IOException
     */
    protected static File createTempFile(String fileName, String content) throws IOException {
        File tmpFile = new File(fileName);
        tmpFile.deleteOnExit();

        try (FileWriter fw = new FileWriter(tmpFile);) {
            fw.write(content);
        }

        return tmpFile;
    }

    protected File getResourceFile(String fileName) throws IOException {
        File resourceFile = new File(fileName);
        resourceFile.deleteOnExit();

        return copyFileFromResource(fileName, resourceFile.getAbsolutePath());
    }


    protected File copyFileFromResource(String resource, String destination) throws IOException {
        File destFile = new File(destination);
        destFile.deleteOnExit();

        byte[] buf = new byte[4096];
        try(InputStream in = RdcpFileReplaceHandlerTest.class.getResourceAsStream("/resources/" + resource);
                FileOutputStream out = new FileOutputStream(destFile)) {

            int bytesRead = 0;
            while ((bytesRead = in.read(buf)) > 0) {
                out.write(buf, 0, bytesRead);
            }
        }

        return destFile;
    }

    /*
     * Calculate directory files size
     * @param dir
     * @return
     */
    protected long getDirectorySize(File dir) {
       long size = 0;
        File[] files = dir.listFiles();

        for (File file : files) {
            if (file.isFile()) {
                size += file.length();
            } else {
                size += getDirectorySize(file);
            }
        }
        return size;
    }

    /*
     * Test to see whether a file exists in a directory
     * @param dir
     * @return
     */
    protected boolean isFileExists(File srcDir, File fileToCompare) {
        for (File file : srcDir.listFiles()) {
            if (file.isFile() && file.getName().equals(fileToCompare.getName())
                    && file.length() == fileToCompare.length()) {
                return true;
            } else if (file.isDirectory()) {
                if (isFileExists(file, fileToCompare)) {
                    return true;
                }
            }
        }
        return false;
    }

    protected void deleteTmpDirectory(File dir) throws IOException {
        if (dir != null &&  dir.exists()) {
            FileUtils.deleteDirectory(dir);
        }
    }
}
