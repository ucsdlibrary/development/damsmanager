package edu.ucsd.library.xdre.ingest.rdlshare;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.security.auth.login.LoginException;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import edu.ucsd.library.xdre.ingest.RdcpFileTransfer;

/**
 * Tests for RdlShareApiDownloader class
 * @author lsitu
 *
 */
@Ignore
public class RdlShareApiDownloaderTest extends RdlShareApiTestBase {
    private RdlShareApiDownloader apiDownloader = null;

    private final String latestContent = "This is the latest attachment ...";

    private List<String> testAttachmentIds = new ArrayList<>();

    @Before
    public void init() throws Exception {
        super.init();

        apiDownloader = new RdlShareApiDownloader(projectPath, apiClient, arkFileMap);
    }

    @After
    public void done() throws IOException, LoginException, ParseException {
        // Delete all test attachments
        List<RdlShareAttachment> attachments = apiDownloader.downloadAttachments();
        for (String attId : testAttachmentIds) {
            for (RdlShareAttachment attachment : attachments) {
                if (attachment.getDownloadUrl().contains(attId)) {
                    apiClient.deleteAttachment(attachment.getMessageUrl());
                }
            }
        }

        super.done();
    }

    @Test
    public void testDownloadInvalidAttachments() throws Exception {
        // initiate the invalid test attachment
        File attFile = createTempFile("invalid_" + testFileName, "Test ...");
        String testAttId = apiClient.addAttachment(attFile.getAbsolutePath());
        testAttachmentIds.add(testAttId);

        List<RdlShareAttachment> attachments = apiDownloader.downloadAttachments();

        assertTrue(attachments.size() > 0);

        for (String testAttachmentId : testAttachmentIds) {

            RdlShareAttachment target = null;
            for (RdlShareAttachment attachment : attachments) {

                if (attachment.getDownloadUrl().contains(testAttachmentId)) {
                    target = attachment;

                    // verify the attachment won't be downloaded
                    File downloadFile = RdcpFileTransfer.getDownloadFile(projectPath, attachment.getFileName());
                    assertFalse(downloadFile.exists());

                    assertTrue(attachment.isInvalid());

                    break;
                }
            }

            assertNotNull(target);
        }
    }

    @Test
    public void testDownloadAttachments() throws Exception {
        // initiate test attachment
        File attFile = createTempFile(testDate + "_" + testFileName, "Test ...");
        String testAttId = apiClient.addAttachment(attFile.getAbsolutePath());
        testAttachmentIds.add(testAttId);

        // download the attachment
        List<RdlShareAttachment> attachments = apiDownloader.downloadAttachments();

        assertTrue(attachments.size() > 0);

        for (String testAttachmentId : testAttachmentIds) {

            RdlShareAttachment target = null;
            for (RdlShareAttachment attachment : attachments) {

                if (attachment.getDownloadUrl().contains(testAttachmentId)) {
                    target = attachment;

                    // verify the attachment is downloaded and the checksum matched
                    File downloadFile = RdcpFileTransfer.getDownloadFile(projectPath, attachment.getFileName());
                    assertTrue(downloadFile.exists());
                    assertEquals(attachment.getChecksumCrc32(), attachment.getLocalChecksumCrc32());

                    downloadFile.deleteOnExit();

                    break;
                }
            }

            assertNotNull(target);
        }
    }

    @Test
    public void testDownloadAttachmentsWithLatestDate() throws Exception {
        // the latest attachment
        String fileName = testDate + "_" + testFileName;
        File datedAtt = createTempFile(fileName, latestContent);
        String latestAttachmentId = apiClient.addAttachment(datedAtt.getAbsolutePath());
        testAttachmentIds.add(latestAttachmentId);

        // add two attachments with the same date tag and the same filename
        String oFileName = "2020-07-27_" + testFileName;
        File oDatedAtt = createTempFile(oFileName, "Test ...");
        String attId = apiClient.addAttachment(oDatedAtt.getAbsolutePath());
        testAttachmentIds.add(attId);

        attId = apiClient.addAttachment(oDatedAtt.getAbsolutePath());
        testAttachmentIds.add(attId);

        // download the attachments
        List<RdlShareAttachment> attachments = apiDownloader.downloadAttachments();

        assertTrue(attachments.size() >= 3);
        for (String attachmentId : testAttachmentIds) {

            RdlShareAttachment target = null;
            for (RdlShareAttachment attachment : attachments) {
                if (attachment.getDownloadUrl().contains(attachmentId)) {
                    target = attachment;

                    if (latestAttachmentId.equals(attachmentId)) {
                        // latest attachment won't be marked as ignore
                        assertFalse(attachment.isIgnore());

                        // verify the attachment is downloaded and the checksum matched
                        File downloadFile = RdcpFileTransfer.getDownloadFile(projectPath, attachment.getFileName());
                        assertTrue(downloadFile.exists());
                        assertEquals(attachment.getChecksumCrc32(), attachment.getLocalChecksumCrc32());

                        // verify the content
                        try (Reader reader = new FileReader(downloadFile);
                                BufferedReader br = new BufferedReader(reader);) {
                            String content = br.readLine();
                            assertEquals("Latest attachment contnet desn't match!", latestContent, content);

                            while ((content = br.readLine()) != null) { ; }
                        }

                        downloadFile.deleteOnExit();
                    } else {
                        // verify that duplicate attachment is marked as junk
                        assertTrue(attachment.isIgnore());
                    }

                    break;
                }
            }

            assertNotNull(target);
        }
    }
}
