package edu.ucsd.library.xdre.ingest.rdlshare;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.security.auth.login.LoginException;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.ClientProtocolException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import edu.ucsd.library.xdre.ingest.RdcpFileReplaceHandler;
import edu.ucsd.library.xdre.ingest.RdcpFileTransfer;
import edu.ucsd.library.xdre.ingest.TestBase;
import edu.ucsd.library.xdre.ingest.fileReplace.ArkFileMapping;

/**
 * Abstract base test class with RdlShareApiClient
 * @author lsitu
 *
 */
public abstract class RdlShareApiTestBase extends TestBase {
    protected RdlShareApiClient apiClient = null;

    protected String testMessageUrl = null;

    protected final String RDL_SHARE_URL_BASE = "https://rdl-share.ucsd.edu/";
    protected final String RDL_SHARE_USER = System.getenv("RDL_SHARE_USER");
    protected final String RDL_SHARE_PWD = System.getenv("RDL_SHARE_PWD");
    protected final List<String> RDL_SHARE_RECIPIENTS = Arrays.asList("siotodamsdev@ucsd.edu");

    private final String arkFileMappingResource = "rdcp-arkfile-mapping.xls";

    protected final String testFileName = "Site1.txt";

    protected String testDate = "";

    protected String projectPath = new File("").getAbsolutePath();

    protected List<ArkFileMapping> arkFileMap = new ArrayList<>();

    private File arkMappingFile = null;
    /*
     * Test initiation
     * @throws Exception
     */
    protected void init() throws Exception {

        Path rootPath = Paths.get("tmp", RdcpFileTransfer.UPLOAD_PATH_POSTFIX);
        File tempDir = rootPath.toFile();
        if (!tempDir.exists()) {
            tempDir.mkdir();
            tempDir.deleteOnExit();
        }

        projectPath = tempDir.getAbsolutePath();
        Path downloadPath = Paths.get(tempDir.getAbsolutePath(), RdcpFileTransfer.UPLOAD_FOLDER);
        tempDir = downloadPath.toFile();
        if (!tempDir.exists()) {
            tempDir.mkdir();
            tempDir.deleteOnExit();
        }

        arkMappingFile = copyFileFromResource(arkFileMappingResource, "./rdcp-arkfile-mapping.xls");
        arkFileMap = RdcpFileReplaceHandler.getArkFileMap(arkMappingFile.getAbsolutePath());
        testDate = new SimpleDateFormat(RdcpFileTransfer.DATE_PATTERN).format(new Date());
        apiClient = new RdlShareApiClient(RDL_SHARE_URL_BASE, RDL_SHARE_USER, RDL_SHARE_PWD, RDL_SHARE_RECIPIENTS);
    }

    /*
     * Test cleanup
     * @throws IOException
     * @throws LoginException
     */
    protected void done() throws IOException, LoginException, ParseException {
        if (StringUtils.isNotBlank(testMessageUrl)) {
            apiClient.deleteAttachment(testMessageUrl);
        }

        if (apiClient != null) {
            apiClient.close();
        }
    }


    /*
     * Retrieve the message with the attachment linked to
     * @param attachmentId - the attachment id
     * @return - the message url
     * @throws ClientProtocolException
     * @throws LoginException
     * @throws IOException
     */
    protected String finadMessageByAttachment(String attachmentId)
            throws ClientProtocolException, LoginException, IOException {
        String messageUrl = "";
        JSONObject messageObj = apiClient.retrieveMessages();
        JSONArray messages = (JSONArray)messageObj.get("messages");

        assertTrue(messages.size() > 0);

        for (Object m : messages) {
            JSONArray attachments = (JSONArray)((JSONObject)m).get("attachments");
            for (Object attachment : attachments) {
                JSONObject a = (JSONObject)attachment;
                if (a.get("id").toString().equals(attachmentId.trim())) {
                    messageUrl = (String)((JSONObject)m).get("url");
                    break;
                }
            }
        }

        return messageUrl;
    }
}
