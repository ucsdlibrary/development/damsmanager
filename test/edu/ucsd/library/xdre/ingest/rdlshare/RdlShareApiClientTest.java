package edu.ucsd.library.xdre.ingest.rdlshare;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;

import javax.security.auth.login.LoginException;

import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

/**
 * Tests for RdlShareApiClient class
 * @author lsitu
 *
 */
@Ignore
public class RdlShareApiClientTest extends RdlShareApiTestBase {

    private String attachmentId = "";
    private File testFile = null;

    @Before
    public void init() throws Exception {
        super.init();

        testFile = createTempFile(testDate + "_" + testFileName, "Test ...");
    }

    @After
    public void done() throws IOException, LoginException, ParseException {
        // retrieve the message for cleanup
        if (attachmentId.length() > 0) {
            testMessageUrl = finadMessageByAttachment(attachmentId);
        }

        super.done();
    }

    @Test
    public void testAddAttachment() throws Exception {
        attachmentId = apiClient.addAttachment(testFile.getAbsolutePath());

        assertTrue(StringUtils.isNotBlank(attachmentId));
    }

    @Test
    public void testRetrieveAttachments() throws Exception {
        attachmentId = apiClient.addAttachment(testFile.getAbsolutePath());

        assertTrue(StringUtils.isNotBlank(attachmentId));

        JSONObject messageObj = apiClient.retrieveMessages();
        JSONArray messages = (JSONArray)messageObj.get("messages");
        assertTrue(messages.size() > 0);
    }

    @Test
    public void testDeleteAttachment() throws Exception {
        // initiate test attachment
        String attachmentId = apiClient.addAttachment(testFile.getAbsolutePath());

        assertTrue(StringUtils.isNotBlank(attachmentId));

        // retrieve all messages from rdl-share api
        JSONObject messageObj = apiClient.retrieveMessages();
        JSONArray messages = (JSONArray)messageObj.get("messages");
        assertTrue(messages.size() > 0);

        // retrieve the message with the attachment added
        String messageUrl = finadMessageByAttachment(attachmentId);
        assertTrue(StringUtils.isNotBlank(messageUrl));

        // delete the message
        apiClient.deleteAttachment(messageUrl);

        // verify the message is gone: message size decreased by 1
        JSONObject messageObjActual = apiClient.retrieveMessages();
        JSONArray messagesActual = (JSONArray)messageObjActual.get("messages");
        assertTrue(messagesActual.size() == messages.size() -1);
    }
}
