package edu.ucsd.library.xdre.ingest.fileReplace;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

/**
 * Test methods for ArkFileMapping model class
 * @author lsitu
 *
 */
public class ArkFileMappingTest {

    private final String arkFileUri = "http://library.ucsd.edu/ark:/20775/xx00000001/1.csv";
    private final String fileNameSyntax = "YYYY-MM-DD_Site1.csv";

    // Gzip compress handling
    private final String gzipArkFileUri = "http://library.ucsd.edu/ark:/20775/xx00000001/1.gz";
    private final String gzipFileNameSyntax = ".*\\.fcs";

    @Before
    public void init() throws Exception {

    }

    @Test
    public void testIsFileNameValid() {
        String fileName = "2021-08-02_Site1.csv";
        ArkFileMapping arkFileMapping = new ArkFileMapping(arkFileUri, fileNameSyntax, ArkFileMapping.ARCHIVE_FORMAT_NONE);
        assertTrue("File name " + fileName + " should be valid!", arkFileMapping.isFileNameValid(fileName));

        String badFileName = "2021-0802_Site1.csv";
        assertFalse("File name " + badFileName + " should not be valid!", arkFileMapping.isFileNameValid(badFileName));
    }

    @Test
    public void testGetSourceFileName() {
        String fileName = "2021-08-02_Site1.csv";
        ArkFileMapping arkFileMapping = new ArkFileMapping(arkFileUri, fileNameSyntax, ArkFileMapping.ARCHIVE_FORMAT_NONE);
        assertEquals("Source filename doesn't match!", "Site1.csv", arkFileMapping.getSourceFileName(fileName));
    }

    @Test
    public void testGzipIsFileNameValid() {
        String fileName = "SCCOOS_YYMMDD-YYMMDD.fcs";
        ArkFileMapping arkFileMapping = new ArkFileMapping(gzipArkFileUri, gzipFileNameSyntax, ArkFileMapping.ARCHIVE_FORMAT_TARBALL);
        assertTrue("File name " + fileName + " should be valid!", arkFileMapping.isFileNameValid(fileName));

        String badFileName = "SCCOOS_YYMMDD-YYMMDD.bad";
        assertFalse("File name " + badFileName + " should not be valid!", arkFileMapping.isFileNameValid(badFileName));
    }

    @Test
    public void testGzipGetSourceFileName() {
        String fileName = "SCCOOS_YYMMDD-YYMMDD.fcs";
        ArkFileMapping arkFileMapping = new ArkFileMapping(gzipArkFileUri, gzipFileNameSyntax, ArkFileMapping.ARCHIVE_FORMAT_TARBALL);
        assertEquals("Source filename doesn't match!", fileName, arkFileMapping.getSourceFileName(fileName));
    }
}
