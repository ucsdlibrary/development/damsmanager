package edu.ucsd.library.xdre.ingest.fileReplace;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import edu.ucsd.library.xdre.ingest.TestBase;

/**
 * Tests for RdcpFileReplaceConfig properties
 * @author lsitu
 *
 */
public class RdcpFileReplaceConfigTest extends TestBase {

    private String urlBase = "https://rdl-share.ucsd.edu/";
    private String apiKey = "any-rdl-share-api-key";
    private String recipient = "siotodamsdev@ucsd.edu";
    private String[] emails = { "Research-Data-Curation@ucsd.edu", "dams-support@ucsd.edu" };

    private final String configFileName = "config.properties";
    private File configFile = null;

    @Before
    public void init() throws IOException {
        configFile = getResourceFile(configFileName);
    }

    @Test
    public void testConfig() throws Exception {
        RdcpFileReplaceConfig config = new RdcpFileReplaceConfig(configFile);
        assertEquals("Wrong rdl-share url base!", urlBase, config.getRdlShareUriBase());
        assertEquals("Wrong rdl-share API key!", apiKey, config.getRdlShareApiKey());
        assertEquals("Wrong rdl-share password!", "pwd", config.getRdlSharePassword());
        assertEquals("Wrong rdl-share recipients!", recipient, config.getRdfShareRecipients().get(0));
        assertTrue("Wrong rdl-share distribution emails!", validateValues(Arrays.asList(emails), config.getDistributionEmails()));
        assertEquals("Wrong file location!", "Active_Files", config.getFileLocation());
        assertFalse("Wrong result for needRdlShareDownload()!", config.needRdlShareDownload());
        assertEquals("Wrong notify internal!", 7, config.getNotifyInterva());
    }

    private boolean validateValues(List<String> expectedValues, List<String> values) {
        for (String value : values) {
            if (!expectedValues.contains(value)) {
                return false;
            }
        }
        return true;
    }
}
