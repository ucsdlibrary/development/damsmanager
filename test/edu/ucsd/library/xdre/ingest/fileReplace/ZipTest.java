package edu.ucsd.library.xdre.ingest.fileReplace;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.ucsd.library.xdre.ingest.TestBase;
import edu.ucsd.library.xdre.utils.Constants;

/**
 * Test methods for Zip class
 * @author lsitu
 *
 */
public class ZipTest extends TestBase {

    private final String testZipFile = "/resources/rdcp-test.zip";
 
    private Zip zip = null;
    private Zip zipNew = null;

    @Before
    public void init() throws Exception {
        Constants.TMP_FILE_DIR = Files.createTempDirectory("zip-test").toFile().getAbsolutePath();
    }

    @After
    public void cleanup() throws Exception {
        deleteTmpDirectory(new File(Constants.TMP_FILE_DIR));
        if(zip != null) {
            zip.cleanUp();
        }
        if(zipNew != null) {
            zipNew.cleanUp();
        }
    }

    @Test
    public void testUncompressZipFile() throws IOException {
        URL zipUri = Zip.class.getResource(testZipFile);
        Path testZipPath = Paths.get(zipUri.getPath());

        Zip zip = new Zip(testZipPath.toFile());
        File zipSrcDir = zip.getDecompressedFolderPath().toFile();

        assertTrue(zipSrcDir.listFiles().length > 0);
        assertTrue(getDirectorySize(zipSrcDir) > 0);
    }

    @Test
    public void testCompressZipRoundTrip() throws IOException {
        URL zipUri = Zip.class.getResource(testZipFile);
        Path testZipPath = Paths.get(zipUri.getPath());

        zip = new Zip(testZipPath.toFile());
        File zipSrcDir = zip.getDecompressedFolderPath().toFile();

        assertTrue(zipSrcDir.listFiles().length > 0);

        File zipFile = File.createTempFile("rdcp-test-zip-compressed", ".zip");
        zipFile.deleteOnExit();
        // compress the folder created by decompress the test .zip file
        zip.compress(zipFile);

        assertTrue(zipFile.length() > 0);

        // decompress the zip file created for validation
        zipNew = new Zip(zipFile);
        File destDirTmp = zipNew.getDecompressedFolderPath().toFile();
        assertEquals("Compressed files count doesn't match!", zipSrcDir.listFiles().length, destDirTmp.listFiles().length);

        assertEquals("Source files total size of compress/decompress round trip doesn't match!",
                getDirectorySize(zipSrcDir), getDirectorySize(destDirTmp));
    }

    @Test
    public void testAddFileToZip() throws IOException {
        URL zipUri = Zip.class.getResource(testZipFile);
        Path testZipPath = Paths.get(zipUri.getPath());

        zip = new Zip(testZipPath.toFile());

        // the file to add to ZIP
        File fileToAdd = createTempFile("test-add-file.fcs", "Test add file to ZIP ....");
        assertTrue(fileToAdd.exists());

        // add the file to ZIP
        zip.addFile(fileToAdd);

        File zipFile = File.createTempFile("rdcp-test-add-file", ".zip");
        zipFile.deleteOnExit();

        // create ZIP from the Zip instance
        zip.compress(zipFile);

        zipNew = new Zip(zipFile);
        File destDirTmp = zipNew.getDecompressedFolderPath().toFile();

        assertTrue(destDirTmp.listFiles().length > 0);
        assertTrue("The file added doesn't exists in ZIP!", isFileExists(destDirTmp, fileToAdd));
    }
}
