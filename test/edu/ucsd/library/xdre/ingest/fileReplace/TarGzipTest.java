package edu.ucsd.library.xdre.ingest.fileReplace;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.ucsd.library.xdre.ingest.TestBase;
import edu.ucsd.library.xdre.utils.Constants;

/**
 * Test methods for TarGzip class
 * @author lsitu
 *
 */
public class TarGzipTest extends TestBase {

    private final String testTgzipFile = "/resources/rdcp-test.tgz";
 
    private TarGzip tarGzip = null;
    private TarGzip tarGzipNew = null;

    @Before
    public void init() throws Exception {
        Constants.TMP_FILE_DIR = Files.createTempDirectory("gzip-test").toFile().getAbsolutePath();
    }

    @After
    public void cleanup() throws Exception {
        deleteTmpDirectory(new File(Constants.TMP_FILE_DIR));
        if(tarGzip != null) {
            tarGzip.cleanUp();
        }
        if(tarGzipNew != null) {
            tarGzipNew.cleanUp();
        }
    }

    @Test
    public void testUncompressTarGzip() throws IOException {
        URL gzipUri = TarGzip.class.getResource(testTgzipFile);
        Path testGzipPath = Paths.get(gzipUri.getPath());

        tarGzip = new TarGzip(testGzipPath.toFile());
        File gzipSrcDir = tarGzip.getDecompressedFolderPath().toFile();

        assertTrue(gzipSrcDir.listFiles().length > 0);
        assertTrue(getDirectorySize(gzipSrcDir) > 0);
    }

    @Test
    public void testCompressTarGzipRoundTrip() throws IOException {
        URL gzipUri = TarGzip.class.getResource(testTgzipFile);
        Path testGzipPath = Paths.get(gzipUri.getPath());

        tarGzip = new TarGzip(testGzipPath.toFile());
        // decompressed directory of the test .tgz file
        File gzipSrcDir = tarGzip.getDecompressedFolderPath().toFile();

        assertTrue(gzipSrcDir.exists());
        assertTrue(gzipSrcDir.listFiles().length > 0);

        File gzipFile = File.createTempFile("rdcp-test-gzip-compressed", ".tgz");
        gzipFile.deleteOnExit();
        // compress the folder created by decompress the test .tgz file
        tarGzip.compress(gzipFile);

        assertTrue(gzipFile.exists());
        assertTrue(gzipFile.length() > 0);

        // decompress the gzip file created for validation
        tarGzipNew = new TarGzip(gzipFile);
        File destDirTmp = tarGzipNew.getDecompressedFolderPath().toFile();
        assertEquals("Compressed files count doesn't match!", gzipSrcDir.listFiles().length, destDirTmp.listFiles().length);

        assertEquals("Source files total size of compress/decompress round trip doesn't match!",
                getDirectorySize(gzipSrcDir), getDirectorySize(destDirTmp));
    }

    @Test
    public void testAddFileToGzip() throws IOException {
        URL gzipUri = TarGzip.class.getResource(testTgzipFile);
        Path testGzipPath = Paths.get(gzipUri.getPath());

        tarGzip = new TarGzip(testGzipPath.toFile());

        // the file to add to GZIP
        File fileToAdd = createTempFile("test-add-file.fcs", "Test add file to GZIP ....");
        assertTrue(fileToAdd.exists());

        // add the file to GZIP
        tarGzip.addFile(fileToAdd);

        File gzipFile = File.createTempFile("rdcp-test-add-file", ".tgz");
        gzipFile.deleteOnExit();

        // create GZIP from the TarGzip object
        tarGzip.compress(gzipFile);

        tarGzipNew = new TarGzip(gzipFile);
        File destDirTmp = tarGzipNew.getDecompressedFolderPath().toFile();

        assertTrue(destDirTmp.listFiles().length > 0);
        assertTrue("The file added doesn't exists in GZIP!", isFileExists(destDirTmp, fileToAdd));
    }
}
