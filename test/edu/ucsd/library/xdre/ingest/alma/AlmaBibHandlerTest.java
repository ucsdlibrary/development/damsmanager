package edu.ucsd.library.xdre.ingest.alma;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

/**
 * Tests for AlmaBibHandler class
 * @author lsitu
 *
 */
@Ignore
public class AlmaBibHandlerTest extends AlmaTestBase{

    private AlmaBibHandler handler = null;

    /*
     * Test initiation
     * @throws Exception
     */
    @Before
    public void init() throws Exception {
        super.init();
        handler = new AlmaBibHandler(apiClient);
    }

    @Test
    public void testGetSingleRecord() throws Exception {
    	List<String> ids = Arrays.asList(TEST_MMS_ID);
        Document bibs = handler.getMarcXml(ids);
        List<Node> nodes = AlmaBibHandler.createXPath("//marc:record").selectNodes(bibs);

        assertEquals("Can't find marc:record element!", 1, nodes.size());
    }

    @Test
    public void testRetrieveMultipleRecords() throws Exception {
        List<String> ids = Arrays.asList(TEST_MMS_IDS.split("\\,"));
        Document bibs = handler.getMarcXml(ids);
        List<Node> nodes = AlmaBibHandler.createXPath("//marc:record").selectNodes(bibs);

        assertEquals("Elements for marc:record doesn't match!", 2, nodes.size());
    }

    @Test
    public void testGetMmsId() throws Exception {
        List<String> ids = Arrays.asList(TEST_MMS_ID);
        Document bibs = handler.getMarcXml(ids);
        Node record = AlmaBibHandler.createXPath("//marc:record").selectSingleNode(bibs);

        assertNotNull(record);
        assertEquals("Cannot retireve mms_id!", TEST_MMS_ID, AlmaBibHandler.getMmsId(record));
    }

    @Test
    public void testConvertToMods() throws Exception {
        List<String> ids = Arrays.asList(TEST_MMS_ID);
        Document bibs = handler.getMarcXml(ids);

        Node marcXmlRecord = AlmaBibHandler.createXPath("//marc:record").selectSingleNode(bibs);
        String mods = AlmaBibHandler.convertToMods(TEST_MMS_ID, marcXmlRecord.asXML());

        // Verify the converted MODS
        Document doc = null;
        SAXReader saxReader = new SAXReader();
        try (InputStream modsInput = new ByteArrayInputStream(mods.getBytes("UTF-8"));) {
            doc = saxReader.read(modsInput);
        }

        assertNotNull(doc);

        String mmsIdActual = AlmaBibHandler.createXPath("//mods:identifier[@type='mms_id']").valueOf(doc);
        assertEquals("Element mms_id identifier doesn't match!", TEST_MMS_ID, mmsIdActual);

        String title = AlmaBibHandler.createXPath("//mods:titleInfo/mods:title").valueOf(doc);
        assertTrue("Title doesn't exist!", title.length() > 0);
    }
}
