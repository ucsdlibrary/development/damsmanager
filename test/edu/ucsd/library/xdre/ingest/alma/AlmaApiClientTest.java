package edu.ucsd.library.xdre.ingest.alma;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import javax.xml.soap.Node;

import org.dom4j.Document;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

/**
 * Tests for AlmaApiClient class
 * @author lsitu
 *
 */
@Ignore
public class AlmaApiClientTest extends AlmaTestBase {

    /*
     * Test initiation
     * @throws Exception
     */
    @Before
    public void init() throws Exception {
        super.init();
    }

    @Test
    public void testGetSingleRecord() throws Exception {
    	List<String> ids = Arrays.asList(TEST_MMS_ID);
        Document bibs = apiClient.retrieveRecords(ids);
        List<Node> nodes = bibs.selectNodes("//record");

        assertEquals("Can't find record element!", 1, nodes.size());
    }

    @Test
    public void testRetrieveMultipleRecords() throws Exception {
        List<String> ids = Arrays.asList(TEST_MMS_IDS.split("\\,"));
        Document bibs = apiClient.retrieveRecords(ids);
        List<Node> nodes = bibs.selectNodes("//record");

        assertEquals("Elements for record doesn't match!", 2, nodes.size());
    }
}
