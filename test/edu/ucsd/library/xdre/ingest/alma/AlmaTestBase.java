package edu.ucsd.library.xdre.ingest.alma;

import org.apache.log4j.Logger;

import edu.ucsd.library.xdre.utils.Constants;

/**
 * AlmaTestBase - Base class for Alma API related tests
 * @author lsitu
 *
 */
public abstract class AlmaTestBase {
    protected static Logger log = Logger.getLogger(AlmaTestBase.class);

    protected static final String TEST_MMS_ID = "991000022469706535";
    protected static final String TEST_MMS_IDS = "991000022469706535,991004124329706535";

    protected AlmaApiClient apiClient = null;
    /*
     * Test initiation base
     * @throws Exception
     */
    protected void init() throws Exception {
        Constants.ALMA_API_BIB_URL = "https://api-na.hosted.exlibrisgroup.com/almaws/v1/bibs";
        Constants.ALMA_API_KEY = System.getenv("ALMA_API_KEY");

        log.debug(Constants.ALMA_API_BIB_URL + "?apikey=" + Constants.ALMA_API_KEY);

        apiClient = new AlmaApiClient();
    }
}
