package edu.ucsd.library.xdre.ingest;

import static edu.ucsd.library.xdre.collection.CollectionHandler.escapeCsvValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import edu.ucsd.library.xdre.ingest.fileReplace.ArkFileMapping;
import edu.ucsd.library.xdre.utils.Constants;

/**
 * Tests for RdcpFileReplaceHandler class
 * @author lsitu
 *
 */
public class RdcpFileReplaceHandlerTest extends TestBase {

    private final String testFileName = "YYYY-MM-DD_Site1.txt";
    private final String testArkUri = "http://library.ucsd.edu/ark:/20775/bd25480650/1/1.txt";

    private final String arkFileMappingResource = "rdcp-arkfile-mapping.xls";
    private final SimpleDateFormat dateFormat = new SimpleDateFormat(RdcpFileTransfer.DATE_PATTERN);

    private File arkMappingFile = null;
    @Before
    public void init() throws IOException {
        arkMappingFile = copyFileFromResource(arkFileMappingResource, "./rdcp-arkfile-mapping.xls");

        Constants.CLUSTER_HOST_NAME = "localhost";
        Constants.DAMS_STORAGE_URL = "http://localhost:8080/dams/api";
    }

    @Test
    public void testGetArkFileMap() throws Exception {
        List<ArkFileMapping> arkFileMap = RdcpFileReplaceHandler.getArkFileMap(arkMappingFile.getAbsolutePath());

        assertEquals(2, arkFileMap.size());

        ArkFileMapping arkFileMapping = arkFileMap.get(0);

        assertEquals(testFileName, arkFileMapping.getFileNameSyntax());
        assertEquals(testArkUri, arkFileMapping.getArkFileUri());
        assertFalse(arkFileMapping.needArchiveFile());
    }

    @Test
    public void testEscapeCsvValue() {
        String text = "\"CSV, comma-separated values\"";
        String expected = "\"\"\"CSV, comma-separated values\"\"\"";
        assertEquals(expected, escapeCsvValue(text));
    }

    @Test
    public void testGetEmailSubject() {
        // email subject from QA
        Constants.CLUSTER_HOST_NAME = "qa";
        String expected = "QA: RDCP File Replacement Alert, " + dateFormat.format(new Date());
        assertTrue(RdcpFileReplaceHandler.getEmailSubject().startsWith(expected));

        // email subject from staging
        Constants.CLUSTER_HOST_NAME = "librarytest";
        expected = "Staging: RDCP File Replacement Alert, " + dateFormat.format(new Date());
        assertTrue(RdcpFileReplaceHandler.getEmailSubject().startsWith(expected));

        // email subject from prod
        Constants.CLUSTER_HOST_NAME = "library";
        expected = "RDCP File Replacement Alert, " + dateFormat.format(new Date());
        assertTrue(RdcpFileReplaceHandler.getEmailSubject().startsWith(expected));
    }

    @Test
    public void testAppendToReportWithNewLineEnded() throws IOException {
        String fileContent = "Test file ended with new line character ... \n";
        String newContent = "New content\n";

        File file = createTempFile("./testNewline.txt", fileContent);

        RdcpFileReplaceHandler.appendToReport(file.getAbsolutePath(), newContent);

        String actualContent = new String(Files.readAllBytes(Paths.get(file.getAbsolutePath())));

        String expectedContent = fileContent + newContent;
        assertEquals("New content does NOT match!", expectedContent, actualContent);
    }

    @Test
    public void testAppendToReportWithNoNewLineEnded() throws IOException {
        String fileContent = "Test file not ended with new line character ...";
        String newContent = "New content\n";

        File file = createTempFile("./testNewline.txt", fileContent);

        RdcpFileReplaceHandler.appendToReport(file.getAbsolutePath(), newContent);
        String actualContent = new String(Files.readAllBytes( Paths.get(file.getAbsolutePath())));

        String expected = fileContent + "\n" + newContent;
        assertEquals("No new line character appended!", expected, actualContent);
    }
}
