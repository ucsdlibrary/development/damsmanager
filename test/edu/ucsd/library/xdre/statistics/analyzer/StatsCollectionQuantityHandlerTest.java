package edu.ucsd.library.xdre.statistics.analyzer;

import static org.junit.Assert.assertTrue;

import org.dom4j.Document;
import org.dom4j.io.SAXReader;
import org.junit.Test;

import edu.ucsd.library.xdre.collection.StatsCollectionQuantityHandler;
import edu.ucsd.library.xdre.utils.UnitTestBasic;

/**
 * Tests methods for StatsCollectionQuantityHandlerTest class
 */
public class StatsCollectionQuantityHandlerTest extends UnitTestBasic {
    @Test
    public void testIsBornDigital() throws Exception {
        Document doc = new SAXReader().read(getResourceFile("dams_object_zz00000001.xml"));

        assertTrue("Wrong born digital object!", StatsCollectionQuantityHandler.isBornDigital(doc));
    }
}
