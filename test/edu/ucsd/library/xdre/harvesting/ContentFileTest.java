package edu.ucsd.library.xdre.harvesting;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import edu.ucsd.library.xdre.utils.Constants;

/**
 * Test methods for ContentFile class
 * @author lsitu
 *
 */
@Ignore
public class ContentFileTest {
    private File contentDir = null;
    private CilApiClient cilApiClient = null;
    private CilApiDownloader cilApiDownloader = null;

    @Before
    public void init() throws Exception {
        Constants.CIL_HARVEST_API = "https://cilia.crbs.ucsd.edu/rest/";
        Constants.CIL_CONTENT_URL_BASE = "https://cildata.crbs.ucsd.edu/";
        Constants.CIL_HARVEST_API_USER = "ucsd_lib";
        Constants.CIL_HARVEST_API_PWD = "xxxxx";
        Constants.CIL_HARVEST_DIR = new File("").getAbsolutePath() + File.separatorChar + "rdcp_staging";

        cilApiClient = new CilApiClient(Constants.CIL_HARVEST_API);
        File sourceDir = new File(Constants.CIL_HARVEST_DIR + File.separatorChar + CilHarvesting.CIL_HARVEST_FOLDER,
                CilHarvesting.METADATA_SOURCE_FOLDER);
        if (!sourceDir.exists())
            sourceDir.mkdirs();

        contentDir = new File(Constants.CIL_HARVEST_DIR + File.separatorChar +CilHarvesting.CIL_HARVEST_FOLDER,
                CilHarvesting.CONTENT_FILES_FOLDER);
        if (!contentDir.exists())
            contentDir.mkdirs();

    }

    @After
    public void done() throws IOException {
        if (cilApiClient != null) {
            cilApiClient.close();
        }

        if (Constants.CIL_HARVEST_DIR.endsWith(File.separatorChar + "rdcp_staging")) {
            FileUtils.deleteDirectory(new File(Constants.CIL_HARVEST_DIR));
        }
    }

    @Test
    public void testDownload() throws Exception {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 2018);
        Date lastModified = cal.getTime();
 
        cilApiDownloader = new CilApiDownloader(cilApiClient, null, lastModified);
        assertTrue(cilApiDownloader.getCilIds().size() > 0);

        String cilId = cilApiDownloader.getCilIds().get(0);
        String harvestLabel = CilApiDownloader.createHarvestLabel();
        MetadataSource source = new MetadataSource(cilId, cilApiClient, harvestLabel);

        ContentFile contentFile = new ContentFile(source.getUri(), cilApiClient, harvestLabel);
        String fileName = contentFile.save(contentDir.getParent(), cilId + ".txt");
        assertTrue(new File(fileName).length() > 0);
    }
}
