package edu.ucsd.library.xdre.utils;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.ucsd.library.xdre.utils.Submission.Status;


/**
 * Tests for equestOrganizer class
 * @author lsitu
 *
 */
public class RequestOrganizerTest {
    private static String TEST_USER = "tuser";
    private static String CATEGORY_KEY = "category";
    private static String[] TEST_CATEGORY = {"zz0000000x"};
    private static String TEST_PARAM_KEY = "test_param";
    private static String[] TEST_MULTI_VALUE = {"zz0000000x", "zz0000000y"};

    private List<Submission> submitions = new ArrayList<>();

    @Before
    public void init() {
        Map<String, String[]> testParams = createTestParams();

        if (RequestOrganizer.findSubmission(testParams) == null) {
            String submissionId = RequestOrganizer.getProcessId(TEST_USER);
            Submission submission = addSubmission(submissionId, TEST_USER, testParams);
            submitions.add(submission);
        }
    }

    @After
    public void tearDown() throws InterruptedException {
        for(Submission submission : submitions) {
            Thread t = submission.getWorker();
            if (t.isAlive()) {
                submission.getWorker().interrupt();
                t.join();
            }
            submission.setStatus(Status.done);
        }
    }

    @Test
    public void testFindSubmission() throws Exception {
        Map<String, String[]> testParams = createTestParams();
        assertNotNull("Submission with the same param set is not found!",
                RequestOrganizer.findSubmission(testParams));
    }

    @Test
    public void testFindSubmissionWithIgnoredParams() throws Exception {
        Map<String, String[]> testParams = createTestParams();

        // submission with application specific sid param that should be ignored
        String[] sid = {"" + System.currentTimeMillis()};
        testParams.put("sid", sid);
        assertNotNull("Submission with application added param sid need to ignored!",
                RequestOrganizer.findSubmission(testParams));
    }
 
    @Test
    public void testFindSubmissionNotExist() throws Exception {
        Map<String, String[]> testParams = createTestParams();
        // submission with category difference other than the active submission initiated
        String[] category = {"anycollection"};
        testParams.put(CATEGORY_KEY, category);

        assertNull("Submission with the different category found!",
                RequestOrganizer.findSubmission(testParams));

        // submission with additional parameters
        testParams = createTestParams();
        String[] testParamValue = {"any value"};
        testParams.put(TEST_PARAM_KEY, testParamValue);
        assertNull("Submission with additional param found!",
                RequestOrganizer.findSubmission(testParams));
    }

    @Test
    public void testFindSubmissionMultiValueParam() throws Exception {
        Map<String, String[]> testParams = createTestParams();
        // create active submission with param that has two values
        testParams.put(TEST_PARAM_KEY, TEST_MULTI_VALUE);
        String submissionId = RequestOrganizer.getProcessId(TEST_USER);
        addSubmission(submissionId, TEST_USER, testParams);

        assertNotNull("Submission with the same multi-value params is not found!",
                RequestOrganizer.findSubmission(testParams));

        testParams = createTestParams();
        // submission with multi-value param that has two values in reverse order
        String[] reversedOrderValue = {TEST_MULTI_VALUE[1], TEST_MULTI_VALUE[0]};
        testParams.put(TEST_PARAM_KEY, reversedOrderValue);
        assertNotNull("Submission with multi-value param in different order is not found!",
                RequestOrganizer.findSubmission(testParams));
    }

    @Test
    public void testFindSubmissionWithMultiValueParamNotExist() throws Exception {
        Map<String, String[]> testParams = createTestParams();
        String[] testValues1 = {"test-value1", "test-value2"};
        testParams.put(TEST_PARAM_KEY, testValues1);

        // initiate active submission with parameter that has two values
        String submissionId = RequestOrganizer.getProcessId(TEST_USER);
        addSubmission(submissionId, TEST_USER, testParams);

        // submission with param that has two values but different
        testParams = createTestParams();
        String[] testValues2 = {"test-value1", "test-value3"};
        testParams.put(TEST_PARAM_KEY, testValues2);
        assertNull("Submission with different multi-value param found!",
                RequestOrganizer.findSubmission(testParams));

        // submission with param that has three values
        testParams = createTestParams();
        String[] testValues3 = {"test-value1", "test-value2", "test-value3"};
        testParams.put(TEST_PARAM_KEY, testValues3);

        assertNull("Submission with different length of multi-value param found!",
                RequestOrganizer.findSubmission(testParams));
    }

    private static Submission addSubmission(String submissionId, String user, Map<String, String[]> params) {
        Thread t = new TestThread();
        Submission submission = new Submission(t, submissionId, TEST_USER, params);
        submission.setStatus(Status.progressing);
        RequestOrganizer.addSubmission(submission);
        t.start();

        return submission;
    }

    private static Map<String, String[]> createTestParams() {
        Map<String, String[]> testParams = new HashMap<>();
        testParams.put("excelImport", new String[1]);
        testParams.put(CATEGORY_KEY, TEST_CATEGORY);
        String[] dataPath = {"relative/path/to/staging/"};
        testParams.put("dataPath", dataPath);
        String[] preingestOption = {"pre-processing"};
        testParams.put("preingestOption", preingestOption);

        return testParams;
    }

    /**
     * Sleeping thread mimic the active process
     * @author longshousitu
     *
     */
    static class TestThread extends Thread{
        public void run(){
            try {
                while (true) {
                    Thread.sleep(1000);
                }
            } catch(InterruptedException e) { }
        }
    }
}

