package edu.ucsd.library.xdre.utils;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertEquals;

import java.util.Properties;

import javax.sql.DataSource;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import org.apache.commons.dbcp2.BasicDataSource;
import org.junit.contrib.java.lang.system.EnvironmentVariables;

/**
 * Utility methods for Constants class
 * @author lsitu
 *
 */
public class ConstantsTest {

    @Rule
    public final EnvironmentVariables env
        = new EnvironmentVariables();
    
    @Before
    public void init() {
        env.set("APPS_DM_XDRE_CLUSTERHOSTNAME", "localhost");
        env.set("APPS_DM_XDRE_DAMSREPO", "http://localhost:8080/dams/api");
        env.set("APPS_DM_XDRE_DAMSREPO_USER", "dams");
        env.set("APPS_DM_XDRE_DAMSREPO_PWD", "password");
        env.set("APPS_DM_XDRE_STAGING", "xdre_staging");
        env.set("APPS_DM_XDRE_TMPFILEDIR", "xdre_tmpFileDir");
        env.set("APPS_DM_XDRE_DEFAULTDERIVATIVES", "xdre_defaultDerivatives");
        env.set("APPS_DM_XDRE_SOLRBASE", "xdre_solrBase");
        env.set("APPS_DM_XDRE_ARK_URLBASE", "xdre_ark_urlBase");
        env.set("APPS_DM_XDRE_CLR_URLBASE", "xdre_clr_urlBase");
        env.set("APPS_DM_XDRE_CLR_IMGDIR", "xdre_clr_imgDir");
        env.set("APPS_DM_XDRE_CLR_THUMBNAILSDIR", "xdre_clr_thumbnailsDir");
        env.set("APPS_DM_XDRE_CLR_SOURCEDIR", "xdre_clr_sourceDir");
        env.set("APPS_DM_XDRE_ARK_NAME", "xdre_ark_name");
        env.set("APPS_DM_XDRE_ARK_ORGCODE", "xdre_ark_orgCode");
        env.set("APPS_DM_MAIL_SUPPORT", "mail_support");
        env.set("APPS_DM_FS_LOCALSTORE_BASEDIR", "fs_localStore_baseDir");
        env.set("APPS_DM_DERIVATIVES_LIST", "derivatives_list");
        env.set("APPS_DM_XDRE_FFMPEG", "xdre_ffmpeg");
        env.set("APPS_DM_FFMPEG_VIDEO_PARAMS", "ffmpeg_video_params");
        env.set("APPS_DM_FFMPEG_AUDIO_PARAMS", "ffmpeg_audio_params");
        env.set("APPS_DM_VIDEO_SIZE", "video_size");
        env.set("APPS_DM_EXIFTOOL_COMMAND", "exiftool_command");
        env.set("APPS_DM_IMAGEMAGICK_COMMAND", "imageMagick_command");
        env.set("APPS_DM_IMAGEMAGICK_PARAMS", "imageMagick_params");
        env.set("APPS_DM_XDRE_CLR_THUMBNAILSSIZE", "xdre_clr_thumbnailsSize");
        env.set("APPS_DM_XDRE_CLR_IMGSIZE", "xdre_clr_imgSize");
        env.set("APPS_DM_ZOOMFY_COMMAND", "zoomfy_command");
        env.set("APPS_DM_WATERMARK_COMMAND", "watermark_command");
        env.set("APPS_DM_WATERMARK_IMAGE", "watermark_image");
        env.set("APPS_DM_WATERMARK_DERIVATIVE_LIST", "watermark_derivative_list");
        env.set("APPS_DM_BATCH_ADDITIONAL_FIELDS", "batch_additional_fields");
        env.set("APPS_DM_NS_PREFIX", "ns_prefix");
        env.set("APPS_DM_DAMS_WEBLOG_DIR", "dams_weblog_dir");
        env.set("APPS_DM_DAMS_STATS_SE_PATTERNS", "dams_stats_se_patterns");
        env.set("APPS_DM_DAMS_STATS_SE_DATA", "dams_stats_se_data");
        env.set("APPS_DM_DAMS_STATS_IP_FILTER", "dams_stats_ip_filter");
        env.set("APPS_DM_DAMS_STATS_QUERY_IP_FILTER", "dams_stats_query_ip_filter");
        env.set("APPS_DM_DAMS_CURATOR_ROLE", "dams_curator_role");
        env.set("APPS_DM_FFMPEG_EMBED_PARAMS", "ffmpeg_embed_params");
        env.set("APPS_DM_CIL_HARVEST_DIR", "cil_harvest_dir");
        env.set("APPS_DM_CIL_HARVEST_METADATA_MAPPING_XSLT", "cil_harvest_metadata_mapping_xslt");
        env.set("APPS_DM_CIL_HARVEST_EMAILS", "cil_harvest_emails");
        env.set("APPS_DM_CIL_HARVEST_API", "cil_harvest_api");
        env.set("APPS_DM_CIL_HARVEST_API_USER", "cil_harvest_api_user");
        env.set("APPS_DM_CIL_HARVEST_API_PWD", "cil_harvest_api_pwd");
        env.set("APPS_DM_CIL_CONTENT_URLBASE", "cil_content_urlBase");
        env.set("APPS_DM_DAMS_DAMS42JSON_XSL", "dams_dams42json_xsl");
        env.set("APPS_DM_DB_DRIVERCLASSNAME", "db_driverClassName");
        env.set("APPS_DM_DB_URL", "db_url");
        env.set("APPS_DM_DB_USERNAME", "db_username");
        env.set("APPS_DM_DB_PWD", "db_pwd");
        env.set("APPS_DM_DB_MAXIDLE", "db_maxIdle");
        env.set("APPS_DM_DB_MAXACTIVE", "db_maxActive");
        env.set("APPS_DM_DB_VALIDATIONQUERY", "db_validationQuery");
        env.set("APPS_DM_DB_MAXWAITMILLIS", "db_maxWaitMillis");
        env.set("APPS_DM_DB_TESTONBORROW", "db_testOnBorrow");
        env.set("APPS_DM_DB_TESTONRETURN", "db_testOnReturn");
        env.set("APPS_DM_DB_REMOVEABANDONED", "db_removeAbandoned");
           env.set("APPS_DM_DB_REMOVEABANDONEDTIMEOUT", "db_removeAbandonedTimeout");
        env.set("APPS_DM_DB_LOGABANDONED", "db_logAbandoned");
    }

    @Test
    public void testPropertiesFromEnv() throws Exception {
        Properties props = Constants.propertiesFromEnv();
        assertEquals("Property xdre.clusterHostName doesn't match!", "localhost", props.get("xdre.clusterHostName"));
        assertEquals("Property xdre.damsRepo doesn't match!", "http://localhost:8080/dams/api", props.get("xdre.damsRepo"));
        assertEquals("Property xdre.damsRepo.user doesn't match!", "dams", props.get("xdre.damsRepo.user"));
        assertEquals("Property xdre.damsRepo.pwd doesn't match!", "password", props.get("xdre.damsRepo.pwd"));
        assertEquals("Property xdre.staging doesn't match!", "xdre_staging", props.get("xdre.staging"));
        assertEquals("Property xdre.tmpFileDir doesn't match!", "xdre_tmpFileDir", props.get("xdre.tmpFileDir"));
        assertEquals("Property xdre.defaultDerivatives doesn't match!", "xdre_defaultDerivatives", props.get("xdre.defaultDerivatives"));
        assertEquals("Property xdre.solrBase doesn't match!", "xdre_solrBase", props.get("xdre.solrBase"));
        assertEquals("Property xdre.ark.urlBase doesn't match!", "xdre_ark_urlBase", props.get("xdre.ark.urlBase"));
        assertEquals("Property xdre.clr.urlBase doesn't match!", "xdre_clr_urlBase", props.get("xdre.clr.urlBase"));
        assertEquals("Property xdre.clr.imgDir doesn't match!", "xdre_clr_imgDir", props.get("xdre.clr.imgDir"));
        assertEquals("Property xdre.clr.thumbnailsDir doesn't match!", "xdre_clr_thumbnailsDir", props.get("xdre.clr.thumbnailsDir"));
        assertEquals("Property xdre.clr.sourceDir doesn't match!", "xdre_clr_sourceDir", props.get("xdre.clr.sourceDir"));
        assertEquals("Property xdre.ark.name doesn't match!", "xdre_ark_name", props.get("xdre.ark.name"));
        assertEquals("Property xdre.ark.orgCode doesn't match!", "xdre_ark_orgCode", props.get("xdre.ark.orgCode"));
        assertEquals("Property mail.support doesn't match!", "mail_support", props.get("mail.support"));
        assertEquals("Property fs.localStore.baseDir doesn't match!", "fs_localStore_baseDir", props.get("fs.localStore.baseDir"));
        assertEquals("Property derivatives.list doesn't match!", "derivatives_list", props.get("derivatives.list"));
        assertEquals("Property xdre.ffmpeg doesn't match!", "xdre_ffmpeg", props.get("xdre.ffmpeg"));
        assertEquals("Property ffmpeg.video.params doesn't match!", "ffmpeg_video_params", props.get("ffmpeg.video.params"));
        assertEquals("Property ffmpeg.audio.params doesn't match!", "ffmpeg_audio_params", props.get("ffmpeg.audio.params"));
        assertEquals("Property video.size doesn't match!", "video_size", props.get("video.size"));
        assertEquals("Property exiftool.command doesn't match!", "exiftool_command", props.get("exiftool.command"));
        assertEquals("Property imageMagick.command doesn't match!", "imageMagick_command", props.get("imageMagick.command"));
        assertEquals("Property imageMagick.params doesn't match!", "imageMagick_params", props.get("imageMagick.params"));
        assertEquals("Property xdre.clr.thumbnailsSize doesn't match!", "xdre_clr_thumbnailsSize", props.get("xdre.clr.thumbnailsSize"));
        assertEquals("Property xdre.clr.imgSize doesn't match!", "xdre_clr_imgSize", props.get("xdre.clr.imgSize"));
        assertEquals("Property zoomfy.command doesn't match!", "zoomfy_command", props.get("zoomfy.command"));
        assertEquals("Property watermark.command doesn't match!", "watermark_command", props.get("watermark.command"));
        assertEquals("Property watermark.image doesn't match!", "watermark_image", props.get("watermark.image"));
        assertEquals("Property watermark.derivative.list doesn't match!", "watermark_derivative_list", props.get("watermark.derivative.list"));
        assertEquals("Property batch.additional.fields doesn't match!", "batch_additional_fields", props.get("batch.additional.fields"));
        assertEquals("Property ns.prefix doesn't match!", "ns_prefix", props.get("ns.prefix"));
        assertEquals("Property dams.weblog.dir doesn't match!", "dams_weblog_dir", props.get("dams.weblog.dir"));
        assertEquals("Property dams.stats.se.patterns doesn't match!", "dams_stats_se_patterns", props.get("dams.stats.se.patterns"));
        assertEquals("Property dams.stats.se.data doesn't match!", "dams_stats_se_data", props.get("dams.stats.se.data"));
        assertEquals("Property dams.stats.ip.filter doesn't match!", "dams_stats_ip_filter", props.get("dams.stats.ip.filter"));
        assertEquals("Property dams.stats.query.ip.filter doesn't match!", "dams_stats_query_ip_filter", props.get("dams.stats.query.ip.filter"));
        assertEquals("Property dams.curator.role doesn't match!", "dams_curator_role", props.get("dams.curator.role"));
        assertEquals("Property ffmpeg.embed.params doesn't match!", "ffmpeg_embed_params", props.get("ffmpeg.embed.params"));
        assertEquals("Property cil.harvest.dir doesn't match!", "cil_harvest_dir", props.get("cil.harvest.dir"));
        assertEquals("Property cil.harvest.metadata.mapping.xslt doesn't match!", "cil_harvest_metadata_mapping_xslt", props.get("cil.harvest.metadata.mapping.xslt"));
        assertEquals("Property cil.harvest.emails doesn't match!", "cil_harvest_emails", props.get("cil.harvest.emails"));
        assertEquals("Property cil.harvest.api doesn't match!", "cil_harvest_api", props.get("cil.harvest.api"));
        assertEquals("Property cil.harvest.api.user doesn't match!", "cil_harvest_api_user", props.get("cil.harvest.api.user"));
        assertEquals("Property cil.harvest.api.pwd doesn't match!", "cil_harvest_api_pwd", props.get("cil.harvest.api.pwd"));
        assertEquals("Property cil.content.urlBase doesn't match!", "cil_content_urlBase", props.get("cil.content.urlBase"));
        assertEquals("Property dams.dams42json.xsl doesn't match!", "dams_dams42json_xsl", props.get("dams.dams42json.xsl"));
        assertEquals("Property db.driverClassName doesn't match!", "db_driverClassName", props.get("db.driverClassName"));
        assertEquals("Property db.url doesn't match!", "db_url", props.get("db.url"));
        assertEquals("Property db.username doesn't match!", "db_username", props.get("db.username"));
        assertEquals("Property db.pwd doesn't match!", "db_pwd", props.get("db.pwd"));
        assertEquals("Property db.maxIdle doesn't match!", "db_maxIdle", props.get("db.maxIdle"));
        assertEquals("Property db.maxActive doesn't match!", "db_maxActive", props.get("db.maxActive"));
        assertEquals("Property db.validationQuery doesn't match!", "db_validationQuery", props.get("db.validationQuery"));
        assertEquals("Property db.maxWaitMillis doesn't match!", "db_maxWaitMillis", props.get("db.maxWaitMillis"));
        assertEquals("Property db.testOnBorrow doesn't match!", "db_testOnBorrow", props.get("db.testOnBorrow"));
        assertEquals("Property db.testOnReturn doesn't match!", "db_testOnReturn", props.get("db.testOnReturn"));
        assertEquals("Property db.removeAbandoned doesn't match!", "db_removeAbandoned", props.get("db.removeAbandoned"));
           assertEquals("Property db.removeAbandonedTimeout doesn't match!", "db_removeAbandonedTimeout", props.get("db.removeAbandonedTimeout"));
        assertEquals("Property db.logAbandoned doesn't match!", "db_logAbandoned", props.get("db.logAbandoned"));
    }

    @Test
    public void testCreateDataSource() throws Exception {
        Properties props = createPropertiesForDBConnection();
        DataSource ds = Constants.createDataSource(props);

        assertNotNull(ds);
        assertEquals("DB driverClassName doesn't match!", "org.postgresql.Driver", ((BasicDataSource)ds).getDriverClassName());
        assertEquals("DB url doesn't match!", "jdbc:postgresql://localhost:5432/public", ((BasicDataSource)ds).getUrl());
        assertEquals("DB username doesn't match!", "postgres", ((BasicDataSource)ds).getUsername());
        assertEquals("DB passwors doesn't match!", "password", ((BasicDataSource)ds).getPassword());
        assertEquals("DB max idle connections doesn't match!", 3, ((BasicDataSource)ds).getMaxIdle());
        assertEquals("DB max active connections doesn't match!", 10, ((BasicDataSource)ds).getMaxTotal());
        assertEquals("DB validation query doesn't match!", "select 1", ((BasicDataSource)ds).getValidationQuery());
        assertEquals("DB max wait millis doesn't match!", 5000L, ((BasicDataSource)ds).getMaxWaitMillis());
        assertEquals("DB test on borrow doesn't match!", true, ((BasicDataSource)ds).getTestOnBorrow());
        assertEquals("DB test on return doesn't match!", true, ((BasicDataSource)ds).getTestOnReturn());
        assertEquals("DB remove abandoned doesn't match!", true, ((BasicDataSource)ds).getRemoveAbandonedOnBorrow());
        assertEquals("DB remove abandoned timeout doesn't match!", 60, ((BasicDataSource)ds).getRemoveAbandonedTimeout());
        assertEquals("DB log abandoned doesn't match!", true, ((BasicDataSource)ds).getLogAbandoned());
    }

    private Properties createPropertiesForDBConnection() {
        Properties props = new Properties();

        props.put("db.driverClassName", "org.postgresql.Driver");
        props.put("db.url", "jdbc:postgresql://localhost:5432/public");
        props.put("db.username", "postgres");
        props.put("db.pwd", "password");
        props.put("db.maxIdle", "3");
        props.put("db.maxActive", "10");
        props.put("db.validationQuery", "select 1");
        props.put("db.maxWaitMillis", "5000");
        props.put("db.testOnBorrow", "true");
        props.put("db.testOnReturn", "true");
        props.put("db.removeAbandoned", "true");
        props.put("db.removeAbandonedTimeout", "60");
        props.put("db.logAbandoned", "true");

        return props;
    }
}

