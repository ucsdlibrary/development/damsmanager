package edu.ucsd.library.xdre.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Tests for ArkFiles class
 * @author lsitu
 *
 */
public class ArkFileTest {
	private static String testObjectUri = "http://library.ucsd.edu/ark:/20775/xx00000001";

    @BeforeClass
    public static void init() throws IOException {
        Constants.DAMS_ARK_URL_BASE = "http://library.ucsd.edu/ark:";
        Constants.ARK_ORG = "20775";
        Constants.FILESTORE_DIR = "/pub/data2/dams/localStore";
    }

	@Test
    public void testFromArkFileUri() throws Exception {
        // simple object
		String fileUri = testObjectUri + "/1.mov";
        ArkFile arkFile = ArkFile.fromArkFileUri(fileUri);
        assertEquals("Wrong object!", testObjectUri, arkFile.getObject());
        assertNull("Wrong Component!", arkFile.getComponent());
        assertEquals("Wrong fileanme!", "1.mov", arkFile.getFileName());

        // complex object
        fileUri = testObjectUri + "/2/1.mov"; 
        arkFile = ArkFile.fromArkFileUri(fileUri);
        assertEquals("Wrong object!", testObjectUri, arkFile.getObject());
        assertEquals("Wrong component!", "2", arkFile.getComponent());
        assertEquals("Wrong fileanme!", "1.mov", arkFile.getFileName());
    }

    @Test
    public void testGetDams4FileName() {
        // simple object
        String expected = "20775-xx00000001-0-1.mov";
        String actual = ArkFile.getDams4FileName(testObjectUri, "", "1.mov");
        assertEquals("Wrong simple object filename!", expected, actual);

        // complex object
        expected = "20775-xx00000001-2-1.mov";
        actual = ArkFile.getDams4FileName(testObjectUri, "2", "1.mov");
        assertEquals("Wrong complex object filename!", expected, actual);
    }

    @Test
    public void testGetLocalStoreFile() throws Exception {
        // simple object
        Path expected = Paths.get("/pub/data2/dams/localStore/xx/00/00/00/01/20775-xx00000001-0-1.mov");
        Path actual = ArkFile.getLocalStoreFile(testObjectUri + "/1.mov");
        assertEquals("Wrong simple object localStore file!", expected, actual);

        // complex object
        expected = Paths.get("/pub/data2/dams/localStore/xx/00/00/00/01/20775-xx00000001-2-1.mov");
        actual = ArkFile.getLocalStoreFile(testObjectUri + "/2/1.mov");
        assertEquals("Wrong complex object localStore file!", expected, actual);
    }

    @Test
    public void testGetArkUrl() {
        // simple object file
        String expected = testObjectUri + "/1.mov";
        String actual = ArkFile.getArkUrl(testObjectUri, "", "1.mov");
        assertEquals("Wrong simple object ARK url!", expected, actual);

        // complex object
        expected = testObjectUri + "/2/1.mov";
        actual = ArkFile.getArkUrl(testObjectUri, "2", "1.mov");
        assertEquals("Wrong complex object ARK url!", expected, actual);
    }
}

