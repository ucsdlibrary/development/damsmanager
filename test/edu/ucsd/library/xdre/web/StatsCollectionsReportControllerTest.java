package edu.ucsd.library.xdre.web;

import static org.junit.Assert.assertEquals;

import java.net.URL;

import org.dom4j.Document;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import org.junit.Before;
import org.junit.Test;

import edu.ucsd.library.xdre.utils.ArkFile;
import edu.ucsd.library.xdre.utils.Constants;
import edu.ucsd.library.xdre.utils.DAMSClient;

/**
 * Test methods for StatsCollectionReportController class
 * @author lsitu
 *
 */
public class StatsCollectionsReportControllerTest {

    private static String COLLECTION_ARK = "zzcc000001";
    private static String NESTED_COLLECTION_ARK = "xxcc000001";

    private Document collDoc;

    @Before
    public void init() throws Exception {
        Constants.DAMS_STORAGE_URL = "http://localhost:8080/dams/api";
        Constants.DAMS_ARK_URL_BASE = "http://library.ucsd.edu/ark:";
        Constants.ARK_ORG = "20775";

        URL collectionPath = getClass().getResource("/resources/dams_collection_zzcc000001.xml");
        collDoc = new SAXReader().read(collectionPath);
    }

    @Test
    public void testGetCreatedDate() throws Exception {
        Node node = collDoc.selectSingleNode("/rdf:RDF//*[@rdf:about='" + ArkFile.getArkUrl(COLLECTION_ARK) + "'");
        String expected = "2024-09-01";
        String createdDate = StatsCollectionsReportController.getCreatedDate(node);

        assertEquals("Got created date" + createdDate + " instead of " + expected + "!", expected, createdDate);
    }

    @Test
    public void testGetReleasedDate() throws Exception {
        Node node = collDoc.selectSingleNode("/rdf:RDF//*[@rdf:about='" + ArkFile.getArkUrl(COLLECTION_ARK) + "'");
        String expected = "2024-10-01";
        String createdDate = StatsCollectionsReportController.getReleasedDate(node);

        assertEquals("Got created date" + createdDate + " instead of " + expected + "!", expected, createdDate);
    }


    @Test
    public void testGetEarliestEventDate() throws Exception {
        Node node = collDoc.selectSingleNode("/rdf:RDF//*[@rdf:about='" + ArkFile.getArkUrl(COLLECTION_ARK) + "'");
        String expected = "2024-09-20";
        String createdDate = StatsCollectionsReportController.getEventDate(node, DAMSClient.RECORD_EDITED, false);

        assertEquals("Got earliest edited date" + createdDate + " instead of " + expected + "!", expected, createdDate);
    }

    @Test
    public void testGetCreatedDateWithNoCreationEvent() throws Exception {
        Node node = collDoc.selectSingleNode("/rdf:RDF//*[@rdf:about='" + ArkFile.getArkUrl(NESTED_COLLECTION_ARK) + "'");
        String expected = "2024-07-01";
        String createdDate = StatsCollectionsReportController.getCreatedDate(node);

        assertEquals("Got created date" + createdDate + " instead of " + expected + "!", expected, createdDate);
    }

    @Test
    public void testGetReleasedDateNested() throws Exception {
        Node node = collDoc.selectSingleNode("/rdf:RDF//*[@rdf:about='" + ArkFile.getArkUrl(NESTED_COLLECTION_ARK) + "'");
        String expected = "2024-08-01";
        String createdDate = StatsCollectionsReportController.getReleasedDate(node);

        assertEquals("Got created date" + createdDate + " instead of " + expected + "!", expected, createdDate);
    }

    @Test
    public void testGetEarliestEventDateNested() throws Exception {
        Node node = collDoc.selectSingleNode("/rdf:RDF//*[@rdf:about='" + ArkFile.getArkUrl(NESTED_COLLECTION_ARK) + "'");
        String expected = "2024-07-01";
        String createdDate = StatsCollectionsReportController.getEventDate(node, DAMSClient.RECORD_EDITED, false);

        assertEquals("Got earliest edited date" + createdDate + " instead of " + expected + "!", expected, createdDate);
    }
}
