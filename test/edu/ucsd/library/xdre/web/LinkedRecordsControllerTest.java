package edu.ucsd.library.xdre.web;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import edu.ucsd.library.xdre.harvesting.CilHavestingTestBase;

/**
 * Test methods for LinkedRecordsController class
 * @author lsitu
 *
 */
public class LinkedRecordsControllerTest extends CilHavestingTestBase {
    private String testArk = "http://library.ucsd.edu/ark:/20775/zz00000001";   // ark for test
    private String predFirst = "http://library.ucsd.edu/ark:/20775/bd6724396t"; // Predicate rdf:first
    private String predRest = "http://library.ucsd.edu/ark:/20775/bd54274567";  // Predicate rdf:resr

    @Test
    public void testBuildSparql() throws Exception {

        // Test 1 level SPARQL
        String sparqlLevel1Expected = "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "
                + "SELECT ?sub ?type WHERE { ?sub ?p1 <" + testArk + "> . "
                                          + "?sub rdf:type ?_bn . ?_bn rdf:label ?type }";
        String sparqlActual = LinkedRecordsController.buildSparqlWithArk(testArk, 1);
        assertEquals("Level 1 SPARQL doesn't match!", sparqlLevel1Expected, sparqlActual);

        // Test 2 levels SPARQL for component/element list
        sparqlLevel1Expected = "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "
                + "SELECT ?sub WHERE { ?sub ?p1 ?o1 . ?o1 ?p2 <" + testArk + "> }";
        sparqlActual = LinkedRecordsController.buildSparqlWithArk(testArk, 2);
        assertEquals("Level 2 SPARQL doesn't match!", sparqlLevel1Expected, sparqlActual);

        // Test 3 levels SPARQL for component/element list
        sparqlLevel1Expected = "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "
                + "SELECT ?sub WHERE { ?sub ?p1 ?o1 . "
                                    + "?o1 <" + predRest + "> ?o2 . "
                                    + "?o2 <" + predFirst + "> <" + testArk + "> }";
        sparqlActual = LinkedRecordsController.buildSparqlWithArk(testArk, 3);
        assertEquals("Level 3 SPARQL doesn't match!", sparqlLevel1Expected, sparqlActual);

        // Test 4 levels SPARQL for component/element list
        sparqlLevel1Expected = "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "
                + "SELECT ?sub WHERE { ?sub ?p1 ?o1 . "
                                    + "?o1 <" + predRest + "> ?o2 . "
                                    + "?o2 <" + predRest + "> ?o3 . "
                                    + "?o3 <" + predFirst + "> <" + testArk + "> }";
        sparqlActual = LinkedRecordsController.buildSparqlWithArk(testArk, 4);
        assertEquals("Level 4 SPARQL doesn't match!", sparqlLevel1Expected, sparqlActual);
    }

    @Test
    public void testBuildSparqlWithPredicate() throws Exception {

        // Test 1 level SPARQL with predicate
        String sparqlLevel1Expected = "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "
                + "SELECT ?sub ?type WHERE { ?sub ?pre <" + testArk + "> . "
                                          + "?sub rdf:type ?_bn . ?_bn rdf:label ?type }";
        String sparqlActual = LinkedRecordsController.buildSparqlWithArk(testArk, 1, "?pre");
        assertEquals("Level 1 SPARQL doesn't match!", sparqlLevel1Expected, sparqlActual);

        // Test 2 levels SPARQL for component list with predicate
        sparqlLevel1Expected = "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "
                + "SELECT ?sub WHERE { ?sub <" + LinkedRecordsController.PREDICATE_MADS_COMPONENT_LIST + ">"
                + " ?o1 . ?o1 ?p2 <" + testArk + "> }";
        sparqlActual = LinkedRecordsController
                .buildSparqlWithArk(testArk, 2, LinkedRecordsController.PREDICATE_MADS_COMPONENT_LIST);
        assertEquals("Level 2 SPARQL doesn't match!", sparqlLevel1Expected, sparqlActual);

        // Test 3 levels SPARQL with mads:componentList predicate
        sparqlLevel1Expected = "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "
                + "SELECT ?sub WHERE { ?sub <" + LinkedRecordsController.PREDICATE_MADS_COMPONENT_LIST + "> ?o1 . "
                                    + "?o1 <" + predRest + "> ?o2 . "
                                    + "?o2 <" + predFirst + "> <" + testArk + "> }";
        sparqlActual = LinkedRecordsController
                .buildSparqlWithArk(testArk, 3, LinkedRecordsController.PREDICATE_MADS_COMPONENT_LIST);
        assertEquals("Level 3 SPARQL doesn't match!", sparqlLevel1Expected, sparqlActual);
    }
}
