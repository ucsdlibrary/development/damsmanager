package edu.ucsd.library.xdre.web;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.Arrays;
import org.junit.Before;
import org.junit.Test;

import edu.ucsd.library.xdre.tab.BatchEditExcelSource;
import edu.ucsd.library.xdre.tab.ExcelSource;
import edu.ucsd.library.xdre.tab.Record;
import edu.ucsd.library.xdre.tab.TabularRecordTestBasic;
import edu.ucsd.library.xdre.utils.Constants;

/**
 * Test methods for CollectionOperationController class
 * @author lsitu
 *
 */
public class CollectionOperationControllerTest extends TabularRecordTestBasic {
    private File xlsInputTemplate = null;
    private File xlsInputTestFile = null;
    private BatchEditExcelSource excelSource = null;

    @Before
    public void init() throws Exception {
        Constants.DAMS_STORAGE_URL = "http://localhost:8080/dams/api";
        Constants.DAMS_ARK_URL_BASE = "http://library.ucsd.edu/ark:";
        Constants.ARK_ORG = "20775";
        Constants.BATCH_ADDITIONAL_FIELDS = "Note:local attribution,collection(s),unit";

        xlsInputTemplate = getResourceFile("xls_standard_input_template.xlsx");
        xlsInputTestFile = getResourceFile("xls_batch_edit_input_test.xlsx");
        BatchEditExcelSource.initControlValues(xlsInputTemplate);

        excelSource = new BatchEditExcelSource(xlsInputTestFile, Arrays.asList(ExcelSource.IGNORED_FIELDS_FOR_OBJECTS));
    }

    @Test
    public void testReportInvalidValueForComponent() throws Exception {
        Record record = excelSource.nextRecord();

        boolean expected = CollectionOperationController.errorReportForObject(record.recordID(), excelSource.getInvalidValues().get(1));
        assertTrue("Invalid value for component ARK is not being caught!", expected);
    }
}
