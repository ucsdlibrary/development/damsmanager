package edu.ucsd.library.xdre.collection;

import static org.junit.Assert.assertEquals;

import org.dom4j.Document;
import org.dom4j.io.SAXReader;
import org.junit.Test;

import edu.ucsd.library.xdre.tab.TabularRecordTestBasic;

/**
 * Tests for DamsDataMigrationHandler class
 */
public class DamsDataMigrationHandlerTest extends TabularRecordTestBasic {
    @Test
    public void testGetVisibility() throws Exception {
        Document doc = new SAXReader().read(getResource("rights_metadata.xml"));

        String actualResult = DamsDataMigrationHandler.getVisibility(doc);
        assertEquals("'Wrong visibility value!", "open", actualResult);
    }
}
