package edu.ucsd.library.xdre.collection;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.security.auth.login.LoginException;

import org.junit.Before;
import org.junit.Test;

import edu.ucsd.library.xdre.model.Subject;
import edu.ucsd.library.xdre.tab.SubjectTabularEditRecord;
import edu.ucsd.library.xdre.tab.TabularRecordTestBasic;
import edu.ucsd.library.xdre.utils.Constants;

/**
 * Tests for SubjectOverlayHandler class
 * @author lsitu
 *
 */
public class SubjectOverlayHandlerTest extends TabularRecordTestBasic {

    @Before
    public void init() throws LoginException, IOException {
        Constants.DAMS_STORAGE_URL = "http://localhost:8080/dams/api";
        Constants.DAMS_ARK_URL_BASE = "http://library.ucsd.edu/ark:";
        Constants.ARK_ORG = "20775";

        Map<String, String> nsPrefixes = new HashMap<>();
        nsPrefixes.put("rdf", "http://www.w3.org/1999/02/22-rdf-syntax-ns#");
        nsPrefixes.put("mads", "http://www.loc.gov/mads/rdf/v1#");
        nsPrefixes.put("dams", "http://library.ucsd.edu/ontology/dams#");

        Constants.NS_PREFIX_MAP = nsPrefixes;
    }

    @Test
    public void testIsElementChangedWithAuthoritativeLabel() throws Exception {
        // Initiate subject tabular data
        String type = "Subject:topic";
        String typeOverlay = "Subject:anatomy";
        String term = "Test term";

        Map<String, String> data = createSubjectDataWithTerm("zzxxxxxxxx", type, term);
        String xpath = "/rdf:RDF/*/" + Subject.MADS + ":" + Subject.AUTHORITATIVE_LABEL;

        // Test subject term with no changes
        String termOverlay = term;
        Map<String, String> overlayData = createSubjectDataWithTerm("zzxxxxxxxx", typeOverlay, termOverlay);
        SubjectTabularEditRecord testSubject = createdSubjectWithOverlay(data, overlayData);

        boolean result = SubjectOverlayHandler.isElementChanged(testSubject.getDocument(), testSubject.toRDFXML(), xpath);
        assertFalse("'subject term' is changed!", result);

        // Test subject term with changes
        termOverlay = getOverlayValue(term);
        overlayData = createSubjectDataWithTerm("zzxxxxxxxx", typeOverlay, termOverlay);
        testSubject = createdSubjectWithOverlay(data, overlayData);

        result = SubjectOverlayHandler.isElementChanged(testSubject.getDocument(), testSubject.toRDFXML(), xpath);
        assertTrue("'subject term'has no changed!", result);
    }

    @Test
    public void testIsElementChangedWithRdfResource() throws Exception {
        // Initiate subject tabular data
        String type = "Subject:topic";
        String term = "Test term";
        Map<String, String> data = createSubjectDataWithTerm("zzxxxxxxxx", type, term);
        String closeMatch1 = "http://viaf.org/viaf/close_match";
        data.put("closematch", closeMatch1);

        String xpath = "/rdf:RDF/*/" + Subject.MADS + ":" + Subject.HAS_CLOSE_EXTERNAL_AUTHORITY + "/@rdf:resource";

        //// Test close match with single value

        // Test close match single field with no changes
        Map<String, String> overlayData = createSubjectDataWithTerm("zzxxxxxxxx", type, term);
        overlayData.put("closematch", closeMatch1);
        SubjectTabularEditRecord testSubject = createdSubjectWithOverlay(data, overlayData);

        boolean result = SubjectOverlayHandler.isElementChanged(testSubject.getDocument(), testSubject.toRDFXML(), xpath);
        assertFalse("'subject closeMatch' is changed!", result);

        // Test close match single field  with changes
        String closeMatch2 = "http://vocab.getty.edu/aat/close_match";
        overlayData.put("closematch", closeMatch2);
        testSubject = createdSubjectWithOverlay(data, overlayData);

        result = SubjectOverlayHandler.isElementChanged(testSubject.getDocument(), testSubject.toRDFXML(), xpath);
        assertTrue("'subject closeMatch' has no changes!", result);

        //// Test close match with multiple values 

        // Test close match multiple values with no changes
        String closeMatchOverlay = closeMatch1 + " | " + closeMatch2;
        data.put("closematch", closeMatchOverlay);
        overlayData.put("closematch", closeMatchOverlay);
        testSubject = createdSubjectWithOverlay(data, overlayData);

        result = SubjectOverlayHandler.isElementChanged(testSubject.getDocument(), testSubject.toRDFXML(), xpath);
        assertFalse("'subject closeMatch' multiple values are changed!", result);

        // Test close match multiple values with changes
        closeMatch2 = "http://vocab.getty.edu/aat/close_match2";
        closeMatchOverlay = closeMatch1 + " | " + closeMatch2;
        overlayData.put("closematch", closeMatchOverlay);
        testSubject = createdSubjectWithOverlay(data, overlayData);

        result = SubjectOverlayHandler.isElementChanged(testSubject.getDocument(), testSubject.toRDFXML(), xpath);
        assertTrue("'subject closeMatch' multiple values have no changes!", result);
    }

    @Test
    public void testIsElementChangedWithVariant() throws Exception {
        // Initiate subject tabular data for edit
        String type = "Subject:topic";
        String term = "Test term";
        Map<String, String> data = createSubjectDataWithTerm("zzxxxxxxxx", type, term);
        String variantLabel = "Variant Label";
        data.put("variant", variantLabel);

        String xpath = "/rdf:RDF/*/" + Subject.MADS + ":" + Subject.HAS_VARIANT + "/mads:Variant/mads:variantLabel";

        //// Test Variant with single value

        // Test Variant single field with no changes
        Map<String, String> overlayData = createSubjectDataWithTerm("zzxxxxxxxx", type, term);
        overlayData.put("variant", variantLabel);
        SubjectTabularEditRecord testSubject = createdSubjectWithOverlay(data, overlayData);

        boolean result = SubjectOverlayHandler.isElementChanged(testSubject.getDocument(), testSubject.toRDFXML(), xpath);
        assertFalse("'subject variant' changed!", result);

        // Test Variant single field with changes
        String variantLabel2 = "Variant Label 2";
        overlayData.put("variant", variantLabel2);
        testSubject = createdSubjectWithOverlay(data, overlayData);

        result = SubjectOverlayHandler.isElementChanged(testSubject.getDocument(), testSubject.toRDFXML(), xpath);
        assertTrue("'subject variant' has no changes!", result);

        //// Test Variant with multiple values 

        // Test Variant multiple values with no changes
        String variantOverlay = variantLabel + " | " + variantLabel2;
        data.put("variant", variantOverlay);
        overlayData.put("variant", variantOverlay);
        testSubject = createdSubjectWithOverlay(data, overlayData);

        result = SubjectOverlayHandler.isElementChanged(testSubject.getDocument(), testSubject.toRDFXML(), xpath);
        assertFalse("'subject variant' multiple values are changed!", result);

        // Test Variant multiple values with changes
        variantLabel2 = "Variant Label 2 Overlayed2";
        variantOverlay = variantLabel + " | " + variantLabel2;
        overlayData.put("variant", variantOverlay);
        testSubject = createdSubjectWithOverlay(data, overlayData);

        result = SubjectOverlayHandler.isElementChanged(testSubject.getDocument(), testSubject.toRDFXML(), xpath);
        assertTrue("'subject variant' multiple values have no changes!", result);
    }

    @Test
    public void testOverlayWithXmlLang() throws Exception {
        // Initiate subject tabular data
        String type = "Subject:topic";
        String term1 = "\"陈昭宏, 1942-\"@zh-Hans";

        Map<String, String> data = createSubjectDataWithTerm("zzxxxxxxxx", type, term1);
        Map<String, String> overlayData = createSubjectDataWithTerm("zzxxxxxxxx", type, term1);

        String xpath = "/rdf:RDF/*/" + Subject.MADS + ":" + Subject.AUTHORITATIVE_LABEL;

        // Test script xml:lang with no changes
        SubjectTabularEditRecord testSubject = createdSubjectWithOverlay(data, overlayData);

        boolean result = SubjectOverlayHandler.isElementChanged(testSubject.getDocument(), testSubject.toRDFXML(), xpath);
        assertFalse("'subject term' with xml:lang is changed!", result);

        // Test script xml:lang with changes
        String term2 = "\"陈昭宏, 1942-\"@zh";
        overlayData = createSubjectDataWithTerm("zzxxxxxxxx", type, term2);
        testSubject = createdSubjectWithOverlay(data, overlayData);

        result = SubjectOverlayHandler.isElementChanged(testSubject.getDocument(), testSubject.toRDFXML(), xpath);
        assertTrue("'subject term' with xml:lang has no changed!", result);
    }

    @Test
    public void testNeedLinkedRecordsIndex() throws Exception {
        // Initiate subject tabular data
        String type = "Subject:topic";
        String term = "Test term";
        Map<String, String> data = createSubjectDataWithTerm("zzxxxxxxxx", type, term);
        String exactMatch1 = "http://viaf.org/viaf/exact_match";
        data.put("exactmatch", exactMatch1);

        //// Test exact match is the only change
        Map<String, String> overlayData = createSubjectDataWithTerm("zzxxxxxxxx", type, term);
        SubjectTabularEditRecord testSubject = createdSubjectWithOverlay(data, overlayData);

        assertFalse("SOLR index is updated for linked records!", SubjectOverlayHandler.needLinkedRecordsIndex(testSubject));

        overlayData = createSubjectDataWithTerm("zzxxxxxxxx", type, term);
        String exactMatch2 = "http://viaf.org/viaf/exact_match2";
        overlayData.put("exactmatch", exactMatch2);
        testSubject = createdSubjectWithOverlay(data, overlayData);

        assertFalse("SOLR index is updated for linked records!", SubjectOverlayHandler.needLinkedRecordsIndex(testSubject));

        //// Test exact match has no changes
        overlayData = createSubjectDataWithTerm("zzxxxxxxxx", type, term + " overlayed");
        overlayData.put("exactmatch", exactMatch1);
        testSubject = createdSubjectWithOverlay(data, overlayData);

        assertTrue("SOLR index is not updated for linked records!", SubjectOverlayHandler.needLinkedRecordsIndex(testSubject));

        //// Test exact match changed with other metadata changed
        overlayData = createSubjectDataWithTerm("zzxxxxxxxx", type, term + " overlayed");
        overlayData.put("exactmatch", exactMatch2);
        testSubject = createdSubjectWithOverlay(data, overlayData);

        assertTrue("SOLR index is not updated for linked records!", SubjectOverlayHandler.needLinkedRecordsIndex(testSubject));

    }
}
