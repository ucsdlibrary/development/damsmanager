package edu.ucsd.library.xdre.collection;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Node;
import org.junit.Test;

import edu.ucsd.library.xdre.tab.TabularRecordTestBasic;

/**
 * Tests for TranscriptUploadHandler class
 * @author lsitu
 *
 */
public class TranscriptUploadHandlerTest extends TabularRecordTestBasic {
    private static String testObjectUri = "http://library.ucsd.edu/ark:/20775/xx00000001";
    private static String  objectXPath = "/rdf:RDF/dams:Object";

    @Test
    public void testHasTranscript() throws Exception {
        String transCompUri = testObjectUri + "/2";

        // simple video object with transcript
        Document transDoc = createObjectElementWithTranscript("Video", TranscriptUploadHandler.TRANSCRIPT_TITLE);
        Node objNode = transDoc.selectSingleNode(objectXPath);
        assertTrue(TranscriptUploadHandler.hasTranscript(objNode, transCompUri));

        // complex object with no transcript component
        Document nonTransDoc = createObjectElementWithTranscript("Video", "Component title");
        objNode = nonTransDoc.selectSingleNode(objectXPath);
        assertFalse(TranscriptUploadHandler.hasTranscript(objNode, transCompUri));
    }

    private Document createObjectElementWithTranscript(String component1, String component2) {
        Element objElem = createDocumentRoot(testObjectUri);
        TranscriptUploadHandler.addComponent(objElem, testObjectUri + "/1", component1, 1);
        TranscriptUploadHandler.addComponent(objElem, testObjectUri + "/2", component2, 2);

        return objElem.getDocument();
    }
}
