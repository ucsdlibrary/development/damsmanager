<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<label class="submenuText" for="data"><span class="requiredLabel">*</span><b>SOLR Index Priority: </b></label>
<select name="indexPriority" id="indexPriority" class="inputText" style="width: 412px;">
    <jsp:include flush="true" page="/jsp/indexpriorityoptions.jsp" />
</select>