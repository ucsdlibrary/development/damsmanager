<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<tr align="left">
    <td>
        <div class="submenuText"><span class="requiredLabel">*</span><b>SOLR Index Priority: </b></div>
    </td>
    <td>
        <select name="indexPriority" id="indexPriority" class="inputText" style="width: 356px;">
            <jsp:include flush="true" page="/jsp/indexpriorityoptions.jsp" />
        </select>
    </td>
</tr>
