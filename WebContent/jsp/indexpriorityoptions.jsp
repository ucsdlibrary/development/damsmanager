<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:forEach var="entry" items="${model.indexPriorities}">
    <option value="${entry.value}" <c:if test="${model.prioritySelected == entry.value}">selected</c:if>>
        <c:out value="${entry.key}" />
    </option>
</c:forEach>