<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="ribbonDisplay" scope="session">${model.environment == "Production" ? "none" : "block"}</c:set>
<c:set var="ribbonText" scope="session">${model.environment}</c:set>

<html>
<jsp:include flush="true" page="/jsp/libhtmlheader.jsp" />
<body style="background-color:#fff;">
<jsp:include flush="true" page="/jsp/libanner.jsp" />

<table align="center" cellspacing="0px" cellpadding="0px" class="bodyTable">
  <tr>
    <td>
      <div style="padding-bottom:30px;">
        <div id="tdr_crumbs">
            <div id="tdr_crumbs_content">
                <a class="logout" style="margin:5px;float:right;" href="logout.do?">Log out</a>
                <ul>
                    <li><a href="http://libraries.ucsd.edu/index.html">Library Home</a></li>
                    <li>DAMS Manager</li>
                </ul>
            </div><!-- /tdr_crumbs_content -->
            
            <!-- This div is for temporarily writing breadcrumbs to for processing purposes -->
                <div id="temporaryBreadcrumb" style="display: none">
            </div>
        </div><!-- /tdr_crumbs -->

        <div id="tdr_content" class="tdr_fonts">
            <div id="tdr_content_content">
                <div style="padding:30 0 300 0;font-size:20px;">
                    <span>Metadata export for collection</span>
                    <a style="text-decoration:none" href="${model.category}">${model.title}</a>
                    <span>is ready for</span>
                    <a href="${model.logLink}&file=${model.fileName}">download</a>.
                </div>
            </div>
        </div>
      </div>
    </td>
  </tr>
</table>
<jsp:include flush="true" page="/jsp/libfooter.jsp" />
</body>
</html>
