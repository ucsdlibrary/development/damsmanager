<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ page errorPage="/jsp/errorPage.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<jsp:include flush="true" page="/jsp/libheader.jsp" />
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
</head>
<body onload="loadForm()" style="background-color:#fff;">
<script type="text/javascript">
    var letters = /^[A-Za-z]+$/;
    function confirmOption() {
        var sourceOption = document.mainForm.source.value;
        var mergOption = $("input[name='mergeOption']:checked").val();

        if (sourceOption == "excelOption") {
            var srcFile = document.mainForm.filesPath.value.toLowerCase();
            if (srcFile == "" || !srcFile.includes(".xls")) {
                alert("Please choose a valid Excel source file.");
                document.mainForm.filesPath.focus();
                return false;
            }
        } else {
        var ark = document.mainForm.ark.value.trim();
        if (ark == "") {
            alert("Please enter the ARK to use.");
            document.mainForm.ark.focus();
            return false;
        } else {
            var arkPart = ark.substring(ark.lastIndexOf("/") + 1)
            if (arkPart.length != 10) {
                alert("Invalid ARK " + ark + "!");
                document.mainForm.ark.focus();
                return false;
            }
        }

        var arks = document.mainForm.arksToMerge.value.trim();
        if (arks == "" || arks.indexOf(ark) >=0) {
            if (arks == "")
                alert("Please enter duplicate ARKs delimited by comma (,) for merging.");
            else
                alert("ARKs field (" + arks + ") is not allowed to include the ARK to use " + ark + "!");

            document.mainForm.arksToMerge.focus();
            return false;
        }
        }

        var message = "Are you sure to you want to generate report for the ARKs?";

        if (mergOption == 'merge') {
            message = "This will delete authority records and replace their linkings in DAMS ${ribbonText}. \nHave all validations been completed?";
        }

        var exeConfirm = confirm(message);
        if(!exeConfirm)
            return false;

        document.mainForm.action = "/damsmanager/operationHandler.do?subjectMerge&progress=0&formId=mainForm&sid=" + getSid();
        document.mainForm.submit();
    }

    function loadForm() {
        var source = "<c:out value="${model.source}"/>";
        var mergeOption = "<c:out value="${model.mergeOption}"/>";

        selectSource(source)

        displaySolrFormOptions(mergeOption, 'merge');
    }

    function selectSource(sourceName){
        var source = document.mainForm.source;
        var srcOptions = source.options;

        if (sourceName == null || sourceName == undefined || sourceName == '') {
            sourceName = source.selectedIndex >= 0 ? srcOptions[source.selectedIndex].value : srcOptions[0].value

            // reset merge option to default report only on selection changed
            document.getElementById('reportOption').checked = true;
            displaySolrFormOptions('report', 'merge');
        }

        for(var i=0; i<srcOptions.length; i++){
            var optionValue = srcOptions[i].value;
            if(optionValue != sourceName) {
              var elems = document.getElementsByClassName(optionValue);
              for (let i = 0; i < elems.length; i++) {
                elems[i].style.display = "none";
              }
            }
        }

        var srcElements = document.getElementsByClassName(sourceName);
        for (let i = 0; i < srcElements.length; i++) {
          srcElements[i].style.display = "table-cell";
        }
    }

    var crumbs = [{"Home":"http://library.ucsd.edu"}, {"Digital Library Collections":"/dc"},{"DAMS Manager":"/damsmanager/"}, {"Subject Merge":""}];
    drawBreadcrumbNMenu(crumbs, "tdr_crumbs_content", true);
</script>
<style>
    span.label {
        font-weight: bold;
    }
</style>
<jsp:include flush="true" page="/jsp/libanner.jsp" />
<table align="center" cellspacing="0px" cellpadding="0px" class="bodytable">
  <tr>
    <td>
      <div id="tdr_crumbs">
        <div id="tdr_crumbs_content"></div><!-- /tdr_crumbs_content -->

        <!-- This div is for temporarily writing breadcrumbs to for processing purposes -->
        <div id="temporaryBreadcrumb" style="display: none"></div>
      </div><!-- /tdr_crumbs -->
    </td>
  </tr>
  <tr>
    <td align="center">
      <div id="main" class="mainDiv">
        <form id="mainForm" name="mainForm" method="POST" action="/damsmanager/operationHandler.do?subjectMerge">
          <div class="emBox_ark">
            <div class="emBoxBanner">Subject Merge</div>

            <div style="margin-top:10px;padding-left:20px;" align="left">
              <table>
                <tr>
                  <td height="30px" class="submenuText">
                     <span class="requiredLabel">*</span><span class="label">Records Source: </span>
                  </td>
                  <td>
                    <select id="source" name="source" class="inputText" onChange="selectSource();" style="width: 99%;">
                      <option value="inputOption" <c:if test="${model.source == null || model.source == 'inputOption'}">selected</c:if>>Text Input</option>
                      <option value="excelOption" <c:if test="${model.source == 'excelOption'}">selected</c:if>>Excel</option>
                    </select>
                  </td>
                </tr>
                <tr>
                  <td height="30px" class="submenuText inputOption">
                    <span class="requiredLabel">*</span><span class="label">ARK To Use: </span>
                  </td>
                  <td class="inputOption">
                    <div class="submenuText" id="modsSpan" title="Please enter the ARK to use.">
                      <input type="text" placeholder="Enter an ARK" style="background-color:#eee;" id="ark" name="ark" size="64" value="">
                    </div>
                  </td>
                </tr>
                <tr>
                  <td height="30px" class="submenuText inputOption">
                    <span class="requiredLabel">*</span><span class="label">Replace ARKs: </span>
                  </td>
                  <td class="inputOption">
                    <div class="submenuText" id="text" title="Please delimited the ARKs or ARK URLs to be merged by comma (,).">
                      <input type="text" placeholder="Enter ARKs delimited by '|', ',' or whitespace" style="background-color:#eee;" id="arksToMerge" name="arksToMerge" size="64" value="">
                     </div>
                  </td>
                </tr>
                <tr>
                  <td height="30px" class="submenuText excelOption" style="display: none;">
                    <span class="requiredLabel">*</span><span class="label">Choose Excel: </span>
                  </td>
                  <td align="left" class="excelOption" style="display: none;">
                    <div id="fileLocation" class="submenuText" title="Please delimited the ARKs or ARK URLs to be merged by comma (,)">
                      <input type="text" id="filesPath" name="filesPath" size="58" value="${model.filesPath}">&nbsp;<input type="button" onclick="showFilePicker('filesPath', event, null, 'files')" value="&nbsp;...&nbsp;">
                    </div>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    <div style="padding-left:2px;">
                      <fieldset class="groupbox_modsIngestOpts"><legend class="slegandText">Subject Merge</legend>
                        <div title="Report only!" class="submenuText">
                          <input type="radio" name="mergeOption" value="report" id="reportOption" onclick="displayPriorityOptions(this, 'merge');" <c:if test="${model.mergeOption == null || model.mergeOption == 'report'}">checked</c:if>>
                          <span class="submenuText" style="margin:10px;">Report only, no merge actions.</span>
                        </div>
                        <div title="Merge subjects!" class="submenuText">
                            <input type="radio" name="mergeOption" value="merge" id="mergeOption" onclick="displayPriorityOptions(this, 'merge');" <c:if test="${model.mergeOption == 'merge'}">checked</c:if>>
                            <span class="submenuText" style="margin:10px;">Merge subjects.</span>
                        </div>
                      </fieldset>
                    </div>
                  </td>
                </tr>

                <jsp:include flush="true" page="/jsp/indexprioritieshide.jsp" />

                <tr>
                  <td colspan="2" id="optOutSolr" style="display: none; padding: 6px 4px;">
                    <input class="pcheckbox" type="checkbox" id="optOutIndex" name="optOutIndex" onchange="optOutSolrChange(this)">
                        <span class="submenuText label" style="vertical-align: text-top;">Opt out SOLR update</span>
                    </input>
                  </td>
                </tr>
                <tr>
                  <td colspan="2" style="padding-left:6px;">
                    <span class="submenuText"><span class="requiredLabel indicator">*</span><b>Required Field</b></span>
                  </td>
                </tr>
              </table>
            </div>
            <div class="buttonDiv">
              <input type="button" name="merge" id="merge" value=" Submit " onClick="javascript:confirmOption();"/>&nbsp;&nbsp;
              <input type="button" name="cancel" value=" Cancel " onClick="document.location.href='/damsmanager/subjectMerge.do'"/>
            </div>
          </div>
        </form>
      </div>
      <div id="message" class="submenuText" style="text-align:left;">${model.message}</div>
    </td>
  </tr>
</table>
<jsp:include flush="true" page="/jsp/libfooter.jsp" />
</body>
</html>
