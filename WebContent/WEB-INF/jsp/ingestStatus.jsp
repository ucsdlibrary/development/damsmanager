<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ page errorPage="/jsp/errorPage.jsp"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
    <head>
        <jsp:include flush="true" page="/jsp/libheader.jsp" />
        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
    </head>
    <body onload="javascript:displayStatus('mainForm', '${model.submissionId}');" style="background-color:#fff;">
        <script type="text/javascript">
            var crumbs = [{"Home":"http://library.ucsd.edu"}, {"Digital Library Collections":"/dc"},{"DAMS Manager":"/damsmanager/"}, {"Ingest Status":""}];
            drawBreadcrumbNMenu(crumbs, "tdr_crumbs_content", true);
        </script>

        <jsp:include flush="true" page="/jsp/libanner.jsp" />

        <table align="center" cellspacing="0px" cellpadding="0px" class="bodytable">
            <tr>
                <td>
                    <div id="tdr_crumbs">
                        <div id="tdr_crumbs_content">
                        </div><!-- /tdr_crumbs_content -->
                        
                        <!-- This div is for temporarily writing breadcrumbs to for processing purposes -->
                            <div id="temporaryBreadcrumb" style="display: none">
                        </div>
                    </div><!-- /tdr_crumbs -->
                </td>
            </tr>
            <tr>
                <td align="center">
                    <div id="main" class="mainDiv">
                        <form id="mainForm" name="mainForm" method="post" action="" ></form>
                    </div>
                    <jsp:include flush="true" page="/jsp/status.jsp" />
                    <div id="message" class="submenuText" style="text-align:left;">${model.message}</div>
                </td>
            </tr>
        </table>

        <jsp:include flush="true" page="/jsp/libfooter.jsp" />

    </body>
</html>
