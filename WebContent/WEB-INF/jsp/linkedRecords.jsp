<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@ page errorPage="/jsp/errorPage.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<jsp:include flush="true" page="/jsp/libheader.jsp" />
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
<style>
    #fileList ul {list-style: none;}
    #fileList ul li {padding-bottom: 3px;margin-left: -15px; white-space: nowrap; }
    .fname {background-color:#eee;margin-right:5px;width:300px;}
    .fid {width:300px;}
    table.results tr th td { border: 1px solid gray; }
    table.results td { padding: 3px; color: #333; font-size: 12px;}
    table.results a { text-decoration: none; }
    div.export a { text-decoration: none; vertical-align: top;}
</style>
</head>
<body style="background-color:#fff;">
<script type="text/javascript">
    function validate() {
        var message = "";
        var ark = $("#ark").val().trim();
        if (ark.length == 0)
            message = "Please enter an ARK.";
        else {
            var idx = ark.lastIndexOf("/");
            if (idx >= 0)
                ark = ark.substring (idx + 1);
            if (ark.length != 10)
                message = "Please enter a valid ark or ark URL."
        }

        if (message.length > 0) {
            alert(message);
            $("#ark").focus();
            return false;
        }

        document.mainForm.submit();
    }

    var crumbs = [{"Home":"http://library.ucsd.edu"}, {"Digital Library Collections":"/dc"},{"DAMS Manager":"/damsmanager/"}, {"Linked Records":""}];
    drawBreadcrumbNMenu(crumbs, "tdr_crumbs_content", true);
</script>
<jsp:include flush="true" page="/jsp/libanner.jsp" />
<table align="center" cellspacing="0px" cellpadding="0px" class="bodytable">
<tr><td>
    
<div id="tdr_crumbs">
    <div id="tdr_crumbs_content">
    </div><!-- /tdr_crumbs_content -->
    
    <!-- This div is for temporarily writing breadcrumbs to for processing purposes -->
        <div id="temporaryBreadcrumb" style="display: none">
    </div>
</div><!-- /tdr_crumbs -->
</td>
</tr>
<tr>
<td align="center">
<div id="main" class="mainDiv">
<form id="mainForm" name="mainForm" method="post" accept-charset="utf-8" action="/damsmanager/linkedRecords.do" >
<div class="submenuText" style="text-align:left;padding:10px 0px 2px 130px;color:red;">${model.message}</div>
<div class="emBox_ark">
<div class="emBoxBanner">Search Linked Records</div>
<div style="margin-top:10px;padding:10px;height:20px" align="left">
    <table>
        <tr align="left">
            <td height="25px">
                <div class="submenuText" style="padding-left:25px;"><strong>ARK: </strong></div>
            </td>
            <td  align="left">
                <div class="submenuText" style="margin-top:3px;padding:0px 10px 10px 0px;" title="Please enter an ARK or ARK URL.">
                    <input type="text" style="background-color:#eee;" id="ark" name="ark" size="52" value="${model.ark}">
                </div>
            </td>
        </tr>
    </table>
</div>
<div class="buttonDiv">
    <input type="button" name="edit" value=" Search " onClick="validate();"/>&nbsp;&nbsp;
    <input type="reset" name="clear" value=" Clear " onClick="document.mainForm.reset()"/>
</div>
</div>
</form>
<div style="text-align: left; padding: 0px 130px 0px 130px;"
    <div>
    <c:choose>
        <c:when test="${model.results.size() > 0}">
            <div class="submenuText"><b>Linked records for ARK ${model.ark}:</b></div>
            <table style="cell-spacing: 0; border-spacing: 0;" class="results">
                <c:forEach var="entry" items="${model.results}">
                <tr>
                    <td>${fn:substringBefore(entry.key, '|')}</td>
                    <td><a href="${entry.value}">${entry.value}</a></td>
                </tr>
                </c:forEach>
            </table>
        </c:when>
        <c:when test="model.ark">
            <div class="submenuText"><b>No linked records forund for ARK ${model.ark}.</b></div>
        </c:when>
    </c:choose>

    <c:if test="${model.results.size() > 0}">
        <hr />
        <div class="export">
            <a href="/damsmanager/linkedRecords.do?ark=${model.ark}&export">
                <img src="images/excel-icon.png" border="0" width="16px" /> &nbsp;Export CSV</a>
        </div>
    </c:if>
    </div>
</div>
</div>
<jsp:include flush="true" page="/jsp/status.jsp" />
</td>
</tr>
</table>
<jsp:include flush="true" page="/jsp/libfooter.jsp" />
</body>
</html>
