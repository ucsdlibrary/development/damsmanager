<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@ page errorPage="/jsp/errorPage.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<jsp:include flush="true" page="/jsp/libheader.jsp" />
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
<style>
    #fileList ul {list-style: none;}
    #fileList ul li {padding-bottom: 3px;margin-left: -32px; white-space: nowrap;}
    input.fname {background-color:#eee;}
    input.header {border:none;background:#F8F8F8}
</style>
</head>
<body style="background-color:#fff;">
<script type="text/javascript">
    var arkName = '${model.arkName}'
    var arkUrlBase = 'http://library.ucsd.edu/ark:/20775/';
    var objectUriPattern = '/dc/object/';

    function confirmUpload(){
        var mappedCount = 0;
        var message = "";
        var ignoreFile = "";
        var oidObj = null;
        var cidObj = null;

        $(".oid").each (function() {
            var oid = trim($(this).val());
            var currId = $(this).attr('id');
            var file = $("#" + currId.replace("oid-", "f-")).val();
            var cid = $("#" + currId.replace("oid-", "cid-")).val();
            var fileName = file.substring(file.lastIndexOf("/") + 1);
            var title = $("#" + currId.replace("oid-", "title-")).val().trim();
            if (oid.length == 0) {
                ignoreFile += fileName + "\n";
            } else if (!isArk(oid)) {
                if (oidObj == null)
                    oidObj = this;
                message += fileName + ' (Wrong ARK ' + oid + ') \n';
            } else if (cid.length > 0 && +cid === parseInt(cid) ) {
                mappedCount++;
            } else {
                if (cidObj == null)
                    cidObj = $("#" + currId.replace("oid-", "cid-"));
                message += fileName + '-> ' + oid + ' (Wrong component index ' + cid + ').\n';
            }

            if (title.length == 0) {
               message += fileName + '-> ' + oid + ' (Missing title).\n';
            }
        });

        if (message.length > 0 || mappedCount == 0) {
            if (message.length > 0)
                alert('Please enter valid ARK, component index or title for the following file(s): \n' + message);
            else
                alert('No object mapping found for component files! Please enter object ark(s).');

            if (oidObj != null)
                oidObj.focus();
            else if (cidObj != null)
                cidObj.focus();

            return false;
        }

        if (ignoreFile.length > 0) {
            message = 'Are you sure you want to continue upload ' + pluralize('transcript', mappedCount) + ' from dams_staging? \n';
            message += 'Note: no ARK file mapping for the following transcript file(s):\n' + ignoreFile;
        } else {
            message = 'Are you sure you want to upload ' + pluralize('component file', mappedCount) + ' from dams_staging? \n';
        }

        var exeConfirm = confirm(message);
        if(!exeConfirm)
            return false;

        document.mainForm.action = '/damsmanager/operationHandler.do?transcriptUpload&progress=0&formId=mainForm&sid=' + getSid();
        displayMessage('message', '');
        getAssignment("mainForm");
        displayProgressBar(0);
    }

    function reloadPage(inputID){
        var filesPath = document.getElementById(inputID).value.trim();
        document.location.href = '/damsmanager/transcriptUpload.do?filesPath=' + encodeURIComponent(filesPath);
    }

    function pluralize(word, count) {
        return count + " " + word + (count > 1 ? 's' : '');
    }

    function isArk(value) {
        var ark = value.substring(value.lastIndexOf('/') + 1);
        if (ark.length != 10 || value.length > 10 && !(value.indexOf(arkUrlBase) == 0 || value.indexOf(objectUriPattern) > 0)) {
            return false;
        }

        if (ark.startsWith('bb') || arkName.length == 0 || ark.startsWith(arkName)) {
            return true;
        }
        return false;
    }

    function listFiles(e) {
        if (e.keyCode == 13) {
            var inputID = 'filesPath';
            var filesPath = document.getElementById(inputID).value.trim();
            if (filesPath.length > 1 && filesPath.startsWith('/')) {
                reloadPage(inputID);
            } else {
                alert('Please enter a valid file path!');
            }
        }
    }

    function transcriptUploadChange(obj) {
        $('.tinput').each (function() {
            $(this).val(obj.checked ? 'Transcript' : '');
        });
    }

    var crumbs = [{"Home":"http://library.ucsd.edu"}, {"Digital Library Collections":"/dc"},{"DAMS Manager":"/damsmanager/"}, {"Add Component":""}];
    drawBreadcrumbNMenu(crumbs, "tdr_crumbs_content", true);
</script>
<jsp:include flush="true" page="/jsp/libanner.jsp" />
<table align="center" cellspacing="0px" cellpadding="0px" class="bodytable">
<tr><td>
    
<div id="tdr_crumbs">
    <div id="tdr_crumbs_content">
    </div><!-- /tdr_crumbs_content -->
    
    <!-- This div is for temporarily writing breadcrumbs to for processing purposes -->
        <div id="temporaryBreadcrumb" style="display: none">
    </div>
</div><!-- /tdr_crumbs -->
</td>
</tr>
<tr>
<td align="center">
<div id="main" class="mainDiv">
<form id="mainForm" name="mainForm" method="post" action="/damsmanager/operationHandler.do?transcriptUpload" onkeydown="return event.key != 'Enter';">
<div class="emBox_ark">
<div class="emBoxBanner">Add Component</div>
<div style="margin-top:10px;padding-left:20px;min-height:300px" align="left">
    <table width="640px">
        <tr align="left">
            <td height="25px">
                <div class="submenuText" style="padding-left:10px;white-space: nowrap;"><strong>Component files Location: </strong></div>
            </td>
            <td  align="left">
                <div class="submenuText" style="margin-top:3px;padding-bottom:10px;white-space: nowrap;" title="Open the file chooser on the right. From the popup, click on the folder to select/deselect a location. Multiple loations allowed.">
                    <input type="text" style="background-color:#eee;padding-right:10px;width:425px;" id="filesPath" name="filesPath" value="${model.filesPath}" onkeyup="listFiles(event)">
                    <input type="button" onclick="showFilePicker('filesPath', event, reloadPage)" value="&nbsp;...&nbsp;">
                </div>
            </td>
        </tr>
        <tr align="left">
            <td height="25px" colspan="2">
            <div id="fileList">
                <ul>
                    <c:forEach var="file" items="${model.files}" varStatus="status">
                        <c:set var="filePaths" value="${fn:split(file,'/')}" />
                        <c:set var="fileName" value="${filePaths[fn:length(filePaths)-1]}" />
                        <c:if test="${status.count == 1}">
                            <li>
                                <input type="text" class="header" style="width:168px;" readOnly="readOnly" value="Component File">
                                <input type="text" class="header" style="width:220px;" readOnly="readOnly" value="Object ARK">
                                <input type="text" class="header" style="width:75px;" readOnly="readOnly" value="Component">
                                <input type="text" class="header" style="width:160px;" readOnly="readOnly" value="Title">
                            </li>
                        </c:if>
                        <li>
                            <input type="text" id="f-${status.count}" class="fname" style="width:168px;" value="${fileName}" readOnly="readOnly">
                            <input type="text" class="oid" style="width:220px;" id="oid-${status.count}" name="oid-${status.count}" value="" >
                            <input type="text" class="cid" style="width:75px;" id="cid-${status.count}" name="cid-${status.count}" value="2" >
                            <input type="text" class="tinput" style="width:160px;" id="title-${status.count}" name="title-${status.count}" value="" placeholder="Component Title">
                            <input type="hidden" name="f-${status.count}" value="${file}">
                        </li>
                    </c:forEach>
                </ul>
            </div>
            </td>
        </tr>
        <tr>
          <td colspan="3" style="padding-left: 4px;">
              <c:if test="${model.files.size() gt 0}">
                  <input class="pcheckbox" type="checkbox" id="transcript" name="transcript" onchange="transcriptUploadChange(this)">
                      <span class="submenuText label" style="vertical-align: text-top;">Transcript Upload</span>
                  </input>
              </c:if>
          </td>
        </tr>
    </table>
</div>
<div class="buttonDiv">
    <input type="button" name="upload" value=" Upload " onClick="confirmUpload();"/>&nbsp;&nbsp;
    <input type="button" name="cancel" value=" Cancel " onClick="document.location.href='/damsmanager/transcriptUpload.do'"/>
</div>
</div>
</form>
</div>
    <jsp:include flush="true" page="/jsp/status.jsp" />
    <div id="message" class="submenuText" style="text-align:left;">${model.message}</div>
</td>
</tr>
</table>
<jsp:include flush="true" page="/jsp/libfooter.jsp" />
</body>
</html>
